package test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianExporter;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping;

@Configuration
@EnableWebMvc
public class RootConfig extends WebMvcConfigurationSupport {
    
    @Autowired
    public PingService pingService;

    @Bean
    public PingService pingService() {
        return new PingServiceImpl();
    }
    
    @Bean
    public BeanNameUrlHandlerMapping beanNameHandlerMapping() {
      BeanNameUrlHandlerMapping handlerMapping = super.beanNameHandlerMapping();
      handlerMapping.setDetectHandlersInAncestorContexts(true);
      return handlerMapping;
    }
    
    @Bean(name="/pingService")
    public HessianServiceExporter pingServiceExport() {
        HessianServiceExporter he = new HessianServiceExporter();
        he.setService(pingService);
        he.setServiceInterface(PingService.class);
        return he;
    }
}
