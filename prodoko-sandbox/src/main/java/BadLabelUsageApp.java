import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;


public class BadLabelUsageApp extends Application {
    
    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox box = new HBox();
        
        TextField field = new TextField();
        Label badLabel = new Label("Field:", field);
        
        box.getChildren().addAll(badLabel, field);
        Scene scene = new Scene(box, 100, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
