import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class SplitPaneApp extends Application {

    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        SplitPane splitPane = new SplitPane();
        AnchorPane anchorPane = new AnchorPane();
        ListView<String> listView = new ListView<>();
        AnchorPane.setTopAnchor(listView, 40d);
        AnchorPane.setRightAnchor(listView, 0d);
        AnchorPane.setBottomAnchor(listView, 0d);
        AnchorPane.setLeftAnchor(listView, 0d);
        anchorPane.getChildren().add(listView);
        splitPane.getItems().add(anchorPane);
        TabPane tabPane = new TabPane();
        splitPane.getItems().add(tabPane);
        splitPane.setDividerPositions(0.2d);
        Scene scene = new Scene(splitPane, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
