import org.controlsfx.control.SegmentedButton;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class SegmentedButtonApp extends Application {

    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        final SegmentedButton segb = new SegmentedButton(new ToggleButton("A"), new ToggleButton("B"));
        Button addb = new Button("Add the C button"); 
        addb.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent arg0) {
                segb.getButtons().add(new ToggleButton("C"));
            }
        });
        VBox box = new VBox(segb, addb);
        primaryStage.setScene(new Scene(box));
        primaryStage.show();
    }
}
