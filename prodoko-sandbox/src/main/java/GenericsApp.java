import java.util.ArrayList;
import java.util.List;


public class GenericsApp {
    
    public static class A {}
    public static class B extends A {}

    public void bla() {
        List<? extends A> al = null;
        List<A> aal = null;
        List<B> bl = new ArrayList<>();
        al = bl;
        al = aal;
    }
}
