import org.springframework.remoting.caucho.HessianProxyFactoryBean;

import test.PingService;


public class PingHessianApp {

    public static void main(String... args) {
        
        HessianProxyFactoryBean pb = new HessianProxyFactoryBean();
        pb.setServiceUrl("http://localhost:8001/pingService");
        pb.setServiceInterface(PingService.class);
        pb.afterPropertiesSet();
        PingService s = (PingService) pb.getObject();
        System.out.println(s.ping("pong"));
    }
}
