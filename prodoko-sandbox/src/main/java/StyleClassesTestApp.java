

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class StyleClassesTestApp extends Application {
    
    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        BorderPane pane = new BorderPane();
        
        Rectangle rect = new Rectangle(40, 40);
        BorderPane.setAlignment(rect, Pos.CENTER);
        pane.setCenter(rect);
        
        Scene scene = new Scene(pane, 400, 400);
        scene.getStylesheets().add("styleClassesTest.css");
        
        rect.getStyleClass().add("green-rect");
        rect.getStyleClass().add("green-rect");
        rect.getStyleClass().remove("green-rect");
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
