

import org.controlsfx.control.NotificationPane;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class NotificationPaneTestApp extends Application {
    
    int count = 0;
    
    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        final NotificationPane notificationPane = new NotificationPane();

        /* quick fix idea that did not work
        final NotificationPane notificationPane = new NotificationPane() {
            
            @Override
            public void show() {
                if(!showingProperty().get()) {
                    super.show();
                }
            }
            
            @Override
            public void hide() {
                if(showingProperty().get()) {
                    super.hide();
                }
            }
        };
        */
        
        AnchorPane pane = new AnchorPane();
        Button showButton = new Button("show");
        showButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent arg0) {
                notificationPane.show();
            }
        });
        Button hideButton = new Button("hide");
        hideButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent arg0) {
                notificationPane.hide();
            }
        });
        Button hideShowButton = new Button("hide and show");
        hideShowButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent arg0) {
                notificationPane.hide();
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {

                        notificationPane.setText("bla " + count++);
                        notificationPane.show();
                    }
                    
                });
            }
        });
        Button showHideButton = new Button("show and hide");
        showHideButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent arg0) {
                notificationPane.show();
                notificationPane.hide();
            }
        });
        ToolBar toolBar = new ToolBar(showButton, hideButton,
                showHideButton, hideShowButton);
        AnchorPane.setBottomAnchor(toolBar, 0d);
        AnchorPane.setLeftAnchor(toolBar, 0d);
        AnchorPane.setRightAnchor(toolBar, 0d);
        pane.getChildren().add(toolBar);
        notificationPane.setContent(pane);
        Scene scene = new Scene(notificationPane, 400, 400);
        notificationPane.setText("blablabla");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
