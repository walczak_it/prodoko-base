package test.context;

import org.springframework.security.access.prepost.PreAuthorize;

public interface SecurePingService {

    @PreAuthorize("hasRole('ROLE_USER')")
    public String ping(String returnValue);
}
