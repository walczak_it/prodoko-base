package test.context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurePingServiceImpl implements SecurePingService {
    
    private static final Log LOG = LogFactory
            .getLog(SecurePingServiceImpl.class);

    @Override
    public String ping(String returnValue) {
        // will throw AuthenticationCredentialsNotFoundException:
        // An Authentication object was not found in the SecurityContext
        //------
        String name = SecurityContextHolder.getContext()
            .getAuthentication().getName();
        //------
        LOG.info("name=" + name);
        return returnValue;
    }

}
