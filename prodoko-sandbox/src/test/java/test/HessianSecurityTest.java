package test;

import static org.junit.Assert.assertEquals;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import test.context.HessianServerConfig;
import test.context.SecurePingService;

@RunWith(JUnit4.class)
public class HessianSecurityTest {

    Server server;
    
    AnnotationConfigApplicationContext clientContext;
    
    @Before
    public void nullify() {
        server = null;
        clientContext = null;
    }
    
    @Test
    public void hessianRemotingTest() throws Exception {
        
        try {
            
            Server server = new Server(8080);
            ServletContextHandler resource = new ServletContextHandler(
                    ServletContextHandler.SESSIONS);
            resource.setContextPath("/");
            
            ServletHolder dispatcher = resource.addServlet(DispatcherServlet.class, "/api/*");
            dispatcher.setInitParameter("contextClass",
                    AnnotationConfigWebApplicationContext.class.getName());
            dispatcher.setInitParameter("contextConfigLocation",
                    HessianServerConfig.class.getName());
            dispatcher.setInitOrder(0);
            
            //resource.add
            
            //server.setHandler(resource);
            server.setStopAtShutdown(true);
            server.start();
            
            HessianProxyFactoryBean proxyFactory = new HessianProxyFactoryBean();
            proxyFactory.setServiceInterface(SecurePingService.class);
            proxyFactory.setServiceUrl("http://localhost:8080/api/SecurePingService");
            proxyFactory.setUsername("somename");
            proxyFactory.setPassword("wrongpass");
            proxyFactory.setConnectTimeout(2000);
            proxyFactory.afterPropertiesSet();
            SecurePingService pingService
                    = (SecurePingService) proxyFactory.getObject();
            String ret = pingService.ping("pong");
            assertEquals(ret, "pong");
            
        } finally {
            if(clientContext != null)
                clientContext.close();
            if(server != null)
                server.stop();
        }
    }
}
