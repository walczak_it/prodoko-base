package test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class JavaLang {
    
    public static class SomeEntity {
        
        private String title;
        
        public SomeEntity(String title) {
            this.title = title;
        }

        /**
         * TODO
         */
        public String getTitle() {
            return title;
        }

        /**
         * @see #setTitle
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((title == null) ? 0 : title.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            SomeEntity other = (SomeEntity) obj;
            if (title == null) {
                if (other.title != null)
                    return false;
            } else if (!title.equals(other.title))
                return false;
            return true;
        }
    }
    
    public static class OtherEntity {
        
        private SomeEntity someEntity;

        /**
         * TODO
         */
        public SomeEntity getSomeEntity() {
            return someEntity;
        }

        /**
         * @see #setSomeEntity
         */
        public void setSomeEntity(SomeEntity someEntity) {
            this.someEntity = someEntity;
        }
    }

    @Test
    public void hashMapTest() {
        
        Map<SomeEntity, OtherEntity> map = new HashMap<>();
        
        SomeEntity sa = new SomeEntity("sa");
        OtherEntity oa = new OtherEntity();
        oa.setSomeEntity(sa);
        map.put(sa, oa);
        
        SomeEntity sb = new SomeEntity("sb");
        OtherEntity ob = new OtherEntity();
        ob.setSomeEntity(sb);
        map.put(sa, ob);
        
        for(OtherEntity oe : map.values()) {
            assertNotNull(oe.getSomeEntity());
        }
    }
    
    public static class Elem {
        
        private int id;
        private int priority;

        public int getId() {
            return id;
        }

        public int getPriority() {
            return priority;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Elem other = (Elem) obj;
            if (id != other.id)
                return false;
            if (priority != other.priority)
                return false;
            return true;
        }
    }
    
    public static class ElemComparator implements Comparator<Elem> {

        @Override
        public int compare(Elem a, Elem b) {
            return a.getPriority() - b.getPriority();
        }
    }
    
    @Test
    public void sortedSetTest() throws Exception {
        
        SortedSet<Elem> set = new TreeSet<>(new ElemComparator());
        Elem a = new Elem();
        a.setId(1);
        a.setPriority(30);
        set.add(a);
        assertEquals(1, set.size());
        Elem b = new Elem();
        b.setId(2);
        b.setPriority(30);
        set.add(b);
        assertEquals(2, set.size()); // this fails
    }
}
