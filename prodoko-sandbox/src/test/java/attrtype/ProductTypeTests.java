package attrtype;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.spi.PersistenceUnitTransactionType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.ejb.EntityManagerFactoryImpl;
import org.hibernate.metamodel.Metadata;
import org.hibernate.metamodel.MetadataSources;
import org.hibernate.service.BootstrapServiceRegistry;
import org.hibernate.service.BootstrapServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

@RunWith(JUnit4.class)
public class ProductTypeTests {
    
    private EntityManager em;
    
    private void buildEntityManager() {
        Configuration cfg = new Configuration();
        
        Properties props = new Properties();
        props.setProperty("hibernate.show_sql", "true");
        props.setProperty("hibernate.format_sql", "true");
        props.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        props.setProperty("hibernate.connection.url", "jdbc:h2:mem:test");
        props.setProperty("hibernate.connection.username", "sa");
        props.setProperty("hibernate.connection.password", "");
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        props.setProperty("hibernate.hbm2ddl.auto", "update");
        cfg.setProperties(props);
        
        cfg.addAnnotatedClass(ProductType.class)
            .addAnnotatedClass(SpecialProductType.class);
        
        BootstrapServiceRegistry bootstrapServiceRegistry = new BootstrapServiceRegistryBuilder()
                .build();
        ServiceRegistryBuilder srb = new ServiceRegistryBuilder(bootstrapServiceRegistry);
        ServiceRegistry serviceRegistry = srb.applySettings(props).buildServiceRegistry();
        
        EntityManagerFactory emf = new EntityManagerFactoryImpl(
                PersistenceUnitTransactionType.RESOURCE_LOCAL, true, null,
                cfg, serviceRegistry, "testPU");
        em = emf.createEntityManager();
    }

    @Test
    public void mostSuperTypeTest() {

        buildEntityManager();
        
        em.getTransaction().begin();
        ProductType typeOne = new ProductType();
        em.persist(typeOne);
        
        SpecialProductType typeTwo = new SpecialProductType();
        typeTwo.setSuperType(typeOne);
        em.persist(typeTwo);
        
        SpecialProductType typeThree = new SpecialProductType();
        typeThree.setSuperType(typeTwo);
        em.persist(typeThree);
        em.flush();
        em.getTransaction().commit();
        
        Query queryB = em.createQuery("SELECT p FROM ProductType p "
                + "LEFT JOIN p.superType sp WHERE TYPE(p) = ?1 "
                + "AND (p.superType IS NULL OR TYPE(sp) != ?1)");
        queryB.setParameter(1, SpecialProductType.class);
        @SuppressWarnings("unchecked")
        List<ProductType> rlB = queryB.getResultList();
        assertEquals(1, rlB.size());
        SpecialProductType selectedTypeFromB = (SpecialProductType) queryB.getSingleResult();
        assertEquals(typeTwo, selectedTypeFromB);
        
        Query queryA = em.createQuery("SELECT p FROM ProductType p WHERE TYPE(p) = ?1 "
                + "AND (p.superType IS NULL OR TYPE(p.superType) != ?1)");
        queryA.setParameter(1, SpecialProductType.class);
        // this will throw no result exception
        SpecialProductType selectedTypeFromA = (SpecialProductType) queryA.getSingleResult();
        // -----------------------------------
        assertEquals(typeTwo, selectedTypeFromA);
    }
}
