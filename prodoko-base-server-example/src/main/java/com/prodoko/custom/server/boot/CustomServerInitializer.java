package com.prodoko.custom.server.boot;

import com.prodoko.base.server.boot.BaseServerInitializer;

public class CustomServerInitializer
        extends BaseServerInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { CustomServerConfig.class };
    }

}