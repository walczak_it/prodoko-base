package com.prodoko.custom.server.boot;

import java.util.List;

import com.prodoko.base.server.boot.BaseEntityInfo;

public class CustomEntityInfo extends BaseEntityInfo {

    @Override
    public List<String> getPackagesToScan() {
        List<String> packages = super.getPackagesToScan();
        packages.add("com.prodoko.custom.model");
        return packages;
    }
}
