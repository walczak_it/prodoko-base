package com.prodoko.custom.server.boot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

import com.prodoko.base.server.boot.BaseServerConfig;
import com.prodoko.custom.model.CustomSystemInfo;

@Configuration
@Import(BaseServerConfig.class)
@ComponentScan(basePackages = {"com.prodoko.custom.model", "com.prodoko.custom.server"},
        excludeFilters=@Filter(value=Configuration.class, type=FilterType.ANNOTATION))
public class CustomServerConfig {
    
    @Bean CustomSystemInfo systemInfo() {
        return new CustomSystemInfo();
    }

    @Bean
    public CustomEntityInfo entityInfo() {
        return new CustomEntityInfo();
    }
}
