package com.prodoko.custom.client.boot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prodoko.base.client.boot.BaseClientConfig;
import com.prodoko.custom.model.CustomSystemInfo;
import com.prodoko.remoting.hessian.EnableHessianAccess;
import com.prodoko.remoting.hessian.fx.FxSerializerFactory;

@Configuration
@Import(BaseClientConfig.class)
@EnableHessianAccess(basePackages = "com.prodoko.custom.model",
        serializerFactoryClass=FxSerializerFactory.class, overloadEnabled=true)
@ComponentScan(basePackages="com.prodoko.custom.client")
public class CustomClientConfig {

    @Bean
    public CustomSystemInfo systemInfo() {
        return new CustomSystemInfo();
    }
}
