package com.prodoko.custom.client.boot;

import javafx.application.Application;

import com.prodoko.base.client.boot.BaseClient;
import com.prodoko.base.model.BaseSystemInfo;
import com.prodoko.custom.model.CustomSystemInfo;

public class CustomClient extends BaseClient {
    
    @Override
    public Class<?> getRootConfig() {
        return CustomClientConfig.class;
    }
    
    @Override
    protected BaseSystemInfo getSystemInfo() {
        return new CustomSystemInfo();
    }

    public static void main(String[] args) {
        System.out.println("Lunching Prodoko BASE - example client");
        Application.launch(CustomClient.class, args);
    }
}
