package com.prodoko.custom.model;
import com.prodoko.base.model.BaseSystemInfo;


public class CustomSystemInfo extends BaseSystemInfo {

    @Override
    public String getSystemName() {
        return "Prodoko BASE - with customizations";
    }
    
    @Override
    public String getCustomLayerVersion() {
        return "0.5";
    }
}
