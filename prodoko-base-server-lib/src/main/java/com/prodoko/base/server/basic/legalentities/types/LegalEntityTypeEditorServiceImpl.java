package com.prodoko.base.server.basic.legalentities.types;

import static com.prodoko.base.util.BundleUtils.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.basic.legalentities.Company;
import com.prodoko.base.model.basic.legalentities.Institution;
import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.basic.legalentities.Organization;
import com.prodoko.base.model.basic.legalentities.Person;
import com.prodoko.base.model.basic.legalentities.types.CompanyType;
import com.prodoko.base.model.basic.legalentities.types.InstitutionType;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeEditorService;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeFinderService;
import com.prodoko.base.model.basic.legalentities.types.OrganizationType;
import com.prodoko.base.model.basic.legalentities.types.PersonType;
import com.prodoko.base.server.beans.dynamic.AbstractMainDynamicClassEditorService;

@Service
@Transactional
public class LegalEntityTypeEditorServiceImpl
        extends AbstractMainDynamicClassEditorService<LegalEntityType>
        implements LegalEntityTypeEditorService {
    
    private LegalEntityTypeFinderService hierarchyService;

    public LegalEntityTypeFinderService getHierarchyService() {
        return hierarchyService;
    }

    @Autowired
    public void setHierarchyService(LegalEntityTypeFinderService hierarchyService) {
        this.hierarchyService = hierarchyService;
    }

    @Override
    public Class<LegalEntityType> getSupportedClass() {
        return LegalEntityType.class;
    }

    @Override
    public void loadMinimalDataIfNeeded() {
        if(!hierarchyService.hasMostSuperclass(LegalEntityType.class)) {
            LegalEntityType legalEntityMostSuperclass = new LegalEntityType();
            legalEntityMostSuperclass.setCode(message(LegalEntityType.class,
                    "mostSuperclass.code"));
            legalEntityMostSuperclass.setName(message(LegalEntity.class, "plural"));
            legalEntityMostSuperclass = save(legalEntityMostSuperclass);
            
            PersonType personMostSuperclass = new PersonType();
            personMostSuperclass.setCode(message(PersonType.class,
                    "mostSuperclass.code"));
            personMostSuperclass.setName(message(Person.class, "plural"));
            personMostSuperclass.setDynamicSuperclass(legalEntityMostSuperclass);
            personMostSuperclass = save(personMostSuperclass);
            
            OrganizationType organizationMostSuperclass = new OrganizationType();
            organizationMostSuperclass.setCode(message(OrganizationType.class,
                    "mostSuperclass.code"));
            organizationMostSuperclass.setName(message(Organization.class, "plural"));
            organizationMostSuperclass.setDynamicSuperclass(legalEntityMostSuperclass);
            organizationMostSuperclass = save(organizationMostSuperclass);
            
            CompanyType companyMostSuperclass = new CompanyType();
            companyMostSuperclass.setCode(message(CompanyType.class,
                    "mostSuperclass.code"));
            companyMostSuperclass.setName(message(Company.class, "plural"));
            companyMostSuperclass.setDynamicSuperclass(organizationMostSuperclass);
            companyMostSuperclass = save(companyMostSuperclass);
            
            InstitutionType institutionMostSuperclass = new InstitutionType();
            institutionMostSuperclass.setCode(message(InstitutionType.class,
                    "mostSuperclass.code"));
            institutionMostSuperclass.setName(message(Institution.class, "plural"));
            institutionMostSuperclass.setDynamicSuperclass(organizationMostSuperclass);
            institutionMostSuperclass = save(institutionMostSuperclass);
        }
    }
}
