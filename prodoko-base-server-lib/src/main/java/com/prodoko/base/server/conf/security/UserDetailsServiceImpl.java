package com.prodoko.base.server.conf.security;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.remoting.support.cloning.RemoteCloner;

@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {
    
    private EntityManager entityManager;
    private RemoteCloner cloner;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public RemoteCloner getCloner() {
        return cloner;
    }

    @Autowired
    public void setCloner(RemoteCloner cloner) {
        this.cloner = cloner;
    }

    @Override
    public UserLogin loadUserByUsername(String username)
            throws UsernameNotFoundException {
        try {
            return (UserLogin)cloner.cloneBean(
                    getEntityManager().createNamedQuery("UserLogin.findByUsername")
                            .setParameter(1, username).getSingleResult());
        } catch(NoResultException e) {
            throw new UsernameNotFoundException("Not found", e);
        }  catch(Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
