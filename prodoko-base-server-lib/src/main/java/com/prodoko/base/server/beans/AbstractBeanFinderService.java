package com.prodoko.base.server.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.Query;

import com.prodoko.base.model.beans.BeanFinderService;
import com.prodoko.base.model.beans.StringRepresentableBean;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.base.server.entities.AbstractEntityFinderService;
import com.prodoko.base.server.util.JpaUtil;

public abstract class AbstractBeanFinderService<T extends StandardEntity & StringRepresentableBean>
        extends AbstractEntityFinderService<T>
        implements BeanFinderService<T> {
    
    private JpaUtil jpaUtil;
    
    abstract protected String getByStringQueryName();
    abstract protected String getSuggestionsByStringQueryName();
    
    @PostConstruct
    public void postConstruct() {
        jpaUtil = JpaUtil.getInstance(getEntityManager().getEntityManagerFactory());
    }
    
    public JpaUtil getJpaUtil() {
        return jpaUtil;
    }
    
    @Override
    public T findByString(String string) {
        return findByString(getSupportedClass(), string);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E extends T> E findByString(Class<E> subclass, String shortString) {
        Query q = getEntityManager().createNamedQuery(getByStringQueryName());
        List<Class<?>> classes = jpaUtil.allSubentityClasses(subclass);
        q.setParameter(1, classes);
        q.setParameter(2, shortString);
        if(q.getResultList().isEmpty()) {
            return null;
        } else {
            return (E) q.getResultList().get(0);
        }
    }
    
    @Override
    public List<T> findSuggestionsByString(String longString) {
        return findSuggestionsByString(getSupportedClass(), longString);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E extends T> List<E> findSuggestionsByString(Class<E> subclass,
            String longString) {
        Query q = getEntityManager().createNamedQuery(getSuggestionsByStringQueryName());
        List<Class<?>> classes = jpaUtil.allSubentityClasses(subclass);
        q.setParameter(1, classes);
        q.setParameter(2, longString);
        q.setMaxResults(10);
        return q.getResultList();
    }
}
