package com.prodoko.base.server.boot;

import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.prodoko.base.model.BaseSystemInfo;
import com.prodoko.base.server.info.BaseServerInfoConfig;

@Order(1)
public abstract class BaseServerInitializer
        extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { BaseServerConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { BaseServerInfoConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }
    
    protected BaseSystemInfo getSystemInfo() {
        return new BaseSystemInfo();
    }
}
