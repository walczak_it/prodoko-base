package com.prodoko.base.server.beans.dynamic;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxyHelper;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.beans.dynamic.DynamicBean;
import com.prodoko.base.model.beans.dynamic.HierarchyDto;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;
import com.prodoko.base.server.beans.AbstractBeanFinderService;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;
import com.prodoko.remoting.support.cloning.CloneRemotedCollection;
import com.prodoko.remoting.support.cloning.CloneRemotedProperties;

@Transactional
public abstract class AbstractMainDynamicClassFinderService<T extends MainDynamicClass>
        extends AbstractBeanFinderService<T>
        implements MainDynamicClassFinderService<T> {
    
    @Override
    public boolean isMostSuperclass(Class<? extends T> dynamicClassClass,
            Long id) {
        Query q = getEntityManager().createNamedQuery("MainDynamicClass.findMostSuperclass");
        q.setParameter(1, dynamicClassClass);
        @SuppressWarnings("unchecked")
        List<MainDynamicClass> rl = q.getResultList();
        if(rl.isEmpty()) {
            return false;
        }
        MainDynamicClass foundDynClass = rl.get(0);
        MainDynamicClass givenDynClass = getEntityManager().find(MainDynamicClass.class, id);
        return foundDynClass.equals(givenDynClass);
    }

    @Override
    public boolean hasMostSuperclass(
            Class<? extends T> dynamicClassClass) {
        Query q = getEntityManager().createNamedQuery("MainDynamicClass.findMostSuperclass");
        q.setParameter(1, dynamicClassClass);
        return !q.getResultList().isEmpty();
    }

    @SuppressWarnings("unchecked")
    @Override
    public @CloneRemotedBean <TT extends T>   TT findMostSuperclass(
            Class<TT> dynamicClassClass) {
        Query q = getEntityManager().createNamedQuery("MainDynamicClass.findMostSuperclass");
        q.setParameter(1, dynamicClassClass);
        return (TT) q.getSingleResult();
    }
    
    @Override
    public @CloneRemotedBean <TT extends T> TT findMostSuperclassForBean(
            Class<? extends DynamicBean> dynamicBeanClass) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public @CloneRemotedCollection <TT extends T> Set<TT> findMostSuperclassSubclasses(
            Class<TT> dynamicClassClass) {
        T mostSuperClass = findMostSuperclass(dynamicClassClass);
        return findSubclasses(dynamicClassClass, mostSuperClass.getId());
    }

    @SuppressWarnings("unchecked")
    @Override
    public @CloneRemotedCollection <TT extends T> Set<TT> findSubclasses(
            Class<TT> dynamicClassClass, Long superId) {
        TT dynamicClass = getEntityManager().find(dynamicClassClass, superId);
        Set<MainDynamicClass> subclasses = dynamicClass.getDynamicSubclasses();
        Hibernate.initialize(subclasses);
        return (Set<TT>) subclasses;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public @CloneRemotedCollection <TT extends T> List<TT> findSubclassesDeep(
            Class<TT> dynamicClassClass, Long superId) {
        T dynamicClass = getEntityManager().find(dynamicClassClass, superId);
        Query q = getEntityManager().createNamedQuery(
                "MainDynamicClass.findDeepSubclassesBySuperPath");
        q.setParameter(1, dynamicClass.getHierarchyPath());
        return (List<TT>) q.getResultList();
    }
    
    @Override
    public @CloneRemotedProperties HierarchyDto<T> createHierarchyDto() {
        return createHierarchyDto(getSupportedClass());
    }
    
    @Override
    public @CloneRemotedProperties <TT extends T> HierarchyDto<TT> createHierarchyDto(
            Class<TT> rootDynamicClassClass) {
        TT mostSuperclass = findMostSuperclass(rootDynamicClassClass);
        return createHierarchyDTO(mostSuperclass);
    }
    
    public <TT extends T> HierarchyDto<TT> createHierarchyDTO(TT mainDynamicClass) {
        HierarchyDto<TT> dto = new HierarchyDto<>(mainDynamicClass);
        dto.setMostSuperclass(isMostSuperclass(Hibernate.getClass(mainDynamicClass),
                mainDynamicClass.getId()));
        dto.setHelperDynamicSubclasses(mainDynamicClass.getHelperDynamicSubclasses());
        dto.setSubclasses(new ArrayList<HierarchyDto<TT>>());
        for(MainDynamicClass subclass : mainDynamicClass.getDynamicSubclasses()) {
            dto.getSubclasses().add(createHierarchyDTO((TT)subclass));
        }
        return dto;
    }
}
