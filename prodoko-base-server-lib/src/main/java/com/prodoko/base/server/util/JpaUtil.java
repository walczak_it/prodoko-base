package com.prodoko.base.server.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.IdentifiableType;

public class JpaUtil {
    
    private static final Map<EntityManagerFactory, JpaUtil> utilsByFactory = new HashMap<>();
    
    private EntityManagerFactory factory;
    
    protected JpaUtil(EntityManagerFactory entityManagerFactory) {
        super();
        this.factory = entityManagerFactory;
    }
    
    public static JpaUtil getInstance(EntityManagerFactory factory) {
        if(!utilsByFactory.containsKey(factory)) {
            utilsByFactory.put(factory, new JpaUtil(factory));
        }
        return utilsByFactory.get(factory);
    }

    /**
     * TODO
     */
    public EntityManagerFactory getEntityManagerFactory() {
        return factory;
    }

    /**
     * @see #getEntityManagerFactory
     */
    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.factory = entityManagerFactory;
    }

    public List<Class<?>> allInheritedEntityClasses(Class<?> entityClass) {
        List<Class<?>> entityClasses = new ArrayList<>();
        EntityType<?> entityType = factory.getMetamodel()
                .entity(entityClass);
        entityClasses.add(entityClass);
        IdentifiableType<?> superType = entityType.getSupertype();
        while(superType instanceof EntityType) {
            EntityType<?> superEntityType = (EntityType<?>)superType;
            entityClasses.add(superEntityType.getJavaType());
            superType = superType.getSupertype();
        }
        return entityClasses;
    }
    
    public List<Class<?>> allSubentityClasses(Class<?> entityClass) {
        List<Class<?>> entityClasses = new ArrayList<>();
        Set<EntityType<?>> allEntityTypes = factory.getMetamodel().getEntities();
        for(EntityType<?> nextEntityType : allEntityTypes) {
            Class<?> nextEntityClass = nextEntityType.getJavaType();
            if(entityClass.isAssignableFrom(nextEntityClass)) {
                entityClasses.add(nextEntityClass);
            }
        }
        return entityClasses;
    }
}
