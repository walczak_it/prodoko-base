package com.prodoko.base.server.boot;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.prodoko.base.model.BaseSystemInfo;
import com.prodoko.base.model.beans.filter.UniversalFilters;
import com.prodoko.base.model.conf.security.BaseUserAuthority;
import com.prodoko.base.server.beans.NumericPropertyQueryInjector;
import com.prodoko.base.server.beans.SingleFilterQueryInjector;
import com.prodoko.base.server.beans.StringPropertyQueryInjector;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.remoting.hibernate.HibernateAwareInstanceProducer;
import com.prodoko.remoting.jpa.ByIdEntityFinder;
import com.prodoko.remoting.support.cloning.RemoteCloneAspect;
import com.prodoko.remoting.support.cloning.RemoteCloner;

@Configuration
@Import(BaseDatabaseConfig.class)
@ComponentScan(basePackages = {"com.prodoko.base.model", "com.prodoko.base.server" },
        excludeFilters=@Filter(value=Configuration.class, type=FilterType.ANNOTATION))
public class BaseServiceConfig {
    
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer pc
            = new PropertySourcesPlaceholderConfigurer();
        pc.setIgnoreResourceNotFound(true);
        pc.setIgnoreUnresolvablePlaceholders(true);
        pc.setLocation(new FileSystemResource("prodoko.server.properties"));
        return pc;
    }
    
    @Bean
    public BaseSystemInfo systemInfo() {
        return new BaseSystemInfo();
    }
    
    @Bean
    public Collection<Class<? extends Enum<?>>> userAuthorities() {
        return Arrays.<Class<? extends Enum<?>>>asList(BaseUserAuthority.class);
    }
    
    @Bean @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE) @UniversalFilters
    public ClassSupportersMap<SingleFilterQueryInjector<?>> formFactoriesForUniversalFilters() {
        return new ClassSupportersMap<>(Arrays.<SingleFilterQueryInjector<?>>asList(
                new StringPropertyQueryInjector(),
                new NumericPropertyQueryInjector()));
    }
    
    @Bean
    public MinimalDataLoadingListener minimalDataLoadingListener() {
        return new MinimalDataLoadingListener();
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
