package com.prodoko.base.server.conf.security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.base.model.conf.security.UserLoginBrowserService;
import com.prodoko.base.server.beans.AbstractBeanBrowserService;

@Service
@Transactional
public class UserLoginBrowserServiceImpl
        extends AbstractBeanBrowserService<UserLogin>
        implements UserLoginBrowserService {
    
    private static final Log LOG = LogFactory.getLog(UserLoginBrowserServiceImpl.class);

    @Override
    public Class<UserLogin> getSupportedClass() {
        return UserLogin.class;
    }
}
