package com.prodoko.base.server.beans;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.base.util.ClassSupporter;

public interface SingleFilterQueryInjector<F extends FilterData>
        extends ClassSupporter {
    
    public void inject(Root<?> root, CriteriaQuery<?> query, CriteriaBuilder cb, F data);

    @Override
    public Class<F> getSupportedClass();
}
