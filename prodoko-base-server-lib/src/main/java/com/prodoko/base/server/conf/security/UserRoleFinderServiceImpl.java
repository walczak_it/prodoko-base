package com.prodoko.base.server.conf.security;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.conf.security.UserRole;
import com.prodoko.base.model.conf.security.UserRoleFinderService;
import com.prodoko.base.server.beans.dynamic.AbstractMainDynamicClassFinderService;

@Service
@Transactional
public class UserRoleFinderServiceImpl extends AbstractMainDynamicClassFinderService<UserRole>
        implements UserRoleFinderService {

    @Override
    public Class<UserRole> getSupportedClass() {
        return UserRole.class;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }

    @Override
    protected String getByStringQueryName() {
        return "UserRole.findByClassAndCode";
    }

    @Override
    protected String getSuggestionsByStringQueryName() {
        return "UserRole.findByClassAndLikeCodeOrName";
    }
}
