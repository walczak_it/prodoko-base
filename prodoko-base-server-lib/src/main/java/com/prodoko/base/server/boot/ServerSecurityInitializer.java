package com.prodoko.base.server.boot;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Order(2)
public class ServerSecurityInitializer
        extends AbstractSecurityWebApplicationInitializer {
}
