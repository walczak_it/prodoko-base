package com.prodoko.base.server.info;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/info")
public class InfoPageController {

    @RequestMapping(method = RequestMethod.GET)
    public String get() {
        return "Server working...";
    }
}
