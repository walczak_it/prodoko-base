package com.prodoko.base.server.basic.legalentities;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.basic.legalentities.LegalEntityEditorService;
import com.prodoko.base.server.entities.AbstractEntityEditorService;

@Service
@Transactional
public class LegalEntityEditorServiceImpl extends AbstractEntityEditorService<LegalEntity>
        implements LegalEntityEditorService {

    @Override
    public Class<LegalEntity> getSupportedClass() {
        return LegalEntity.class;
    }
}
