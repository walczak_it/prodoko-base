package com.prodoko.base.server.basic.legalentities;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.basic.legalentities.LegalEntityFinderService;
import com.prodoko.base.server.beans.AbstractBeanFinderService;

@Service
@Transactional
public class LegalEntityFinderServiceImpl extends AbstractBeanFinderService<LegalEntity>
        implements LegalEntityFinderService {

    @Override
    public Class<LegalEntity> getSupportedClass() {
        return LegalEntity.class;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return true;
    }

    @Override
    protected String getByStringQueryName() {
        return "LegalEntity.findByClassAndCode";
    }

    @Override
    protected String getSuggestionsByStringQueryName() {
        return "LegalEntity.findByClassAndLikeCodeOrName";
    }

}
