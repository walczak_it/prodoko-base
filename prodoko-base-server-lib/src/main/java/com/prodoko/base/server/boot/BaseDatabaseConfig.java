package com.prodoko.base.server.boot;

import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class BaseDatabaseConfig {
    
    private static final Log LOG = LogFactory.getLog(BaseDatabaseConfig.class);

    @Value("${prodoko.server.db.jdbcDriverClass:org.postgresql.Driver}")
    private String jdbcDriverClass;

    @Value("${prodoko.server.db.platform:postgresql}")
    private String platform;

    @Value("${prodoko.server.db.host:localhost}")
    private String host;

    @Value("${prodoko.server.db.port:5432}")
    private String port;

    @Value("${prodoko.server.db.name:prodoko}")
    private String name;

    @Value("${prodoko.server.db.username:prodoko}")
    private String username;

    @Value("${prodoko.server.db.password:changeMe}")
    private String password;

    @Value("${prodoko.server.db.hibernateDialectClass:"
    		+ "org.hibernate.dialect.ProgressDialect}")
    private String hibernateDialectClass;

    @Value("${prodoko.server.db.showSql:false}")
    private Boolean showSql;
    
    @Autowired
    private DataSource dataSource;

    @Autowired
    private BaseEntityInfo entityInfo;
    
    @Autowired
    private JpaVendorAdapter jpaVendorAdapter;
    
    @Autowired
    private JpaProperties jpaProperties;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcDriverClass);
        String dbUrl = "jdbc:" + platform + "://" + host + ":" + port + "/"
                + name;
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean @Autowired
    public JpaTransactionManager transactionManager(
            LocalContainerEntityManagerFactoryBean entityManagerFactoryBean)
            throws ClassNotFoundException {

        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(entityManagerFactoryBean
                .getObject());

        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory()
            throws ClassNotFoundException {
        
        LOG.info("Packages to scan:");
        LOG.info(entityInfo.getPackagesToScan());

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean
            = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPackagesToScan(entityInfo.getPackagesToScan()
                .toArray(new String[] {}));
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        entityManagerFactoryBean.setMappingResources(entityInfo.getMappingResources()
                .toArray(new String[] {}));

        return entityManagerFactoryBean;
    }

    @Bean
    public BaseEntityInfo entityInfo() {
        return new BaseEntityInfo();
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateAdapter
            = new HibernateJpaVendorAdapter();
        hibernateAdapter.setDatabasePlatform(hibernateDialectClass);
        if(showSql) {
            hibernateAdapter.setShowSql(true);
        }
        return hibernateAdapter;
    }
    
    @Bean
    public JpaProperties jpaProperties() {
        JpaProperties props = new JpaProperties();
        props.setProperty("hibernate.hbm2ddl.auto", "create");
        return props;
    }
    
    static public List<String> baseModelPackages() {
        return Arrays.asList("com.prodoko.base.model");
    }
}
