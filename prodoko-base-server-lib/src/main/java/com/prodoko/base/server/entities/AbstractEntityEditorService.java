package com.prodoko.base.server.entities;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.base.model.entities.StandardEntity;

@Transactional
public abstract class AbstractEntityEditorService<T extends StandardEntity>
        implements EntityEditorService<T> {

    private EntityManager entityManager;
    
    @Override
    public boolean isSupportsSubclasses() {
        return true;
    }

    /**
     * TODO
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @see #getEntityManager
     */
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    @Override
    public <E extends T> E createStub(Class<E> stubClass) {
        try {
            E stub = stubClass.newInstance();
            stub.initStub();
            return stub;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException("Couldn't create stub of given class "
                    + stubClass.getName());
        }
    }

    @Override
    public <E extends T> E save(E entity) {
        if(entity.getId() == null) {
            entityManager.persist(entity);
        }
        return entity;
    }
    
    @Override
    public void remove(Long id) {
        T obj = (T) entityManager.find(getSupportedClass(), id);
        entityManager.remove(obj);
    }
}
