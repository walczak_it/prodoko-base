package com.prodoko.base.server.info;

import org.springframework.context.annotation.Configuration;

import com.prodoko.remoting.hessian.EnableHessianExport;

@Configuration
//@ComponentScan(basePackages = { "com.prodoko.base.server.info"})
//@EnableWebMvc
@EnableHessianExport
public class BaseServerInfoConfig {

}
