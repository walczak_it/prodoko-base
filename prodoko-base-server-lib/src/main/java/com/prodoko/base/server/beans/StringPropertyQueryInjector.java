package com.prodoko.base.server.beans;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import com.prodoko.base.model.beans.filter.StringPropertyFilter;

public class StringPropertyQueryInjector
        implements SingleFilterQueryInjector<StringPropertyFilter> {

    @Override
    public void inject(Root<?> root, CriteriaQuery<?> query, CriteriaBuilder cb,
            StringPropertyFilter filter) {

        Path<String> property = root.get(filter.getPropertyPath());
        String comparingString = filter.getComparingString().toLowerCase();
        Expression<String> comparedExpression = cb.lower(property);
        switch(filter.getOperator()) {
        case EXACTLY_THE_SAME:
            query.where(cb.equal(comparedExpression, comparingString));
            break;
        case CONTAINS:
            query.where(cb.like(comparedExpression, "%" + comparingString + "%"));
            break;
        case ENDS_WITH:
            query.where(cb.like(comparedExpression, "%" + comparingString));
            break;
        case STARTS_WITH:
            query.where(cb.like(comparedExpression, comparingString + "%"));
            break;
        default:
            throw new IllegalStateException("Unknown filter operator");
        }
    }

    @Override
    public Class<StringPropertyFilter> getSupportedClass() {
        return StringPropertyFilter.class;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }

}
