package com.prodoko.base.server.boot;

import java.util.Properties;

import org.springframework.orm.jpa.JpaVendorAdapter;

/**
 * JPA properties initialized by PRODOKO BASE defaults. Used for
 * {@link JpaVendorAdapter#getJpaPropertyMap()}
 * 
 * @author walec51
 *
 */
public class JpaProperties extends Properties {

    private static final long serialVersionUID = 2785879600747990109L;

}
