package com.prodoko.base.server.conf.security;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.conf.security.BaseUserAuthority;
import com.prodoko.base.model.conf.security.SecurityService;
import com.prodoko.base.model.conf.security.UserAuthority;
import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.base.model.conf.security.UserLoginBrowserService;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;
import com.prodoko.remoting.support.cloning.RemoteCloner;

@Service("baseSecurityService")
@Transactional
public class BaseSecurityService implements SecurityService {
    
    private static final Log LOG = LogFactory.getLog(BaseSecurityService.class);
    
    private EntityManager entityManager;
    private UserLoginBrowserService userLoginsBrowserService;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public UserLoginBrowserService getUserLoginsBrowserService() {
        return userLoginsBrowserService;
    }

    @Autowired
    public void setUserLoginsBrowserService(
            UserLoginBrowserService userLoginsBrowserService) {
        this.userLoginsBrowserService = userLoginsBrowserService;
    }

    @Override
    public UserLogin getYourUserLogin() {
        String username = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        UserLogin user = (UserLogin)getEntityManager().createNamedQuery("UserLogin.findByUsername")
                .setParameter(1, username).getSingleResult();
        return user;
    }
    
    @Override
    public boolean isNotDevAdmin(UserLogin login) {
        UserLogin loginFromPersistance = getEntityManager().find(UserLogin.class, login.getId());
        return loginFromPersistance == null
                || loginFromPersistance.getUsername().equals(DEV_ADMIN_USERNAME);
    }
    
    @Override
    public void loadMinimalDataIfNeeded() {
        loadAuthoritiesFromEnums(BaseUserAuthority.values());
    }
    
    protected void loadAuthoritiesFromEnums(Enum<?>... enumValues){
        for(Enum<?> authorityEnum : enumValues) {
            UserAuthority authorityEntity = entityManager.find(UserAuthority.class,
                    authorityEnum.toString());
            if(authorityEntity == null) {
                authorityEntity = new UserAuthority(authorityEnum);
                entityManager.persist(authorityEntity);
                entityManager.flush();
            }
        }
    }
}
