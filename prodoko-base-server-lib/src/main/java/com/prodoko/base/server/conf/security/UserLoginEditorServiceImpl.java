package com.prodoko.base.server.conf.security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.prodoko.base.model.conf.security.BaseUserAuthority;
import com.prodoko.base.model.conf.security.UserAuthority;
import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.base.model.conf.security.UserLoginsEditorService;
import com.prodoko.base.model.conf.security.UserRole;
import com.prodoko.base.model.conf.security.UserRoleFinderService;
import com.prodoko.base.server.entities.AbstractEntityEditorService;

@Service
@Transactional
public class UserLoginEditorServiceImpl
        extends AbstractEntityEditorService<UserLogin>
        implements UserLoginsEditorService {
    
    private static final Log LOG = LogFactory.getLog(UserLoginEditorServiceImpl.class);
    
    private UserRoleFinderService hierarchyService;
    private PasswordEncoder passwordEncoder;
    
    @Override
    public Class<UserLogin> getSupportedClass() {
        return UserLogin.class;
    }

    /**
     * TODO
     */
    public UserRoleFinderService getHierarchyService() {
        return hierarchyService;
    }

    /**
     * @see #getHierarchyService
     */
    @Autowired
    public void setHierarchyService(UserRoleFinderService hierarchyService) {
        this.hierarchyService = hierarchyService;
    }

    /**
     * TODO
     */
    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    /**
     * @see #getPasswordEncoder
     */
    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public <E extends UserLogin> E saveWithNewPassword(E entity, String newPassword) {
        if(!StringUtils.isEmpty(newPassword)) {
            entity.setPassword(passwordEncoder.encode(newPassword));
        }
        return save(entity);
    }

    @Override
    public void loadMinimalDataIfNeeded() {
        if(getEntityManager().createNamedQuery("UserLogin.findByUsername")
                .setParameter(1, "devAdmin").getResultList().isEmpty()) {
            UserLogin devAdmin = createStub(UserLogin.class);
            devAdmin.setUsername("devAdmin");
            devAdmin.setPassword(passwordEncoder.encode("devAdmin"));
            UserRole role = hierarchyService.findMostSuperclass(UserRole.class);
            devAdmin.setMainDynamicClass(role);
            UserAuthority devAdminAuth = getEntityManager().find(UserAuthority.class,
                    BaseUserAuthority.DEV_ADMIN.toString());
            if(devAdminAuth == null) {
                throw new IllegalStateException(BaseUserAuthority.DEV_ADMIN.toString()
                        + " authority not found");
            }
            devAdmin.getIndividualAuthorities().add(devAdminAuth);
            save(devAdmin);
        }
    }
}
