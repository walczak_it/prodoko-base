package com.prodoko.base.server.beans.dynamic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassEditorService;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;
import com.prodoko.base.server.entities.AbstractEntityEditorService;

@Transactional
public abstract class AbstractMainDynamicClassEditorService<T extends MainDynamicClass>
        extends AbstractEntityEditorService<T>
        implements MainDynamicClassEditorService<T> {
    
    private MainDynamicClassFinderService<T> finderService;
    
    public MainDynamicClassFinderService<T> getFinderService() {
        return finderService;
    }

    @Autowired
    public void setFinderService(MainDynamicClassFinderService<T> finderService) {
        this.finderService = finderService;
    }

    @Override
    public <E extends T> E createStub(Class<E> stubClass) {
        E stub = super.createStub(stubClass);
        return stub;
    }
}
