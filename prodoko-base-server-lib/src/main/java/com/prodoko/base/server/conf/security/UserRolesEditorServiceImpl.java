package com.prodoko.base.server.conf.security;

import static com.prodoko.base.util.BundleUtils.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.base.model.conf.security.UserRole;
import com.prodoko.base.model.conf.security.UserRoleFinderService;
import com.prodoko.base.model.conf.security.UserRolesEditorService;
import com.prodoko.base.server.entities.AbstractEntityEditorService;

@Service
@Transactional
public class UserRolesEditorServiceImpl extends AbstractEntityEditorService<UserRole>
        implements UserRolesEditorService {
    
    private UserRoleFinderService hierarchyService;

    @Override
    public Class<UserRole> getSupportedClass() {
        return UserRole.class;
    }

    /**
     * TODO
     */
    public UserRoleFinderService getHierarchyService() {
        return hierarchyService;
    }

    /**
     * @see #getHierarchyService
     */
    @Autowired
    public void setHierarchyService(UserRoleFinderService hierarchyService) {
        this.hierarchyService = hierarchyService;
    }

    @Override
    public void loadMinimalDataIfNeeded() {
        if(!hierarchyService.hasMostSuperclass(UserRole.class)) {
            UserRole userRole = new UserRole();
            userRole.setCode(message(UserRole.class, "mostSuperclass.code"));
            userRole.setName(message(UserLogin.class, "plural"));
            userRole = save(userRole);
        }
    }
}
