package com.prodoko.base.server.basic.legalentities;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.basic.legalentities.LegalEntitiesBrowserService;
import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.server.beans.AbstractBeanBrowserService;

@Service
@Transactional
public class LegalEntityBrowserServiceImpl<T extends LegalEntity>
        extends AbstractBeanBrowserService<T>
        implements LegalEntitiesBrowserService<T> {

    @SuppressWarnings("unchecked")
    @Override
    public Class<T> getSupportedClass() {
        return (Class<T>) LegalEntity.class;
    }

}
