package com.prodoko.base.server.beans;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.DataLoad;
import com.prodoko.base.model.beans.browser.BeansBrowserService;
import com.prodoko.base.model.beans.browser.BeansLoadRequest;
import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.base.model.beans.filter.UniversalFilters;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.remoting.support.cloning.RemoteCloner;

@Transactional
public abstract class AbstractBeanBrowserService<T extends StandardEntity>
        implements BeansBrowserService<T> {
    
    private EntityManager entityManager;

    private ClassSupportersMap<SingleFilterQueryInjector<?>> filterSpecificationsFactories;
    
    @Override
    public boolean isSupportsSubclasses() {
        return true;
    }

    /**
     * TODO
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @see #getEntityManager
     */
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * TODO
     */
    public ClassSupportersMap<SingleFilterQueryInjector<?>>
            getFilterSpecificationsFactories() {
        return filterSpecificationsFactories;
    }

    /**
     * @see #getFilterSpecificationsFactories
     */
    public void setFilterSpecificationsFactories(
            ClassSupportersMap<SingleFilterQueryInjector<?>>
                    filterSpecificationsFactories) {
        this.filterSpecificationsFactories = filterSpecificationsFactories;
    }

    @Autowired
    public void buildFilterSpecificationFactories(
            @UniversalFilters ClassSupportersMap<SingleFilterQueryInjector<?>>
                    universalFilterSpecificationsFactories) {
        filterSpecificationsFactories = universalFilterSpecificationsFactories;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public DataLoad<T> browse(BeansLoadRequest request) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(getSupportedClass());
        Root<T> root = cq.from(getSupportedClass());
        if(request.getFilters() != null) {
            for(FilterData filterData : request.getFilters()) {
                SingleFilterQueryInjector factory = filterSpecificationsFactories.getOrThrow(
                        filterData.getClass());
                factory.inject(root, cq, cb, filterData);
            }
        }
        Query query = getEntityManager().createQuery(cq);
        DataLoad<T> load = new DataLoad<>();
        load.setTotalResults(query.getResultList().size());
        query.setFirstResult(request.getFirstResult());
        query.setMaxResults(request.getMaxResults());
        load.setData(query.getResultList());
        return load;
    }
}
