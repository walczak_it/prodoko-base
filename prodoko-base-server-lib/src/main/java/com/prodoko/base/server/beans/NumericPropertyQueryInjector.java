package com.prodoko.base.server.beans;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import com.prodoko.base.model.beans.filter.NumericPropertyFilter;

public class NumericPropertyQueryInjector
        implements SingleFilterQueryInjector<NumericPropertyFilter> {

    @Override
    public void inject(Root<?> root, CriteriaQuery<?> query, CriteriaBuilder cb,
            NumericPropertyFilter data) {

            Path<Number> property = root.get(data.getPropertyPath());
            switch(data.getOperator()) {
            case EQUAL:
                query.where(cb.equal(property, data.getComparingNumber()));
                break;
            case GREATER:
                query.where(cb.gt(property, data.getComparingNumber()));
                break;
            case GREATER_OR_EQUAL:
                query.where(cb.ge(property, data.getComparingNumber()));
                break;
            case LESS:
                query.where(cb.lt(property, data.getComparingNumber()));
                break;
            case LESS_OR_EQUAL:
                query.where(cb.le(property, data.getComparingNumber()));
                break;
            default:
                throw new IllegalStateException("Unknown filter operator");
            }
    }

    @Override
    public Class<NumericPropertyFilter> getSupportedClass() {
        return NumericPropertyFilter.class;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }

}
