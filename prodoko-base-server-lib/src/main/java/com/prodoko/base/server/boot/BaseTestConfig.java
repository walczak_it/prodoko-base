package com.prodoko.base.server.boot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:/com/prodoko/base/server/baseTest.properties")
@Import(BaseServiceConfig.class)
public class BaseTestConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer pc
            = new PropertySourcesPlaceholderConfigurer();
        pc.setIgnoreResourceNotFound(true);
        pc.setIgnoreUnresolvablePlaceholders(true);
        return pc;
    }
    
    @Bean
    public JpaProperties jpaProperties() {
        JpaProperties props = new JpaProperties();
        props.setProperty("hibernate.hbm2ddl.auto", "create");
        return props;
    }
}
