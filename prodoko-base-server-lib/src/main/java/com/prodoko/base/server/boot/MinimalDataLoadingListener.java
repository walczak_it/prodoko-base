package com.prodoko.base.server.boot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeEditorService;
import com.prodoko.base.model.conf.security.SecurityService;
import com.prodoko.base.model.conf.security.UserLoginsEditorService;
import com.prodoko.base.model.conf.security.UserRolesEditorService;
import com.prodoko.base.model.entities.MinimalDataLoaderService;

public class MinimalDataLoadingListener implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        
        ApplicationContext context = event.getApplicationContext();
        
        loadMinimalDataIfNeededByConf(context);
        loadMinimalDataIfNeededByBasic(context);
    }
    
    protected void loadMinimalDataIfNeededByConf(ApplicationContext context) {
        loadMinimalDataIfNeeded(context, SecurityService.class);
        loadMinimalDataIfNeeded(context, UserRolesEditorService.class);
        loadMinimalDataIfNeeded(context, UserLoginsEditorService.class);
    }
    
    protected void loadMinimalDataIfNeededByBasic(ApplicationContext context) {
        loadMinimalDataIfNeeded(context, LegalEntityTypeEditorService.class);
    }
    
    protected <T extends MinimalDataLoaderService> void loadMinimalDataIfNeeded(
            ApplicationContext context, Class<T> serviceClass) {
        T service = context.getBean(serviceClass);
        service.loadMinimalDataIfNeeded();
    }
}
