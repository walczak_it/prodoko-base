package com.prodoko.base.server.basic.legalentities.types;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeFinderService;
import com.prodoko.base.server.beans.dynamic.AbstractMainDynamicClassFinderService;

@Service
@Transactional
public class LegalEntityTypeFinderServiceImpl
        extends AbstractMainDynamicClassFinderService<LegalEntityType>
        implements LegalEntityTypeFinderService {

    @Override
    public Class<LegalEntityType> getSupportedClass() {
        return LegalEntityType.class;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }

    @Override
    protected String getByStringQueryName() {
        return "LegalEntityType.findByClassAndCode";
    }

    @Override
    protected String getSuggestionsByStringQueryName() {
        return "LegalEntityType.findByClassAndLikeCodeOrName";
    }
}
