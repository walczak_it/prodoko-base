package com.prodoko.base.server.entities;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.entities.EntityFinderService;
import com.prodoko.base.model.entities.StandardEntity;

@Transactional
public abstract class AbstractEntityFinderService<T extends StandardEntity>
        implements EntityFinderService<T> {
    
    private EntityManager entityManager;
    
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public T find(Long id) {
        return find(getSupportedClass(), id);
    }
    
    @Override
    public <E extends T> E find(Class<E> subclass, Long id) {
        return entityManager.find(subclass, id);
    }
}
