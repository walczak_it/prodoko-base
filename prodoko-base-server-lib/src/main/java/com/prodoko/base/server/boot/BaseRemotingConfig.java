package com.prodoko.base.server.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping;

import com.prodoko.remoting.hessian.EnableHessianExport;
import com.prodoko.remoting.hibernate.HibernateAwareInstanceProducer;
import com.prodoko.remoting.hibernate.NullHibernateSerializerFactory;
import com.prodoko.remoting.jpa.ByIdEntityFinder;
import com.prodoko.remoting.support.cloning.RemoteCloner;
import com.prodoko.remoting.support.cloning.RemoteCloningInterceptor;
import com.prodoko.remoting.support.cloning.TransactionalCloningInterceptor;

@Configuration
@Import(BaseServiceConfig.class)
@EnableHessianExport(interceptorBeanNames="remoteCloningInterceptor",
        serializerFactoryClass=NullHibernateSerializerFactory.class)
@EnableWebMvc
public class BaseRemotingConfig extends WebMvcConfigurationSupport {
    
    @Bean
    public BeanNameUrlHandlerMapping beanNameHandlerMapping() {
      BeanNameUrlHandlerMapping handlerMapping = super.beanNameHandlerMapping();
      handlerMapping.setDetectHandlersInAncestorContexts(true);
      return handlerMapping;
    }
    
    @Bean
    public ByIdEntityFinder byIdEntityFinder() {
        return new ByIdEntityFinder();
    }
    
    @Bean
    public HibernateAwareInstanceProducer hibernateAwareInstanceProducer() {
        return new HibernateAwareInstanceProducer();
    }
    
    @Bean @Autowired
    public RemoteCloner remoteCloner(ByIdEntityFinder byIdEntityFinder,
            HibernateAwareInstanceProducer hibernateAwareInstanceProducer) {
        RemoteCloner cloner = new RemoteCloner();
        cloner.setDefaultProducer(hibernateAwareInstanceProducer);
        cloner.setDefaultFinder(byIdEntityFinder);
        return cloner;
    }
    
    @Bean(name="remoteCloningInterceptor") @Autowired
    public TransactionalCloningInterceptor remoteCloningInterceptor(
            RemoteCloner remoteCloner) {
        TransactionalCloningInterceptor tci = new TransactionalCloningInterceptor();
        tci.setCloner(remoteCloner);
        return tci;
    }
}
