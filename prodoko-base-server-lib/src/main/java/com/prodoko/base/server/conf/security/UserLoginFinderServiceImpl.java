package com.prodoko.base.server.conf.security;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.base.model.conf.security.UserLoginFinderService;
import com.prodoko.base.server.beans.AbstractBeanFinderService;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;
import com.prodoko.remoting.support.cloning.RemoteCloner;

@Service
@Transactional
public class UserLoginFinderServiceImpl extends AbstractBeanFinderService<UserLogin>
        implements UserLoginFinderService {
    
    @Override
    public Class<UserLogin> getSupportedClass() {
        return UserLogin.class;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return true;
    }

    @Override
    protected String getByStringQueryName() {
        return "UserLogin.findByClassAndUsername";
    }

    @Override
    protected String getSuggestionsByStringQueryName() {
        return "UserLogin.findByClassAndLikeUsername";
    }
}
