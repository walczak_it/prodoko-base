package com.prodoko.base.server.boot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BaseEntityInfo {

    public List<String> getPackagesToScan() {
        return new ArrayList<>(Arrays.asList("com.prodoko.base.model"));
    }
    
    public List<String> getMappingResources() {
        return new ArrayList<>(Arrays.asList(
                "com/prodoko/base/server/conf/security/orm.xml",
                "com/prodoko/base/server/beans/dynamic/orm.xml",
                "com/prodoko/base/server/basic/legalentities/orm.xml",
                "com/prodoko/base/server/basic/legalentities/types/orm.xml"));
    }
}
