package com.prodoko.base.server.legalentities.types;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.basic.legalentities.types.CompanyType;
import com.prodoko.base.model.basic.legalentities.types.InstitutionType;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeFinderService;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeEditorService;
import com.prodoko.base.model.basic.legalentities.types.OrganizationType;
import com.prodoko.base.model.basic.legalentities.types.PersonType;
import com.prodoko.base.server.boot.BaseTestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class,
    classes={BaseTestConfig.class})
@Transactional
public class DynamicClassFinderServiceTests {

    @Autowired
    private LegalEntityTypeEditorService typeEditorService;
    
    @Autowired
    private LegalEntityTypeFinderService typeFinderService;
    
    @Before
    public void loadMinimalData() {
        typeEditorService.loadMinimalDataIfNeeded();
    }
    
    @Test
    public void findMostSuperOrganisationClassTest() throws Exception {
        assertTrue(typeFinderService.hasMostSuperclass(
                OrganizationType.class));
        OrganizationType odc = typeFinderService.findMostSuperclass(
                OrganizationType.class);
        assertFalse(odc instanceof CompanyType);
        assertFalse(odc instanceof InstitutionType);
    }
    
    @Test
    public void findOrganisationSubclassesTest() throws Exception {
        assertTrue(typeFinderService.hasMostSuperclass(
                OrganizationType.class));
        Set<OrganizationType> subclasses = typeFinderService
                .findMostSuperclassSubclasses(OrganizationType.class);
        CompanyType companyDynamicClass = typeFinderService.findMostSuperclass(
                CompanyType.class);
        InstitutionType institutionDynamicClass = typeFinderService
                .findMostSuperclass(InstitutionType.class);
        assertTrue(subclasses.contains(companyDynamicClass));
        assertTrue(subclasses.contains(institutionDynamicClass));
        assertEquals(2, subclasses.size());
    }
    
    @Test
    public void findSuggestionsStringTest() throws Exception {
        
        PersonType personType = typeFinderService.findMostSuperclass(PersonType.class);
        
        String partOfCode = personType.getCode().substring(1, 2);
        List<PersonType> suggestionsFromPartOfCode = typeFinderService.findSuggestionsByString(
                PersonType.class, partOfCode);
        assertEquals(1, suggestionsFromPartOfCode.size());
        assertEquals(personType, suggestionsFromPartOfCode.get(0));
        
        String partOfName = personType.getCode().substring(1, 3);
        List<PersonType> suggestionsFromPartOfName = typeFinderService.findSuggestionsByString(
                PersonType.class, partOfName);
        assertEquals(1, suggestionsFromPartOfName.size());
        assertEquals(personType, suggestionsFromPartOfName.get(0));
    }
    
    @Test
    public void findSuggestionsInheritanceTest() throws Exception {
        
        PersonType personType = typeFinderService.findMostSuperclass(PersonType.class);
        
        List<LegalEntityType> suggestionsFromPartOfCode = typeFinderService.findSuggestionsByString(
                LegalEntityType.class, personType.getCode());
        assertEquals(1, suggestionsFromPartOfCode.size());
        assertEquals(personType, suggestionsFromPartOfCode.get(0));
    }
}
