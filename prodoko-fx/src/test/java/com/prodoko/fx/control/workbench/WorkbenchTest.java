package com.prodoko.fx.control.workbench;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.scene.Node;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.prodoko.fx.control.workbench.MenuNodeDefinition;
import com.prodoko.fx.control.workbench.MenuPositionDefinition;
import com.prodoko.fx.control.workbench.MenuSectionDefinition;
import com.prodoko.fx.control.workbench.Workbench;
import com.prodoko.fx.control.workbench.impl.MenuPosition;
import com.prodoko.fx.control.workbench.impl.MenuSection;
import com.prodoko.fx.control.workbench.impl.MenuSiblingOrderComparator;
import com.prodoko.fx.junit.JavaFxJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(JavaFxJUnit4ClassRunner.class)
public class WorkbenchTest {

    @Test
    public void extractAllMenuPositionsTest() throws Exception {
        TestDataA testData = new TestDataA();
        Collection<MenuPositionDefinition> positionDefinitions = testData.positionDefinitions();
        Workbench w = new Workbench(positionDefinitions);
        Map<MenuPositionDefinition,MenuPosition> positionMap
            = w.extractAllMenuPositions();
        assertTrue(positionMap.containsKey(testData.a));
        assertTrue(positionMap.containsKey(testData.c));
        assertTrue(positionMap.containsKey(testData.d));
        assertTrue(positionMap.containsKey(testData.e));
        assertTrue(positionMap.containsKey(testData.b));
        assertTrue(positionMap.containsKey(testData.x));
        assertEquals(6, positionMap.size());
        
        for(MenuPosition position : positionMap.values()) {
            assertNotNull(position.getDefinition());
        }
    }
    
    @Test
    public void buildMenuStructureTest() throws Exception {
        TestDataA testData = new TestDataA();
        Workbench w = new Workbench(testData.positionDefinitions());
        w.buildMenuStructure();
        List<MenuSection> sections = w.getMenuStructure();
        assertEquals(1, sections.size());
        
        MenuSection testSection = sections.iterator().next();
        assertEquals(testData.testSection, testSection.getDefinition());
        MenuPosition x = MenuPosition.findByDefinition(testData.x,
                testSection.getRootPositions());
        assertNotNull(x);
        assertEquals(0, x.getChildPositions().size());
        MenuPosition a = MenuPosition.findByDefinition(testData.a,
                testSection.getRootPositions());
        assertNotNull(a);
        assertEquals(2, a.getChildPositions().size());
        
        assertEquals(2, testSection.getRootPositions().size());
        
        MenuPosition b = MenuPosition.findByDefinition(testData.b, a.getChildPositions());
        assertNotNull(b);
        MenuPosition c = MenuPosition.findByDefinition(testData.c,
                a.getChildPositions());
        assertNotNull(c);
        assertEquals(0, c.getChildPositions().size());
        
        MenuPosition d = MenuPosition.findByDefinition(testData.d,
                b.getChildPositions());
        assertNotNull(d);
        assertEquals(0, d.getChildPositions().size());
        
        MenuPosition e = MenuPosition.findByDefinition(testData.e,
                b.getChildPositions());
        assertNotNull(e);
        assertEquals(0, e.getChildPositions().size());
        
        assertEquals(2, b.getChildPositions().size());
    }
    
    @Test
    public void orderComparatorTest() throws Exception {
        
        MenuSiblingOrderComparator<MenuSection> comp = new MenuSiblingOrderComparator<>();
        MenuSection a = new MenuSection();
        a.setDefinition(new MenuSectionDefinition() {
            
            @Override
            public String getTitle() {
                return "a";
            }
            
            @Override
            public int getOrderPriority() {
                return MenuNodeDefinition.BIG_PRIORITY_STEP * 1;
            }
        });
        MenuSection b = new MenuSection();
        b.setDefinition(new MenuSectionDefinition() {
            
            @Override
            public String getTitle() {
                return "b";
            }
            
            @Override
            public int getOrderPriority() {
                return MenuNodeDefinition.BIG_PRIORITY_STEP * 2;
            }
        });
        
        int resAB = comp.compare(a, b);
        assertTrue(resAB < 0);
        
        int resBA = comp.compare(b, a);
        assertTrue(resBA > 0);
        
        MenuSection c = new MenuSection();
        c.setDefinition(new MenuSectionDefinition() {
            
            @Override
            public String getTitle() {
                return "c";
            }
            
            @Override
            public int getOrderPriority() {
                return MenuNodeDefinition.BIG_PRIORITY_STEP * 1;
            }
        });
        int resAC = comp.compare(a, c);
        assertTrue(resAC == 0);
    }
    
    @Test
    public void menuOrderingTest() throws Exception {
        TestDataB testData = new TestDataB();
        Workbench w = new Workbench(testData.presentersSet());
        w.buildMenuStructure();
        List<MenuSection> sections = w.getMenuStructure();
        Iterator<MenuSection> sectionsItr = sections.iterator();
        
        MenuSection secOne = sectionsItr.next();
        assertEquals(testData.secOne, secOne.getDefinition());
        
        MenuSection secTwo = sectionsItr.next();
        assertEquals(testData.secTwo, secTwo.getDefinition());
        
        MenuSection secThree = sectionsItr.next();
        assertEquals(testData.secThree, secThree.getDefinition());
        assertFalse(sectionsItr.hasNext());
        
        Iterator<MenuPosition> secOneItr = secOne.getRootPositions().iterator();
        assertEquals(testData.a, secOneItr.next().getDefinition());
        assertEquals(testData.b, secOneItr.next().getDefinition());
        assertEquals(testData.c, secOneItr.next().getDefinition());
        assertFalse(secOneItr.hasNext());
        
        Iterator<MenuPosition> secTwoItr = secTwo.getRootPositions().iterator();
        assertEquals(testData.d, secTwoItr.next().getDefinition());
        assertFalse(secTwoItr.hasNext());
        
        Iterator<MenuPosition> secThreeItr = secThree.getRootPositions().iterator();
        MenuPosition e = secThreeItr.next();
        assertEquals(testData.e, e.getDefinition());
        assertFalse(secThreeItr.hasNext());
        
        Iterator<MenuPosition> eItr = e.getChildPositions().iterator();
        assertEquals(testData.f, eItr.next().getDefinition());
        assertEquals(testData.g, eItr.next().getDefinition());
        assertEquals(testData.h, eItr.next().getDefinition());
        assertFalse(eItr.hasNext());
    }
    
    /**
     * testSection:
     * - A
     * -- B - just position with no presenter
     * --- D
     * --- E
     * -- C
     * - x
     */
    public class TestDataA {
        
        public final MenuSectionDefinition testSection = new MenuSectionDefinition() {
            
            @Override
            public String getTitle() {
                return "test section";
            }
        };
        
        public final MenuPositionDefinition a = new MenuPositionDefinition() {
            
                @Override
                public String getTitle() {
                    return "A";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return testSection;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition b = new MenuPositionDefinition() {
            
            @Override
            public String getTitle() {
                return "B";
            }
            
            @Override
            public MenuNodeDefinition getParent() {
                return a;
            }

            @Override
            public Node createContent() {
                return null;
            }
        };
        
        public final MenuPositionDefinition d = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "D";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return b;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition e = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "E";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return b;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition c = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "C";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return a;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition x = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "X";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return testSection;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public Collection<MenuPositionDefinition> positionDefinitions() {
            return Arrays.asList(a, c, d, e, x);
        }
    }
    
    /**
     * secOne:
     * - A
     * - B
     * - C
     * secTwo:
     * - D
     * secThree:
     * - E
     * -- F
     * -- G
     * -- H
     */
    public class TestDataB {
        
        public final MenuSectionDefinition secOne = new MenuSectionDefinition() {
            
            @Override
            public String getTitle() {
                return "secOne";
            }
            
            @Override
            public int getOrderPriority() {
                return MenuNodeDefinition.BIG_PRIORITY_STEP * 1;
            }
        };
        
        public final MenuSectionDefinition secTwo = new MenuSectionDefinition() {
            
            @Override
            public String getTitle() {
                return "secTwo";
            }
            
            @Override
            public int getOrderPriority() {
                return MenuNodeDefinition.BIG_PRIORITY_STEP * 2;
            }
        };
        
        public final MenuSectionDefinition secThree = new MenuSectionDefinition() {
            
            @Override
            public String getTitle() {
                return "secThree";
            }
            
            @Override
            public int getOrderPriority() {
                return MenuNodeDefinition.BIG_PRIORITY_STEP * 3;
            }
        };
        
        public final MenuPositionDefinition a = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "A";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return secOne;
                }
                
                @Override
                public int getOrderPriority() {
                    return MenuNodeDefinition.BIG_PRIORITY_STEP * 1;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition b = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "B";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return secOne;
                }
                
                @Override
                public int getOrderPriority() {
                    return MenuNodeDefinition.BIG_PRIORITY_STEP * 2;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition c = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "C";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return secOne;
                }
                
                @Override
                public int getOrderPriority() {
                    return MenuNodeDefinition.BIG_PRIORITY_STEP * 3;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition d = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "D";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return secTwo;
                }
                
                @Override
                public int getOrderPriority() {
                    return MenuNodeDefinition.BIG_PRIORITY_STEP * 1;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition e = new MenuPositionDefinition() {
            
            @Override
            public String getTitle() {
                return "E";
            }
            
            @Override
            public MenuNodeDefinition getParent() {
                return secThree;
            }
            
            @Override
            public int getOrderPriority() {
                return MenuNodeDefinition.BIG_PRIORITY_STEP * 1;
            }

            @Override
            public Node createContent() {
                return null;
            }
        };
        
        public final MenuPositionDefinition f = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "F";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return e;
                }
                
                @Override
                public int getOrderPriority() {
                    return MenuNodeDefinition.BIG_PRIORITY_STEP * 1;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition g = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "F";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return e;
                }
                
                @Override
                public int getOrderPriority() {
                    return MenuNodeDefinition.BIG_PRIORITY_STEP * 2;
                }

                @Override
                public Node createContent() {
                    return null;
                }
        };
        
        public final MenuPositionDefinition h = new MenuPositionDefinition() {
                    
                @Override
                public String getTitle() {
                    return "F";
                }
                
                @Override
                public MenuNodeDefinition getParent() {
                    return e;
                }
                
                @Override
                public int getOrderPriority() {
                    return MenuNodeDefinition.BIG_PRIORITY_STEP * 3;
                }

                @Override
                public Node createContent() {
                    // TODO Auto-generated method stub
                    return null;
                }
        };
        
        public Set<MenuPositionDefinition> presentersSet() {
            return new HashSet<MenuPositionDefinition>(Arrays.asList(h, f, a, d, c, b, g));
        }
    }
}
