package com.prodoko.fx.tools;

import java.util.List;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import com.prodoko.fx.ProdokoFxConsts;

public class PreviewApp extends Application {
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        List<String> unnamedParams = getParameters().getUnnamed();
        if(unnamedParams.size() == 0) {
            throw new IllegalArgumentException(
                    "Wrong number of parameters. Expected at least the class "
                    + "name.");
        }
        String clazzName = unnamedParams.get(0);
        Class<?> clazz = Class.forName(clazzName);
        Object obj = clazz.newInstance();
        Scene scene;
        if(obj instanceof Scene) {
            scene = (Scene) obj;
        } else if(obj instanceof Parent) {
            Parent parent = (Parent)obj;
            scene = new Scene(parent);
        } else {
            throw new IllegalArgumentException( "Class " + clazzName
                    + " is not a Scene nor a Parent node");
        }
        scene.getStylesheets().addAll(ProdokoFxConsts.getFxStyleSheets());
        if(unnamedParams.size() > 1) {
            for(int i = 1; i < unnamedParams.size(); ++i) {
                scene.getStylesheets().add(unnamedParams.get(i));
            }
        }
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
