package com.prodoko.fx.control;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Undoable for now.
 * TODO: https://javafx-jira.kenai.com/browse/RT-35274
 */
public class ResTextField extends TextField {
    
    private DoubleProperty maxFontSize;

    public ResTextField() {
        super();
        init();
    }

    public ResTextField(String arg0) {
        super(arg0);
        init();
    }
    
    private void init() {
        setPrefWidth(300);
        maxFontSize = new SimpleDoubleProperty(12);
        textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> arg0,
                    String arg1, String newValue) {
                adjustFontSizeToWidth();
            }
        });
        maxFontSize.addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> arg0,
                    Number arg1, Number arg2) {
                adjustFontSizeToWidth();
            }
        });
    }
    
    public DoubleProperty maxFontSizeProperty() {
        return maxFontSize;
    }
    
    public double getMaxFontSize() {
        return maxFontSize.get();
    }
    
    public void setMaxFontSize(double value) {
        maxFontSize.set(value);
    }
    
    protected void adjustFontSizeToWidth() {
        Text text = new Text(getText());
        double fontSize = maxFontSize.get();
        text.setFont(getFont());
        double textWidth = text.getLayoutBounds().getWidth();
        final double fieldPrefWidth = getPrefWidth();
        final double fieldMaxWidth = getMaxWidth();
        final double fieldWidth = getWidth();
        final double fieldHeight = getHeight();
        while(textWidth > fieldWidth - 10) {
            --fontSize;
            if(fontSize <= 0) {
                throw new IllegalStateException("Couldn't find font size to fit into "
                        + fieldWidth + " field width");
            }
            // TODO: https://javafx-jira.kenai.com/browse/RT-35273
            text.setFont(Font.font(text.getFont().getFamily(), fontSize));
            textWidth = text.getLayoutBounds().getWidth();
        }
        setFont(text.getFont());
        setMaxHeight(fieldHeight);
        setHeight(fieldHeight);
        setPrefWidth(fieldPrefWidth);
        setMaxWidth(fieldMaxWidth);
    }
}
