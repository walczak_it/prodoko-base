package com.prodoko.fx.control.workbench.impl;

import java.util.Comparator;

import com.prodoko.fx.control.workbench.MenuNodeDefinition;

public class MenuDefinitionSiblingOrderComparator<T extends MenuNodeDefinition>
        implements Comparator<T> {
    
    @Override
    public int compare(T a, T b) {
        int ap = a.getOrderPriority();
        int bp = b.getOrderPriority();
        return ap - bp;
    }
}