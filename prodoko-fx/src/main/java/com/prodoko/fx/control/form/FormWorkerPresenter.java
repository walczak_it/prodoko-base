package com.prodoko.fx.control.form;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.prodoko.fx.validation.ValidationPresenter;

public class FormWorkerPresenter implements ChangeListener<Worker.State> {
    
    private static final Log LOG = LogFactory
            .getLog(FormWorkerPresenter.class);
    
    private FormSkin<? extends Form> formSkin;
    private Worker<?> worker;

    public FormWorkerPresenter(FormSkin<? extends Form> formSkin, Worker<?> worker) {
        this.formSkin = formSkin;
        this.worker = worker;
    }
    
    /**
     * TODO
     */
    public FormSkin<? extends Form> getFormSkin() {
        return formSkin;
    }

    public ValidationPresenter getValidationPresenter() {
        return getFormSkin().getValidationPresenter();
    }

    @Override
    public void changed(ObservableValue<? extends State> observableValue,
            Worker.State oldState, Worker.State newState) {
        
        switch (newState) {
        case SCHEDULED:
            getValidationPresenter().clear();
            getFormSkin().blockingTaskStarted();
            break;
        case SUCCEEDED:
            getFormSkin().blockingTaskEnded();
            getValidationPresenter().ok("Zakończono pomyślnie");
            break;
        case FAILED:
            getFormSkin().blockingTaskEnded();
            Throwable t = worker.getException();
            LOG.error("Form task ended with error", t);
            getValidationPresenter().error(t);
            break;
        case CANCELLED:
            getFormSkin().blockingTaskEnded();
            getValidationPresenter().warning("Anulowano");
            break;
        default: // nothing
            break;
      }
    }
}
