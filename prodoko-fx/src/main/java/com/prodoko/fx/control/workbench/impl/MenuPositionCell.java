package com.prodoko.fx.control.workbench.impl;

import javafx.scene.control.TreeCell;

import com.prodoko.fx.control.workbench.MenuPositionDefinition;

public class MenuPositionCell extends TreeCell<MenuPositionDefinition> {
    
    @Override
    protected void updateItem(MenuPositionDefinition definition, boolean empty) {
        super.updateItem(definition, empty);
        setText(definition != null ? definition.getTitle() : "");
    }
    
    
}