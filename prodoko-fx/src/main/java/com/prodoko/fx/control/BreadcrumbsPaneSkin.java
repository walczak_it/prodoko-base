package com.prodoko.fx.control;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Labeled;
import javafx.scene.control.SkinBase;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;

import org.controlsfx.control.SegmentedButton;

import com.prodoko.fx.MirrorBinder;
import com.prodoko.fx.OnceRunner;

public class BreadcrumbsPaneSkin extends SkinBase<BreadcrumbsPane> {
    
    private static final double MAX_BREADCRUMBS_HEIGHT = 30;
    private static final double MAX_BREADCRUMBS_PANE_GAP = 20;
    
    private AnchorPane pane;
    private SegmentedButton breadcrumbs;
    private OnceRunner populatePaneRunner;

    public BreadcrumbsPaneSkin(BreadcrumbsPane control) {
        super(control);
        pane = new AnchorPane();
        getChildren().add(pane);
        populatePaneRunner = new OnceRunner(new Runnable() {
            
            @Override
            public void run() {
                populatePane();
            }
        });
        buildItemsChangeListeners();
        populatePane();
    }
    
    protected void buildItemsChangeListeners() {
        getSkinnable().itemsProperty().addListener(new ListChangeListener<Labeled>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends Labeled> change) {
                populatePaneRunner.runLaterOnce();
            }
            
        });
        getSkinnable().displayedItemProperty().addListener(new ChangeListener<Labeled>() {

            @Override
            public void changed(ObservableValue<? extends Labeled> observable,
                    Labeled oldValue, Labeled newValue) {
                if(newValue == null) {
                    throw new IllegalArgumentException("Displayed item cannot be null");
                }
                int index = getSkinnable().itemsProperty().indexOf(newValue);
                if(index == -1) {
                    throw new IllegalArgumentException("Item is not a part of this pane");
                }
                populatePaneRunner.runLaterOnce();
            }
        });
    }
    
    protected void populatePane() {
        pane.getChildren().clear();
        breadcrumbs = new SegmentedButton();
        breadcrumbs.setMaxHeight(MAX_BREADCRUMBS_HEIGHT);
        AnchorPane.setTopAnchor(breadcrumbs, MAX_BREADCRUMBS_PANE_GAP);
        AnchorPane.setLeftAnchor(breadcrumbs, MAX_BREADCRUMBS_PANE_GAP);
        pane.getChildren().add(breadcrumbs);
        for(final Labeled item : getSkinnable().itemsProperty()) {
            ToggleButton button = new ToggleButton();
            MirrorBinder.bindLabelProperties(button, item);
            button.onActionProperty().set(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    getSkinnable().setDisplayedItem(item);
                }
            });
            breadcrumbs.getButtons().add(button);
        }
        Labeled dispayedItem = getSkinnable().getDisplayedItem();
        int displayedIndex = getSkinnable().getItems().indexOf(dispayedItem);
        breadcrumbs.getButtons().get(displayedIndex)
                .setSelected(true);
        AnchorPane.setTopAnchor(dispayedItem,
                MAX_BREADCRUMBS_HEIGHT + MAX_BREADCRUMBS_PANE_GAP*3);
        AnchorPane.setRightAnchor(dispayedItem, 0d);
        AnchorPane.setBottomAnchor(dispayedItem, 0d);
        AnchorPane.setLeftAnchor(dispayedItem, 0d);
        pane.getChildren().add(dispayedItem);
    }
}
