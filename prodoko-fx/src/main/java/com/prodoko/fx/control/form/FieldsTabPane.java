package com.prodoko.fx.control.form;

import javafx.scene.control.TabPane;

public class FieldsTabPane extends TabPane {
    
    private static final String DEFAULT_STYLE_CLASS = "fields-tab-pane";

    public FieldsTabPane() {
        super();
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
    }
}
