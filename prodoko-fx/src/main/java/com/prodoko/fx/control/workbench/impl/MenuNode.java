package com.prodoko.fx.control.workbench.impl;

import com.prodoko.fx.control.workbench.MenuNodeDefinition;

public abstract class MenuNode {

    public abstract MenuNodeDefinition getDefinition();
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getDefinition().hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MenuNode other = (MenuNode) obj;
        if (getDefinition() == null) {
            if (other.getDefinition() != null)
                return false;
        } else if (!getDefinition().equals(other.getDefinition()))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "MenuNode [title=" + getDefinition().getTitle() + "]";
    }
}
