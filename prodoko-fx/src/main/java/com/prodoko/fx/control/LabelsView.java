package com.prodoko.fx.control;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.util.Callback;

public class LabelsView<T> extends Control {
    
    private static final String DEFAULT_STYLE_CLASS = "labels-view";

    private ListProperty<T> items;
    private ObjectProperty<Callback<LabelsView<T>, LabelCell<T>>> cellFactory;
    private ObjectProperty<Node> afterLastItem;
    
    public LabelsView() {
        this(FXCollections.<T>observableArrayList());
    }
    
    public LabelsView(ObservableList<T> items) {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.items = new SimpleListProperty<>(items);
        this.cellFactory = new SimpleObjectProperty<>(this, "cellFactory");
        this.afterLastItem = new SimpleObjectProperty<>(this, "afterLastItem");
    }
    
    public ObjectProperty<Callback<LabelsView<T>, LabelCell<T>>> cellFactoryProperty() {
        return cellFactory;
    }
    
    public Callback<LabelsView<T>, LabelCell<T>> getCellFactory() {
        return cellFactory.get();
    }
    
    public void setCellFactory(Callback<LabelsView<T>, LabelCell<T>> value) {
        cellFactory.set(value);
    }
    
    public ListProperty<T> itemsProperty() {
        return items;
    }
    
    public ObservableList<T> getItems() {
        return items.get();
    }
    
    public void setItems(ObservableList<T> value) {
        items.set(value);
    }
    
    public ObjectProperty<Node> afterLastItemProperty() {
        return afterLastItem;
    }
    
    public Node getAfterLastItem() {
        return afterLastItem.get();
    }
    
    public void setAfterLastItem(Node node) {
        afterLastItem.set(node);
    }
    
    @Override
    protected Skin<?> createDefaultSkin() {
        return new LabelsViewSkin<LabelsView<T>, T>(this);
    }
}
