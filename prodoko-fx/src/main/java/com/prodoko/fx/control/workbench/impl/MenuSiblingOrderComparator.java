package com.prodoko.fx.control.workbench.impl;

import java.util.Comparator;

import com.prodoko.fx.control.workbench.MenuNodeDefinition;

public class MenuSiblingOrderComparator<T extends MenuNode> implements Comparator<T> {
    
    private MenuDefinitionSiblingOrderComparator<MenuNodeDefinition> comp
        = new MenuDefinitionSiblingOrderComparator<>();
    
    @Override
    public int compare(T a, T b) {
        MenuNodeDefinition ad = a.getDefinition();
        MenuNodeDefinition bd = b.getDefinition();
        return comp.compare(ad, bd);
    }
}