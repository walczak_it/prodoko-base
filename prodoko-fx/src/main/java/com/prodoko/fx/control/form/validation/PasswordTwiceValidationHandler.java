package com.prodoko.fx.control.form.validation;

import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.util.Callback;

import com.prodoko.fx.validation.ValidationPresenter;
import com.prodoko.fx.validation.ValidationResult;

public class PasswordTwiceValidationHandler implements Callback<Void, ValidationResult> {
    
    private ValidationPresenter validationPresenter;
    private TextInputControl passwordField;
    private TextInputControl passwordAgainField;
    private String passwordNotEqualMessage;

    public PasswordTwiceValidationHandler(
            ValidationPresenter validationPresenter, TextInputControl passwordField,
            TextInputControl passwordAgainField, String passwordNotEqualMessage) {
        super();
        this.validationPresenter = validationPresenter;
        this.passwordField = passwordField;
        this.passwordAgainField = passwordAgainField;
        this.passwordNotEqualMessage = passwordNotEqualMessage;
    }

    @Override
    public ValidationResult call(Void arg) {
        if(passwordField.getText() != null && !passwordField.getText().isEmpty()) {
            if(passwordAgainField.getText() == null || passwordAgainField.getText().isEmpty()
                    || !passwordField.getText().equals(passwordAgainField.getText())) {
                validationPresenter.error(passwordNotEqualMessage,
                        passwordField, passwordAgainField);
                return ValidationResult.ERROR;
            }
        }
        return ValidationResult.OK;
    }

}
