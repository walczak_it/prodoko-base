package com.prodoko.fx.control.workbench.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.scene.Node;

import com.prodoko.fx.control.workbench.MenuPositionDefinition;

public class MenuPosition extends MenuNode {

    private MenuPositionDefinition definition;
    private Node content;
    private List<MenuPosition> childPositions = new ArrayList<>();
    
    @Override
    public MenuPositionDefinition getDefinition() {
        return definition;
    }
    
    /**
     * @see #getDefinition
     */
    public void setDefinition(MenuPositionDefinition definition) {
        this.definition = definition;
    }
    
    public List<MenuPosition> getChildPositions() {
        return childPositions;
    }
    
    /**
     * @see #getChildPositions
     */
    public void setChildPositions(List<MenuPosition> childPositions) {
        this.childPositions = childPositions;
    }
    
    public static MenuPosition findByDefinition(MenuPositionDefinition definition,
            Collection<MenuPosition> positions) {
        for(MenuPosition p : positions) {
            if(p.getDefinition().equals(definition)) {
                return p;
            }
        }
        return null;
    }

    /**
     * TODO
     */
    public Node getContent() {
        return content;
    }

    /**
     * @see #getContent
     */
    public void setContent(Node content) {
        this.content = content;
    }
}
