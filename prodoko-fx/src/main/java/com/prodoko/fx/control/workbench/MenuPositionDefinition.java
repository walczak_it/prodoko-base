package com.prodoko.fx.control.workbench;

import javafx.scene.Node;

public abstract class MenuPositionDefinition extends MenuNodeDefinition implements TabDefinition {

    public abstract MenuNodeDefinition getParent();
    public abstract Node createContent();

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((getParent() == null) ? 0 : getParent().hashCode());
        result = prime * result + super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MenuPositionDefinition other = (MenuPositionDefinition) obj;
        if (getParent() == null) {
            if (other.getParent() != null)
                return false;
        } else if (!getParent().equals(other.getParent()))
            return false;
        if (!getTitle().equals(other.getTitle()))
            return false;
        return true;
    }
}
