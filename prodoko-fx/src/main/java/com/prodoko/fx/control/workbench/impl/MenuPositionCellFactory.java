package com.prodoko.fx.control.workbench.impl;

import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.util.Callback;

import com.prodoko.fx.control.workbench.MenuPositionDefinition;

public class MenuPositionCellFactory
        implements Callback<TreeView<MenuPositionDefinition>, TreeCell<MenuPositionDefinition>> {

    @Override
    public MenuPositionCell call(TreeView<MenuPositionDefinition> view) {
        
        MenuPositionCell cell = new MenuPositionCell();
        return cell;
    }

}