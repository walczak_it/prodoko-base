package com.prodoko.fx.control;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;

public class ExpTextArea extends TextArea {

    public ExpTextArea() {
        super();
        init();
    }

    public ExpTextArea(String text) {
        super(text);
        init();
    }

    private void init() {
        setWrapText(true);
        setPrefRowCount(0);
        setPrefColumnCount(0);
        setPrefWidth(300);
        setMaxWidth(Double.MAX_VALUE);
        textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> arg0,
                    String arg1, String newValue) {
                adjustHeigthToText();
            }
        });
    }
    
    protected void adjustHeigthToText() {
        Text text = new Text(getText());
        text.setFont(getFont());
        text.setWrappingWidth(getWidth());
        double height = text.getLayoutBounds().getHeight();
        double newPrefHeight = height*1.06+10;
        if(newPrefHeight > getPrefHeight()) {
            setPrefHeight(newPrefHeight);
        }
        requestLayout();
    }
}
