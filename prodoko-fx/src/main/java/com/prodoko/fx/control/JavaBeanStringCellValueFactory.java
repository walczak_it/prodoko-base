package com.prodoko.fx.control;

import javafx.beans.property.adapter.JavaBeanStringProperty;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;

public class JavaBeanStringCellValueFactory<T>
        extends JavaBeanCellValueFactory<T, String> {

    public JavaBeanStringCellValueFactory(String propertyName) {
        super(propertyName);
    }
    
    @Override
    public JavaBeanStringProperty call(CellDataFeatures<T, String> features) {
        T bean = features.getValue().getValue();
        try {
            return JavaBeanStringPropertyBuilder.create().bean(bean)
                    .name(getPropertyName()).build();
        } catch (NoSuchMethodException ex) {
            throw new IllegalArgumentException("The value of class " + bean.getClass().getName()
                    + " in the given cell does not have valid getter/setter for property "
                    + getPropertyName(), ex);
        }
    }
}
