package com.prodoko.fx.control.workbench;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Labeled;
import javafx.scene.control.SkinBase;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import com.prodoko.fx.control.BreadcrumbsPane;
import com.prodoko.fx.control.BreadcrumbsPaneAware;
import com.prodoko.fx.control.workbench.impl.MenuPosition;
import com.prodoko.fx.control.workbench.impl.MenuPositionCell;
import com.prodoko.fx.control.workbench.impl.MenuPositionCellFactory;
import com.prodoko.fx.control.workbench.impl.MenuSection;
import com.prodoko.fx.image.LogoView;

public class WorkbenchSkin<T extends Workbench> extends SkinBase<T> {
    
    private final static double LEFT_PANE_PREF_WIDTH = 200;
    private final static double TAB_PANE_PREF_WIDTH = 1200;

    private MenuPositionCellFactory menuPositionCellFactory;
    private SplitPane splitPane;
    private AnchorPane leftPane;
    private LogoView logoView;
    private Accordion mainMenu;
    private TabPane tabPane;

    public WorkbenchSkin(T control) {
        super(control);

        buildMenuPositionCellFactory();
        buildSplitPane();
        buildLeftPane();
        buildTabPane();
        populateMainMenu();
    }
    
    protected void buildMenuPositionCellFactory() {
        menuPositionCellFactory = new MenuPositionCellFactory() {
            
            @Override
            public MenuPositionCell call(
                    TreeView<MenuPositionDefinition> view) {
                final MenuPositionCell cell = super.call(view);
                cell.setOnMouseClicked(new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        mainMenuPositionCliecked(cell.getItem(), event);
                    }
                });
                return cell;
            }
        };
    }
    
    protected void mainMenuPositionCliecked(MenuPositionDefinition positionDefinition,
            MouseEvent event) {
        if(positionDefinition == null) {
            return;
        }
        if(event.getButton() == MouseButton.PRIMARY) {
            Node node = positionDefinition.createContent();
            int index = getTabPane().getSelectionModel().getSelectedIndex();
            if(index == -1) {
                openNewTab(positionDefinition, node);
            } else {
                openInTab(getTabPane().getSelectionModel().getSelectedIndex(),
                        positionDefinition, node);
            }
        } else if(event.getButton() == MouseButton.MIDDLE) {
            Node node = positionDefinition.createContent();
            openNewTab(positionDefinition, node);
        }
    }
    
    protected void buildSplitPane() {
        splitPane = new SplitPane();
        getChildren().add(splitPane);
        AnchorPane.setTopAnchor(splitPane, 0d);
        AnchorPane.setRightAnchor(splitPane, 0d);
        AnchorPane.setBottomAnchor(splitPane, 0d);
        AnchorPane.setLeftAnchor(splitPane, 0d);
        splitPane.setDividerPositions(0.1d);
    }
    
    protected void buildLeftPane() {
        leftPane = new AnchorPane();
        leftPane.setPrefWidth(LEFT_PANE_PREF_WIDTH);
        getSplitPane().getItems().add(leftPane);
        
        if(getSkinnable().getLogo() != null) {
            logoView = new LogoView(getSkinnable().getLogo());
            AnchorPane.setLeftAnchor(logoView, 0d);
            AnchorPane.setRightAnchor(logoView, 0d);
            AnchorPane.setTopAnchor(logoView, 0d);
            leftPane.getChildren().add(logoView);
        }
        
        mainMenu = new Accordion();
        mainMenu.setPrefWidth(200);
        leftPane.setPrefWidth(200);
        leftPane.getChildren().add(mainMenu);
        SplitPane.setResizableWithParent(leftPane, false);
        AnchorPane.setLeftAnchor(mainMenu, 0d);
        AnchorPane.setRightAnchor(mainMenu, 0d);
        if(getLogoView() != null) {
            AnchorPane.setTopAnchor(mainMenu, LogoView.LOGO_VIEW_MAX_HEIGHT);
        } else {
            AnchorPane.setTopAnchor(mainMenu, 0d);
        }
    }
    
    protected void populateMainMenu() {
        for(MenuSection section : getSkinnable().getMenuStructure()) {
            addMainMenuSection(section);
        }
    }
    
    protected void buildTabPane() {
        tabPane = new TabPane();
        tabPane.setPrefWidth(TAB_PANE_PREF_WIDTH);
        tabPane.setMaxWidth(Double.MAX_VALUE);
        getSplitPane().getItems().add(tabPane);
    }

    /**
     * TODO
     */
    public MenuPositionCellFactory getMenuPositionCellFactory() {
        return menuPositionCellFactory;
    }

    /**
     * TODO
     */
    public SplitPane getSplitPane() {
        return splitPane;
    }

    /**
     * TODO
     */
    public AnchorPane getLeftPane() {
        return leftPane;
    }

    /**
     * TODO
     */
    public LogoView getLogoView() {
        return logoView;
    }

    /**
     * TODO
     */
    public Accordion getMainMenu() {
        return mainMenu;
    }

    /**
     * TODO
     */
    public TabPane getTabPane() {
        return tabPane;
    }

    public void addMainMenuSection(MenuSection section) {
        
        TitledPane sectionPane = createMainMenuScetionPane(section);
        getMainMenu().getPanes().add(sectionPane);
        
        TreeView<MenuPositionDefinition> positionsTree = createMainMenuSectionTree(
                getMenuPositionCellFactory());
        sectionPane.setContent(positionsTree);
        
        for(MenuPosition rootPosition : section.getRootPositions()) {
            TreeItem<MenuPositionDefinition> rootPositionItem = createMainMenuPosition(
                    rootPosition);
            positionsTree.getRoot().getChildren().add(rootPositionItem);
            createMenuChildPositions(rootPosition, rootPositionItem);
        }
    }
    
    protected TitledPane createMainMenuScetionPane(MenuSection section) {
        TitledPane sectionPane = new TitledPane();
        sectionPane.setText(section.getDefinition().getTitle());
        return sectionPane;
    }
    
    protected TreeView<MenuPositionDefinition> createMainMenuSectionTree(
            MenuPositionCellFactory factory) {
        TreeView<MenuPositionDefinition> positionsTree = new TreeView<>();
        positionsTree.setCellFactory(factory);
        TreeItem<MenuPositionDefinition> fakeRoot = new TreeItem<>();
        positionsTree.setRoot(fakeRoot);
        positionsTree.setShowRoot(false);
        return positionsTree;
    }
    
    protected Callback<TreeView<MenuPositionDefinition>, TreeCell<MenuPositionDefinition>>
            createMainMenuCellFactory(final EventHandler<MouseEvent> positionClieckedHandler) {
        return new Callback<TreeView<MenuPositionDefinition>,
                TreeCell<MenuPositionDefinition>>() {
            
            @Override
            public TreeCell<MenuPositionDefinition> call(
                    TreeView<MenuPositionDefinition> view) {
                MenuPositionCell cell = new MenuPositionCell();
                cell.setOnMouseClicked(positionClieckedHandler);
                return cell;
            }
        };
    }
    
    protected TreeItem<MenuPositionDefinition> createMainMenuPosition(MenuPosition position) {
        TreeItem<MenuPositionDefinition> positionItem = new TreeItem<MenuPositionDefinition>(
                position.getDefinition());
        return positionItem;
    }
    
    protected void createMenuChildPositions(MenuPosition position,
            TreeItem<MenuPositionDefinition> positionItem) {
        
        if(position.getChildPositions() == null) {
            return;
        }
        
        for(MenuPosition childPosition : position.getChildPositions()) {
            TreeItem<MenuPositionDefinition> childPositionItem = createMainMenuPosition(childPosition);
            positionItem.getChildren().add(childPositionItem);
            createMenuChildPositions(childPosition, childPositionItem);
        }
    }

    public Tab openInTab(int tabIndex, TabDefinition definition, Node content) {
        Tab tab = getTabPane().getTabs().get(tabIndex);
        tab.setText(definition.getTitle());
        initTabContent(tab, content);
        return tab;
    }

    public Tab openNewTab(TabDefinition definition, Node content) {
        Tab tab = new Tab(definition.getTitle());
        getTabPane().getTabs().add(tab);
        initTabContent(tab, content);
        return tab;
    }

    protected void initTabContent(Tab tab, Node content) {
        if(!(content instanceof Labeled && content instanceof BreadcrumbsPaneAware)) {
            tab.setContent(content);
        } else {
            Labeled labeledContent = (Labeled)content;
            BreadcrumbsPane breadcrumbsPane = new BreadcrumbsPane(labeledContent);
            tab.setContent(breadcrumbsPane);
        }
    }
}
