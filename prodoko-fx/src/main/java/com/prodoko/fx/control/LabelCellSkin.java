package com.prodoko.fx.control;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.SkinBase;
import javafx.scene.control.ToggleButton;

import org.controlsfx.control.SegmentedButton;

public class LabelCellSkin extends SkinBase<LabelCell<?>>{

    public LabelCellSkin(LabelCell<?> control) {
        super(control);
        build();
    }

    protected void build() {
        final ToggleButton labelButton = new ToggleButton();
        labelButton.textProperty().bind(getSkinnable().textProperty());
        labelButton.onActionProperty().set(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                if(getSkinnable().isEditable()) {
                    getSkinnable().startEdit();
                }
            }
        });
        final ToggleButton removeButton = new ToggleButton("X");
        removeButton.onActionProperty().set(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                getSkinnable().getView().getItems().remove(getSkinnable().getItem());
            }
        });
        SegmentedButton cellButton = new SegmentedButton(labelButton, removeButton);
        getChildren().add(cellButton);
    }
}
