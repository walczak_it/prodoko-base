package com.prodoko.fx.control;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.IndexedCell;
import javafx.scene.control.Skin;

public class LabelCell<T> extends IndexedCell<T> {
    
    private static final String DEFAULT_STYLE_CLASS = "labels-view";
    private LabelsView<T> view;
    
    public LabelCell(LabelsView<T> view) {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.view = view;
        itemProperty().addListener(new ChangeListener<T>() {

            @Override
            public void changed(ObservableValue<? extends T> observable, T oldValue,
                    T newValue) {
                updateItem(newValue, LabelCell.this.isEmpty());
            }
        });
    }
    
    /**
     * TODO
     */
    public LabelsView<T> getView() {
        return view;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new LabelCellSkin(this);
    }
}
