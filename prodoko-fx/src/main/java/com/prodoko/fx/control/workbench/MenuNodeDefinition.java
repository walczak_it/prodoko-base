package com.prodoko.fx.control.workbench;


public abstract class MenuNodeDefinition {
    
    public static final int BIG_PRIORITY_STEP = 10000;
    public static final int MEDIUM_PRIORITY_STEP = 100;

    /**
     * Must not be null or empty
     */
    public abstract String getTitle();
    
    public int getOrderPriority() {
        return Integer.MAX_VALUE;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getTitle().hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MenuNodeDefinition other = (MenuNodeDefinition) obj;
        if (getTitle() == null) {
            if (other.getTitle() != null)
                return false;
        } else if (!getTitle().equals(other.getTitle()))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "MenuNodeDefinition [title=" + getTitle() + "]";
    }
}
