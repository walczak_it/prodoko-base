package com.prodoko.fx.control.workbench;

public interface TabDefinition {

    public String getTitle();
}
