package com.prodoko.fx.control.form;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextInputControl;

import com.prodoko.fx.control.LabledControlPane;
import com.prodoko.fx.control.LookupField;
import com.prodoko.fx.control.form.ItemForm.SyncItemHandler;

public abstract class ItemFormSkin<C extends ItemForm<T>, T> extends FormSkin<C>{

    public ItemFormSkin(C form) {
        super(form);
        getSkinnable().itemProperty().addListener(new ChangeListener<T>() {

            @Override
            public void changed(ObservableValue<? extends T> observable,
                    T oldValue, T newValue) {
                itemChanged(oldValue);
            }
            
        });
    }

    protected void itemChanged(T oldValue) {
    }
    
    public void ensyncItemsPropertyTextControl(
            String propertyPath, TextInputControl control) {
        StringPropertySync handler = new StringPropertySync(
                propertyPath, control.textProperty());
        getSkinnable().getSyncItemHandlers().add(handler);
        handler.updateForm();
    }
    
    public <LT> void ensyncItemsPropertyLookupField(String propertyPath, LookupField<LT> field) {
        ObjectPropertySync<LT> handler = new ObjectPropertySync<LT>(
                propertyPath, field.valueProperty());
        getSkinnable().getSyncItemHandlers().add(handler);
        handler.updateForm();
    }
    
    public abstract class ItemPropertySync implements SyncItemHandler {
        
        private List<String> propertyPath;
        
        public ItemPropertySync(String propertyPath) {
            super();
            StringTokenizer st = new StringTokenizer(propertyPath, ".");
            this.propertyPath = new ArrayList<String>();
            while(st.hasMoreTokens()) {
                this.propertyPath.add(st.nextToken());
            }
        }

        public List<String> getPropertyPath() {
            return propertyPath;
        }
        
        protected Object getItemsPropertyParent()  {
            Object parent = getSkinnable().getItem();
            for(int i = 0; i < propertyPath.size()-1; ++i) {
                String propertyName = propertyPath.get(i);
                String getterName = "get" + Character.toUpperCase(propertyName.charAt(0))
                        + propertyName.substring(1);
                try {
                    Method getter = parent.getClass().getMethod(getterName);
                    Object moreDirectParent = getter.invoke(parent);
                    if(moreDirectParent == null) {
                        throw new IllegalStateException("Parent property " + propertyName
                                + " of class " + parent.getClass() + " was null");
                    }
                    parent = moreDirectParent;
                } catch (NoSuchMethodException | SecurityException | IllegalAccessException
                        | IllegalArgumentException | InvocationTargetException e) {
                    throw new IllegalStateException("Couldn't access getter " + getterName
                            + " of class " + parent.getClass().getName(), e);
                }
            }
            return parent;
        }

        protected void setItemsProperty(Object value) {
            
            Object parent = getItemsPropertyParent();
            String propertyName = propertyPath.get(propertyPath.size()-1);
            String setterName = "set" + Character.toUpperCase(propertyName.charAt(0))
                    + propertyName.substring(1);
            String getterName = "get" + Character.toUpperCase(propertyName.charAt(0))
                    + propertyName.substring(1);
            try {
                Method getter = parent.getClass().getMethod(getterName);
                Method setter = parent.getClass().getMethod(setterName, getter.getReturnType());
                setter.invoke(parent, value);
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException 
                    | IllegalArgumentException | InvocationTargetException e) {
                throw new IllegalStateException("Couldn't access setter " + setterName
                        + " for arguments type " + value.getClass().getName() 
                        + " of class " + parent.getClass().getName(), e);
            }
        }
        
        protected Object getItemsProperty() {
            Object parent = getItemsPropertyParent();
            String propertyName = propertyPath.get(propertyPath.size()-1);
            String getterName = "get" + Character.toUpperCase(propertyName.charAt(0))
                    + propertyName.substring(1);
            try {
                Method getter = parent.getClass().getMethod(getterName);
                Object value = getter.invoke(parent);
                return value;
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                throw new IllegalStateException("Couldn't access getter " + getterName
                        + " of class " + parent.getClass().getName(), e);
            }
        }
    }
    
    public class StringPropertySync extends ItemPropertySync {
        
        private StringProperty source;

        public StringPropertySync(String propertyPath,
                StringProperty source) {
            super(propertyPath);
            this.source = source;
        }
        
        @Override
        public void updateItem() {
            setItemsProperty(source.get());
        }
        
        @Override
        public void updateForm() {
            String value = (String)getItemsProperty();
            if(value == null) {
                // remove this after https://javafx-jira.kenai.com/browse/RT-34959
                value = "";
            }
            source.set(value);
        }
    }
    
    public class ObjectPropertySync<ST> extends ItemPropertySync {
        
        private ObjectProperty<ST> source;

        public ObjectPropertySync(String propertyPath,
                ObjectProperty<ST> source) {
            super(propertyPath);
            this.source = source;
        }

        @Override
        public void updateItem() {
            setItemsProperty(source.get());
        }
        
        @SuppressWarnings("unchecked")
        @Override
        public void updateForm() {
            source.set((ST)getItemsProperty());
        }
        
    }
}
