package com.prodoko.fx.control;

public interface BreadcrumbsPaneAware {

    public void setBreadcrumbsContext(BreadcrumbsPane pane, int yourIndex);
    public BreadcrumbsPane getBreadcrumbsPane();
    public int getBreadcrumbsIndex();
}
