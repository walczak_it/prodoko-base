package com.prodoko.fx.control.form;

import javafx.event.Event;
import javafx.event.EventType;

public class ValidateEvent extends Event {
    
    private static final long serialVersionUID = 1L;
    
    public ValidateEvent() {
        super(new EventType<ValidateEvent>("VALIDATE"));
    }
}
