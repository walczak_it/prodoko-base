package com.prodoko.fx.control.workbench.impl;

import java.util.ArrayList;
import java.util.List;

import com.prodoko.fx.control.workbench.MenuSectionDefinition;

public class MenuSection extends MenuNode {
    
    private MenuSectionDefinition definition;
    private List<MenuPosition> rootPositions = new ArrayList<>();

    @Override
    public MenuSectionDefinition getDefinition() {
        return definition;
    }

    public void setDefinition(MenuSectionDefinition definition) {
        this.definition = definition;
    }

    /**
     * TODO
     */
    public List<MenuPosition> getRootPositions() {
        return rootPositions;
    }

    /**
     * @see #setRootPositions
     */
    public void setRootPositions(List<MenuPosition> rootPositions) {
        this.rootPositions = rootPositions;
    }
}
