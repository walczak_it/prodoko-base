package com.prodoko.fx.control;

import javafx.beans.binding.ListExpression;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Labeled;
import javafx.scene.control.Skin;

public class BreadcrumbsPane extends Control {
    
    private static final String DEFAULT_STYLE_CLASS = "breadcrumbs-pane";

    private ListProperty<Labeled> items;
    private ObjectProperty<Labeled> displayedItem;

    public BreadcrumbsPane(Labeled... items) {
        super();
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.items = new SimpleListProperty<>(FXCollections.observableArrayList(items));
        autowireAwareItems(items);
        this.displayedItem = new SimpleObjectProperty<Labeled>(items[items.length-1]);
    }
    
    public ListExpression<Labeled> itemsProperty() {
        return items;
    }
    
    public ObservableList<Labeled> getItems() {
        return items.get();
    }
    
    public ObjectProperty<Labeled> displayedItemProperty() {
        return displayedItem;
    }
    
    public Labeled getDisplayedItem() {
        return displayedItem.get();
    }
    
    public void setDisplayedItem(Labeled value) {
        displayedItem.set(value);
    }
    
    public void closeAllAfter(int index) {
        closeAllFrom(index+1);
    }
    
    public void closeAllFrom(int index) {
        if(index > items.getSize() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        if(index == items.size()) {
            return;
        }
        items.remove(index, items.getSize());
        if(getDisplayedItem() == null || !items.contains(getDisplayedItem())) {
            setDisplayedItem(items.get(items.getSize()-1));
        }
    }
    
    public void openAfter(int index, Labeled newItem) {
        closeAllAfter(index);
        items.add(newItem);
        displayedItem.set(newItem);
        autowireAwareItems(newItem);
    }
    
    protected void autowireAwareItems(Labeled... items) {
        for(int i = 0; i < items.length; ++i) {
            Labeled item = items[i];
            if(item instanceof BreadcrumbsPaneAware) {
                BreadcrumbsPaneAware awareItem = (BreadcrumbsPaneAware)item;
                awareItem.setBreadcrumbsContext(this, i);
            }
        }
    }
    
    @Override
    protected Skin<?> createDefaultSkin() {
        return new BreadcrumbsPaneSkin(this);
    }
}
