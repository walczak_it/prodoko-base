package com.prodoko.fx.control.form;

import javafx.scene.Node;
import javafx.scene.layout.GridPane;

public class FieldsetGrid extends GridPane {
    
    private static final String DEFAULT_STYLE_CLASS = "fieldset-grid";

    public FieldsetGrid() {
        super();
        getStyleClass().addAll(DEFAULT_STYLE_CLASS, FormConsts.FIELDS_PANE_CLASS);
    }
    
    @Override
    public void addRow(int rowIndex, Node... children) {
        int columnIndex = 0;
        for(Node child : children) {
            if(child != null) {
                add(child, columnIndex, rowIndex);
            }
            ++columnIndex;
        }
    }
}
