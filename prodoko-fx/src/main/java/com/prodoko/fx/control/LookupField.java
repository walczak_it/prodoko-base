package com.prodoko.fx.control;

import java.util.List;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.scene.control.Control;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Skin;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LookupField<T> extends Control {
    
    private static final Log LOG = LogFactory.getLog(LookupField.class);

    private final static String DEFAULT_STYLE_CLASS = "lookup-field";
    
    private StringProperty text;
    private ObjectProperty<StringConverter<T>> converter;
    private ObjectProperty<T> value;
    private ListProperty<T> items;
    private ObjectProperty<Callback<ListView<T>,ListCell<T>>> cellFactory;
    private Service<List<T>> lazyItemsLoader;
    
    public LookupField() {
        this(null);
    }

    public LookupField(final Service<List<T>> lazyItemsLoader) {
        super();
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        setPrefWidth(300);
        this.text = new SimpleStringProperty();
        this.converter = new SimpleObjectProperty<>();
        this.value = new SimpleObjectProperty<>();
        this.items = new SimpleListProperty<>(FXCollections.<T>observableArrayList());
        this.cellFactory = new SimpleObjectProperty<>();
        this.lazyItemsLoader = lazyItemsLoader;
        text.addListener(createTextChangedListener());
    }
    
    protected ChangeListener<? super String> createTextChangedListener() {
        return new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
                    LOG.info("Restarting lazy items loader");
                    lazyItemsLoader.restart();
                }
        };
    }

    public StringProperty textProperty() {
        return text;
    }
    
    public String getText() {
        return text.getValue();
    }
    
    public void setText(String value) {
        text.set(value);
    }
    
    public ObjectProperty<StringConverter<T>> converterProperty() {
        return converter;
    }
    
    public StringConverter<T> getConverter() {
        return converter.getValue();
    }
    
    public void setConverter(StringConverter<T> value) {
        converter.setValue(value);
    }
    
    public ObjectProperty<T> valueProperty() {
        return value;
    }
    
    /**
     * TODO
     */
    public T getValue() {
        return value.get();
    }

    /**
     * @see #getValue
     */
    public void setValue(T value) {
        this.value.set(value);
    }

    public ListProperty<T> itemsProperty() {
        return items;
    }
    
    public ObservableList<T> getItems() {
        return items.getValue();
    }
    
    public void setItems(ObservableList<T> value) {
        items.setValue(items);
    }
    
    public ObjectProperty<Callback<ListView<T>,ListCell<T>>> cellFactoryProperty() {
        return cellFactory;
    }
    
    public Callback<ListView<T>,ListCell<T>> getCellFactory() {
        return cellFactory.getValue();
    }
    
    public void setCellFactory(Callback<ListView<T>,ListCell<T>> value) {
        cellFactory.setValue(value);
    }
    
    /**
     * TODO
     */
    public Service<List<T>> getLazyItemsLoader() {
        return lazyItemsLoader;
    }

    /**
     * @see #getLazyItemsLoader
     */
    public void setLazyItemsLoader(Service<List<T>> lazyItemsLoader) {
        this.lazyItemsLoader = lazyItemsLoader;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new LookupFieldSkin<>(this);
    }
}
