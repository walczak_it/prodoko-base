package com.prodoko.fx.control.form;

public class TabFormSkin<T extends Form> extends FormSkin<T> {
    
    private FieldsTabPane fieldsTabPane;

    public TabFormSkin(T form) {
        super(form);
        buildFieldsTabPane();
    }

    protected void buildFieldsTabPane() {
        fieldsTabPane = new FieldsTabPane();
        getFieldsPane().setContent(fieldsTabPane);
    }

    /**
     * TODO
     */
    protected FieldsTabPane getFieldsTabPane() {
        return fieldsTabPane;
    }
}
