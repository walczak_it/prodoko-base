package com.prodoko.fx.control.workbench.impl;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.util.Callback;

public class DefaultEmptyTabContentFactory implements Callback<Void, Node> {

    @Override
    public Node call(Void arg0) {
        BorderPane pane = new BorderPane();
        Text text = new Text("Select an position in the main menu");
        pane.getChildren().add(text);
        return pane;
    }

}
