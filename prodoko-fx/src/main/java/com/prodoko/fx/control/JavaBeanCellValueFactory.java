package com.prodoko.fx.control;

import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.util.Callback;

public abstract class JavaBeanCellValueFactory<T, P>
        implements Callback<CellDataFeatures<T, P>, ObservableValue<P>> {

    private String propertyName;

    protected JavaBeanCellValueFactory(String propertyName) {
        super();
        this.propertyName = propertyName;
    }

    /**
     * TODO
     */
    public String getPropertyName() {
        return propertyName;
    }
    
    @Override
    public abstract JavaBeanProperty<P> call(CellDataFeatures<T, P> features);
}
