package com.prodoko.fx.control.form;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javafx.beans.binding.ObjectExpression;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.event.ActionEvent;

import com.prodoko.fx.action.ServiceAction;
import com.prodoko.fx.validation.ValidationResult;

public abstract class ItemForm<T> extends Form {

    private static final String DEFAULT_STYLE_CLASS = "item-form";

    private ObjectProperty<T> item;
    private BooleanProperty readOnly;
    private ObservableList<SyncItemHandler> syncItemHandlers;
    
    public ItemForm() {
        super();
        build(null);
    }

    public ItemForm(String text) {
        super(text);
        build(null);
    }

    public ItemForm(T item) {
        super();
        build(item);
    }

    public ItemForm(T item, String text) {
        super(text);
        build(item);
    }
    
    protected void build(T item) {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.item = new SimpleObjectProperty<>(item);
        this.item.addListener(new ChangeListener<T>() {

            @Override
            public void changed(ObservableValue<? extends T> observable,
                    T oldValue, T newValue) {
                itemChanged(oldValue);
            }
        });
        this.readOnly = new SimpleBooleanProperty(false);
        this.syncItemHandlers = FXCollections.observableArrayList();
    }
    
    protected void itemChanged(T oldValue) {
        
        for(SyncItemHandler handler : syncItemHandlers) {
            handler.updateForm();
        }
    }
    
    public boolean isItemSyncNeeded() {
        return syncItemHandlers.size() > 0;
    }
    
    public void updateItem() {

        for(SyncItemHandler handler : syncItemHandlers) {
            handler.updateItem();
        }
    }
    
    public ObjectProperty<T> itemProperty() {
        return item;
    }
    
    public T getItem() {
        return item.get();
    }
    
    public void setItem(T value) {
        item.set(value);
    }

    public BooleanProperty readOnlyProperty() {
        return readOnly;
    }
    
    public boolean isReadOnly() {
        return readOnly.get();
    }
    
    public void setReadOnly(boolean value) {
        readOnly.set(value);
    }
    
    /**
     * TODO
     */
    public ObservableList<SyncItemHandler> getSyncItemHandlers() {
        return syncItemHandlers;
    }
    
    protected class UpdatedItemServiceAction extends ServiceAction {

        public UpdatedItemServiceAction(Service<?> service, String text) {
            super(service, text);
        }

        public UpdatedItemServiceAction(Service<?> service) {
            super(service);
        }

        public UpdatedItemServiceAction(String text) {
            super(text);
        }
        
        @Override
        public void execute(ActionEvent ae) {
            if(validate() != ValidationResult.ERROR) {
                updateItem();
                super.execute(ae);
            }
        }
    }
    
    public static interface SyncItemHandler {
        
        public void updateItem();
        public void updateForm();
    }
}
