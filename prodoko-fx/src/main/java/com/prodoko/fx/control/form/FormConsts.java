package com.prodoko.fx.control.form;

public class FormConsts {

    public static final String FIELDS_PANE_CLASS = "fields-pane";
    public static final double MAX_FIELDS_ROW_HEIGHT = 60;
    public static final double FIELDS_PADDING = 10;
}
