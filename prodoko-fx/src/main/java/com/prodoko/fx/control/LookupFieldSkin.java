package com.prodoko.fx.control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SkinBase;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class LookupFieldSkin<T> extends SkinBase<LookupField<T>> {
    
    private static final Log LOG = LogFactory.getLog(LookupFieldSkin.class);
    
    private HBox pane;
    private ComboBox<T> comboBox;
    private ToolBar toolBar;

    public LookupFieldSkin(LookupField<T> control) {
        super(control);
        
        this.pane = new HBox(5d);
        this.pane.setAlignment(Pos.CENTER_LEFT);
        getChildren().add(pane);
        
        this.comboBox = new ComboBox<>();
        comboBox.cellFactoryProperty().bindBidirectional(getSkinnable().cellFactoryProperty());
        comboBox.converterProperty().bindBidirectional(getSkinnable().converterProperty());
        comboBox.setEditable(true);
        getSkinnable().textProperty().bindBidirectional(comboBox.getEditor().textProperty());
        comboBox.valueProperty().bindBidirectional(getSkinnable().valueProperty());
        comboBox.getEditor().focusedProperty().and(getSkinnable().textProperty().isNotEmpty())
                .addListener(new ChangeListener<Boolean>() {

                    @Override
                    public void changed(
                            ObservableValue<? extends Boolean> observable,
                            Boolean oldValue, Boolean newValue) {
                        if(newValue) {
                            LOG.info("Showing popup");
                            comboBox.show();
                        } else {
                            LOG.info("Hiding popup");
                            comboBox.hide();
                        }
                    }
                });
        comboBox.itemsProperty().bind(getSkinnable().itemsProperty());
        comboBox.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(comboBox, Priority.ALWAYS);
        HBox.setHgrow(comboBox.getEditor(), Priority.ALWAYS);
        pane.getChildren().add(comboBox);
    }

}
