package com.prodoko.fx.control;

import javafx.geometry.Pos;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class LabledControlPane<C extends Control> extends HBox {

    private Label label;
    private C field;
    
    public LabledControlPane(Label label, C field) {
        super(5d, label, field);
        this.label = label;
        this.field = field;
        HBox.setHgrow(field, Priority.ALWAYS);
        setAlignment(Pos.CENTER_LEFT);
    }

    /**
     * TODO
     */
    public Label getLabel() {
        return label;
    }

    /**
     * TODO
     */
    public C getField() {
        return field;
    }
}
