package com.prodoko.fx.control;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.FlowPane;
import javafx.util.Callback;

public class LabelsViewSkin<T extends LabelsView<E>, E> extends SkinBase<T> {
    
    private FlowPane pane;

    public LabelsViewSkin(T control) {
        super(control);
        
        buildPane();
        populatePane();
        
        getSkinnable().itemsProperty().get().addListener(new ListChangeListener<E>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends E> change) {
                itemsChanged(change);
            }
            
        });
        
        getSkinnable().afterLastItemProperty().addListener(new ChangeListener<Node>() {

            @Override
            public void changed(ObservableValue<? extends Node> observable,
                    Node oldValue, Node newValue) {
                afterLastItemChanged(oldValue, newValue);
            }
        });
    }

    protected void buildPane() {
        pane = new FlowPane();
        pane.setPadding(new Insets(10));
        pane.setHgap(10d);
        pane.setVgap(10d);
        getChildren().add(pane);
    }
    
    protected void populatePane() {
        pane.getChildren().clear();
        ObservableList<E> items = getSkinnable().getItems();
        for(int i = 0; i < items.size(); ++i) {
            Node cellNode = createCell(items.get(i), i);
            pane.getChildren().add(cellNode);
        }
        if(getSkinnable().getAfterLastItem() != null) {
            pane.getChildren().add(getSkinnable().getAfterLastItem());
        }
    }
    
    protected Node createCell(final E item, int index) {
        Callback<LabelsView<E>, LabelCell<E>> cellFactory = getSkinnable().getCellFactory();
        if(cellFactory == null) {
            cellFactory = createDefaultCellFactory();
            getSkinnable().setCellFactory(cellFactory);
        }
        final LabelCell<E> cell = cellFactory.call(getSkinnable());
        cell.setItem(item);
        cell.updateIndex(index);
        return cell;
    }
    
    protected Callback<LabelsView<E>, LabelCell<E>> createDefaultCellFactory() {
        return new Callback<LabelsView<E>, LabelCell<E>>() {

            @Override
            public LabelCell<E> call(LabelsView<E> view) {
                LabelCell<E> cell = new LabelCell<>(view);
                return cell;
            }
            
        };
    }
    
    protected void itemsChanged(ListChangeListener.Change<? extends E> change) {
        populatePane();
    }
    
    protected void afterLastItemChanged(Node oldValue, Node newValue) {
        if(oldValue != null) {
            getPane().getChildren().remove(oldValue);
        }
        if(newValue != null) {
            getPane().getChildren().add(newValue);
        }
    }

    /**
     * TODO
     */
    public FlowPane getPane() {
        return pane;
    }
}
