package com.prodoko.fx.control.form;

import java.util.Collection;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.scene.control.SkinBase;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import org.controlsfx.control.NotificationPane;
import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.control.action.ActionUtils.ActionTextBehavior;

import com.prodoko.fx.action.ServiceAction;
import com.prodoko.fx.control.form.Form.ActionsPresentation;
import com.prodoko.fx.validation.FieldsValidationDecorator;
import com.prodoko.fx.validation.ValidationNotifier;
import com.prodoko.fx.validation.ValidationPresenter;
import com.prodoko.fx.validation.ValidationResult;

public class FormSkin<T extends Form> extends SkinBase<T> {
    
    public static final double TOOL_BAR_MAX_HEIGHT = 40;
    
    private AnchorPane mainPane;
    private NotificationPane fieldsPane;
    private ToolBar actionsBar;
    private ValidationPresenter validationPresenter;

    protected FormSkin(T form) {
        super(form);
        buildMainPane();
        buildFieldsPane();
        buildValidationPresneter();
        enableFormWorkerPresentersForServiceActions(getSkinnable().getActions());
        replaceActionsBar();
        getSkinnable().actionsProperty().addListener(createActionsChangeListener());
        buildActionChangeListeners();
        buildValidationHandlers();
    }
    
    protected ListChangeListener<Action> createActionsChangeListener() {
        return new ListChangeListener<Action>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends Action> change) {
                if(change.wasAdded()) {
                    enableFormWorkerPresentersForServiceActions(change.getAddedSubList());
                    for(Action a : change.getAddedSubList()) {
                        if(a instanceof ServiceAction) {
                            enableFormWorkerPresenter(((ServiceAction) a).getService());
                        }
                    }
                }
                //TODO: other changes
                replaceActionsBar();
            }
            
        };
    }
    
    protected void buildMainPane() {
        mainPane = new AnchorPane();
        getChildren().add(mainPane);
    }
    
    protected void buildFieldsPane() {
        fieldsPane = new NotificationPane();
        AnchorPane.setTopAnchor(fieldsPane, 0d);
        AnchorPane.setLeftAnchor(fieldsPane, 0d);
        AnchorPane.setRightAnchor(fieldsPane, 0d);
        AnchorPane.setBottomAnchor(fieldsPane, TOOL_BAR_MAX_HEIGHT);
        fieldsPane.setShowFromTop(false);
        fieldsPane.getStyleClass().add("form-fields-pane");
        getMainPane().getChildren().add(fieldsPane);
    }
    
    protected void buildValidationPresneter() {
        this.validationPresenter = new ValidationPresenter(
                FieldsValidationDecorator.newDecoratingParentsToTreshold(getSkinnable()),
                new ValidationNotifier(getFieldsPane()));
    }
    
    protected void buildActionChangeListeners() {
        getSkinnable().actionsProperty().addListener(new ChangeListener<ObservableList<Action>>() {

            @Override
            public void changed(
                    ObservableValue<? extends ObservableList<Action>> observable,
                    ObservableList<Action> oldValue, ObservableList<Action> newValue) {
                removeActionsPresentation(getSkinnable().getActionsPresentation());
                replaceActionsBar();
            }
        });
        getSkinnable().actionsProperty().addListener(new ListChangeListener<Action>() {

            @Override
            public void onChanged(
                    javafx.collections.ListChangeListener.Change<? extends Action> changes) {
                removeActionsPresentation(getSkinnable().getActionsPresentation());
                replaceActionsBar();
            }
            
        });
        getSkinnable().actionsPresentationProperty().addListener(
                new ChangeListener<Form.ActionsPresentation>() {

                    @Override
                    public void changed(
                            ObservableValue<? extends ActionsPresentation> observable,
                            ActionsPresentation oldValue, ActionsPresentation newValue) {
                        removeActionsPresentation(oldValue);
                        removeActionsPresentation(newValue);
                    }
        });
    }
    
    protected void replaceActionsBar() {
        if(actionsBar != null) {
            getMainPane().getChildren().remove(actionsBar);
        }
        if(getSkinnable().getActionsPresentation() == ActionsPresentation.TOOL_BAR &&
                !getSkinnable().getActions().isEmpty()) {
            actionsBar = createActionsBar();
            getMainPane().getChildren().add(actionsBar);
        }
    }
    
    protected ToolBar createActionsBar() {
        ToolBar newActionsBar = ActionUtils.createToolBar(getSkinnable().getActions(),
                ActionTextBehavior.SHOW);
        newActionsBar.getStyleClass().add("form-actions-bar");
        AnchorPane.setLeftAnchor(newActionsBar, 0d);
        AnchorPane.setRightAnchor(newActionsBar, 0d);
        AnchorPane.setBottomAnchor(newActionsBar, 0d);
        return newActionsBar;
    }
    
    protected void enableFormWorkerPresentersForServiceActions(
            Collection<? extends Action> actions) {
        for(Action a : actions) {
            if(a instanceof ServiceAction) {
                enableFormWorkerPresenter(((ServiceAction) a).getService());
            }
        }
    }
    
    public void enableFormWorkerPresenter(Worker<?> worker) {
        worker.stateProperty().addListener(new FormWorkerPresenter(this, worker));
    }
    
    public void removeActionsPresentation(Form.ActionsPresentation presentation) {
        switch(getSkinnable().getActionsPresentation()) {
        case EXTERNAL_OR_NONE:
            break;
        case TOOL_BAR:
            getMainPane().getChildren().remove(actionsBar);
            break;
        }
    }
    
    protected void buildValidationHandlers() {
        getSkinnable().getValidationHandlers().add(createValidationClearingHandler());
    }
    
    public Callback<Void, ValidationResult> createValidationClearingHandler() {
        return new Callback<Void, ValidationResult>() {

            @Override
            public ValidationResult call(Void arg0) {
                getValidationPresenter().clear();
                return null;
            }
            
        };
    }
    
    /**
     * TODO
     */
    public AnchorPane getMainPane() {
        return mainPane;
    }

    public NotificationPane getFieldsPane() {
        return fieldsPane;
    }

    /**
     * TODO
     */
    public ValidationPresenter getValidationPresenter() {
        return validationPresenter;
    }

    public ToolBar getActionsBar() {
        return actionsBar;
    }

    public void block() {
        getFieldsPane().setDisable(true);
        getActionsBar().setDisable(true);
    }
    
    public void unblock() {
        getFieldsPane().setDisable(false);
        getActionsBar().setDisable(false);
    }
    
    public void blockingTaskStarted() {
        getFieldsPane().setText("Przetwarzanie ...");
        ImageView g = new ImageView("com/prodoko/graphics/icons/refresh.png");
        g.fitHeightProperty().set(27);
        g.preserveRatioProperty().set(true);
        getFieldsPane().setGraphic(g);
        getFieldsPane().show();
        block();
    }
    
    public void blockingTaskEnded() {
        getFieldsPane().hide();
        getFieldsPane().setText(null);
        getFieldsPane().setGraphic(null);
        unblock();
    }
}
