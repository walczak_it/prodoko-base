package com.prodoko.fx.control.workbench;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.image.Image;
import javafx.util.Callback;

import com.prodoko.fx.control.workbench.impl.DefaultEmptyTabContentFactory;
import com.prodoko.fx.control.workbench.impl.MenuPosition;
import com.prodoko.fx.control.workbench.impl.MenuSection;
import com.prodoko.fx.control.workbench.impl.MenuSiblingOrderComparator;

public class Workbench extends Control {

    private Image logo;
    private Collection<MenuPositionDefinition> positionDefinitions;
    private List<MenuSection> menuStructure;
    private Callback<Void, Node> emptyTabContentFactory;

    public Workbench(Collection<MenuPositionDefinition> positionDefinitions) {
        this(positionDefinitions, (Image)null);
    }
    
    public Workbench(Collection<MenuPositionDefinition> positionDefinitions,
            String logoPath) {
        this(positionDefinitions, new Image(logoPath), new DefaultEmptyTabContentFactory());
    }
    
    public Workbench(Collection<MenuPositionDefinition> positionDefinitions,
            Image logo) {
        this(positionDefinitions, logo, new DefaultEmptyTabContentFactory());
    }
    
    public Workbench(Collection<MenuPositionDefinition> positionDefinitions,
            Image logo, Callback<Void, Node> emptyTabContentFactory) {
        super();
        this.logo = logo;
        this.positionDefinitions = positionDefinitions;
        this.emptyTabContentFactory = emptyTabContentFactory;
        buildMenuStructure();
    }

    /**
     * TODO
     */
    public Image getLogo() {
        return logo;
    }

    /**
     * TODO
     */
    public Collection<MenuPositionDefinition> getPositionDefinitions() {
        return positionDefinitions;
    }
    
    /**
     * TODO
     */
    public List<MenuSection> getMenuStructure() {
        return menuStructure;
    }

    /**
     * TODO
     */
    public Callback<Void, Node> getEmptyTabContentFactory() {
        return emptyTabContentFactory;
    }

    /**
     * @see #getEmptyTabContentFactory
     */
    public void setEmptyTabContentFactory(
            Callback<Void, Node> emptyTabContentFactory) {
        this.emptyTabContentFactory = emptyTabContentFactory;
    }
    
    public void openInTab(int tabIndex, TabDefinition definition, Node content) {
        @SuppressWarnings("unchecked")
        WorkbenchSkin<? extends Workbench> skin = (WorkbenchSkin<? extends Workbench>) getSkin();
        skin.openInTab(tabIndex, definition, content);
    }

    public void openNewTab(TabDefinition definition, Node content) {
        @SuppressWarnings("unchecked")
        WorkbenchSkin<? extends Workbench> skin = (WorkbenchSkin<? extends Workbench>) getSkin();
        skin.openNewTab(definition, content);
    }

    protected void buildMenuStructure() {

        Map<MenuPositionDefinition, MenuPosition> positionsByDefinition = extractAllMenuPositions();
        Map<MenuSectionDefinition, MenuSection> sectionsByDefiniton = new HashMap<>();
        menuStructure = new ArrayList<>();
        for(MenuPositionDefinition positionDefinition : positionsByDefinition.keySet()) {
            
            MenuPosition position = positionsByDefinition.get(positionDefinition);
            MenuNodeDefinition parentDefinition = position.getDefinition().getParent();
            if(parentDefinition instanceof MenuSectionDefinition) {
                MenuSection parentSection;
                if(sectionsByDefiniton.containsKey(parentDefinition)) {
                    parentSection = sectionsByDefiniton.get(parentDefinition);
                } else {
                    parentSection = new MenuSection();
                    parentSection.setDefinition((MenuSectionDefinition) parentDefinition);
                    sectionsByDefiniton.put(parentSection.getDefinition(), parentSection);
                    menuStructure.add(parentSection);
                }
                parentSection.getRootPositions().add(position);
            } else if(parentDefinition instanceof MenuPositionDefinition) {
                if(positionsByDefinition.containsKey(parentDefinition)) {
                    MenuPosition parentPosition = positionsByDefinition.get(parentDefinition);
                    parentPosition.getChildPositions().add(position);
                } else {
                    throw new IllegalStateException("Could not find parent position with title '" 
                            + parentDefinition.getTitle() + "'");
                }
            } else {
                throw new IllegalStateException("Unsupported subclass of "
                        + MenuNodeDefinition.class.getSimpleName() + " named " + parentDefinition
                        + " as position parent");
            }
        }
        
        orderMenuStructure(menuStructure);
    }
    
    protected void orderMenuStructure(List<MenuSection> menuStructure) {
        Collections.sort(menuStructure, new MenuSiblingOrderComparator<MenuSection>());
        for(MenuSection section : menuStructure) {
            List<MenuPosition> rootPositions = section.getRootPositions();
            Collections.sort(rootPositions, new MenuSiblingOrderComparator<MenuPosition>());
            for(MenuPosition rootPosition : rootPositions) {
                orderMenuPositions(rootPosition);
            }
        }
    }
    
    protected void orderMenuPositions(MenuPosition parentPosition) {
        List<MenuPosition> childPositions = parentPosition.getChildPositions();
        if(childPositions.isEmpty()) {
            return;
        }
        Collections.sort(childPositions, new MenuSiblingOrderComparator<MenuPosition>());
        for(MenuPosition childPosition : childPositions) {
            orderMenuPositions(childPosition);
        }
    }
    
    protected Map<MenuPositionDefinition, MenuPosition> extractAllMenuPositions() {
        
        Map<MenuPositionDefinition, MenuPosition> allPositions = new HashMap<>();
        
        for(MenuPositionDefinition positionDefinition : getPositionDefinitions()) {
            
            MenuPosition position = new MenuPosition();
            position.setDefinition(positionDefinition);
            allPositions.put(positionDefinition, position);
            
            MenuNodeDefinition parentDefinition = positionDefinition.getParent();
            while(parentDefinition != null && parentDefinition instanceof MenuPositionDefinition
                    && !allPositions.containsKey((MenuPositionDefinition)parentDefinition)) {
                
                MenuPositionDefinition ppd = (MenuPositionDefinition)parentDefinition;
                MenuPosition parentPosition = new MenuPosition();
                parentPosition.setDefinition(ppd);
                allPositions.put(ppd, parentPosition);
            }
        }
        
        return allPositions;
    }
    
    @Override
    protected Skin<?> createDefaultSkin() {
        return new WorkbenchSkin<Workbench>(this);
    }
}
