package com.prodoko.fx.control.form;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.control.Labeled;
import javafx.util.Callback;

import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;

import com.prodoko.fx.control.BreadcrumbsPane;
import com.prodoko.fx.control.BreadcrumbsPaneAware;
import com.prodoko.fx.validation.ValidationResult;

public abstract class Form extends Labeled implements BreadcrumbsPaneAware {
    
    private static final String DEFAULT_STYLE_CLASS = "form";
    
    public static final EventType<Event> CLOSE_FORM_EVENT = new EventType<>("CLOSE_FORM_EVENT");
    
    private BooleanProperty readOnly;
    private final ListProperty<Action> actions;
    private final ObjectProperty<ActionsPresentation> actionsPresentation;
    private final ObservableList<Callback<Void, ValidationResult>> validationHandlers;
    private ObjectProperty<Object> dialogOwner;
    private Dialog showingDialog = null;
    private BreadcrumbsPane breadcrumbsPane = null;
    private int breadcrumbsPaneIndex;
    private EventHandler<Event> onClose;
    
    public static enum ActionsPresentation {
        TOOL_BAR, EXTERNAL_OR_NONE
    }
    
    public Form() {
        this(null);
    }
    
    public Form(String text) {
        super(text);
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.readOnly = new SimpleBooleanProperty();
        this.actions = new SimpleListProperty<Action>(FXCollections.<Action>observableArrayList());
        this.actionsPresentation = new SimpleObjectProperty<>(ActionsPresentation.TOOL_BAR);
        this.validationHandlers
                = FXCollections.<Callback<Void, ValidationResult>>observableArrayList();
        this.dialogOwner = new SimpleObjectProperty<>();
    }
    
    public BooleanProperty readOnlyProperty() {
        return readOnly;
    }
    
    public boolean isReadOnly() {
        return readOnly.get();
    }
    
    public void setReadOnly(boolean value) {
        readOnly.set(value);
    }

    public ListProperty<Action> actionsProperty() {
        return actions;
    }
    
    public ObservableList<Action> getActions() {
        return actions.get();
    }
    
    public void setActions(ObservableList<Action> value) {
        actions.set(value);
    }
    
    public ObjectProperty<ActionsPresentation> actionsPresentationProperty() {
        return actionsPresentation;
    }
    
    public ActionsPresentation getActionsPresentation() {
        return actionsPresentation.get();
    }
    
    public void setActionsPresentation(ActionsPresentation value) {
        actionsPresentation.set(value);
    }
    
    public ObservableList<Callback<Void, ValidationResult>> getValidationHandlers() {
        return validationHandlers;
    }
    
    public void close() {
        if(onClose != null) {
            onClose.handle(new Event(CLOSE_FORM_EVENT));
        }
        if(showingDialog != null) {
            showingDialog.hide();
        }
        if(breadcrumbsPane != null) {
            breadcrumbsPane.closeAllFrom(breadcrumbsPaneIndex);
        }
    }
    
    /**
     * @see #setBreadcrumbsPane(BreadcrumbsPane)
     */
    public BreadcrumbsPane getBreadcrumbsPane() {
        return breadcrumbsPane;
    }

    @Override
    public void setBreadcrumbsContext(BreadcrumbsPane pane, int yourIndex) {
        this.breadcrumbsPane = pane;
        this.breadcrumbsPaneIndex = yourIndex;
    }
    
    /**
     * TODO
     */
    public int getBreadcrumbsIndex() {
        return breadcrumbsPaneIndex;
    }

    /**
     * TODO
     */
    public EventHandler<Event> getOnClose() {
        return onClose;
    }

    /**
     * @see #getOnClose
     */
    public void setOnClose(EventHandler<Event> onClose) {
        this.onClose = onClose;
    }

    public ObjectProperty<Object> dialogOwnerProperty() {
        return dialogOwner;
    }
    
    public void setDialogOwner(Object value) {
        dialogOwner.set(value);
    }
    
    public Object getDialogOwner() {
        return dialogOwner.get();
    }
    
    /**
     * TODO
     */
    public Dialog getShowingDialog() {
        return showingDialog;
    }

    public ValidationResult validate() {
        ValidationResult currentResult = ValidationResult.OK;
        for(Callback<Void, ValidationResult> handler : getValidationHandlers()) {
            ValidationResult newResult = handler.call(null);
            if(currentResult.canOverrideMe(newResult)) {
                currentResult = newResult;
            }
        }
        return currentResult;
    }
    
    public void showAsDialog() {
        showAsDialog(getDialogOwner());
    }
    
    public void showAsDialog(Object owner) {
        setActionsPresentation(ActionsPresentation.EXTERNAL_OR_NONE);
        showingDialog = new Dialog(owner, getText(), true);
        showingDialog.setContent(this);
        showingDialog.getActions().setAll(getActions());
        showingDialog.show();
    }

    @Override
    protected String getUserAgentStylesheet() {
        // TODO: why passing just "form.css" to getResource does not work ?
        return getClass().getResource("/com/prodoko/fx/control/forms/form.css").toExternalForm();
    }
}
