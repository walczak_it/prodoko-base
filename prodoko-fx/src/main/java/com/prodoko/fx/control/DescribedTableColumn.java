package com.prodoko.fx.control;

import java.net.URL;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableColumn;

public class DescribedTableColumn<S, T> extends TableColumn<S, T> {

    private SimpleStringProperty shortDescription;
    private SimpleObjectProperty<URL> moreInfo;
    
    public DescribedTableColumn(String text, String shortDescription, URL moreInfo) {
        super(text);
        this.shortDescription = new SimpleStringProperty(shortDescription);
        this.moreInfo = new SimpleObjectProperty<URL>(moreInfo);
    }
    
    public String getShortDescription() {
        return shortDescription.getValue();
    }
    
    public void setShortDescription(String shortDescription) {
        this.shortDescription.setValue(shortDescription);
    }
    
    public StringProperty shortDescriptionProperty() {
        return shortDescription;
    }
    
    public URL getMoreInfo() {
        return moreInfo.getValue();
    }
    
    public void setMoreInfo(URL moreInfo) {
        this.moreInfo.setValue(moreInfo);
    }
    
    public ObjectProperty<URL> moreInfoProperty() {
        return moreInfo;
    }
}
