package com.prodoko.fx.concurrent;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

public class SimpleService<T> extends Service<T> {

    private Class<?> taskClass;
    private Object[] tasksConstructorArgs;
    private Class<?>[] tasksConstructorArgClasses;

    public SimpleService(Class<?> taskClass) {
        super();
        this.taskClass = taskClass;
    }
    
    public SimpleService(Class<?> taskClass, Object enclosingObject) {
        super();
        this.taskClass = taskClass;
        tasksConstructorArgs = new Object[] {enclosingObject};
        tasksConstructorArgClasses = new Class<?>[] {taskClass.getEnclosingClass()};
    }

    /**
     * TODO
     */
    public Class<?> getTaskClass() {
        return taskClass;
    }

    /**
     * @see #getTaskClass
     */
    public void setTaskClass(Class<?> taskClass) {
        this.taskClass = taskClass;
    }

    /**
     * TODO
     */
    public Object[] getTasksConstructorArgs() {
        return tasksConstructorArgs;
    }

    /**
     * @see gsetTasksConstructorArgs
     */
    public void setTasksConstructorArgs(Object... tasksConstructorArgs) {
        this.tasksConstructorArgs = tasksConstructorArgs;
    }

    /**
     * TODO
     */
    public Class<?>[] getTasksConstructorArgClasses() {
        return tasksConstructorArgClasses;
    }

    /**
     * @see #getTasksConstructorArgClasses
     */
    public void setTasksConstructorArgClasses(Class<?>... tasksConstructorArgClasses) {
        this.tasksConstructorArgClasses = tasksConstructorArgClasses;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Task<T> createTask() {
        try {
            if(tasksConstructorArgs == null || tasksConstructorArgs.length == 0) {
                return (Task<T>) taskClass.newInstance();
            } else {
                try {
                    Constructor<?> constr = taskClass.getDeclaredConstructor(
                            tasksConstructorArgClasses);
                    if(!constr.isAccessible()) {
                        constr.setAccessible(true);
                    }
                    return (Task<T>) constr.newInstance(tasksConstructorArgs);
                } catch (NoSuchMethodException | SecurityException | IllegalArgumentException
                        | InvocationTargetException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("[");
                    if(tasksConstructorArgClasses.length >= 1) {
                        sb.append(tasksConstructorArgClasses[0].getName());
                    }
                    for(int i = 1; i < tasksConstructorArgClasses.length; ++i) {
                        sb.append(", ").append(tasksConstructorArgClasses[i].getName());
                    }
                    sb.append("]");
                    throw new IllegalStateException("Couldn't use constructor of class "
                            + taskClass.getName() + " with the given arguments "
                            + sb.toString(), e);
                }
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException("Could not instanciate task od class "
                    + taskClass.getName(), e);
        }
    }
}
