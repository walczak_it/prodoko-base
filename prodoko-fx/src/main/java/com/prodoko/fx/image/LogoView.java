package com.prodoko.fx.image;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class LogoView extends HBox {
    
    public final static double LOGO_VIEW_MAX_HEIGHT = 50;

    public LogoView(Image logo) {
        super();
        setAlignment(Pos.CENTER);
        ImageView logoView = new ImageView(logo);
        logoView.fitHeightProperty().set(LOGO_VIEW_MAX_HEIGHT);
        logoView.preserveRatioProperty().set(true);
        getChildren().add(logoView);
        setMaxHeight(LOGO_VIEW_MAX_HEIGHT);
        setPrefWidth(100);
        HBox.setHgrow(logoView, Priority.NEVER);
    }
}
