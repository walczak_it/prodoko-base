package com.prodoko.fx;

import javafx.scene.control.Labeled;

public class MirrorBinder {

    public static void bindLabelProperties(Labeled target, Labeled source) {
        target.textProperty().bind(source.textProperty());
    }
}
