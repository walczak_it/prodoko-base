package com.prodoko.fx.action;

import javafx.concurrent.Service;
import javafx.concurrent.Task;


public abstract class SimpleConcurrentAction<R> extends ServiceAction {

    public SimpleConcurrentAction(String text) {
        super(text);
        setService(new Service<R>(){

            @Override
            protected Task<R> createTask() {
                return new Task<R>() {

                    @Override
                    protected R call() throws Exception {
                        return executeConcurrently();
                    }
                };
            }
            
        });
    }

    protected abstract R executeConcurrently();
}
