package com.prodoko.fx.action;

import javafx.event.ActionEvent;

import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.control.action.Action;

public class PseudoAcion extends AbstractAction {

    private Action realAction;
    
    public PseudoAcion(Action decoretedAction) {
        this(decoretedAction.textProperty().get(), decoretedAction);
    }

    public PseudoAcion(String text, Action realAction) {
        super(text);
        this.realAction = realAction;
    }
    
    /**
     * TODO
     */
    public Action getDecoretedAction() {
        return realAction;
    }

    @Override
    public void execute(ActionEvent ae) {
        realAction.execute(ae);
    }
}
