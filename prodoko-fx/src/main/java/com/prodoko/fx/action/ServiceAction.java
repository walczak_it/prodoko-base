package com.prodoko.fx.action;

import javafx.concurrent.Service;
import javafx.event.ActionEvent;

import org.controlsfx.control.action.AbstractAction;

public class ServiceAction extends AbstractAction {

    private Service<?> service;
    
    public ServiceAction(String text) {
        super(text);
    }

    public ServiceAction(Service<?> service) {
        this(service, service.getTitle());
    }
    
    public ServiceAction(Service<?> service, String text) {
        super(text);
        this.service = service;
    }

    /**
     * TODO
     */
    public Service<?> getService() {
        return service;
    }

    /**
     * @see #getService
     */
    public void setService(Service<?> service) {
        this.service = service;
    }

    @Override
    public void execute(ActionEvent ae) {
        if(getService() == null) {
            throw new IllegalStateException("Trying to execute an action without a set service");
        }
        if(!getService().isRunning()) {
            getService().reset();
            getService().start();
        }
    }
}
