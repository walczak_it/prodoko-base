package com.prodoko.fx;

import java.util.ArrayList;
import java.util.List;

public class ProdokoFxConsts {

    private static final ArrayList<String> FX_STYLE_SHEETS;
    
    static {
        FX_STYLE_SHEETS = new ArrayList<>();
        FX_STYLE_SHEETS.add("/com/prodoko/fx/validation/validation.css");
    }
    
    @SuppressWarnings("unchecked")
    public static List<String> getFxStyleSheets() {
        return (List<String>) FX_STYLE_SHEETS.clone();
    }
}
