package com.prodoko.fx;

import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

public class GridPaneLayouter {

    private GridPane pane;
    private int row;
    private int column;
    
    public GridPaneLayouter(GridPane pane) {
        this(pane, 0, 0);
    }
    
    public GridPaneLayouter(GridPane pane, int row, int column) {
        super();
        this.pane = pane;
        this.row = row;
        this.column = column;
    }

    /**
     * TODO
     */
    public GridPane getPane() {
        return pane;
    }

    /**
     * TODO
     */
    public int getRow() {
        return row;
    }

    /**
     * TODO
     */
    public int getColumn() {
        return column;
    }

    public GridPaneLayouter jumpTo(int row, int column) {
        this.row = row;
        this.column = column;
        return this;
    }
    
    public GridPaneLayouter newRowLine() {
        return newRowLine(1);
    }
    
    public GridPaneLayouter newRowLine(int rowsToSkip) {
        this.row += rowsToSkip;
        this.column = 0;
        return this;
    }
    
    public GridPaneLayouter rowBelow() {
        return rowBelow(1);
    }
    
    public GridPaneLayouter rowBelow(int rowsToSkip) {
        row += rowsToSkip;
        return this;
    }
    
    public GridPaneLayouter add(Node node) {
        pane.add(node, column, row);
        ++column;
        return this;
    }
    
    public GridPaneLayouter add(Node node, int spanColumns) {
        return add(node, spanColumns, 1);
    }
    
    public GridPaneLayouter add(Node node, int spanColumns, int spanRows) {
        pane.add(node, column, row, spanColumns, spanRows);
        column += 1 + spanRows;
        return this;
    }
    
    public GridPaneLayouter skip() {
        ++column;
        return this;
    }
}
