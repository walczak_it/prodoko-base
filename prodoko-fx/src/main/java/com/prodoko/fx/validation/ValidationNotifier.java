package com.prodoko.fx.validation;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.Node;
import javafx.scene.image.ImageView;

import org.controlsfx.control.NotificationPane;
import org.controlsfx.control.action.Action;

/**
 * TODO pane with multiple messages
 * 
 * @author walec51
 *
 */
public class ValidationNotifier {
    
    private NotificationPane notificationPane;
    
    private Map<String, ValidationResult> notificationStyleClassResultMapping;

    public ValidationNotifier(NotificationPane notificationPane) {
        this.notificationPane = notificationPane;
        notificationStyleClassResultMapping = new HashMap<>();
        for(ValidationResult vr : ValidationResult.values()) {
            notificationStyleClassResultMapping.put(
                    notificationStyleClassForResult(vr), vr);
        }
    }

    public NotificationPane getNotificationPane() {
        return notificationPane;
    }

    public void notify(ValidationResult vr, String message, Action... actions) {
        notificationPane.getStyleClass().add(
                notificationStyleClassForResult(vr));
        notificationPane.setGraphic(notificationGraphicForResult(vr));
        notificationPane.setText(message);
        //TODO actions
        notificationPane.show();
    }
    
    public void clear() {
        notificationPane.getStyleClass().removeAll(
                getNotificationStyleClassResultMapping().keySet());
        removeControlsAndHide();
    }

    public String notificationStyleClassForResult(ValidationResult result) {
        switch(result) {
        case OK : return "validation-ok-notification";
        case WARNING : return "validation-warning-notification";
        case ERROR : return "validation-error-notification";
        }
        throw new IllegalStateException("Unrecognized validation result");
    }
    
    
    protected Node notificationGraphicForResult(ValidationResult result) {
        String imagePath = "com/prodoko/graphics/icons/";
        switch(result) {
        case OK : imagePath += "ok.png"; break;
        case WARNING : imagePath += "warning.png"; break;
        case ERROR : imagePath += "error.png"; break;
        default : throw new IllegalStateException(
                "Unrecognized validation result");
        }
        ImageView g = new ImageView(imagePath);
        g.fitHeightProperty().set(27);
        g.preserveRatioProperty().set(true);
        return g;
    }

    protected Map<String, ValidationResult>
            getNotificationStyleClassResultMapping() {
        return notificationStyleClassResultMapping;
    }
    
    protected void removeControlsAndHide() {

        notificationPane.setGraphic(null);
        notificationPane.setText(null);
        notificationPane.hide();
    }
}
