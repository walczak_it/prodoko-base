package com.prodoko.fx.validation;

public interface FieldsValidator {
    
    public void setDecorator(FieldsValidationDecorator decorator);

    public ValidationResult validate();
}
