package com.prodoko.fx.validation;

public enum ValidationResult {
    OK, WARNING, ERROR;
    
    public boolean canOverrideMe(ValidationResult other) {
        if(other == null) {
            return false;
        } else {
            return other.ordinal() > this.ordinal();
        }
    }
}
