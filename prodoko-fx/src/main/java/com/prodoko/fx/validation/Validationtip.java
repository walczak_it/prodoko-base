package com.prodoko.fx.validation;

import javafx.scene.control.Tooltip;

public class Validationtip extends Tooltip {

    private Tooltip replacedTooltip;

    public Validationtip() {
        super();
    }

    public Validationtip(String message) {
        super(message);
    }

    public Tooltip getReplacedTooltip() {
        return replacedTooltip;
    }

    public void setReplacedTooltip(Tooltip tooltip) {
        this.replacedTooltip = tooltip;
    }
}
