package com.prodoko.fx.validation;

import javafx.scene.control.Control;

public abstract class BatchFieldsValidator<T extends Control> implements FieldsValidator {


    @Override
    public ValidationResult validate() {
        ValidationResult currentRes = ValidationResult.OK;
        for(T control : getControlsToValidate()) {
            ValidationResult newRes = validate(control);
            if(currentRes.canOverrideMe(newRes)) {
                currentRes = newRes;
            }
        }
        return currentRes;
    }

    public abstract T[] getControlsToValidate();
    
    public abstract ValidationResult validate(T control);
}
