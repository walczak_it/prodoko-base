package com.prodoko.fx.validation;

import java.util.ResourceBundle;

public class GenericExceptionPresenter<T extends Exception>
        implements ExceptionPresenter<T> {
    
    private static final ResourceBundle rb = ResourceBundle.getBundle(
            "com.prodoko.fx.validation.messages");

    @Override
    public void present(ValidationPresenter vp, T ex) {
        vp.getNotifier().notify(ValidationResult.ERROR,
                rb.getString("internalError"));
    }

    @Override
    public Class<?> getExceptionClass() {
        
        return Exception.class;
     }
}
