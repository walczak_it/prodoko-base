package com.prodoko.fx.validation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import javafx.css.Styleable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Control;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;

public class FieldsValidationDecorator {
    
    private Set<Control> controlsWithAddedValidationtip = new HashSet<>();
    private Set<Styleable> decoratedElements = new HashSet<>();

    private Boolean decorateParents;
    
    private static final HashSet<Class<? extends Parent>>
            DEFAULT_INCLUDED_PARENT_CLASSES;
    
    private Set<Class<? extends Parent>> includeParentClasses = new HashSet<>(
            DEFAULT_INCLUDED_PARENT_CLASSES);
    
    private Parent parentDecorationThreshold = null;
    
    private Map<String, ValidationResult> decorationStyleClassResultMapping;
    
    static {
        DEFAULT_INCLUDED_PARENT_CLASSES = new HashSet<>();
        DEFAULT_INCLUDED_PARENT_CLASSES.add(TabPane.class);
    }

    protected FieldsValidationDecorator(Boolean decorateParents,
            Parent parentDecorationThreshold,
            Set<Class<? extends Parent>> includeParentClasses) {
        super();
        this.decorateParents = decorateParents;
        this.parentDecorationThreshold = parentDecorationThreshold;
        this.includeParentClasses = includeParentClasses;
        decorationStyleClassResultMapping = new HashMap<>();
        for(ValidationResult vr : ValidationResult.values()) {
            decorationStyleClassResultMapping.put(
                    decorationsStyleClassForResult(vr), vr);
        }
    }
    
    @SuppressWarnings("unchecked")
    protected static Set<Class<? extends Parent>>
            getDefaultIncludedParentClasses() {
        return (Set<Class<? extends Parent>>) DEFAULT_INCLUDED_PARENT_CLASSES
                .clone();
    }
    
    public static FieldsValidationDecorator newWithoutParentDecoration() {
        return new FieldsValidationDecorator(false, null,
                DEFAULT_INCLUDED_PARENT_CLASSES);
    }
    
    public static FieldsValidationDecorator newDecoratingAllParents() {
        return newDecoratingAllParents(
                getDefaultIncludedParentClasses());
    }
    
    public static FieldsValidationDecorator newDecoratingAllParents(
            Set<Class<? extends Parent>> includeParentClasses) {
        return new FieldsValidationDecorator(true, null, includeParentClasses);
    }
    
    public static FieldsValidationDecorator newDecoratingParentsToTreshold(
            Parent treshold) {
        return newDecoratingParentsToTreshold(treshold,
                getDefaultIncludedParentClasses());
    }
    
    public static FieldsValidationDecorator newDecoratingParentsToTreshold(
            Parent treshold,
            Set<Class<? extends Parent>> includeParentClasses) {
        return new FieldsValidationDecorator(true, treshold, includeParentClasses);
    }

    public Boolean isDecoratingParents() {
        return decorateParents;
    }

    public Parent getParentDecorationThreshold() {
        return parentDecorationThreshold;
    }
    
    public void decorate(ValidationResult vr, Styleable elem) {
        OverridePreparationStatus ops = prepareForValidationOverride(vr, elem);
        decorate(vr, true, isDecoratingParents(), ops, elem);
    }

    public void decorate(ValidationResult vr, String message, Control control) {
        OverridePreparationStatus ops = prepareForValidationOverride(vr, control);
        decorate(vr, true, isDecoratingParents(), ops, control);
        installValidationtip(control, vr, true, ops, message);
    }

    public void clear() {
        for(Styleable elem : decoratedElements) {
            elem.getStyleClass().removeAll(
                    getDecorationStyleClassResultMapping().keySet());
        }
        decoratedElements.clear();
        for(Control control : controlsWithAddedValidationtip) {
            Tooltip currentTooltip = control.getTooltip(); 
            if(currentTooltip == null) {
                throw new IllegalStateException("A control that was to be "
                        + "cleared has no tooltip");
            }
            if(!(currentTooltip instanceof Validationtip)) {
                throw new IllegalStateException("A control that was to be "
                        + "cleared has a tooltip that is not an instance of "
                        + "a Validationtip");
            }
            Validationtip validationtip = (Validationtip)currentTooltip;
            Tooltip replacedTooltip = validationtip.getReplacedTooltip();
            if(replacedTooltip != null) {
                control.setTooltip(replacedTooltip);
            } else {
                Tooltip.uninstall(control, validationtip);
            }
        }
        controlsWithAddedValidationtip.clear();
    }
    
    public String decorationsStyleClassForResult(ValidationResult result) {
        switch(result) {
        case OK : return "validation-ok-decoration";
        case WARNING : return "validation-warning-decoration";
        case ERROR : return "validation-error-decoration";
        }
        throw new IllegalStateException("Unrecognized validation result");
    }
    
    public String tooltipStyleClassForResult(ValidationResult result) {
        switch(result) {
        case OK : return "validation-ok-tooltip";
        case WARNING : return "validation-warning-tooltip";
        case ERROR : return "validation-error-tooltip";
        }
        throw new IllegalStateException("Unrecognized validation result");
    }
    
    protected Map<String, ValidationResult>
            getDecorationStyleClassResultMapping() {
        return decorationStyleClassResultMapping;
    }
    
    protected Set<Class<? extends Parent>> getIncludeParentClasses() {
        return includeParentClasses;
    }
    
    protected boolean includedParent(Parent currentParent) {
        for(Class<?> clazz : getIncludeParentClasses()) {
            if(clazz.isInstance(currentParent)) {
                return true;
            }
        }
        return false;
    }
    
    protected void decorate(ValidationResult vr, boolean rememberToClean,
            boolean decorateParents, OverridePreparationStatus ops,
            Styleable elem) {
        
        if(ops != OverridePreparationStatus.CANNOT_OVERRIDE) {
            elem.getStyleClass().add(decorationsStyleClassForResult(vr));
            if(rememberToClean) {
                decoratedElements.add(elem);
            }
            if(elem instanceof Node) {
                Node node = (Node)elem;
                decorateParents(vr, rememberToClean, node);
            }
        }
    }
    
    protected void decorateParents(ValidationResult vr, Boolean rememberToClean, Node node) {
        decorateParents(vr, rememberToClean, node.getParent(), new LinkedList<Parent>(), node);
    }
    
    protected void decorateParents(ValidationResult vr, Boolean rememberToClean,
            Parent currentParent, LinkedList<Parent> previousParents, Styleable decoratedChild) {
        
        if(currentParent == null || currentParent.equals(
                parentDecorationThreshold)) {
            return;
        }
        
        if(includedParent(currentParent)) {
            
            Styleable elemToDecorate = null;
            
            if(currentParent instanceof TabPane) {
                TabPane tabPaneParent = (TabPane)currentParent;
                elemToDecorate = findTabToDecorate(tabPaneParent,
                        previousParents);
            } else {
                elemToDecorate = currentParent;
            }
        
            OverridePreparationStatus ops = prepareForValidationOverride(vr, elemToDecorate);
            decorate(vr, rememberToClean, false, ops, elemToDecorate);
        }
        
        previousParents.addFirst(currentParent);
        decorateParents(vr, rememberToClean,
                currentParent.getParent(), previousParents, decoratedChild);
    }
    
    protected Tab findTabToDecorate(TabPane tabPaneParent,
            LinkedList<Parent> previousParents) {
        
        if(previousParents.size() < 2) {
            throw new IllegalArgumentException("Tried to decorate a "
                    + "TabPane but previous parents don't have the "
                    + "tab contents node or the implementation of "
                    + "TabPane changed");
        }
        Node tabsContent = previousParents.get(1);
        Tab parentTab = null;
        for(Tab tab : tabPaneParent.getTabs()) {
            if(tab.getContent().equals(tabsContent)) {
                parentTab = tab;
                break;
            }
        }
        if(parentTab == null) {
            throw new IllegalArgumentException("The second previous "
                    + "parent was not the content of any of the given "
                    + "TabPanes tabs. TabPanes implementation must "
                    + "of changed");
        }
        return parentTab;
    }
    
    protected void installValidationtip(Control control, ValidationResult vr,
            boolean rememberToClear, OverridePreparationStatus ops,
            String message) {
        
        if(ops == OverridePreparationStatus.CANNOT_OVERRIDE) {
            return;
        }
        
        Validationtip validationtip = new Validationtip(message);
        decorate(vr, false, isDecoratingParents(), OverridePreparationStatus.NO_OVERRIDE_NEEDED,
                validationtip);
        if(ops == OverridePreparationStatus.PREPARED_FOR_OVERRIDE) {
            Tooltip currentTooltip = control.getTooltip();
            if(currentTooltip instanceof Validationtip) {
                Validationtip oldValidationtip = (Validationtip)currentTooltip;
                validationtip.setReplacedTooltip(
                        oldValidationtip.getReplacedTooltip());
                if(!rememberToClear && controlsWithAddedValidationtip
                        .contains(control)) {
                    
                    controlsWithAddedValidationtip.remove(control);
                }
            } else {
                validationtip.setReplacedTooltip(control.getTooltip());
            }
        }
        
        control.setTooltip(validationtip);
        if(rememberToClear) {
            controlsWithAddedValidationtip.add(control);
        }
    }
    
    protected enum OverridePreparationStatus {
        NO_OVERRIDE_NEEDED, CANNOT_OVERRIDE, PREPARED_FOR_OVERRIDE;
    }
    
    protected OverridePreparationStatus prepareForValidationOverride(
            ValidationResult newResult, Styleable styleable) {
        String styleClassToOverride = null;
        for(String styleClass : styleable.getStyleClass()) {
            if(getDecorationStyleClassResultMapping().containsKey(styleClass)) {
                ValidationResult currentResult
                    = getDecorationStyleClassResultMapping().get(styleClass);
                if(currentResult.canOverrideMe(newResult)) {
                    return OverridePreparationStatus.CANNOT_OVERRIDE;
                } else {
                    styleClassToOverride = styleClass;
                    break;
                }
            }
        }
        if(styleClassToOverride != null) {
            styleable.getStyleClass().remove(styleClassToOverride);
            return OverridePreparationStatus.PREPARED_FOR_OVERRIDE;
        } else {
            return OverridePreparationStatus.NO_OVERRIDE_NEEDED;
        }
    }
}
