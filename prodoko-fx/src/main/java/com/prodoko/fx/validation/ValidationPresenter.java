package com.prodoko.fx.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.scene.control.Control;

public class ValidationPresenter {
    
    private static final ResourceBundle rb = ResourceBundle.getBundle(
            "com.prodoko.fx.validation.messages");
    
    private static final HashMap<Class<?>, ExceptionPresenter<?>>
            DEFAULT_CLASS_EXCEPTION_PRESENTER_MAPPING;
    
    static {
        DEFAULT_CLASS_EXCEPTION_PRESENTER_MAPPING = new HashMap<>();
        addClassExceptionPresenterMapping(
                DEFAULT_CLASS_EXCEPTION_PRESENTER_MAPPING,
                new GenericExceptionPresenter<>());
        
    }
    
    private Map<Class<?>, ExceptionPresenter<?>>
            classExceptionPresenterMapping;

    private FieldsValidationDecorator decorator;
    private ValidationNotifier notifier;
    
    @SuppressWarnings("unchecked")
    public ValidationPresenter(FieldsValidationDecorator decorator,
            ValidationNotifier notifier) {
        super();
        this.decorator = decorator;
        this.notifier = notifier;
        this.classExceptionPresenterMapping
            = (Map<Class<?>, ExceptionPresenter<?>>)
                DEFAULT_CLASS_EXCEPTION_PRESENTER_MAPPING.clone();
        
    }
    
    public Map<Class<?>, ExceptionPresenter<?>>
            getClassExceptionPresenterMapping() {
        return classExceptionPresenterMapping;
    }

    public void addClassExceptionPresenterMappings(
            ExceptionPresenter<?>... eps) {
        addClassExceptionPresenterMapping(
                getClassExceptionPresenterMapping(),
                eps);
    }
    
    public static void addClassExceptionPresenterMapping(
            Map<Class<?>, ExceptionPresenter<?>> map,
            ExceptionPresenter<?>... eps) {
        
        for(ExceptionPresenter<?> ep : eps) {
            map.put(ep.getExceptionClass(), ep);
        }
    }

    public FieldsValidationDecorator getDecorator() {
        return decorator;
    }

    public ValidationNotifier getNotifier() {
        return notifier;
    }
    
    public void error(Throwable ex) {
        @SuppressWarnings("unchecked")
        ExceptionPresenter<Throwable> ep = findExceptionNotification(
                ex.getClass());
        ep.present(this, ex);
    }
    
    public void error(String message, Control... control) {
        present(ValidationResult.ERROR, message, control);
    }
    
    public void warning(String message) {
        present(ValidationResult.WARNING, message);
    }

    public void ok(String message) {
        present(ValidationResult.OK, message);
    }
    
    public void present(ValidationResult result, String message, Control... controls) {
        for(Control control : controls) {
            getDecorator().decorate(result, message, control);
        }
        getNotifier().notify(result, rb.getString("invalidFields"));
    }
    
    public void present(ValidationResult result, String message) {
        getNotifier().notify(result, message);
    }
    
    public ValidationResult validateFields(FieldsValidator... validators) {
        ValidationResult currentRes = ValidationResult.OK;
        for(FieldsValidator validator : validators) {
            validator.setDecorator(getDecorator());
            ValidationResult newRes = validator.validate();
            if(currentRes.canOverrideMe(newRes)) {
                currentRes = newRes;
            }
        }
        if(currentRes == ValidationResult.ERROR) {
            getNotifier().notify(ValidationResult.ERROR,
                    rb.getString("invalidFields"));
        }
        return currentRes;
    }
    
    public void clear() {
        getDecorator().clear();
        getNotifier().clear();
    }
    
    @SuppressWarnings("rawtypes")
    protected ExceptionPresenter findExceptionNotification(
            Class<?> ex) {
        Class<?> itr = ex;
        while(itr != null && !itr.equals(Object.class)) {
            ExceptionPresenter en = getClassExceptionPresenterMapping()
                    .get(itr);
            if(en != null) {
                return en;
            }
            itr = itr.getSuperclass();
        }
        throw new IllegalStateException("This ValidationNotifier does not even "
                + "have and mapping for Exception");
    }
}
