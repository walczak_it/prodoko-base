package com.prodoko.fx.validation;

import java.util.ResourceBundle;

import javafx.scene.control.TextInputControl;

public class RequiredTextFields extends BatchFieldsValidator<TextInputControl> {
    
    private static final ResourceBundle rb = ResourceBundle.getBundle(
            "com.prodoko.fx.validation.messages");
    
    private TextInputControl[] controlsToValidate;
    private FieldsValidationDecorator decorator;

    public RequiredTextFields(TextInputControl... controlsToValidate) {
        super();
        this.controlsToValidate = controlsToValidate;
    }

    @Override
    public void setDecorator(FieldsValidationDecorator decorator) {
        this.decorator = decorator;
    }

    @Override
    public TextInputControl[] getControlsToValidate() {
        return controlsToValidate;
    }

    @Override
    public ValidationResult validate(TextInputControl control) {
        String text = control.getText();
        if(text == null || text.trim().length() == 0) {
            decorator.decorate(ValidationResult.ERROR, rb.getString("requiredTextField"),
                    control);
            return ValidationResult.ERROR;
        } else {
            return ValidationResult.OK;
        }
    }
}
