package com.prodoko.fx.validation;


public interface ExceptionPresenter<T extends Throwable> {

    public void present(ValidationPresenter vp, T ex);
    public Class<?> getExceptionClass();
}
