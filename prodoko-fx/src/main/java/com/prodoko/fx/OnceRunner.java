package com.prodoko.fx;

import javafx.application.Platform;

public class OnceRunner {

    private Runnable runnable;
    private boolean runRequested = false;

    public OnceRunner(Runnable runnable) {
        super();
        this.runnable = runnable;
    }

    /**
     * TODO
     */
    public Runnable getRunnable() {
        return runnable;
    }

    /**
     * TODO
     */
    public boolean isRunRequested() {
        return runRequested;
    }
    
    public void runLaterOnce() {
        if(!runRequested) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    runRequested = false;
                    runnable.run();
                }
                
            });
        }
    }
}
