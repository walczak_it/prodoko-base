package com.prodoko.base.client.basic.legalentities;

import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.client.beans.BeanEntityForm;
import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeFinderService;

public abstract class LegalEntityForm<T extends LegalEntity> extends BeanEntityForm<T> {
    
    private LegalEntityTypeFinderService typeFinderService;

    public LegalEntityForm() {
        super();
    }

    public LegalEntityForm(T item) {
        super(item);
    }

    /**
     * TODO
     */
    public LegalEntityTypeFinderService getTypeFinderService() {
        return typeFinderService;
    }

    /**
     * @see #getTypeFinderService
     */
    @Autowired
    public void setTypeFinderService(LegalEntityTypeFinderService typeFinderService) {
        this.typeFinderService = typeFinderService;
    }
}
