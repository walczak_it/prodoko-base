package com.prodoko.base.client.beans.browser;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.beans.binding.Bindings;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import com.prodoko.base.client.beans.filter.FilterAdder;
import com.prodoko.base.client.beans.filter.FiltersView;
import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;
import com.prodoko.base.model.beans.presentation.NamedBeansSubsetPresentation;
import com.prodoko.fx.control.form.FormConsts;
import com.prodoko.fx.control.form.FormSkin;

public class BeansBrowserSkin<C extends BeansBrowser<T>, T> extends FormSkin<C> {
    
    private final static double PRESENTATION_COMBO_BOX_MAX_WIDTH = 300d;
    
    private TitledPane titlePane;
    private VBox pane;
    private AnchorPane filtersAndPresentationSection;
    private HBox filtersViewPane;
    private FiltersView filtersView;
    private ComboBox<NamedBeansSubsetPresentation> presentationComboBox;
    private TableView<T> loadedItemsView;
    private AnchorPane loadedItemsSummarySection;
    private Label loadedItemsSummaryLabel;

    public BeansBrowserSkin(C control) {
        super(control);
        titlePane = createTitlePane();
        pane = createPane();
        buildFiltersAndPresentationSection();
        buildLoadedItemsView();
        buildLoadedItemsSummarySection();
        enableFormWorkerPresenter(getSkinnable().getDataSource());
    }
    
    protected TitledPane createTitlePane() {
        String title = message(getSkinnable().getSupportedClass(), "plural");
        TitledPane pane = new TitledPane();
        pane.setText(title);
        pane.setCollapsible(false);
        getFieldsPane().setContent(pane);
        return pane;
    }
    
    protected VBox createPane() {
        pane = new VBox();
        titlePane.setContent(pane);
        return pane;
    }

    protected void buildFiltersAndPresentationSection() {
        filtersAndPresentationSection = new AnchorPane();
        FilterAdder filterAdder = new FilterAdder(getSkinnable().currentPresentationProperty(),
                getSkinnable().getFilterFormFactories());
        filterAdder.setDialogOwner(getSkinnable());
        filtersView = new FiltersView(filterAdder);
        getSkinnable().getDataSource().filtersProperty().bind(filtersView.itemsProperty());
        filtersViewPane = new HBox(new Label(message(BeansBrowserSkin.class, "filtersLabel")),
                filtersView);
        filtersViewPane.getStyleClass().add(FormConsts.FIELDS_PANE_CLASS);
        AnchorPane.setTopAnchor(filtersViewPane, 0d);
        AnchorPane.setRightAnchor(filtersViewPane, PRESENTATION_COMBO_BOX_MAX_WIDTH);
        AnchorPane.setBottomAnchor(filtersViewPane, 0d);
        AnchorPane.setLeftAnchor(filtersViewPane, 0d);
        filtersAndPresentationSection.getChildren().add(filtersViewPane);
        
        presentationComboBox = new ComboBox<>();
        presentationComboBox.setCellFactory(createPresentationComboBoxCellFactory());
        presentationComboBox.setButtonCell(new PresentationsListCell());
        presentationComboBox.setItems(getSkinnable().getBeansPresentations());
        presentationComboBox.getSelectionModel().select(0);
        HBox presentationPane = new HBox(
                new Label(message(BeansBrowserSkin.class, "presentationComboBox")),
                presentationComboBox);
        AnchorPane.setTopAnchor(presentationPane, 0d);
        AnchorPane.setRightAnchor(presentationPane, 0d);
        AnchorPane.setBottomAnchor(presentationPane, 0d);
        presentationPane.setMaxWidth(PRESENTATION_COMBO_BOX_MAX_WIDTH);
        presentationPane.getStyleClass().add(FormConsts.FIELDS_PANE_CLASS);
        filtersAndPresentationSection.getChildren().add(presentationPane);
        
        pane.getChildren().add(filtersAndPresentationSection);
    }
    
    protected Callback<ListView<NamedBeansSubsetPresentation>, ListCell<NamedBeansSubsetPresentation>>
            createPresentationComboBoxCellFactory() {
        return new Callback<ListView<NamedBeansSubsetPresentation>, ListCell<NamedBeansSubsetPresentation>>() {

            @Override
            public ListCell<NamedBeansSubsetPresentation> call(
                    ListView<NamedBeansSubsetPresentation> view) {
                return new PresentationsListCell();
            }
            
        };
    }
    
    protected void buildLoadedItemsView() {
        loadedItemsView = new TableView<>(getSkinnable().getDataSource().getLoadedItems());
        loadedItemsView.setMaxHeight(Double.MAX_VALUE);
        loadedItemsView.setPrefHeight(1000);
        changePresentation(presentationComboBox.getValue());
        getSkinnable().setSelectedItems(loadedItemsView.getSelectionModel()
                .getSelectedItems());
        pane.getChildren().add(loadedItemsView);
    }
    
    protected void changePresentation(BeansSubsetPresentation presentation) {
        // TODO
        loadedItemsView.getColumns().setAll(getSkinnable().getPresentationFactory().
                <T>create(presentation));
    }
    
    protected void buildLoadedItemsSummarySection() {
        
        loadedItemsSummarySection = new AnchorPane();
        loadedItemsSummaryLabel = new Label();
        loadedItemsSummaryLabel.textProperty().bind(Bindings.format(
                message(BeansBrowserSkin.class, "loadedItemsSummaryLabel"),
                getSkinnable().getDataSource().loadedItemsProperty().sizeProperty(),
                getSkinnable().getDataSource().totalItemsCountProperty()));
        AnchorPane.setRightAnchor(loadedItemsSummaryLabel, 0d);
        AnchorPane.setTopAnchor(loadedItemsSummaryLabel, 0d);
        AnchorPane.setBottomAnchor(loadedItemsSummaryLabel, 0d);
        loadedItemsSummarySection.getStyleClass().add(FormConsts.FIELDS_PANE_CLASS);
        loadedItemsSummarySection.getChildren().add(loadedItemsSummaryLabel);
        pane.getChildren().add(loadedItemsSummarySection);
    }

    /**
     * TODO
     */
    public VBox getFieldsVBox() {
        return pane;
    }

    /**
     * TODO
     */
    public FiltersView getFiltersView() {
        return filtersView;
    }

    /**
     * TODO
     */
    public TableView<T> getLoadedItemsView() {
        return loadedItemsView;
    }
    
    protected static class PresentationsListCell extends ListCell<NamedBeansSubsetPresentation> {
    
        @Override
        protected void updateItem(
                NamedBeansSubsetPresentation value, boolean empty) {
            super.updateItem(value, empty);
            if(value != null) {
                setText(value.getPresentationName());
            }
        }
    }
}
