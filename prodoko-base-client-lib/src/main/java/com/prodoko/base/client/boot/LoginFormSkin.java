package com.prodoko.base.client.boot;

import static com.prodoko.base.util.BundleUtils.message;
import static com.prodoko.remoting.hessian.HessianAccessPropertyNames.HOST;
import static com.prodoko.remoting.hessian.HessianAccessPropertyNames.PASSWORD;
import static com.prodoko.remoting.hessian.HessianAccessPropertyNames.PORT;
import static com.prodoko.remoting.hessian.HessianAccessPropertyNames.USERNAME;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Properties;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.util.StringUtils;

import com.prodoko.fx.control.form.FieldsTabPane;
import com.prodoko.fx.control.form.FieldsetGrid;
import com.prodoko.fx.control.form.FormSkin;
import com.prodoko.fx.control.form.ItemForm.SyncItemHandler;
import com.prodoko.fx.image.LogoView;
import com.prodoko.fx.validation.ExceptionPresenter;
import com.prodoko.fx.validation.RequiredTextFields;
import com.prodoko.fx.validation.ValidationPresenter;
import com.prodoko.fx.validation.ValidationResult;

public class LoginFormSkin<T extends LoginForm> extends FormSkin<T> {
    
    private static final Log LOG = LogFactory.getLog(LoginFormSkin.class);
    
    private LogoView logoView = null;
    private FieldsTabPane fieldsTabPane;
    private Tab userFieldsTab;
    private UserFieldset userFieldset;
    private Tab serverFieldsTab;
    private ServerFieldset serverFieldset;
    private Button loginButton;

    public LoginFormSkin(T form) {
        super(form);
        
        if(getSkinnable().getLogo() != null) {
            buildLogoView();
        }
        
        buildFieldsTabPane();
        buildUserFieldsTab();
        buildServerFieldsTab();
        populateFields();
        buildExceptionPresenters();
        buildPrepareItemHandler();
    }
    
    protected void buildFieldsTabPane() {
        fieldsTabPane = new FieldsTabPane();
        getFieldsPane().setContent(fieldsTabPane);
    }
    
    protected void buildLogoView() {
        logoView = new LogoView(getSkinnable().getLogo());
        AnchorPane.setTopAnchor(logoView, 0d);
        AnchorPane.setLeftAnchor(logoView, 0d);
        AnchorPane.setRightAnchor(logoView, 0d);
        getMainPane().getChildren().add(logoView);
        
        AnchorPane.setTopAnchor(getFieldsPane(), LogoView.LOGO_VIEW_MAX_HEIGHT);
    }
    
    protected void buildUserFieldsTab() {
        userFieldsTab = new Tab(message(LoginFormSkin.class, "userFieldsTab"));
        fieldsTabPane.getTabs().add(userFieldsTab);
        userFieldset = new UserFieldset();
        userFieldsTab.setContent(userFieldset);
    }
    
    protected void buildServerFieldsTab() {

        serverFieldsTab = new Tab(message(LoginFormSkin.class, "serverFieldsTab"));
        fieldsTabPane.getTabs().add(serverFieldsTab);
        serverFieldset = new ServerFieldset();
        serverFieldsTab.setContent(serverFieldset);
    }
    
    protected void buildPrepareItemHandler() {
        getSkinnable().getSyncItemHandlers().add(new SyncItemHandler() {

            @Override
            public void updateItem() {
                Properties props = getSkinnable().getItem();
                props.clear();
                props.put(USERNAME.getPropertyName(), getUserFieldset()
                        .getLoginField().getText());
                if(StringUtils.hasText(getUserFieldset().getPasswordField()
                        .getText())) {
                    props.put(PASSWORD.getPropertyName(), getUserFieldset()
                            .getPasswordField().getText());
                }
                props.put(HOST.getPropertyName(), getServerFieldset()
                        .getHostField().getText());
                props.put(PORT.getPropertyName(), getServerFieldset()
                        .getPortField().getText());
            }

            @Override
            public void updateForm() {
            }
            
        });
    }
    
    protected void populateFields() {
        Properties cp = getSkinnable().getInitialItem();
        getUserFieldset().getLoginField().setText(cp.getProperty(USERNAME.getPropertyName()));
        getUserFieldset().getPasswordField().setText(cp.getProperty(PASSWORD.getPropertyName()));
        getServerFieldset().getHostField().setText(cp.getProperty(HOST.getPropertyName()));
        getServerFieldset().getPortField().setText(cp.getProperty(PORT.getPropertyName()));
    }
    
    protected void buildExceptionPresenters() {
        getValidationPresenter().addClassExceptionPresenterMappings(
                createRemoteAccessExceptionPresenter(),
                createRemoteConnectFailureExceptionPresenter());
    }

    
    protected ExceptionPresenter<RemoteAccessException>
            createRemoteAccessExceptionPresenter() {
        return new ExceptionPresenter<RemoteAccessException>() {

            @Override
            public void present(ValidationPresenter vp,
                    RemoteAccessException ex) {
                
                Throwable root = ex.getRootCause();
                if(root instanceof UnknownHostException) {
                    vp.error(message(LoginFormSkin.class, "UnknownHostException"),
                            getServerFieldset().getHostField()); 
                    LOG.warn("Server not found", ex);
                } else if(root instanceof ConnectException) {
                    vp.error(message(LoginFormSkin.class, "ConnectException"),
                            getServerFieldset().getHostField(),
                            getServerFieldset().getPortField()); 
                    LOG.warn("Server not found", ex);
                } else {
                    vp.getNotifier().notify(ValidationResult.ERROR,
                            message(LoginFormSkin.class, "otherRemoteAccessException"));
                    LOG.error("Failed to connect to server because of unknown "
                            + "reason", ex);
                }
            }

            @Override
            public Class<?> getExceptionClass() {
                return RemoteAccessException.class;
            }
            
        };
    }
    
    public ExceptionPresenter<RemoteConnectFailureException>
            createRemoteConnectFailureExceptionPresenter() {
        
        return new ExceptionPresenter<RemoteConnectFailureException>() {

            @Override
            public void present(ValidationPresenter vp,
                    RemoteConnectFailureException ex) {
                
                Throwable root = ex.getRootCause();
                if(root instanceof IOException && root.getMessage().startsWith(
                        "Server returned HTTP response code: 401")) {
                    vp.error(message(LoginFormSkin.class, "wrongUserFields"),
                            getUserFieldset().getLoginField(),
                            getUserFieldset().getPasswordField()); 
                    LOG.info("Wrong user fields, login or password");
                } else {
                    vp.getNotifier().notify(ValidationResult.ERROR,
                            message(LoginFormSkin.class, "otherRemoteConnectFailureException"));
                    LOG.error("Failed to connect to server because of unknown "
                            + "reason", ex);
                }
            }

            @Override
            public Class<?> getExceptionClass() {
                return RemoteConnectFailureException.class;
            }
            
        };
    }

    public ValidationResult validateForm() {
        getValidationPresenter().clear();
        return getValidationPresenter().validateFields(
                new RequiredTextFields(getUserFieldset().getLoginField(),
                        getServerFieldset().getHostField(), getServerFieldset().getPortField()));
    }

    /**
     * TODO
     */
    public LogoView getLogoView() {
        return logoView;
    }

    /**
     * TODO
     */
    public Tab getUserFieldsTab() {
        return userFieldsTab;
    }

    /**
     * TODO
     */
    public UserFieldset getUserFieldset() {
        return userFieldset;
    }

    /**
     * TODO
     */
    public Tab getServerFieldsTab() {
        return serverFieldsTab;
    }

    /**
     * TODO
     */
    public ServerFieldset getServerFieldset() {
        return serverFieldset;
    }

    /**
     * TODO
     */
    public Button getLoginButton() {
        return loginButton;
    }

    public class UserFieldset extends FieldsetGrid {
        
        private TextField loginField;
        
        private PasswordField passwordField;
        
        protected UserFieldset() {
            loginField = new TextField();
            addRow(0, new Label(message(UserFieldset.class, "loginField")),
                    loginField);
            passwordField = new PasswordField();
            addRow(1, new Label(message(UserFieldset.class, "passwordField")),
                    passwordField);
        }

        public TextField getLoginField() {
            return loginField;
        }

        public PasswordField getPasswordField() {
            return passwordField;
        }
    }
    
    public class ServerFieldset extends FieldsetGrid {
        
        protected TextField hostField;
        protected TextField portField;
        
        public ServerFieldset() {
            hostField = new TextField();
            portField = new TextField();
            addRow(0, new Label(message(ServerFieldset.class, "hostField")),
                    hostField);
            addRow(1, new Label(message(ServerFieldset.class, "portField")),
                    portField);
        }

        public TextField getHostField() {
            return hostField;
        }

        public TextField getPortField() {
            return portField;
        }
    }
}
