package com.prodoko.base.client.beans;

import com.prodoko.base.util.ClassSupporter;

public interface BeanFormFactory<T> extends ClassSupporter {
    
    @Override
    public Class<T> getSupportedClass();

    public BeanForm<T> createWithNewBean();
    
    public BeanForm<T> createWith(T bean);
}
