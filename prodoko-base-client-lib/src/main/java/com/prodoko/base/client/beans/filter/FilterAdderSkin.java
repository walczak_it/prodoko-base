package com.prodoko.base.client.beans.filter;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.prodoko.base.client.beans.cells.BusinessDescriptionListCell;
import com.prodoko.base.client.beans.cells.BusinessDescriptionTreeCell;
import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessBeanDescription;
import com.prodoko.base.model.beans.BusinessDescription;
import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.beans.BusinessProperty.RelationDistance;
import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.base.model.beans.BusinessPropertyDescription;
import com.prodoko.fx.control.form.FormConsts;
import com.prodoko.fx.control.form.FormSkin;
import com.prodoko.fx.control.form.ItemForm;


public class FilterAdderSkin extends FormSkin<FilterAdder> {
    
    private static final Log LOG = LogFactory.getLog(FilterAdderSkin.class);
    
    private SplitPane pane;
    private TreeView<BusinessDescription<?>> beanPropertiesTree;
    private ComboBox<BusinessBeanDescription> filterTypeField; 
    private HBox filterTypePane;
    private ScrollPane filterFormPane;
    private AnchorPane filterPane;
    private Node selectPropertyPlaceholder;
    private Node selectFilterTypePlaceholder;

    public FilterAdderSkin(FilterAdder control) {
        super(control);
        
        getSkinnable().setMaxWidth(600);
        getSkinnable().setPrefWidth(600);
        buildSplitPane();
        buildBeanPropertiesTree();
        buildSelectPropertyPlaceholder();
        buildSelectFilterTypePlaceholder();
        buildFilterPane();
        
    }

    protected void buildSplitPane() {
        pane = new SplitPane();
        pane.setDividerPositions(0.3f);
        getChildren().add(pane);
    }
    
    protected void buildBeanPropertiesTree() {
        TreeItem<BusinessDescription<?>> propertyFiltersRoot = new TreeItem<BusinessDescription<?>>(
                getSkinnable().getBeanDescription());
        propertyFiltersRoot.getChildren().addAll(createBeanPropertyFilterItems(
                getSkinnable().getBeanDescription(), RelationDistance.CLOSE));
        getSkinnable().getBeanDescription().getMetadata();
        beanPropertiesTree = new TreeView<>(propertyFiltersRoot);
        beanPropertiesTree.setCellFactory(createFiltersTreeCellFactory());
        beanPropertiesTree.getSelectionModel().setSelectionMode(
                SelectionMode.SINGLE);
        beanPropertiesTree.getSelectionModel().select(propertyFiltersRoot);
        beanPropertiesTree.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<TreeItem<BusinessDescription<?>>>() {

                    @Override
                    public void changed(
                            ObservableValue<? extends TreeItem<BusinessDescription<?>>> observable,
                            TreeItem<BusinessDescription<?>> oldValue,
                            TreeItem<BusinessDescription<?>> newValue) {
                        if(newValue != null) {
                            populateFilterPane(newValue);
                        }
                    }
        });
        pane.getItems().add(beanPropertiesTree);
    }

    protected List<TreeItem<BusinessDescription<?>>> createBeanPropertyFilterItems(
            BusinessBeanDescription beanDesc, BusinessProperty.RelationDistance beanRelation) {

        List<TreeItem<BusinessDescription<?>>> items = new ArrayList<>();
        for(BusinessPropertyDescription propDesc : beanDesc.getBusinessProperties()) {
            if(propDesc.getMetadata().possibleFilters().length != 0 || propDesc.isBusinessBean()) {
                TreeItem<BusinessDescription<?>> item
                        = new TreeItem<BusinessDescription<?>>(propDesc);
                items.add(item);
                if(propDesc.isBusinessBean() && beanRelation == RelationDistance.CLOSE) {
                    item.getChildren().addAll(createBeanPropertyFilterItems(
                            propDesc.getBusinessBeanDescription(),
                            propDesc.getMetadata().relation()));
                }
            }
        }
        return items;
    }
    
    protected Callback<TreeView<BusinessDescription<?>>,TreeCell<BusinessDescription<?>>>
            createFiltersTreeCellFactory() {
        return new Callback<TreeView<BusinessDescription<?>>,
                TreeCell<BusinessDescription<?>>>() {

                    @Override
                    public TreeCell<BusinessDescription<?>> call(
                            TreeView<BusinessDescription<?>> view) {
                        return new BusinessDescriptionTreeCell<>();
                    }
        };
    }

    protected void buildSelectPropertyPlaceholder() {
        selectPropertyPlaceholder = new BorderPane(new Label(message(
                FilterAdderSkin.class, "selectPropertyPlaceholder")));
    }

    protected void buildSelectFilterTypePlaceholder() {
        selectFilterTypePlaceholder = new BorderPane(new Label(message(
                FilterAdderSkin.class, "selectFilterTypePlaceholder")));
    }
    
    protected void buildFilterPane() {
        filterTypeField = new ComboBox<>();
        filterTypeField.setCellFactory(createFilterTypeCellFactory());
        filterTypeField.setButtonCell(new BusinessDescriptionListCell<BusinessBeanDescription>());
        filterTypeField.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<BusinessBeanDescription>() {

                    @SuppressWarnings("unchecked")
                    @Override
                    public void changed(
                            ObservableValue<? extends BusinessBeanDescription> observable,
                            BusinessBeanDescription oldValue,
                            BusinessBeanDescription newValue) {
                        
                        if(newValue != null) {
                            setFilterForm((Class<? extends FilterData>)newValue.getBeanClass(),
                                    beanPropertiesTree.getSelectionModel().getSelectedItem());
                        }
                    }
        });
        filterTypePane = new HBox(new Label(message(FilterAdderSkin.class, "filterTypeField")),
                filterTypeField);
        filterTypePane.setMaxHeight(FormConsts.MAX_FIELDS_ROW_HEIGHT);
        AnchorPane.setTopAnchor(filterTypePane, 0d);
        AnchorPane.setLeftAnchor(filterTypePane, 0d);
        filterFormPane = new ScrollPane();
        AnchorPane.setTopAnchor(filterFormPane, FormConsts.MAX_FIELDS_ROW_HEIGHT);
        AnchorPane.setRightAnchor(filterFormPane, 0d);
        AnchorPane.setLeftAnchor(filterFormPane, 0d);
        AnchorPane.setBottomAnchor(filterFormPane, 0d);
        filterPane = new AnchorPane(filterTypePane, filterFormPane);
        pane.getItems().add(filterPane);
        populateFilterPane(beanPropertiesTree.getRoot());
    }
    
    protected Callback<ListView<BusinessBeanDescription>, ListCell<BusinessBeanDescription>>
            createFilterTypeCellFactory() {
        return new Callback<ListView<BusinessBeanDescription>, ListCell<BusinessBeanDescription>>() {

            @Override
            public ListCell<BusinessBeanDescription> call(
                    ListView<BusinessBeanDescription> view) {
                return new BusinessDescriptionListCell<>();
            }
            
        };
    }
    
    protected String createSelectedPropertyPath(TreeItem<BusinessDescription<?>> selectedSubject) {
        TreeItem<BusinessDescription<?>> item = selectedSubject;
        if(item.getValue() instanceof BusinessBeanDescription) {
            return null;
        }
        BusinessPropertyDescription propDesc = (BusinessPropertyDescription)item.getValue();
        String path = propDesc.getPropertyName();
        item = item.getParent();
        while(!(item.getValue() instanceof BusinessBeanDescription)) {
            propDesc = (BusinessPropertyDescription)item.getValue();
            path = propDesc.getPropertyName() + "." + path;
        }
        return path;
    }

    @SuppressWarnings("unchecked")
    protected void populateFilterPane(TreeItem<BusinessDescription<?>> selectedSubject) {
        BusinessDescription<?> filterSubjectsDescription = selectedSubject.getValue();
        LOG.debug("Populating filter pane for ...");
        LOG.debug(filterSubjectsDescription);
        Class<? extends FilterData>[] possibleFilters = null;
        boolean useFirstFilterAsDefault = false;
        if(filterSubjectsDescription instanceof BusinessBeanDescription) {
            BusinessBean meta = ((BusinessBeanDescription)filterSubjectsDescription).getMetadata();
            possibleFilters = meta.possibleFilters();
            useFirstFilterAsDefault = meta.useFirstFilterAsDefault();
        } else if(filterSubjectsDescription instanceof BusinessPropertyDescription) {
            BusinessProperty meta = ((BusinessPropertyDescription)filterSubjectsDescription)
                    .getMetadata();
            possibleFilters = meta.possibleFilters();
            useFirstFilterAsDefault = meta.useFirstFilterAsDefault();
        }
        
        if(possibleFilters == null || possibleFilters.length == 0) {
            LOG.debug("... subject not filtrable");
            filterFormPane.setContent(selectPropertyPlaceholder);
            filterTypeField.getItems().clear();
            filterTypePane.setVisible(false);
        } else {
            if(!useFirstFilterAsDefault) {
                LOG.debug("... filtrable but without default filter");
                filterFormPane.setContent(selectFilterTypePlaceholder);
                filterTypeField.getItems().clear();
                filterTypePane.setVisible(false);
            } else {
                LOG.debug("... filtrable and with default filter");
                filterTypeField.getItems().setAll(BusinessDescriptionIntrospector.getInstance()
                        .beanDescriptions(possibleFilters));
                filterTypeField.getSelectionModel().select(0);
                Class<?> selectedFilterClass = filterTypeField.getSelectionModel()
                        .getSelectedItem().getBeanClass();
                filterTypePane.setVisible(true);
                setFilterForm((Class<? extends FilterData>)selectedFilterClass, selectedSubject);
            }
        }
    }
    
    protected void setFilterForm(Class<? extends FilterData> filterClass,
            TreeItem<BusinessDescription<?>> selectedSubject) {
        String propertyPath = createSelectedPropertyPath(selectedSubject);
        BusinessDescription<?> filterSubjectsDescription = selectedSubject.getValue();
        SingleFilterFormFactory<?> factory = getSkinnable().getFilterFormFactories().getOrThrow(
                filterClass);
        ItemForm<? extends FilterData> form = factory.create(propertyPath,
                filterSubjectsDescription, getSkinnable().getFilterOwner());
        filterFormPane.setContent(form);
        getSkinnable().setSelectedFilterFrom(form);
    }
    
    public void reset() {
        TreeItem<BusinessDescription<?>> selectedItem = beanPropertiesTree.getSelectionModel()
                .getSelectedItem();
        if(selectedItem.getValue().equals(beanPropertiesTree.getRoot())) {
            populateFilterPane(selectedItem);
        } else {
            beanPropertiesTree.getSelectionModel().select(beanPropertiesTree.getRoot());
        }
    }
    
    /**
     * TODO
     */
    public SplitPane getPane() {
        return pane;
    }
}
