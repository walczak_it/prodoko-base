package com.prodoko.base.client.conf.security;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.TitledPane;
import javafx.util.Callback;

import com.prodoko.base.client.beans.BusinessControlsUtil;
import com.prodoko.base.client.beans.SectionedBeanFormSkin;
import com.prodoko.base.client.util.BundleLabelFactory;
import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.fx.GridPaneLayouter;
import com.prodoko.fx.control.LabledControlPane;
import com.prodoko.fx.control.form.FieldsetGrid;
import com.prodoko.fx.control.form.validation.PasswordTwiceValidationHandler;
import com.prodoko.fx.validation.ValidationResult;

public class UserLoginFormSkin<C extends UserLoginForm<T>, T extends UserLogin>
        extends SectionedBeanFormSkin<C, T> {
    
    private LabledControlPane<TextInputControl> usernameField;
    private LabledControlPane<TextInputControl> newPasswordField;
    private LabledControlPane<TextInputControl> newPasswordAgainField;

    public UserLoginFormSkin(C form) {
        super(form);
        this.usernameField = createUsernameField();
        this.newPasswordField = createNewPasswordField();
        this.newPasswordAgainField = createNewPasswordAgainField();
        addHeaderScetionFields();
        getSkinnable().getValidationHandlers().add(createNewPasswordValidationHandler());
    }

    protected LabledControlPane<TextInputControl> createUsernameField() {
        return labelAndEnsyncItemsPropertyTextControl("username", new TextField());
    }
    
    protected LabledControlPane<TextInputControl> createNewPasswordField() {
        LabledControlPane<TextInputControl> newPassowrdField
                = new LabledControlPane<TextInputControl>(
                        new Label(message(UserLoginFormSkin.class, "newPasswordField")),
                        new PasswordField());
        getSkinnable().newPasswordProperty().bindBidirectional(
                newPassowrdField.getField().textProperty());
        return newPassowrdField;
    }
    
    protected LabledControlPane<TextInputControl> createNewPasswordAgainField() {
        LabledControlPane<TextInputControl> newPassowrdField
                = new LabledControlPane<TextInputControl>(
                        new Label(message(UserLoginFormSkin.class, "newPasswordAgainField")),
                        new PasswordField());
        return newPassowrdField;
    }
    
    protected void addHeaderScetionFields() {
        new GridPaneLayouter(getHeaderSectionFieldset())
            .add(usernameField).skip().skip().add(newPasswordField)
            .skip().skip().skip().add(newPasswordAgainField);
    }
    
    protected Callback<Void, ValidationResult> createNewPasswordValidationHandler() {
        return new PasswordTwiceValidationHandler(getValidationPresenter(),
                newPasswordField.getField(), newPasswordAgainField.getField(),
                message(UserLoginFormSkin.class, "newPasswordsNotEqual"));
    }

    /**
     * TODO
     */
    protected LabledControlPane<TextInputControl> getUsernameField() {
        return usernameField;
    }

    /**
     * TODO
     */
    protected LabledControlPane<TextInputControl> getNewPasswordField() {
        return newPasswordField;
    }

    /**
     * TODO
     */
    protected LabledControlPane<TextInputControl> getNewPasswordAgainField() {
        return newPasswordAgainField;
    }
}
