package com.prodoko.base.client.beans.filter;

import com.prodoko.base.model.beans.BusinessDescription;
import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.fx.control.form.ItemForm;

public interface SingleFilterFormFactory<T extends FilterData> extends ClassSupporter {

    @Override
    public Class<T> getSupportedClass();
    public ItemForm<T> create(String propertyPath,
            BusinessDescription<?> filteredSubjectsDescription,
            BeansSubsetPresentation filterOwner);
}
