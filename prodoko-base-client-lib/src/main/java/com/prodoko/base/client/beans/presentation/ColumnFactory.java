package com.prodoko.base.client.beans.presentation;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumnBase;
import javafx.scene.control.TreeTableColumn;
import javafx.util.Callback;

import com.prodoko.base.client.beans.filter.FiltrableTableColumn;
import com.prodoko.base.client.beans.filter.SingleFilterFormFactory;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.base.model.beans.presentation.PropertyPresentation;
import com.prodoko.base.model.beans.presentation.TypePresentation;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.base.util.ClassSupportersMap;

public abstract class ColumnFactory<T> implements ClassSupporter {
    
    private ClassSupportersMap<SingleFilterFormFactory<?>> filterFormFactories;
    
    public ColumnFactory() { }
    
    public ColumnFactory(ClassSupportersMap<SingleFilterFormFactory<?>> filterFormFactories) {
        super();
        this.filterFormFactories = filterFormFactories;
    }

    /**
     * TODO
     */
    public ClassSupportersMap<SingleFilterFormFactory<?>> getFilterFormFactories() {
        return filterFormFactories;
    }

    public <S> TableColumn<S, T> create(PropertyPresentation presentation) {
        assertSupportedPresentation(presentation);
        Class<? extends FilterData> quickFilterClass = defaultFilterClass(presentation);
        String columnTitle = presentation.getPropertyDescription().getLocalised().getDisplayName();
        TableColumn<S, T> column;
        if(quickFilterClass != null && filterFormFactories != null) {
            SingleFilterFormFactory<?> factory = filterFormFactories.getOrThrow(quickFilterClass);
            column = new FiltrableTableColumn<>(factory, columnTitle);
        } else {
            column = new TableColumn<S, T>(columnTitle);
        }
        column.setCellValueFactory(this.<S>createCellValueFactory(presentation));
        setupColumn(column, presentation);
        return column;
    }
    
    public <S> TreeTableColumn<S, T> createForTreeTable(PropertyPresentation presentation) {
        assertSupportedPresentation(presentation);
        TreeTableColumn<S, T> column = new TreeTableColumn<S, T>(presentation
                .getPropertyDescription().getLocalised().getDisplayName());
        column.setCellValueFactory(this.<S>createTreeCellValueFactory(presentation));
        setupColumn(column, presentation);
        return column;
    }
    
    protected void assertSupportedPresentation(PropertyPresentation presentation) {
        Class<? extends TypePresentation> presentationClass = presentation
                .getPropertiesTypePresentation().getClass();
        if(!(getSupportedClass().equals(presentationClass))) {
            throw new IllegalArgumentException("This factory supports "
                    + getSupportedClass().getName() + ", not "
                    + presentationClass.getName());
        }
    }
    
    protected Class<? extends FilterData> defaultFilterClass(PropertyPresentation presentation) {
        BusinessProperty meta = presentation.getPropertyDescription().getMetadata();
        Class<? extends FilterData>[] possibleFilters = meta.possibleFilters();
        if(possibleFilters.length != 0 && meta.useFirstFilterAsDefault()) {
            return possibleFilters[0];
        } else {
            return null;
        }
    }
    
    public abstract <S> Callback<TableColumn.CellDataFeatures<S,T>,ObservableValue<T>>
        createCellValueFactory(PropertyPresentation presentation);
    
    public abstract <S> Callback<TreeTableColumn.CellDataFeatures<S,T>,ObservableValue<T>>
        createTreeCellValueFactory(PropertyPresentation presentation);
    
    public abstract <S> void setupColumn(TableColumnBase<S, T> column,
            PropertyPresentation presentation);
    
    public abstract Class<? extends TypePresentation> getSupportedClass();
}
