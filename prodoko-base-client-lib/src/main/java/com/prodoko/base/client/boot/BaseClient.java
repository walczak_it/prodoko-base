package com.prodoko.base.client.boot;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Map;
import java.util.Properties;

import javafx.application.Application;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.controlsfx.dialog.Dialogs;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import org.springframework.util.StringUtils;

import com.prodoko.base.client.GlobalStylesheets;
import com.prodoko.base.client.conf.security.CurrentUserCacheService;
import com.prodoko.base.model.BaseSystemInfo;
import com.prodoko.base.model.conf.security.SecurityService;
import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.fx.control.workbench.Workbench;
import com.prodoko.remoting.hessian.HessianAccessPropertyNames;
import com.prodoko.remoting.hessian.HessianAccessRegistrar;
import com.prodoko.remoting.support.ServiceInterfaceMetadata;

public abstract class BaseClient extends Application {
    
    private static final Log LOG = LogFactory.getLog(BaseClient.class);
    
    private Properties clientProperties;
    
    private Stage primaryStage;
    
    protected Class<?> getRootConfig() {
        return BaseClientConfig.class;
    }
    
    protected BaseSystemInfo getSystemInfo() {
        return new BaseSystemInfo();
    }
    
    protected GlobalStylesheets getGlobalStylesheets() {
        return new GlobalStylesheets();
    }

    /**
     * TODO
     */
    public Properties getClientProperties() {
        return clientProperties;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        if(this.primaryStage != null) {
            throw new IllegalStateException("Client app already started.");
        }
        this.primaryStage = primaryStage;
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                Dialogs.create().showExceptionInNewWindow(e);
                //LOG.error(e);
            }
        });
        
        primaryStage.setTitle(getSystemInfo().getSystemName());

        initializeClientProperties();
        
        if(needsStartParametersFromUser()) {
            startParametersScene();
        }
        else {
            UserLogin userLogin = login(getClientProperties());
            startContextAndAppScene(userLogin);
        }
        
        primaryStage.show();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public UserLogin login(Properties loginProperties) {
        
        HessianProxyFactoryBean proxyFactory = new HessianProxyFactoryBean();
        Class<?> securityServiceClass = SecurityService.class; 
        proxyFactory.setServiceInterface(securityServiceClass);
        String serviceRemoteName = ServiceInterfaceMetadata.remoteName(
                securityServiceClass);
        String serviceUrl = HessianAccessRegistrar.serviceUrl(
                (String) loginProperties.getProperty(
                        HessianAccessPropertyNames.HOST.getPropertyName()),
                Integer.parseInt(loginProperties.getProperty(
                        HessianAccessPropertyNames.PORT.getPropertyName())),
                null, false, serviceRemoteName);
        proxyFactory.setServiceUrl(serviceUrl);
        proxyFactory.setUsername(loginProperties.getProperty(
                HessianAccessPropertyNames.USERNAME.getPropertyName()));
        proxyFactory.setPassword(loginProperties.getProperty(
                HessianAccessPropertyNames.PASSWORD.getPropertyName()));
        proxyFactory.setDebug(true);
        proxyFactory.setConnectTimeout(2000);
        proxyFactory.afterPropertiesSet();
        SecurityService securityService
                = (SecurityService) proxyFactory.getObject();
        return securityService.getYourUserLogin();
    }
    
    protected void initializeClientProperties() throws IOException {
        
        Parameters appParams = getParameters();
        clientProperties = new Properties();
        File clientFileProperties = new File("prodoko.client.properties");
        if(clientFileProperties.canRead()) {
            clientProperties.load(new FileReader(clientFileProperties));
        }
        Map<String, String> namedAppParams = appParams.getNamed();
        clientProperties.putAll(namedAppParams);
    }
    
    protected Boolean needsStartParametersFromUser() {
        return !clientProperties.containsKey(
                HessianAccessPropertyNames.PASSWORD);
    }
    
    protected void startParametersScene()
            throws IOException {
        final LoginForm loginForm = new LoginForm(clientProperties, new Image(
                getSystemInfo().getSystemsLogoPath()));
        loginForm.initLoginService(new Service<UserLogin>() {

            @Override
            protected Task<UserLogin> createTask() {
                return new Task<UserLogin>() {
                    @Override
                    protected UserLogin call() throws Exception {
                        updateMessage("Logowanie ...");
                        loginForm.updateItem();
                        UserLogin ul = login(loginForm.getItem());
                        updateMessage("Zalogowano pomyślnie");
                        return ul;
                    }
                    
                    @Override
                    protected void succeeded() {
                        super.succeeded();
                        getClientProperties().putAll(loginForm.getItem());
                        startContextAndAppScene(this.getValue());
                    }
                };
            }
            
        });
        Scene loginScene = new Scene(loginForm);
        loginScene.getStylesheets().addAll(getGlobalStylesheets());
        getPrimaryStage().setScene(loginScene);
    }

    protected void startContextAndAppScene(UserLogin userLogin) {
        
        AnnotationConfigApplicationContext rootContext
            = new AnnotationConfigApplicationContext();
        AbstractBeanDefinition currentUserCacheServiceDef = BeanDefinitionBuilder
                .genericBeanDefinition(CurrentUserCacheService.class)
                .addPropertyValue("userLogin", userLogin)
                .setScope("singleton")
                .getBeanDefinition();
        rootContext.registerBeanDefinition("currentUserCacheService", currentUserCacheServiceDef);
        rootContext.getEnvironment().getPropertySources().addFirst(
                new PropertiesPropertySource("prodoko.client", clientProperties));
        
        String singleControlBeanName = (String)getClientProperties().get(
                "prodoko.client.singleControlBeanName");
        rootContext.register(getRootConfig());
        
        rootContext.refresh();
        
        if(StringUtils.isEmpty(singleControlBeanName)) {
            Workbench workbench= rootContext.getBean(Workbench.class);
            startWorkbenchScene(workbench);
        } else {
            Control singleControl = rootContext.getBean(singleControlBeanName,
                    Control.class);
            startSingleControlScene(singleControl);
        }
    }

    protected void startWorkbenchScene(Workbench workbench) {
        Scene workbenchScene = new Scene(workbench);
        workbenchScene.getStylesheets().addAll(getGlobalStylesheets());
        getPrimaryStage().setMaximized(true);
        getPrimaryStage().setScene(workbenchScene);
    }

    protected void startSingleControlScene(Control singleControl) {
        // TODO Auto-generated method stub
        
    }
}
