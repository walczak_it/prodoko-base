package com.prodoko.base.client.basic.legalentities;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.scene.control.Skin;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.BeanEntityForm;
import com.prodoko.base.model.basic.legalentities.Company;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CompanyForm<T extends Company> extends LegalEntityForm<T> {
    
    private final static String DEFAULT_STYLE_CLASS = "company-form";

    public CompanyForm() {
        super();
        construct();
    }
    
    public CompanyForm(T item) {
        super(item);
        construct();
    }
    
    private void construct() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new CompanyFormSkin<CompanyForm<T>, T>(this);
    }
}
