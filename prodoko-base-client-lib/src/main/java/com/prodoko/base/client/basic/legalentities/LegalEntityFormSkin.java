package com.prodoko.base.client.basic.legalentities;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.TitledPane;

import com.prodoko.base.client.beans.BeanEntityLookupField;
import com.prodoko.base.client.beans.SectionedBeanFormSkin;
import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.fx.GridPaneLayouter;
import com.prodoko.fx.control.ExpTextArea;
import com.prodoko.fx.control.LabledControlPane;
import com.prodoko.fx.control.ResTextField;
import com.prodoko.fx.control.form.FieldsetGrid;
import com.prodoko.fx.control.LookupField;;

public abstract class LegalEntityFormSkin<C extends LegalEntityForm<T>, T extends LegalEntity>
        extends SectionedBeanFormSkin<C, T> {
    
    private FieldsetGrid headerSectionFieldset;
    private LabledControlPane<TextInputControl> codeField;
    private LabledControlPane<TextInputControl> nameField;
    private LabledControlPane<LookupField<LegalEntityType>> mainTypeField;

    public LegalEntityFormSkin(C form) {
        super(form);
        headerSectionFieldset = createAndSetupHeaderSectionFieldset();
        codeField = createCodeField();
        nameField = createNameField();
        mainTypeField = createMainTypeField();
        addHeaderSectionFields();
    }
    
    protected FieldsetGrid createAndSetupHeaderSectionFieldset() {
        FieldsetGrid headerSectionFieldset = new FieldsetGrid();
        getHeaderSectionPane().setContent(headerSectionFieldset);
        return headerSectionFieldset;
    }
    
    protected LabledControlPane<TextInputControl> createCodeField() {
        return labelAndEnsyncItemsPropertyTextControl("code", new ResTextField());
    }
    
    protected LabledControlPane<TextInputControl> createNameField() {
        return labelAndEnsyncItemsPropertyTextControl("name", new ExpTextArea());
    }
    
    protected LabledControlPane<LookupField<LegalEntityType>> createMainTypeField() {
        return labelAndEnsyncItemsPropertyLookupField("mainDynamicClass",
                new BeanEntityLookupField<>(LegalEntityType.class, 
                        getSkinnable().getTypeFinderService()));
    }
    
    protected void addHeaderSectionFields() {
        new GridPaneLayouter(headerSectionFieldset)
                .add(codeField).add(nameField, 2).add(mainTypeField);
    }

    /**
     * TODO
     */
    protected FieldsetGrid getHeaderSectionFieldset() {
        return headerSectionFieldset;
    }

    /**
     * TODO
     */
    protected LabledControlPane<TextInputControl> getCodeField() {
        return codeField;
    }

    /**
     * TODO
     */
    protected LabledControlPane<TextInputControl> getNameField() {
        return nameField;
    }
}
