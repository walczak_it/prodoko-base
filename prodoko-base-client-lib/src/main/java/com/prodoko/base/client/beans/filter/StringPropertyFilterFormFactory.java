package com.prodoko.base.client.beans.filter;

import com.prodoko.base.model.beans.BusinessDescription;
import com.prodoko.base.model.beans.BusinessPropertyDescription;
import com.prodoko.base.model.beans.filter.StringPropertyFilter;
import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;
import com.prodoko.fx.control.form.ItemForm;

public class StringPropertyFilterFormFactory
        implements SingleFilterFormFactory<StringPropertyFilter>{

    @Override
    public Class<StringPropertyFilter> getSupportedClass() {
        return StringPropertyFilter.class;
    }

    @Override
    public ItemForm<StringPropertyFilter> create(
            String propertyPath,
            BusinessDescription<?> filteredSubjectsDescription,
            BeansSubsetPresentation filterOwner) {
        BusinessPropertyDescription filteredPropertyDescription
                = (BusinessPropertyDescription) filteredSubjectsDescription;
        return new StringPropertyFilterForm(filteredPropertyDescription, propertyPath, filterOwner);
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }

}
