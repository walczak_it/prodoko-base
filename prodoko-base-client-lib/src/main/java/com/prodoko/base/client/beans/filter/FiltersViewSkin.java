package com.prodoko.base.client.beans.filter;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.util.Callback;

import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.fx.control.LabelCell;
import com.prodoko.fx.control.LabelsView;
import com.prodoko.fx.control.LabelsViewSkin;

public class FiltersViewSkin extends LabelsViewSkin<FiltersView, FilterData> {

    public FiltersViewSkin(FiltersView control) {
        super(control);
        
        buildAddFilterButton();
    }

    protected void buildAddFilterButton() {
        Button addFilterButton = new Button(message(FiltersViewSkin.class, "addFilterButton"));
        addFilterButton.onActionProperty().set(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                showFilterAdder();
            }
        });
        getSkinnable().setAfterLastItem(addFilterButton);
    }
    
    public void showFilterAdder() {
        getSkinnable().getAdder().showAsDialog();
        getSkinnable().getAdder().requestLayout();
    }
    
    @Override
    protected Callback<LabelsView<FilterData>, LabelCell<FilterData>> createDefaultCellFactory() {
        return new Callback<LabelsView<FilterData>, LabelCell<FilterData>>() {

            @Override
            public LabelCell<FilterData> call(LabelsView<FilterData> view) {
                return new FilterCell(view);
            }
            
        };
    }
    
    protected static class FilterCell extends LabelCell<FilterData> {

        public FilterCell(LabelsView<FilterData> view) {
            super(view);
            // TODO Auto-generated constructor stub
        }

        @Override
        protected void updateItem(FilterData value, boolean empty) {
            super.updateItem(value, empty);
            
            if(value != null) {
                setText(value.toDisplayString());
            }
        }
    }
}
