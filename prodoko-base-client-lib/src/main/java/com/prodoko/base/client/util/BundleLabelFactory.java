package com.prodoko.base.client.util;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.scene.control.Label;

public class BundleLabelFactory {

    private Class<?> suffixClass;

    public BundleLabelFactory(Class<?> suffixClass) {
        super();
        this.suffixClass = suffixClass;
    }
    
    /**
     * TODO
     */
    public Class<?> getSuffixClass() {
        return suffixClass;
    }

    /**
     * @see #getSuffixClass
     */
    public void setSuffixClass(Class<?> suffixClass) {
        this.suffixClass = suffixClass;
    }

    public Label create(String keyMiddix) {
        return new Label(message(suffixClass, keyMiddix));
    }
}
