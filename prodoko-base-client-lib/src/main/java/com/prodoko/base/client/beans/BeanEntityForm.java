package com.prodoko.base.client.beans;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.Arrays;
import java.util.List;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

import javax.annotation.PostConstruct;

import org.controlsfx.control.action.Action;
import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.base.model.entities.EntityFinderService;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.fx.action.ServiceAction;
import com.prodoko.fx.concurrent.SimpleService;

public abstract class BeanEntityForm<T extends StandardEntity>
        extends BeanForm<T> {
    
    private EntityFinderService<? super T> finderService;
    private EntityEditorService<? super T> editorService;
    private ServiceAction saveAction;
    private ServiceAction removeAction;
    private ServiceAction refreshAction;

    public BeanEntityForm() {
        super();
    }

    public BeanEntityForm(T item) {
        super(item);
    }
    
    /**
     * TODO
     */
    public EntityFinderService<? super T> getFinderService() {
        return finderService;
    }

    /**
     * @see #getFinderService
     */
    @Autowired
    public void initFinderService(EntityFinderService<? super T> finderService) {
        this.finderService = finderService;
    }

    @Autowired
    public void initEditorService(EntityEditorService<? super T> editorService) {
        this.editorService = editorService;
    }
    
    /**
     * TODO
     */
    public EntityEditorService<? super T> getEditorService() {
        return editorService;
    }

    @PostConstruct
    public void postConstruct() {
        saveAction = createSaveAction();
        removeAction = createRemoveAction();
        refreshAction = createRefreshAction();
        getActions().addAll(createDeafultBeanCrudFromAction());
    }

    protected ServiceAction createSaveAction() {
        return new UpdatedItemServiceAction(new SimpleService<T>(SaveTask.class, this),
                message(BeanEntityForm.class, "saveAction"));
    }

    protected ServiceAction createRemoveAction() {
        return new UpdatedItemServiceAction(new RemoveMethodService(),
                message(BeanEntityForm.class, "removeAction"));
    }

    protected ServiceAction createRefreshAction() {
        return new UpdatedItemServiceAction(new RefreshMethodService(),
                message(BeanEntityForm.class, "refreshAction"));
    }
    
    final protected List<Action> createDeafultBeanCrudFromAction() {
        return Arrays.asList(getSaveAction(), getRemoveAction(), getRefreshAction());
    }

    /**
     * TODO
     */
    public ServiceAction getSaveAction() {
        return saveAction;
    }

    /**
     * TODO
     */
    public ServiceAction getRemoveAction() {
        return removeAction;
    }

    /**
     * TODO
     */
    public Action getRefreshAction() {
        return refreshAction;
    }
    
    protected class SaveTask extends Task<T> {
        
        @SuppressWarnings("unchecked")
        @Override
        protected T call() throws Exception {
            return (T) editorService.save(getItem());
        }
        
        @Override
        protected void succeeded() {
            super.succeeded();
            setItem(getValue());
        }
    }
    
    protected class RemoveMethodService extends Service<Void> {

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {

                @Override
                protected Void call() throws Exception {
                    editorService.remove(getItem().getId());
                    return null;
                }
                
                @Override
                protected void succeeded() {
                    super.succeeded();
                    close();
                }
            };
        }
    }
    
    protected class RefreshMethodService extends Service<T> {

        @Override
        protected Task<T> createTask() {
            return new Task<T>() {

                @SuppressWarnings("unchecked")
                @Override
                protected T call() throws Exception {
                    return (T) finderService.find(getItem().getId());
                }
                
                @Override
                protected void succeeded() {
                    super.succeeded();
                    setItem(getValue());
                }
            };
        }
        
    }
}
