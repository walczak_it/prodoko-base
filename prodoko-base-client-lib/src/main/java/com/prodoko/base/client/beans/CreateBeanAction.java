package com.prodoko.base.client.beans;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.HashMap;
import java.util.Map;

import javafx.event.ActionEvent;

import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionGroup;

import com.prodoko.base.model.beans.BusinessBeanDescription;
import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.fx.action.PseudoAcion;
import com.prodoko.fx.control.BreadcrumbsPaneAware;
import com.prodoko.fx.control.form.ItemForm;

public class CreateBeanAction<T> extends AbstractAction {
    
    private BreadcrumbsPaneAware paneAware;
    private BeanFormFactory<? extends T> formFactory;
    
    public CreateBeanAction(BreadcrumbsPaneAware paneAware,
            BeanFormFactory<? extends T> formFactory) {
        this(message(CreateBeanAction.class), paneAware, formFactory);
    }

    public CreateBeanAction(String text, BreadcrumbsPaneAware paneAware,
            BeanFormFactory<? extends T> formFactory) {
        super(text);
        this.paneAware = paneAware;
        this.formFactory = formFactory;
    }

    /**
     * TODO
     */
    public BreadcrumbsPaneAware getPaneAware() {
        return paneAware;
    }

    /**
     * TODO
     */
    public BeanFormFactory<? extends T> getFormFactory() {
        return formFactory;
    }

    @Override
    public void execute(ActionEvent ae) {
        ItemForm<? extends T> form = formFactory.createWithNewBean();
        getPaneAware().getBreadcrumbsPane().openAfter(
                getPaneAware().getBreadcrumbsIndex(), form);
    }
    
    public static <T> Map<Class<?>, Action> createCreateBeanActionsByClass(BreadcrumbsPaneAware owner,
            ClassSupportersMap<BeanFormFactory<? extends T>> beanFormFactories) {
        Map<Class<?>, Action> actions = new HashMap<>(beanFormFactories.size());
        for(BeanFormFactory<? extends T> formFactory : beanFormFactories.values()) {
            BusinessBeanDescription desc = BusinessDescriptionIntrospector.getInstance()
                    .beanDescription(formFactory.getSupportedClass());
            Action action = new CreateBeanAction<T>(desc.getLocalised().getDisplayName(),
                    owner, formFactory);
            actions.put(formFactory.getSupportedClass(), action);
        }
        return actions;
    }
    
    public static <T> Action createCreateBeanRootAction(BreadcrumbsPaneAware owner,
            ClassSupportersMap<BeanFormFactory<? extends T>> beanFormFactories) {
        Map<Class<?>, Action> actions = createCreateBeanActionsByClass(owner, beanFormFactories);
        String text = message(CreateBeanAction.class);
        if(actions.size() == 1) {
            Action realAction = actions.values().iterator().next();
            return new PseudoAcion(text, realAction);
        } else {
            return new ActionGroup(text, actions.values());
        }
    }
}