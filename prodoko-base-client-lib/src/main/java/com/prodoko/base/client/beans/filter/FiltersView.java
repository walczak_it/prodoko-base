package com.prodoko.base.client.beans.filter;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Skin;

import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.fx.control.LabelsView;

public class FiltersView extends LabelsView<FilterData> {
    
    private static final String DEFAULT_STYLE_CLASS = "filters-view";
    
    private FilterAdder adder;

    public FiltersView(FilterAdder adder) {
        super();
        buildFiltersView(adder);
    }

    public FiltersView(FilterAdder adder, ObservableList<FilterData> items) {
        super(items);
        buildFiltersView(adder);
    }
    
    protected void buildFiltersView(FilterAdder adder) {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.adder = adder;
        adder.setOnAddFilter(new AddFilterToViewHandler(this));
    }
    
    /**
     * TODO
     */
    public FilterAdder getAdder() {
        return adder;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new FiltersViewSkin(this);
    }
    
    public static class AddFilterToViewHandler implements EventHandler<SaveFilterEvent<?>> {
        
        private FiltersView view;

        public AddFilterToViewHandler(FiltersView view) {
            super();
            this.view = view;
        }

        @Override
        public void handle(SaveFilterEvent<?> event) {
            view.getItems().add(event.getFilter());
        }
        
    }
}
