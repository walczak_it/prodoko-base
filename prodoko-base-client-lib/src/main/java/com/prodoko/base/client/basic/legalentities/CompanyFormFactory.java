package com.prodoko.base.client.basic.legalentities;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.BeanForm;
import com.prodoko.base.client.beans.BeanFormFactory;
import com.prodoko.base.model.basic.legalentities.Company;

@Component
public class CompanyFormFactory implements BeanFormFactory<Company> {
    
    private ObjectFactory<CompanyForm<Company>> objectFactory;

    @Autowired
    public void setObjectFactory(
            ObjectFactory<CompanyForm<Company>> objectFactory) {
        this.objectFactory = objectFactory;
    }

    @Override
    public Class<Company> getSupportedClass() {
        return Company.class;
    }

    @Override
    public BeanForm<Company> createWithNewBean() {
        return createWith(new Company());
    }

    @Override
    public BeanForm<Company> createWith(Company bean) {
        BeanForm<Company> form = objectFactory.getObject();
        form.setItem(bean);
        return form;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }
}
