package com.prodoko.base.client.beans.cells;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import com.prodoko.base.model.beans.BusinessDescription;

public class BusinessDescriptionListCell<T extends BusinessDescription<?>> extends ListCell<T> {

    @Override
    protected void updateItem(T value, boolean empty) {
        super.updateItem(value, empty);
        if(value != null) {
            setText(value.getLocalised().getDisplayName());
        }
    }
}
