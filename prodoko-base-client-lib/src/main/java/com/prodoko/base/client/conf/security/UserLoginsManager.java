package com.prodoko.base.client.conf.security;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.Arrays;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.browser.BeansManager;
import com.prodoko.base.model.beans.presentation.NamedBeansSubsetPresentation;
import com.prodoko.base.model.beans.presentation.PropertyPresentation;
import com.prodoko.base.model.beans.presentation.SimpleStringPresentation;
import com.prodoko.base.model.conf.security.UserLogin;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserLoginsManager extends BeansManager<UserLogin> {
    
    private static final String DEFAULT_STYLE_CLASS = "user-logins-browser";

    public UserLoginsManager() {
        super(message(UserLoginsManager.class));
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }

    @Override
    public NamedBeansSubsetPresentation getStandardBeansPresentation() {
        NamedBeansSubsetPresentation std = createStandardBeansPresentationStub();
        std.getPresentedProperties().addAll(Arrays.asList(
                new PropertyPresentation(std, UserLogin.class, "username",
                        new SimpleStringPresentation())
        ));
        return std;
    }

}
