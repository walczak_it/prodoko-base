package com.prodoko.base.client.basic.legalentities.types;

import com.prodoko.base.client.beans.dynamic.DynamicClassFormSkin;
import com.prodoko.base.client.beans.dynamic.MainDynamicClassFormSkin;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;

public class LegalEntityTypeFormSkin<
            C extends LegalEntityTypeForm<T>,
            T extends LegalEntityType>
        extends MainDynamicClassFormSkin<C, T, LegalEntityType> {

    public LegalEntityTypeFormSkin(C form) {
        super(form);
    }
}
