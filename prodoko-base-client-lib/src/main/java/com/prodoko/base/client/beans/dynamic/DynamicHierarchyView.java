package com.prodoko.base.client.beans.dynamic;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.scene.control.Skin;
import javafx.scene.control.TreeItem;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.control.action.Action;
import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.client.beans.BeanFormFactory;
import com.prodoko.base.model.beans.dynamic.HierarchyDto;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.fx.action.ServiceAction;
import com.prodoko.fx.concurrent.SimpleService;
import com.prodoko.fx.control.form.Form;
import com.prodoko.fx.control.form.ItemForm;

public abstract class DynamicHierarchyView<T extends MainDynamicClass> extends Form
        implements ClassSupporter {

    private static final Log LOG = LogFactory.getLog(DynamicHierarchyView.class);
    private static final String DEFAULT_STYLE_CLASS = "dynamic-hierarchy-viewer";

    private ClassSupportersMap<DynamicClassFormFactory<? extends T, T>> dynamicClassFormFactories;
    private MainDynamicClassFinderService<T> finderService;
    private ObjectProperty<TreeItem<T>> rootItem;
    private Action openAction;
    private Action refreshAction;
    private ObjectProperty<T> selectedItem;
    
    public DynamicHierarchyView() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.rootItem = new SimpleObjectProperty<>();
        this.selectedItem = new SimpleObjectProperty<>();
    }
    
    @Autowired
    public void initFinderService(MainDynamicClassFinderService<T> hierarchyService) {
        this.finderService = hierarchyService;
    }

    /**
     * TODO
     */
    public MainDynamicClassFinderService<T> getFinderService() {
        return finderService;
    }
    
    @Autowired
    public void initDynamicClassFormFactories(
            Collection<DynamicClassFormFactory<? extends T, T>> dynamicClassFormFactories) {
        this.dynamicClassFormFactories = new ClassSupportersMap<>(dynamicClassFormFactories);
    }
    
    @Override
    public Class<?> getSupportedClass() {
        return finderService.getSupportedClass();
    }
    
    @Override
    public boolean isSupportsSubclasses() {
        return finderService.isSupportsSubclasses();
    }

    /**
     * TODO
     */
    public ClassSupportersMap<DynamicClassFormFactory<? extends T, T>> getDynamicClassFormFactories() {
        return dynamicClassFormFactories;
    }

    public ObjectProperty<T> selectedItemProperty() {
        return selectedItem;
    }
    
    public T getSelectedItem() {
        return selectedItem.getValue();
    }
    
    public ObjectProperty<TreeItem<T>> rootItemProperty() {
        return rootItem;
    }
    
    public TreeItem<T> getRootItem() {
        return rootItem.getValue();
    }
    
    /**
     * TODO
     */
    public Action getOpenAction() {
        return openAction;
    }

    /**
     * TODO
     */
    public Action getRefreshAction() {
        return refreshAction;
    }

    @PostConstruct
    public void postContruct() {
        this.openAction = createOpenAction();
        this.refreshAction = createRefreshAction();
        getActions().addAll(createDefaultViewActions());
    }
    
    protected Action createOpenAction() {
        return new OpenAction();
    }
    
    protected Action createRefreshAction() {
        return new ServiceAction(new SimpleService<>(RefreshTask.class, this),
                message(DynamicHierarchyView.class, "refreshAction"));
    }
    
    final protected List<Action> createDefaultViewActions() {
        return Arrays.asList(getOpenAction(), getRefreshAction());
    }
    
    @Override
    protected Skin<?> createDefaultSkin() {
        return new DynamicHierarchyViewSkin<DynamicHierarchyView<T>, T>(this);
    }
    
    protected class OpenAction extends AbstractAction {
        
        public OpenAction() {
            super(message(OpenAction.class));
        }

        @Override
        public void execute(ActionEvent ae) {
            if(selectedItem.get() == null) {
                throw new IllegalStateException("An item must be selected");
            }
            getBreadcrumbsPane().openAfter(getBreadcrumbsIndex(), createForm(
                    selectedItem.getValue()));
        }

        @SuppressWarnings({ "rawtypes", "unchecked" })
        protected ItemForm createForm(T item) {
            BeanFormFactory factory = getDynamicClassFormFactories().getOrThrow(item.getClass());
            ItemForm form = factory.createWith(item);
            return form;
        }
    }
    
    public class RefreshTask extends Task<HierarchyDto<T>> {
        
        @Override
        protected HierarchyDto<T> call() throws Exception {
            HierarchyDto<T> dto = finderService.createHierarchyDto();
            return dto;
        }
        
        @Override
        protected void succeeded() {
            super.succeeded();
            TreeItem<T> newRootItem = createItemHierarchy(getValue());
            rootItem.set(newRootItem);
        }
        
        protected TreeItem<T> createItemHierarchy(HierarchyDto<T> dto) {
            TreeItem<T> item = new TreeItem<T>(dto.getMainDynamicClass());
            if(dto.isMostSuperclass()) {
                item.setExpanded(true);
            }
            for(HierarchyDto<T> subdto : dto.getSubclasses()) {
                TreeItem<T> subitem = createItemHierarchy(subdto);
                item.getChildren().add(subitem);
            }
            return item;
        }
    }
}
