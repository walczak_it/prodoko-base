package com.prodoko.base.client.beans.browser;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Skin;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.control.action.Action;
import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.client.beans.BeanFormFactory;
import com.prodoko.base.client.beans.filter.SingleFilterFormFactory;
import com.prodoko.base.client.beans.presentation.ColumnFactory;
import com.prodoko.base.client.beans.presentation.TablePresentationFactory;
import com.prodoko.base.client.beans.presentation.UniversalPresentation;
import com.prodoko.base.model.beans.browser.BeansBrowserService;
import com.prodoko.base.model.beans.filter.UniversalFilters;
import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;
import com.prodoko.base.model.beans.presentation.NamedBeansSubsetPresentation;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.fx.action.SimpleConcurrentAction;
import com.prodoko.fx.control.BreadcrumbsPane;
import com.prodoko.fx.control.BreadcrumbsPaneAware;
import com.prodoko.fx.control.form.Form;
import com.prodoko.fx.control.form.ItemForm;

public abstract class BeansBrowser<T> extends Form implements ClassSupporter {
    
    private static final Log LOG = LogFactory.getLog(BeansBrowser.class);
    private static final String DEFAULT_STYLE_CLASS = "beans-browser";
    
    private BeansBrowserService<T> browserService;
    private BeanLoadingService<T> beanLoadingService;
    private ObservableList<T> selectedItems;
    private ClassSupportersMap<SingleFilterFormFactory<?>> filterFormFactories;
    private ClassSupportersMap<BeanFormFactory<? extends T>> beanFormFactories;
    private TablePresentationFactory presentationFactory;
    private ListProperty<NamedBeansSubsetPresentation> beansPresentations;
    private ObjectProperty<BeansSubsetPresentation> currentPresentation;
    private Action refreshAction;
    private Action openAction;
    
    public BeansBrowser(String text) {
        super(text);
    }
    
    @Autowired
    public void initBeansBrowserService(BeansBrowserService<T> browserService) {
        this.browserService = browserService;
    }

    @Autowired
    public void initTablePresentationFactory(
            @UniversalPresentation ClassSupportersMap<ColumnFactory<?>> universalColumnFactories) {
        this.presentationFactory = new TablePresentationFactory(universalColumnFactories);
    }
    
    @Autowired
    public void initFilterFormFactories(
            @UniversalFilters ClassSupportersMap<SingleFilterFormFactory<?>>
                    universalFilterFormFactories) {
        this.filterFormFactories = universalFilterFormFactories;
    }
    
    @Autowired
    public void initBeanFormFactories(
            Collection<BeanFormFactory<? extends T>> formFactories) {
        LOG.info("Initializing bean form factories with:");
        LOG.info(formFactories);
        this.beanFormFactories = new ClassSupportersMap<>(formFactories);
    }
    
    @PostConstruct
    public void postConstruct() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.beanLoadingService = new BeanLoadingService(browserService);
        this.selectedItems = new SimpleListProperty<>();
        this.currentPresentation = new SimpleObjectProperty<>();
        this.currentPresentation.addListener(createCurrentPresentationChangeListener());
        this.refreshAction = createConstructAndExecuteRefreshAction();
        this.openAction = createOpenAction();
        setActions(FXCollections.observableArrayList(createDefaultBrowserActions()));
        currentPresentation.set(getStandardBeansPresentation().clone());
        beansPresentations = new SimpleListProperty<NamedBeansSubsetPresentation>(
                FXCollections.observableArrayList(getStandardBeansPresentation()));
    }
    
    @Override
    public Class<?> getSupportedClass() {
        return browserService.getSupportedClass();
    }
    
    @Override
    public boolean isSupportsSubclasses() {
        return browserService.isSupportsSubclasses();
    }
    
    protected Action createConstructAndExecuteRefreshAction() {
        RefreshAction refreshAction = new RefreshAction();
        refreshAction.execute(null);
        return refreshAction;
    }
    
    protected Action createOpenAction() {
        return new OpenAction();
    }
    
    protected ChangeListener<BeansSubsetPresentation> createCurrentPresentationChangeListener() {
        return new ChangeListener<BeansSubsetPresentation>() {

            @Override
            public void changed(
                    ObservableValue<? extends BeansSubsetPresentation> observable,
                    BeansSubsetPresentation oldValue, BeansSubsetPresentation newValue) {
                
                getDataSource().filtersProperty().set(FXCollections.observableList(newValue
                        .getFilters()));
            }
            
        };
    }

    public abstract NamedBeansSubsetPresentation getStandardBeansPresentation();
    
    protected NamedBeansSubsetPresentation createStandardBeansPresentationStub() {
        return new NamedBeansSubsetPresentation(browserService.getSupportedClass(),
                message(BeansBrowser.class, "standardBeansPresentation"));
    }

    /**
     * TODO
     */
    public BeanLoadingService<T> getDataSource() {
        return beanLoadingService;
    }
    
    public ObservableList<T> getSelectedItems() {
        return selectedItems;
    }
    
    public void setSelectedItems(ObservableList<T> value) {
        selectedItems = value;
    }
    
    /**
     * TODO
     */
    public ClassSupportersMap<SingleFilterFormFactory<?>> getFilterFormFactories() {
        return filterFormFactories;
    }

    /**
     * TODO
     */
    public ClassSupportersMap<BeanFormFactory<? extends T>> getBeanFormFactories() {
        return beanFormFactories;
    }

    /**
     * TODO
     */
    public TablePresentationFactory getPresentationFactory() {
        return presentationFactory;
    }
    
    public ListProperty<NamedBeansSubsetPresentation> beansPresentationsProperty() {
        return beansPresentations;
    }
    
    public ObservableList<NamedBeansSubsetPresentation> getBeansPresentations() {
        return beansPresentations.get();
    }
    
    public void setBeansPresentations(ObservableList<NamedBeansSubsetPresentation> value) {
        beansPresentations.set(value);
    }
    
    public ObjectProperty<BeansSubsetPresentation> currentPresentationProperty() {
        return currentPresentation;
    }
    
    /**
     * TODO
     */
    public Action getRefreshAction() {
        return refreshAction;
    }

    /**
     * @see #getRefreshAction
     */
    protected void initRefreshAction(Action refreshAction) {
        this.refreshAction = refreshAction;
    }

    /**
     * TODO
     */
    public Action getOpenAction() {
        return openAction;
    }

    /**
     * @see #getOpenAction
     */
    protected void initOpenAction(Action openAction) {
        this.openAction = openAction;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new BeansBrowserSkin<BeansBrowser<T>, T>(this);
    }

    final protected List<Action> createDefaultBrowserActions() {
        return Arrays.<Action>asList(getOpenAction(), getRefreshAction());
    }
    
    protected class RefreshAction extends AbstractAction {
        
        public RefreshAction() {
            super(message(RefreshAction.class));
        }

        @Override
        public void execute(ActionEvent ae) {
            beanLoadingService.clearLoaded();
            beanLoadingService.scheduleLoading();
        }
    }
    
    protected class OpenAction extends AbstractAction {
        
        public OpenAction() {
            super(message(OpenAction.class));
        }

        @Override
        public void execute(ActionEvent ae) {
            if(getSelectedItems().size() != 1) {
                throw new IllegalStateException("Open action should not be executen if selected "
                        + "items count != 1");
            }
            T item = getSelectedItems().get(0);
            getBreadcrumbsPane().openAfter(getBreadcrumbsIndex(), createForm(item));
        }

        @SuppressWarnings({ "rawtypes", "unchecked" })
        protected ItemForm createForm(T item) {
            BeanFormFactory factory = getBeanFormFactories().getOrThrow(item.getClass());
            ItemForm form = factory.createWith(item);
            return form;
        }
    }
    
    protected class DownloadAsCsvAction extends SimpleConcurrentAction<Void> {

        public DownloadAsCsvAction() {
            super(message(DownloadAsCsvAction.class));
        }

        @Override
        protected Void executeConcurrently() {
            // TODO Auto-generated method stub
            return null;
        }
        
    }
}
