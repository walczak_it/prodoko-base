package com.prodoko.base.client.beans.dynamic;

import static com.prodoko.base.util.BundleUtils.message;

import javafx.scene.control.SkinBase;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;

import com.prodoko.base.client.beans.browser.BeansManager;
import com.prodoko.base.client.beans.browser.BeansManagerSkin;
import com.prodoko.base.model.beans.dynamic.DynamicBean;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;

public class BeansManagerWithHierarchySkin<C extends BeansManagerWithHierarchy<T, D>,
            T extends DynamicBean, D extends MainDynamicClass>
        extends SkinBase<C> {
    
    private SplitPane splitPane;

    public BeansManagerWithHierarchySkin(C control) {
        super(control);
        this.splitPane = createSplitPane();
    }

    /**
     * TODO
     */
    protected SplitPane getHierarchyAndBeansPane() {
        return splitPane;
    }

    protected SplitPane createSplitPane() {
        SplitPane splitPane = new SplitPane();
        splitPane.getItems().addAll(getSkinnable().getHierarchyEditor(),
                getSkinnable().getBeansManager());
        getChildren().add(splitPane);
        splitPane.setDividerPositions(0.15d);
        return splitPane;
    }
}
