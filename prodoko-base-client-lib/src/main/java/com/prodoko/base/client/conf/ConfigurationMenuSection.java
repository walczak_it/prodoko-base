package com.prodoko.base.client.conf;

import static com.prodoko.base.util.BundleUtils.message;

import org.springframework.stereotype.Component;

import com.prodoko.fx.control.workbench.MenuSectionDefinition;

@Component
public class ConfigurationMenuSection extends MenuSectionDefinition {

    @Override
    public String getTitle() {
        return message(ConfigurationMenuSection.class);
    }
}
