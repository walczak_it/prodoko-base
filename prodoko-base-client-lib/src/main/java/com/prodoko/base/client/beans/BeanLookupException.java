package com.prodoko.base.client.beans;

public class BeanLookupException extends RuntimeException {

    public BeanLookupException() {
        super();
    }

    public BeanLookupException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BeanLookupException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeanLookupException(String message) {
        super(message);
    }

    public BeanLookupException(Throwable cause) {
        super(cause);
    }

}
