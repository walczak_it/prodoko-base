package com.prodoko.base.client.beans.presentation;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumnBase;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.util.Callback;

import com.prodoko.base.model.beans.presentation.PropertyPresentation;
import com.prodoko.base.model.beans.presentation.SimpleStringPresentation;
import com.prodoko.base.model.beans.presentation.TypePresentation;

public class SimpleStringColumnFactory extends ColumnFactory<String> {

    @Override
    public <S> Callback<CellDataFeatures<S, String>, ObservableValue<String>>
            createCellValueFactory(PropertyPresentation presentation) {
        return new PropertyValueFactory<>(presentation.getPropertyName());
    }

    @Override
    public <S> Callback<TreeTableColumn.CellDataFeatures<S, String>, ObservableValue<String>>
            createTreeCellValueFactory(PropertyPresentation presentation) {
        return new TreeItemPropertyValueFactory<>(presentation.getPropertyName());
    }
    
    @Override
    public Class<? extends TypePresentation> getSupportedClass() {
        return SimpleStringPresentation.class;
    }

    @Override
    public <S> void setupColumn(TableColumnBase<S, String> column,
            PropertyPresentation presentation) {
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }
}
