package com.prodoko.base.client.basic.legalentities;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.scene.Node;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.basic.BasicMenuSection;
import com.prodoko.fx.control.workbench.MenuNodeDefinition;
import com.prodoko.fx.control.workbench.MenuPositionDefinition;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class LegalEntitiesMenuPosition extends MenuPositionDefinition {
    
    private BasicMenuSection basicMenuSection;
    private ObjectFactory<LegalEntitiesManagerWithHierarchy>
            legalEntitiesManagerWithHierarchyFactory;

    @Override
    public MenuNodeDefinition getParent() {
        return basicMenuSection;
    }

    @Autowired
    public void setBasicMenuSection(BasicMenuSection basicMenuSection) {
        this.basicMenuSection = basicMenuSection;
    }

    @Autowired
    protected void setLegalEntitiesManagerWithHierarchyFactory(
            ObjectFactory<LegalEntitiesManagerWithHierarchy> legalEntitiesManagerWithHierarchyFactory) {
        this.legalEntitiesManagerWithHierarchyFactory = legalEntitiesManagerWithHierarchyFactory;
    }

    @Override
    public Node createContent() {
        return legalEntitiesManagerWithHierarchyFactory.getObject();
    }

    @Override
    public String getTitle() {
        return message(LegalEntitiesMenuPosition.class);
    }
}
