package com.prodoko.base.client.basic.legalentities;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.BeanForm;
import com.prodoko.base.client.beans.EntityBeanFromFactory;
import com.prodoko.base.model.basic.legalentities.Person;

@Component
public class PersonFormFactory extends EntityBeanFromFactory<Person> {
    
    private ObjectFactory<PersonForm<Person>> objectFactory;

    @Autowired
    public void setObjectFactory(
            ObjectFactory<PersonForm<Person>> objectFactory) {
        this.objectFactory = objectFactory;
    }

    @Override
    public Class<Person> getSupportedClass() {
        return Person.class;
    }

    @Override
    public BeanForm<Person> createWith(Person bean) {
        BeanForm<Person> form = objectFactory.getObject();
        form.setItem(bean);
        return form;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }
}
