package com.prodoko.base.client.beans.dynamic;

import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.client.beans.BeanForm;
import com.prodoko.base.client.beans.BeanFormFactory;
import com.prodoko.base.client.beans.EntityBeanFromFactory;
import com.prodoko.base.model.beans.dynamic.DynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;

public abstract class DynamicClassFormFactory<T extends DynamicClass, RT extends MainDynamicClass>
        extends EntityBeanFromFactory<T>{
    
    private MainDynamicClassFinderService<RT> finderService;

    /**
     * TODO
     */
    public MainDynamicClassFinderService<RT> getFinderService() {
        return finderService;
    }

    /**
     * @see #getFinderService
     */
    @Autowired
    public void setFinderService(MainDynamicClassFinderService<RT> finderService) {
        this.finderService = finderService;
    }

    public BeanForm<T> createWithNewBean(Long dynSuperclassId) {
        T stub = getEditorService().createStub(getSupportedClass());
        MainDynamicClass dynSuperclass = finderService.find(dynSuperclassId);
        stub.setDynamicSuperclass(dynSuperclass);
        return createWith(stub);
    }
}
