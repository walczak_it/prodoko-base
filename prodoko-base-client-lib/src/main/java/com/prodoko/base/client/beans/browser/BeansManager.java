package com.prodoko.base.client.beans.browser;

import java.util.Arrays;
import java.util.List;

import javafx.event.Event;
import javafx.event.EventHandler;

import javax.annotation.PostConstruct;

import org.controlsfx.control.action.Action;
import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.client.beans.CreateBeanAction;
import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.fx.control.form.ItemForm;

public abstract class BeansManager<T extends StandardEntity> extends BeansBrowser<T> {
    
    private EntityEditorService<T> editorService;
    private Action createBeanRootAction;
    
    public BeansManager(String text) {
        super(text);
    }
    
    @Autowired
    public void initCrudService(EntityEditorService<T> crudService) {
        this.editorService = crudService;
    }

    /**
     * TODO
     */
    public EntityEditorService<T> getEditorService() {
        return editorService;
    }

    /**
     * TODO
     */
    public Action getCreateBeanRootAction() {
        return createBeanRootAction;
    }

    final protected List<Action> getBeansManagerActions() {
        return Arrays.asList(getCreateBeanRootAction());
    }
    
    @Override
    @PostConstruct
    public void postConstruct() {
        super.postConstruct();
        this.createBeanRootAction = createCreateBeanRootAction();
        getActions().addAll(getBeansManagerActions());
    }
    
    protected Action createCreateBeanRootAction() {
        return CreateBeanAction.createCreateBeanRootAction(this, getBeanFormFactories());
    }
    
    @Override
    protected Action createOpenAction() {
        return new OpenToEditAction();
    }
    
    public class OpenToEditAction extends OpenAction {
        
        @SuppressWarnings("rawtypes")
        @Override
        protected ItemForm createForm(T item) {
            ItemForm form = super.createForm(item);
            form.setOnClose(new EventHandler<Event>() {
                
                @Override
                public void handle(Event event) {
                    getDataSource().clearLoaded();
                    getDataSource().scheduleLoading();
                }
            });
            return form;
        }
    }
}
