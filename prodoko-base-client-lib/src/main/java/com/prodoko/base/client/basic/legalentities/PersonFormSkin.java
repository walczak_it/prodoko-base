package com.prodoko.base.client.basic.legalentities;

import com.prodoko.base.model.basic.legalentities.Person;

public class PersonFormSkin<C extends PersonForm<T>, T extends Person>
        extends LegalEntityFormSkin<C, T> {

    public PersonFormSkin(C form) {
        super(form);
    }
}
