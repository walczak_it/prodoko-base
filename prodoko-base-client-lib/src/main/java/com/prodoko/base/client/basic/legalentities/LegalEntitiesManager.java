package com.prodoko.base.client.basic.legalentities;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.Arrays;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.browser.BeansManager;
import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.beans.presentation.NamedBeansSubsetPresentation;
import com.prodoko.base.model.beans.presentation.PropertyPresentation;
import com.prodoko.base.model.beans.presentation.SimpleStringPresentation;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LegalEntitiesManager<T extends LegalEntity>
    extends BeansManager<T> {

    public LegalEntitiesManager() {
        super(message(LegalEntitiesManager.class));
    }

    @Override
    public NamedBeansSubsetPresentation getStandardBeansPresentation() {
        NamedBeansSubsetPresentation std = createStandardBeansPresentationStub();
        std.getPresentedProperties().addAll(Arrays.asList(
                new PropertyPresentation(std, LegalEntity.class, "code",
                        new SimpleStringPresentation()),
                new PropertyPresentation(std, LegalEntity.class, "name",
                        new SimpleStringPresentation())
        ));
        return std;
    }
}
