package com.prodoko.base.client.beans.dynamic;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import com.prodoko.base.model.beans.dynamic.HelperDynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.fx.control.form.FormSkin;

public class DynamicHierarchyViewSkin<C extends DynamicHierarchyView<T>, T extends MainDynamicClass>
        extends FormSkin<C> {
    
    private SplitPane pane;
    private TitledPane mainDynamicClassesPane;
    private TreeView<T> mainDynamicClassesView;
    private TitledPane helperDynamicClassesPane;
    private ListView<HelperDynamicClass> helperdynamicClassesView;

    public DynamicHierarchyViewSkin(C control) {
        super(control);
        this.pane = createPane();
        this.mainDynamicClassesPane = createMainDynamicClassesPane();
        this.mainDynamicClassesView = createMainDynamicClassesView();
        this.helperDynamicClassesPane = createHelperDynamicClassesPane();
        this.helperdynamicClassesView = createHelperDynamicClassesView();
        getSkinnable().getRefreshAction().execute(null);
    }
    
    protected SplitPane createPane() {
        SplitPane pane = new SplitPane();
        pane.setOrientation(Orientation.VERTICAL);
        pane.setDividerPositions(0.7);
        getFieldsPane().setContent(pane);
        return pane;
    }
    
    protected TitledPane createMainDynamicClassesPane() {
        TitledPane tpane = new TitledPane();
        tpane.setCollapsible(false);
        String title = message(getSkinnable().getSupportedClass(), "plural");
        tpane.setText(title);
        pane.getItems().add(tpane);
        return tpane;
    }
    
    protected TreeView<T> createMainDynamicClassesView() {
        final TreeView<T> treeView = new TreeView<>();
        treeView.setCellFactory(createTreeViewCellFactory());
        getSkinnable().rootItemProperty().addListener(new ChangeListener<TreeItem<T>>() {

            @Override
            public void changed(ObservableValue<? extends TreeItem<T>> observable,
                    TreeItem<T> oldValue, TreeItem<T> newValue) {
                treeView.setRoot(newValue);
            }
            
        });
        treeView.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<TreeItem<T>>() {

                    @Override
                    public void changed(
                            ObservableValue<? extends TreeItem<T>> observable,
                            TreeItem<T> oldValue, TreeItem<T> newValue) {
                        if(newValue != null) {
                            getSkinnable().selectedItemProperty().set(newValue.getValue());
                        } else {
                            getSkinnable().selectedItemProperty().set(null);
                        }
                    }
        });
        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        getSkinnable().rootItemProperty().addListener(new ChangeListener<TreeItem<T>>() {

            @Override
            public void changed(ObservableValue<? extends TreeItem<T>> observable,
                    TreeItem<T> oldValue, TreeItem<T> newValue) {
                treeView.getSelectionModel().select(newValue);
            }
        });
        treeView.setPrefHeight(1000);
        mainDynamicClassesPane.setContent(treeView);
        return treeView;
    }
    
    protected TitledPane createHelperDynamicClassesPane() {
        TitledPane tpane = new TitledPane();
        tpane.setCollapsible(false);
        String title = message(HelperDynamicClass.class, "plural");
        tpane.setText(title);
        pane.getItems().add(tpane);
        return tpane;
    }
    
    protected ListView<HelperDynamicClass> createHelperDynamicClassesView() {
        ListView<HelperDynamicClass> view = new ListView<>();
        helperDynamicClassesPane.setContent(view);
        view.setPrefHeight(1000);
        return view;
    }

    protected Callback<TreeView<T>, TreeCell<T>> createTreeViewCellFactory() {
        return new Callback<TreeView<T>, TreeCell<T>>() {

            @Override
            public TreeCell<T> call(TreeView<T> view) {
                return new DynamicClassTreeCell<>();
            }
            
        };
    }
}
