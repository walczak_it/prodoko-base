package com.prodoko.base.client.basic.legalentities.types;

import javafx.scene.control.Skin;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.base.model.basic.legalentities.types.PersonType;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PersonTypeForm<T extends PersonType>
        extends LegalEntityTypeForm<T> {

    private final static String DEFAULT_STYLE_CLASS = "person-dynamic-class";

    public PersonTypeForm() {
        super();
        construct();
    }

    public PersonTypeForm(T item) {
        super(item);
        construct();
    }
    
    private void construct() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }
    
    @Override
    protected Class<PersonType> getAcceptableSuperclassClass() {
        return PersonType.class;
    }
    
    @Override
    protected Skin<?> createDefaultSkin() {
        return new PersonTypeFormSkin<PersonTypeForm<T>, T>(this);
    }
}
