package com.prodoko.base.client.boot;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.Properties;

import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.scene.control.Skin;
import javafx.scene.image.Image;

import org.controlsfx.control.action.Action;

import com.prodoko.fx.action.ServiceAction;
import com.prodoko.fx.control.form.ItemForm;
import com.prodoko.fx.validation.ValidationResult;

public class LoginForm extends ItemForm<Properties> {

    private static final String DEFAULT_STYLE_CLASS = "login-form";
    private Service<?> loginService;
    private Action loginAction;
    private Properties initialItem;
    private Image logo;
    
    public LoginForm(Properties initialItem, Image logo) {
        super(new Properties());
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.initialItem = initialItem;
        this.logo = logo;
    }
    
    public void initLoginService(Service<?> loginService) {
        this.loginService = loginService;
        buildLoginAction();
    }
    
    protected void buildLoginAction() {
        loginAction = new ServiceAction(loginService,
                message(LoginFormSkin.class, "loginAction")) {
            
            @Override
            public void execute(ActionEvent ae) {
                if(validate() == ValidationResult.ERROR) {
                    return;
                }
                super.execute(ae);
            }
        };
        getActions().add(loginAction);
    }

    /**
     * TODO
     */
    public Service<?> getLoginService() {
        return loginService;
    }

    /**
     * TODO
     */
    public Properties getInitialItem() {
        return initialItem;
    }
    
    /**
     * TODO
     */
    public Image getLogo() {
        return logo;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new LoginFormSkin<LoginForm>(this);
    }
}
