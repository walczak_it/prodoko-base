package com.prodoko.base.client.beans.dynamic;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;

public abstract class MainDynamicClassForm<
            T extends RT,
            RT extends MainDynamicClass>
        extends DynamicClassForm<T, RT> {

    private static final String DEFAULT_STYLE_CLASS = "main-dynamic-class-form";
    
    private BooleanProperty itemMostAcceptableSuperclass;
    
    public MainDynamicClassForm() {
        super();
        init();
    }

    public MainDynamicClassForm(T item) {
        super(item);
        init();
    }
    
    private void init() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.itemMostAcceptableSuperclass = new SimpleBooleanProperty();
        setMostSuperclassOfAcceptableFromItem();
    }
    
    @PostConstruct
    public void postContruct() {
        super.postConstruct();
    }
    
    public BooleanProperty itemMostAcceptableSuperclassProperty() {
        return itemMostAcceptableSuperclass;
    }
    
    public boolean isItemMostAcceptableSuperclass() {
        return itemMostAcceptableSuperclass.get();
    }
    
    @Override
    protected void itemChanged(T oldValue) {
        super.itemChanged(oldValue);
        setMostSuperclassOfAcceptableFromItem();
    }
    
    public void setMostSuperclassOfAcceptableFromItem() {
        if(getItem() == null || getItem().getId() == null) {
            itemMostAcceptableSuperclass.set(false);
        } else {
            if(getSuperclassFinderService().isMostSuperclass(getAcceptableSuperclassClass(),
                    getItem().getId())) {
                itemMostAcceptableSuperclass.set(true);
            } else {
                itemMostAcceptableSuperclass.set(false);
            }
        }
    }
}
