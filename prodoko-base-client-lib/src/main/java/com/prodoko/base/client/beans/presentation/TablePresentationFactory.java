package com.prodoko.base.client.beans.presentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javafx.scene.control.TableColumn;

import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;
import com.prodoko.base.model.beans.presentation.PropertyPresentation;
import com.prodoko.base.model.beans.presentation.TypePresentation;
import com.prodoko.base.util.ClassSupportersMap;

public class TablePresentationFactory {
    
    private ClassSupportersMap<ColumnFactory<?>> columnFactoryByPresentation;
    
    public TablePresentationFactory(Collection<ColumnFactory<?>> supportedPropertyPresentations) {
        this(new ClassSupportersMap<>(supportedPropertyPresentations));
    }

    public TablePresentationFactory(
            ClassSupportersMap<ColumnFactory<?>> columnFactoryByPresentation) {
        super();
        this.columnFactoryByPresentation = columnFactoryByPresentation;
    }

    public <S> List<TableColumn<S, ?>> create(BeansSubsetPresentation presentation) {
        List<TableColumn<S, ?>> columns = new ArrayList<>();
        for(PropertyPresentation propPres : presentation.getPresentedProperties()) {
            Class<? extends TypePresentation> presentationClass = propPres
                    .getPropertiesTypePresentation().getClass();
            ColumnFactory<?> factory = columnFactoryByPresentation.get(
                    presentationClass);
            if(factory == null) {
                throw new IllegalStateException("No column factory found for presentation class "
                        + presentationClass.getName());
            }
            columns.add(factory.<S>create(propPres));
        }
        return columns;
    }
    
    public static Collection<ColumnFactory<?>> defaultColumnFactories() {
        return Arrays.<ColumnFactory<?>>asList(
                new SimpleStringColumnFactory());
    }
}
