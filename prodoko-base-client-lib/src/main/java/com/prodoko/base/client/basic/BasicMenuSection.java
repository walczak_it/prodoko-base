package com.prodoko.base.client.basic;

import static com.prodoko.base.util.BundleUtils.message;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.fx.control.workbench.MenuSectionDefinition;

@Component
@Scope("singleton")
public class BasicMenuSection extends MenuSectionDefinition {

    @Override
    public String getTitle() {
        return message(BasicMenuSection.class);
    }
    
    @Override
    public int getOrderPriority() {
        return BIG_PRIORITY_STEP * 1;
    }
}
