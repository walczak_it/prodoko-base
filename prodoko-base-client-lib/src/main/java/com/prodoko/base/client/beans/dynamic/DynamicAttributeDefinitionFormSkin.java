package com.prodoko.base.client.beans.dynamic;

import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;

import com.prodoko.base.client.beans.BeanEntityForm;
import com.prodoko.base.client.beans.BeanEntityLookupField;
import com.prodoko.base.client.beans.BeanFormSkin;
import com.prodoko.base.client.beans.SectionedBeanFormSkin;
import com.prodoko.base.model.beans.dynamic.DynamicAttributeDefinition;
import com.prodoko.base.model.beans.dynamic.DynamicClass;
import com.prodoko.fx.control.LabledControlPane;
import com.prodoko.fx.control.form.FieldsetGrid;

public class DynamicAttributeDefinitionFormSkin<
                C extends DynamicAttributeDefinitionForm<T>,
                T extends DynamicAttributeDefinition>
        extends SectionedBeanFormSkin<C, T> {
    
    private LabledControlPane<TextInputControl> codeField;
    private LabledControlPane<TextInputControl> nameField;
    private LabledControlPane<TextInputControl> definingDynamicClassField;

    public DynamicAttributeDefinitionFormSkin(C form) {
        super(form);
        codeField = createCodeField();
        nameField = createNameField();
        definingDynamicClassField = createDefiningDynamicClassField();
    }

    protected LabledControlPane<TextInputControl> createCodeField() {
        return labelAndEnsyncItemsPropertyTextControl("code", new TextField());
    }

    protected LabledControlPane<TextInputControl> createNameField() {
        TextField field = new TextField();
        field.setPrefColumnCount(Math.round(TextField.DEFAULT_PREF_COLUMN_COUNT*1.5f));
        return labelAndEnsyncItemsPropertyTextControl("name", field);
    }

    protected LabledControlPane<TextInputControl> createDefiningDynamicClassField() {
        TextField field = new TextField();
        field.setDisable(true);
        return labelAndEnsyncItemsPropertyTextControl("definingDynamicClass.code", field);
    }

    public LabledControlPane<TextInputControl> getCodeField() {
        return codeField;
    }

    public LabledControlPane<TextInputControl> getNameField() {
        return nameField;
    }

    public LabledControlPane<TextInputControl> getDefiningDynamicClassField() {
        return definingDynamicClassField;
    }
}
