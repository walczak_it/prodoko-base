package com.prodoko.base.client.beans.dynamic;

import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;

import com.prodoko.base.client.beans.BeanEntityLookupField;
import com.prodoko.base.client.beans.SectionedBeanFormSkin;
import com.prodoko.base.model.beans.BeanFinderService;
import com.prodoko.base.model.beans.dynamic.DynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.fx.GridPaneLayouter;
import com.prodoko.fx.control.LabledControlPane;
import com.prodoko.fx.control.LookupField;
import com.prodoko.fx.control.form.FieldsetGrid;

public abstract class DynamicClassFormSkin<
            C extends DynamicClassForm<T, RT>,
            T extends DynamicClass,
            RT extends MainDynamicClass>
        extends SectionedBeanFormSkin<C, T> {
    
    private LabledControlPane<LookupField<RT>> dynamicSuperclassField;
    private LabledControlPane<TextInputControl> codeField;
    private LabledControlPane<TextInputControl> nameField;

    public DynamicClassFormSkin(C form) {
        super(form);
        codeField = createCodeField();
        nameField = createNameField();
        dynamicSuperclassField = createDynamicSuperclassFieldField();
        addHeaderSectionFieldset();
    }
    
    protected LabledControlPane<TextInputControl> createCodeField() {
        return labelAndEnsyncItemsPropertyTextControl("code", new TextField());
    }
    
    protected LabledControlPane<TextInputControl> createNameField() {
        TextField nameField = new TextField();
        return labelAndEnsyncItemsPropertyTextControl("name", nameField);
    }
    
    protected LabledControlPane<LookupField<RT>> createDynamicSuperclassFieldField() {
        LabledControlPane<LookupField<RT>> dynamicSupperclassField
                = labelAndEnsyncItemsPropertyLookupField("dynamicSuperclass",
                        new BeanEntityLookupField<RT>(
                                getSkinnable().getAcceptableSuperclassClass(),
                                getSkinnable().getSuperclassFinderService()));
        return dynamicSupperclassField;
    }
    
    protected void addHeaderSectionFieldset() {
        new GridPaneLayouter(getHeaderSectionFieldset())
            .add(codeField).add(nameField, 2).add(dynamicSuperclassField);
    }

    public LabledControlPane<LookupField<RT>> getDynamicSuperclassField() {
        return dynamicSuperclassField;
    }

    public LabledControlPane<TextInputControl> getCodeField() {
        return codeField;
    }

    public LabledControlPane<TextInputControl> getNameField() {
        return nameField;
    }
}
