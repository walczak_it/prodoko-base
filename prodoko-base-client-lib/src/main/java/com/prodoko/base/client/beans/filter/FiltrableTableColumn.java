package com.prodoko.base.client.beans.filter;

import javafx.beans.property.ObjectProperty;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;

public class FiltrableTableColumn<S, T> extends TableColumn<S, T> {
    
    private SingleFilterFormFactory<?> filterFormFactory;
    private ObjectProperty<EventHandler<SaveFilterEvent>> onAddQuickFilter;

    public FiltrableTableColumn(SingleFilterFormFactory<?> filterFormFactory) {
        super();
        this.filterFormFactory = filterFormFactory;
    }

    public FiltrableTableColumn(SingleFilterFormFactory<?> filterFormFactory, String text) {
        super(text);
        this.filterFormFactory = filterFormFactory;
    }
    
    public void setOnAddQuickFilter(EventHandler<SaveFilterEvent> value) {
        onAddQuickFilter.set(value);
    }
}
