package com.prodoko.base.client.beans.filter;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;
import com.prodoko.base.model.beans.BusinessEnumDescription;
import com.prodoko.base.model.beans.filter.StringPropertyFilter;
import com.prodoko.base.model.beans.filter.StringPropertyFilter.Operator;
import com.prodoko.fx.control.form.FormConsts;
import com.prodoko.fx.control.form.ItemFormSkin;

public class StringPropertyFilterFormSkin
        extends ItemFormSkin<StringPropertyFilterForm, StringPropertyFilter> {
    
    private static BusinessEnumDescription operatorDescription = null;
    
    private VBox pane;
    private Label propertyLabel;
    private ComboBox<StringPropertyFilter.Operator> operatorComboBox;
    private TextField comparingStringField;

    public StringPropertyFilterFormSkin(StringPropertyFilterForm control) {
        super(control);
        init();
    }

    private void init() {
        propertyLabel = new Label(getSkinnable().getPropertyDescription()
                .getLocalised().getDisplayName());
        operatorComboBox = new ComboBox<>(FXCollections.observableArrayList(
                StringPropertyFilter.Operator.values()));
        operatorComboBox.getSelectionModel().selectedItemProperty();
        operatorComboBox.setCellFactory(createOperatorCellFactory());
        operatorComboBox.setButtonCell(new OperatorCell());
        getSkinnable().getSyncItemHandlers().add(new ObjectPropertySync<Operator>(
                "operator", operatorComboBox.valueProperty()));
        comparingStringField = new TextField();
        getSkinnable().getSyncItemHandlers().add(new StringPropertySync(
                "comparingString", comparingStringField.textProperty()));
        pane = new VBox(propertyLabel, operatorComboBox, comparingStringField);
        pane.getStyleClass().add(FormConsts.FIELDS_PANE_CLASS);
        pane.setAlignment(Pos.CENTER);
        getChildren().add(pane);
    }
    
    protected Callback<ListView<Operator>, ListCell<Operator>> createOperatorCellFactory() {
        return new Callback<ListView<Operator>, ListCell<Operator>>() {

            @Override
            public ListCell<Operator> call(ListView<Operator> arg0) {
                return new OperatorCell();
            }
            
        };
    }
    
    public static BusinessEnumDescription getOperatorDescription() {
        if(operatorDescription == null) {
            operatorDescription = BusinessDescriptionIntrospector.getInstance()
                    .enumDescription(Operator.class);
        }
        return operatorDescription;
    }

    protected static class OperatorCell extends ListCell<Operator> {
        
        @Override
        protected void updateItem(Operator value, boolean empty) {
            super.updateItem(value, empty);
            if(value != null) {
                setText(StringPropertyFilterFormSkin.getOperatorDescription().getValuesLocalised()
                        .get(value).getDisplayName());
            }
        }
    }
}
