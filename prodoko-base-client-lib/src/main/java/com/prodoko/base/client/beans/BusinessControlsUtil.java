package com.prodoko.base.client.beans;

import javafx.scene.control.Label;
import javafx.scene.control.Labeled;

import com.prodoko.base.model.beans.BusinessDescription;
import com.prodoko.base.model.beans.LocalisedDescription;

public class BusinessControlsUtil {

    private static BusinessControlsUtil inst = null;
    
    protected BusinessControlsUtil() {
        
    }
    
    public static BusinessControlsUtil getInstance() {
        if(inst == null) {
            inst = new BusinessControlsUtil();
        }
        return inst;
    }
    
    public Label createDescriptionLabel(BusinessDescription<?> desc) {
        return createDescriptionLabel(desc.getLocalised());
    }
    
    public Label createDescriptionLabel(LocalisedDescription desc) {
        Label label = new Label();
        setupDescription(desc, label);
        return label;
    }
    
    public void setupDescription(BusinessDescription<?> desc, Labeled control) {
        setupDescription(desc.getLocalised(), control);
    }
    
    public void setupDescription(LocalisedDescription desc, Labeled control) {
        control.setText(desc.getDisplayName());
    }
}
