package com.prodoko.base.client.basic.legalentities;

import com.prodoko.base.model.basic.legalentities.Company;

public class CompanyFormSkin<C extends CompanyForm<T>, T extends Company>
        extends LegalEntityFormSkin<C, T> {

    public CompanyFormSkin(C form) {
        super(form);
    }
}
