package com.prodoko.base.client.beans.dynamic;

import org.springframework.beans.factory.annotation.Autowired;

import javafx.beans.InvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;

import com.prodoko.base.client.beans.BeanEntityForm;
import com.prodoko.base.model.beans.dynamic.DynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;

public abstract class DynamicClassForm<T extends DynamicClass, RT extends MainDynamicClass>
        extends BeanEntityForm<T> {

    private static final String DEFAULT_STYLE_CLASS = "dynamic-class-form";
    
    private MainDynamicClassFinderService<RT> superclassFinderService;
    
    public DynamicClassForm() {
        super();
        init();
    }
    
    public DynamicClassForm(T item) {
        super(item);
        init();
    }

    private void init() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }
    
    protected abstract Class<? extends RT> getAcceptableSuperclassClass();
    
    /**
     * TODO
     */
    public MainDynamicClassFinderService<RT> getSuperclassFinderService() {
        return superclassFinderService;
    }

    /**
     * @see #getSuperclassFinderService
     */
    @Autowired
    public void setSuperclassFinderService(
            MainDynamicClassFinderService<RT> superclassFinderService) {
        this.superclassFinderService = superclassFinderService;
    }
}
