package com.prodoko.base.client.beans.dynamic;

import com.prodoko.base.client.beans.BeanEntityLookupField;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.fx.control.LabledControlPane;
import com.prodoko.fx.control.LookupField;

public abstract class MainDynamicClassFormSkin<
            C extends MainDynamicClassForm<T, RT>,
            T extends RT,
            RT extends MainDynamicClass>
        extends DynamicClassFormSkin<C, T, RT> {
    
    public MainDynamicClassFormSkin(C form) {
        super(form);
    }
    
    @Override
    protected LabledControlPane<LookupField<RT>> createDynamicSuperclassFieldField() {
        LabledControlPane<LookupField<RT>> field = super
                .createDynamicSuperclassFieldField();
        field.getField().disableProperty().bind(getSkinnable()
                .itemMostAcceptableSuperclassProperty());
        return field;
    }
}
