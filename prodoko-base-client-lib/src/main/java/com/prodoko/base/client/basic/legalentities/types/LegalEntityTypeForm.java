package com.prodoko.base.client.basic.legalentities.types;

import com.prodoko.base.client.beans.dynamic.DynamicClassForm;
import com.prodoko.base.client.beans.dynamic.DynamicClassFormSkin;
import com.prodoko.base.client.beans.dynamic.MainDynamicClassForm;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.base.model.beans.dynamic.DynamicClass;

public abstract class LegalEntityTypeForm<T extends LegalEntityType>
        extends MainDynamicClassForm<T, LegalEntityType> {

    private final static String DEFAULT_STYLE_CLASS = "legal-entities-dynamic-class-form";
    
    public LegalEntityTypeForm() {
        super();
        init();
    }

    public LegalEntityTypeForm(T item) {
        super(item);
        init();
    }
    
    private void init() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }
}
