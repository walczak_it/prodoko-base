package com.prodoko.base.client.beans.filter;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Skin;

import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.control.action.Action;

import com.prodoko.base.model.beans.BusinessBeanDescription;
import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.fx.control.form.Form;
import com.prodoko.fx.control.form.ItemForm;
import com.prodoko.fx.validation.ValidationResult;

public class FilterAdder extends Form {
    
    private static final String DEFAULT_STYLE_CLASS = "filter-adder";

    private ClassSupportersMap<SingleFilterFormFactory<?>> filterFormFactories;
    private ObjectProperty<BeansSubsetPresentation> filterOwner;
    private BusinessBeanDescription beanDescription;
    private ObjectProperty<EventHandler<SaveFilterEvent<?>>> onAddFilter;
    private ObjectProperty<ItemForm<? extends FilterData>> selectedFilterForm;

    public FilterAdder(ObjectProperty<BeansSubsetPresentation> filterOwner,
            ClassSupportersMap<SingleFilterFormFactory<?>> filterFormFactories) {
        super();
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.filterOwner = new SimpleObjectProperty<>();
        this.filterOwner.bind(filterOwner);
        this.beanDescription = filterOwner.get().getBaseBeanDescription();
        this.onAddFilter = new SimpleObjectProperty<>();
        this.selectedFilterForm = new SimpleObjectProperty<>();
        this.filterFormFactories = filterFormFactories;
        getActions().add(createAddAction());
    }

    /**
     * TODO
     */
    public BeansSubsetPresentation getFilterOwner() {
        return filterOwner.get();
    }

    public BusinessBeanDescription getBeanDescription() {
        return beanDescription;
    }
    
    /**
     * TODO
     */
    public ClassSupportersMap<SingleFilterFormFactory<?>> getFilterFormFactories() {
        return filterFormFactories;
    }

    public ObjectProperty<ItemForm<? extends FilterData>> selectedFilterFormProperty() {
        return selectedFilterForm;
    }
    
    public ItemForm<? extends FilterData> getSelectedFilterForm() {
        return selectedFilterForm.get();
    }
    
    public void setSelectedFilterFrom(ItemForm<? extends FilterData> value) {
        selectedFilterForm.set(value);
    }
    
    public ObjectProperty<EventHandler<SaveFilterEvent<?>>> onAddFilterProperty() {
        return onAddFilter;
    }
    
    public EventHandler<SaveFilterEvent<?>> getOnAddFilter() {
        return onAddFilter.get();
    }
    
    public void setOnAddFilter(EventHandler<SaveFilterEvent<?>> value) {
        onAddFilter.set(value);
    }

    protected Action createAddAction() {
        return new AbstractAction(message(FilterAdder.class, "addFilterAction")) {
            
            @Override
            public void execute(ActionEvent ae) {
                ItemForm<? extends FilterData> itemForm = getSelectedFilterForm();
                if(itemForm != null && itemForm.validate() != ValidationResult.ERROR) {
                    if(onAddFilter.get() != null) {
                        itemForm.updateItem();
                        FilterData fd = itemForm.getItem();
                        if(fd == null) {
                            throw new IllegalStateException("Filter form " + itemForm.getClass()
                                    + " returned null filter");
                        }
                        SaveFilterEvent<?> event = new SaveFilterEvent<>(
                                itemForm.getItem(), true);
                        onAddFilter.get().handle(event);
                        getShowingDialog().hide();
                        ((FilterAdderSkin)getSkin()).reset();
                    }
                }
            }
        };
    }
    
    @Override
    protected Skin<?> createDefaultSkin() {
        return new FilterAdderSkin(this);
    }
}
