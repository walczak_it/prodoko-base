package com.prodoko.base.client;

import java.util.ArrayList;

import com.prodoko.fx.ProdokoFxConsts;

public class GlobalStylesheets extends ArrayList<String> {

    private static final long serialVersionUID = 6123772066045533797L;

    public GlobalStylesheets() {
        addAll(ProdokoFxConsts.getFxStyleSheets());
        add("/com/prodoko/base/client/normalGui.css");
        add("/com/prodoko/base/client/paperGui.css");
    }
}
