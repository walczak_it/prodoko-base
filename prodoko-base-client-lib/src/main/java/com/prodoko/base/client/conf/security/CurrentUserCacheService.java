package com.prodoko.base.client.conf.security;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.prodoko.base.model.conf.security.UserLogin;

@Service
@Scope("singleton")
public class CurrentUserCacheService {

    private UserLogin userLogin;

    /**
     * TODO
     */
    public UserLogin getUserLogin() {
        return userLogin;
    }


    /**
     * @see #getUserLogin
     */
    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }
}
