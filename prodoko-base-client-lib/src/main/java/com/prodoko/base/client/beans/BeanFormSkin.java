package com.prodoko.base.client.beans;

import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;

import com.prodoko.base.model.beans.BusinessBeanDescription;
import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;
import com.prodoko.base.model.beans.BusinessPropertyDescription;
import com.prodoko.fx.control.LabledControlPane;
import com.prodoko.fx.control.LookupField;
import com.prodoko.fx.control.form.ItemFormSkin;

public abstract class BeanFormSkin<C extends BeanForm<T>, T> extends ItemFormSkin<C, T> {
    
    private BusinessControlsUtil bcu = BusinessControlsUtil.getInstance();

    public BeanFormSkin(C form) {
        super(form);
    }
    
    public Label createItemsPropertyLabel(String propertyPath) {
        BusinessPropertyDescription desc = BusinessDescriptionIntrospector.getInstance()
                .deepPropertyDescription(getSkinnable().getItem().getClass(), propertyPath);
        return bcu.createDescriptionLabel(desc);
    }
    
    public Label createItemsDescriptionLabel() {
        BusinessBeanDescription desc = BusinessDescriptionIntrospector.getInstance()
                .beanDescription(getSkinnable().getItem().getClass());
        return bcu.createDescriptionLabel(desc);
    }
    
    public void setupItemsPropertyDescription(String propertyPath, Labeled control) {
        BusinessPropertyDescription desc = BusinessDescriptionIntrospector.getInstance()
                .deepPropertyDescription(getSkinnable().getItem().getClass(), propertyPath);
        bcu.setupDescription(desc, control);
    }
    
    public void setupItemsDescription(Labeled control) {
        BusinessBeanDescription desc = BusinessDescriptionIntrospector.getInstance()
                .beanDescription(getSkinnable().getItem().getClass());
        bcu.setupDescription(desc, control);
    }
    
    public LabledControlPane<TextInputControl> labelAndEnsyncItemsPropertyTextControl(
            String propertyPath, TextInputControl control) {
        ensyncItemsPropertyTextControl(propertyPath, control);
        return new LabledControlPane<TextInputControl>(
                createItemsPropertyLabel(propertyPath), control);
    }
    
    public <LT> LabledControlPane<LookupField<LT>>
            labelAndEnsyncItemsPropertyLookupField(String propertyPath, LookupField<LT> field) {
        ensyncItemsPropertyLookupField(propertyPath, field);
        return new LabledControlPane<LookupField<LT>>(
                createItemsPropertyLabel(propertyPath), field);
    }
}
