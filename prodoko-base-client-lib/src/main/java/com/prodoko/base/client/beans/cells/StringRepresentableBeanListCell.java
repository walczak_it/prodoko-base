package com.prodoko.base.client.beans.cells;

import com.prodoko.base.model.beans.StringRepresentableBean;
import com.prodoko.base.model.beans.StringRepresentableBean.Hint;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class StringRepresentableBeanListCell<T extends StringRepresentableBean>
        extends ListCell<T> {

    public static <T extends StringRepresentableBean> Callback<ListView<T>,ListCell<T>>
            forListView() {
        return new Callback<ListView<T>,ListCell<T>>() {

            @Override
            public ListCell<T> call(ListView<T> view) {
                return new StringRepresentableBeanListCell<>();
            }
            
        };
    }
    
    @Override
    protected void updateItem(T value, boolean empty) {
        super.updateItem(value, empty);
        if(value != null) {
            setText(value.toRepresentiveString(Hint.LONG));
        }
    }
}
