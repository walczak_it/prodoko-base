package com.prodoko.base.client.beans.filter;

import javafx.scene.control.Skin;

import com.prodoko.base.model.beans.BusinessPropertyDescription;
import com.prodoko.base.model.beans.filter.StringPropertyFilter;
import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;
import com.prodoko.fx.control.form.ItemForm;

public class StringPropertyFilterForm extends ItemForm<StringPropertyFilter> {
    
    private static final String DEFAULT_STYLE_CLASS = "string-property-filter-form";
    
    private BusinessPropertyDescription propertyDescription;

    public StringPropertyFilterForm(
            BusinessPropertyDescription propertyDescription,
            String propertyPath,
            BeansSubsetPresentation filterOwner) {
        super(new StringPropertyFilter());
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.propertyDescription = propertyDescription;
        getItem().setPropertyPath(propertyPath);
        getItem().setPresentation(filterOwner);
    }

    /**
     * TODO
     */
    public BusinessPropertyDescription getPropertyDescription() {
        return propertyDescription;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new StringPropertyFilterFormSkin(this);
    }
}
