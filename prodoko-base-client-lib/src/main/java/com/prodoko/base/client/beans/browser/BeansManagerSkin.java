package com.prodoko.base.client.beans.browser;

import javafx.scene.control.SplitPane;

import com.prodoko.base.client.beans.dynamic.DynamicHierarchyEditor;
import com.prodoko.base.model.entities.StandardEntity;

public class BeansManagerSkin<C extends BeansManager<T>, T extends StandardEntity>
        extends BeansBrowserSkin<C, T> {

    public BeansManagerSkin(C control) {
        super(control);
    }
}
