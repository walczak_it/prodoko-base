package com.prodoko.base.client.beans.dynamic;

import com.prodoko.base.client.beans.BeanEntityForm;
import com.prodoko.base.model.beans.dynamic.DynamicAttributeDefinition;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;

public abstract class DynamicAttributeDefinitionForm<T extends DynamicAttributeDefinition>
        extends BeanEntityForm<T> {

    public DynamicAttributeDefinitionForm() {
        super();
    }

    public DynamicAttributeDefinitionForm(T item) {
        super(item);
    }

    public abstract Class<? extends MainDynamicClass> getRootMainDynamicClassClass();
}
