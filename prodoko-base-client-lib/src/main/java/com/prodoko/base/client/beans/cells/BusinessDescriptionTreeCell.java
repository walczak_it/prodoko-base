package com.prodoko.base.client.beans.cells;

import javafx.scene.control.TreeCell;

import com.prodoko.base.model.beans.BusinessDescription;

public class BusinessDescriptionTreeCell<T extends BusinessDescription<?>> extends TreeCell<T> {

    @Override
    protected void updateItem(T value, boolean empty) {
        super.updateItem(value, empty);
        if(value != null) {
            setText(value.getLocalised().getDisplayName());
        }
    }
}
