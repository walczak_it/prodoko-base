package com.prodoko.base.client.beans.browser;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.binding.NumberBinding;
import javafx.beans.binding.NumberExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.prodoko.base.model.DataLoad;
import com.prodoko.base.model.beans.browser.BeansBrowserService;
import com.prodoko.base.model.beans.browser.BeansLoadRequest;
import com.prodoko.base.model.beans.filter.BeanOrder;
import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.fx.OnceRunner;

public class BeanLoadingService<T> extends Service<DataLoad<T>> {
    
    private static final Log LOG = LogFactory.getLog(BeanLoadingService.class);
    
    public static final int DEFAULT_LAZY_LOADING_STEP = 100;
    
    private BeansBrowserService<T> browserService;
    private ListProperty<T> loadedItems;
    private LongProperty totalItemsCount;
    private BooleanBinding hasNext;
    private ListProperty<FilterData> filters;
    private ListProperty<BeanOrder> orders;
    private int pageSize;
    private NumberBinding loadedPagesCount;
    private BooleanProperty invalidLoadedItems;
    private OnceRunner scheduleLoadingRunner;
    
    public BeanLoadingService(BeansBrowserService<T> browserService) {
        this(browserService, FXCollections.<FilterData>emptyObservableList());
    }
    
    public BeanLoadingService(BeansBrowserService<T> browserService,
            ObservableList<FilterData> filters) {
        this(browserService, filters, DEFAULT_LAZY_LOADING_STEP);
    }
    
    public BeanLoadingService(BeansBrowserService<T> browserService,
            ObservableList<FilterData> filters,
            int pageSize) {
        this.browserService = browserService;
        this.filters = new SimpleListProperty<>(filters);
        this.filters.addListener(createFiltersChangedListener());
        this.orders =  new SimpleListProperty<>(FXCollections.<BeanOrder>observableArrayList());
        this.orders.addListener(createOrdersChangedListener());
        this.pageSize = pageSize;
        this.loadedItems = new SimpleListProperty<>(FXCollections.<T>observableArrayList());
        this.totalItemsCount = new SimpleLongProperty();
        this.hasNext = Bindings.greaterThan(totalItemsCount, loadedItems.sizeProperty());
        this.loadedPagesCount = Bindings.divide(loadedItems.sizeProperty(), pageSize);
        this.invalidLoadedItems = new SimpleBooleanProperty(false);
        this.invalidLoadedItems.addListener(createInvalidLoadedItemsChangeListener());
        this.scheduleLoadingRunner = new OnceRunner(new Runnable() {

            @Override
            public void run() {
                scheduleLoading();
            }
            
        });
    }

    protected ChangeListener<? super Boolean> createInvalidLoadedItemsChangeListener() {
        return new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if(oldValue == false && newValue == true) {
                    clearLoaded();
                    scheduleLoadingRunner.runLaterOnce();
                }
            }
        };
    }

    protected ListChangeListener<? super FilterData> createFiltersChangedListener() {
        return createItemsInfluencingListChangeListener();
    }

    protected ListChangeListener<? super BeanOrder> createOrdersChangedListener() {
        return createItemsInfluencingListChangeListener();
    }
    
    protected ListChangeListener<Object> createItemsInfluencingListChangeListener() {
        return new ListChangeListener<Object>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends Object> arg0) {
                invalidLoadedItems.set(true);
            }
            
        };
    }

    /**
     * TODO
     */
    public BeansBrowserService<T> getBrowserService() {
        return browserService;
    }

    public ReadOnlyListProperty<T> loadedItemsProperty() {
        return loadedItems;
    }
    
    public ObservableList<T> getLoadedItems() {
        return loadedItems.get();
    }
    
    public ReadOnlyLongProperty totalItemsCountProperty() {
        return totalItemsCount;
    }
    
    public void setTotalItemsCount(long value) {
        totalItemsCount.set(value);
    }
    
    public ListProperty<FilterData> filtersProperty() {
        return filters;
    }
    
    public ObservableList<FilterData> getFilters() {
        return filters.get();
    }

    public ListProperty<BeanOrder> ordersProperty() {
        return orders;
    }
    
    public ObservableList<BeanOrder> getOrders() {
        return orders.get();
    }
    
    public int getPageSize() {
        return pageSize;
    }
    
    public NumberExpression loadedPagesCountProperty() {
        return loadedPagesCount;
    }
    
    public int getLoadedPagesCount() {
        return loadedPagesCount.intValue();
    }
    
    public boolean hasNext() {
        return hasNext.get();
    }
    
    public BooleanExpression hasNextProperty() {
        return hasNext;
    }
    
    public ReadOnlyBooleanProperty invalidLoadedItemsProperty() {
        return invalidLoadedItems;
    }
    
    public boolean isInvalidLoadedItems() {
        return invalidLoadedItems.get();
    }
    
    public void setInvalidLoadedItems(boolean value) {
        invalidLoadedItems.set(value);
    }
    
    /**
     * TODO
     */
    public OnceRunner getScheduleLoadingRunner() {
        return scheduleLoadingRunner;
    }

    public void scheduleLoading() {
        switch(getState()) {
        case READY:
            start();
            break;
        case RUNNING:
            break;
        case SCHEDULED:
            break;
        default:
            reset();
            start();
            break;
        
        }
    }
    
    public void assureNextLoad() {
        if(isInvalidLoadedItems()) {
            throw new IllegalStateException("Currently loaded items are invalid. "
                    + "They should have been cleared.");
        }
        LOG.info("Assured load next");
    }
    
    public void clearLoaded() {
        LOG.info("Clearing loaded items");
        loadedItems.clear();
        setInvalidLoadedItems(false);
    }

    @Override
    protected Task<DataLoad<T>> createTask() {
        return new LoadTask();
    }
    
    protected class LoadTask extends Task<DataLoad<T>> {
        
        @Override
        protected DataLoad<T> call() throws Exception {
            BeansLoadRequest req = new BeansLoadRequest();
            req.setFirstResult(getLoadedItems().size());
            req.setMaxResults(getPageSize());
            req.setOrders(getOrders());
            req.setFilters(getFilters());
            return browserService.browse(req);
        }
        
        @Override
        protected void succeeded() {
            super.succeeded();
            loadedItems.addAll(getValue().getData());
            setTotalItemsCount(getValue().getTotalResults());
            
        }
    }
}
