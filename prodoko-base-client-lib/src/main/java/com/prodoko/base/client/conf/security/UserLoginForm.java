package com.prodoko.base.client.conf.security;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Skin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.prodoko.base.client.beans.BeanEntityForm;
import com.prodoko.base.model.conf.security.UserLogin;
import com.prodoko.base.model.conf.security.UserLoginsEditorService;
import com.prodoko.fx.action.ServiceAction;
import com.prodoko.fx.concurrent.SimpleService;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserLoginForm<T extends UserLogin> extends BeanEntityForm<T> {
    
    private UserLoginsEditorService userLoginsCrudService;
    private StringProperty newPassword;

    public UserLoginForm() {
        super();
        init();
    }
    
    public UserLoginForm(T item) {
        super(item);
        init();
    }

    private void init() {
        newPassword = new SimpleStringProperty();
    }
    
    @Autowired
    public void initUserLoginsCrudService(UserLoginsEditorService userLoginsCrudService) {
        this.userLoginsCrudService = userLoginsCrudService;
    }
    
    @Override
    protected ServiceAction createSaveAction() {
        ServiceAction action = super.createSaveAction();
        action.setService(new SimpleService<>(SaveWithNewPasswordTask.class, this));
        return action;
    }
    
    protected class SaveWithNewPasswordTask extends SaveTask {
        
        @Override
        protected T call() throws Exception {
            if(StringUtils.isEmpty(getNewPassword())) {
                return super.call();
            } else {
                return userLoginsCrudService.saveWithNewPassword(getItem(), getNewPassword());
            }
        }
    }
    
    public StringProperty newPasswordProperty() {
        return newPassword;
    }
    
    public String getNewPassword() {
        return newPassword.get();
    }
    
    @Override
    protected Skin<?> createDefaultSkin() {
        return new UserLoginFormSkin<UserLoginForm<T>, T>(this);
    }
}
