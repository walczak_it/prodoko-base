package com.prodoko.base.client.basic.legalentities.types;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.BeanForm;
import com.prodoko.base.client.beans.BeanFormFactory;
import com.prodoko.base.client.beans.dynamic.DynamicClassFormFactory;
import com.prodoko.base.model.basic.legalentities.LegalEntityEditorService;
import com.prodoko.base.model.basic.legalentities.Person;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeEditorService;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityTypeFinderService;
import com.prodoko.base.model.basic.legalentities.types.PersonType;

@Component
public class PersonTypeFormFactory
        extends DynamicClassFormFactory<PersonType, LegalEntityType> {
    
    private ObjectFactory<PersonTypeForm<PersonType>> objectFactory;

    @Autowired
    public void setObjectFactory(
            ObjectFactory<PersonTypeForm<PersonType>> objectFactory) {
        this.objectFactory = objectFactory;
    }

    @Override
    public Class<PersonType> getSupportedClass() {
        return PersonType.class;
    }

    @Override
    public BeanForm<PersonType> createWith(PersonType bean) {
        PersonTypeForm<PersonType> form = objectFactory.getObject();
        form.setItem(bean);
        return form;
    }
}
