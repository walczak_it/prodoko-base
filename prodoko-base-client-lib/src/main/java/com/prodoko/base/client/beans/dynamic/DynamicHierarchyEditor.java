package com.prodoko.base.client.beans.dynamic;

import static com.prodoko.base.util.BundleUtils.message;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.controlsfx.control.action.Action;
import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClassEditorService;

public class DynamicHierarchyEditor<T extends MainDynamicClass> extends DynamicHierarchyView<T> {
    
    private static final String DEFAULT_STYLE_CLASS = "dynamic-hierarchy-editor";
    
    private MainDynamicClassEditorService<T> dynamicClassesCrudService;
    private Action createDynamicSubclassAction;

    public DynamicHierarchyEditor() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }

    @Autowired
    public void initDynamicClassesCrudService(
            MainDynamicClassEditorService<T> dynamicClassesCrudService) {
        this.dynamicClassesCrudService = dynamicClassesCrudService;
    }
    
    /**
     * TODO
     */
    public MainDynamicClassEditorService<T> getDynamicClassesCrudService() {
        return dynamicClassesCrudService;
    }

    @PostConstruct
    public void postConstruct() {
        this.createDynamicSubclassAction = createCreateDynamicSubclassAction();
        getActions().addAll(createDefaultHierarchyEditorActions());
    }
    
    protected Action createCreateDynamicSubclassAction() {
        CreateDynamicSubclassAction<T, T> action = new CreateDynamicSubclassAction<>(
                getCreateDynamicSubclassActionText(), this, getDynamicClassFormFactories());
        action.dynamicSupperclassProperty().bind(selectedItemProperty());
        return action;
    }
    
    protected String getCreateDynamicSubclassActionText() {
        return message(CreateDynamicSubclassAction.class);
    }

    protected final List<Action> createDefaultHierarchyEditorActions() {
        return Arrays.asList(getCreateDynamicSubclassAction());
    }
    
    /**
     * TODO
     */
    public Action getCreateDynamicSubclassAction() {
        return createDynamicSubclassAction;
    }
}
