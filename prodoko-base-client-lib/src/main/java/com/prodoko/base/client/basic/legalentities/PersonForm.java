package com.prodoko.base.client.basic.legalentities;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.scene.control.Skin;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.BeanEntityForm;
import com.prodoko.base.model.basic.legalentities.Person;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PersonForm<T extends Person> extends LegalEntityForm<T> {

    public PersonForm() {
        super();
    }
    
    public PersonForm(T item) {
        super(item);
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new PersonFormSkin<PersonForm<T>, T>(this);
    }
}
