package com.prodoko.base.client.conf.security;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.BeanForm;
import com.prodoko.base.client.beans.BeanFormFactory;
import com.prodoko.base.model.conf.security.UserLogin;

@Component
public class UserLoginFormFactory implements BeanFormFactory<UserLogin> {

    private ObjectFactory<UserLoginForm<UserLogin>> objectFactory;
    
    @Autowired
    protected void setObjectFactory(
            ObjectFactory<UserLoginForm<UserLogin>> objectFactory) {
        this.objectFactory = objectFactory;
    }

    @Override
    public Class<UserLogin> getSupportedClass() {
        return UserLogin.class;
    }

    @Override
    public BeanForm<UserLogin> createWithNewBean() {
        return createWith(new UserLogin());
    }
    
    @Override
    public BeanForm<UserLogin> createWith(UserLogin bean) {
        UserLoginForm<UserLogin> form = objectFactory.getObject();
        form.setItem(bean);
        return form;
    }

    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }
}
