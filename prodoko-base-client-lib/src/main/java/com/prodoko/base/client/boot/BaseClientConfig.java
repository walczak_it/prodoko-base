package com.prodoko.base.client.boot;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Scope;

import com.prodoko.base.client.beans.filter.SingleFilterFormFactory;
import com.prodoko.base.client.beans.filter.StringPropertyFilterFormFactory;
import com.prodoko.base.client.beans.presentation.ColumnFactory;
import com.prodoko.base.client.beans.presentation.SimpleStringColumnFactory;
import com.prodoko.base.client.beans.presentation.UniversalPresentation;
import com.prodoko.base.model.BaseSystemInfo;
import com.prodoko.base.model.beans.filter.UniversalFilters;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.fx.control.workbench.MenuPositionDefinition;
import com.prodoko.fx.control.workbench.Workbench;
import com.prodoko.remoting.hessian.EnableHessianAccess;
import com.prodoko.remoting.hessian.fx.FxSerializerFactory;

@Configuration
@EnableHessianAccess(basePackages = "com.prodoko.base.model",
        serializerFactoryClass=FxSerializerFactory.class, overloadEnabled=true)
@ComponentScan(basePackages = {"com.prodoko.base.client"},
        excludeFilters=@Filter(value=Configuration.class, type=FilterType.ANNOTATION))
public class BaseClientConfig {
    
    private static final Log LOG = LogFactory.getLog(BaseClientConfig.class);
    
    @Autowired
    private BaseSystemInfo systemInfo;
    
    @Bean
    public BaseSystemInfo systemInfo() {
        return new BaseSystemInfo();
    }
    
    @Bean @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE) @UniversalFilters
    public ClassSupportersMap<SingleFilterFormFactory<?>> formFactoriesForUniversalFilters() {
        return new ClassSupportersMap<>(Arrays.<SingleFilterFormFactory<?>>asList(
                new StringPropertyFilterFormFactory()
        ));
    }
    
    @Bean @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE) @UniversalPresentation
    public ClassSupportersMap<ColumnFactory<?>> universalColumnFactories() {
        return new ClassSupportersMap<>(Arrays.<ColumnFactory<?>>asList(
                new SimpleStringColumnFactory()));
    }
    
    @Bean
    @Autowired
    public Workbench workbench(Collection<MenuPositionDefinition> wps) {
        Workbench wp = new Workbench(wps, systemInfo.getSystemsLogoPath());
        return wp;
    }
}
