package com.prodoko.base.client.beans;

import java.util.List;

import javafx.concurrent.Task;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.client.beans.cells.StringRepresentableBeanListCell;
import com.prodoko.base.model.beans.BeanFinderService;
import com.prodoko.base.model.beans.StringRepresentableBean;
import com.prodoko.base.model.beans.StringRepresentableBean.Hint;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.fx.concurrent.SimpleService;
import com.prodoko.fx.control.LookupField;
import com.prodoko.fx.control.LookupFieldSkin;

public class BeanEntityLookupField<T extends StandardEntity & StringRepresentableBean>
        extends LookupField<T> implements ClassSupporter {
    
    private static final Log LOG = LogFactory.getLog(BeanEntityLookupField.class);

    private static final String DEFAULT_STYLE_CLASS = "bean-entity-lookup-field";
    
    private BeanFinderService<T> finderService;
    private Class<? extends T> supportedClass;

    public BeanEntityLookupField(Class<? extends T> supportedClass,
            BeanFinderService<T> finderService) {
        super();
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        this.supportedClass = supportedClass;
        this.finderService = finderService;
        this.setConverter(new BeanStringConverter());
        this.setCellFactory(StringRepresentableBeanListCell.<T>forListView());
        this.setLazyItemsLoader(new SimpleService<List<T>>(FindSuggestionsTask.class, this));
    }

    /**
     * TODO
     */
    @Override
    public Class<? extends T> getSupportedClass() {
        return supportedClass;
    }
    
    @Override
    public boolean isSupportsSubclasses() {
        return true;
    }

    /**
     * TODO
     */
    public BeanFinderService<? extends T> getFinderService() {
        return finderService;
    }
    
    protected class BeanStringConverter extends StringConverter<T> {

        @Override
        public T fromString(String text) {
            try {
                LOG.info("Finding bean by string...");
                T bean = finderService.findByString(supportedClass, text);
                if(bean == null) {
                    LOG.info("By string '" + text + "' we found didn't find any bean");
                } else {
                    LOG.info("By string '" + text + "' we found bean id=" + bean.getId());
                }
                return bean;
            } catch(Exception e) {
                getParent().requestFocus();
                throw new BeanLookupException("Couldn't convert string to bean", e);
            }
        }

        @Override
        public String toString(T bean) {
            try {
                if(bean != null) {
                    LOG.info("Getting representative string from bean");
                    return bean.toRepresentiveString(Hint.SHORT);
                } else {
                    LOG.info("No bean to get representative skin");
                    return null;
                }
            } catch(Exception e) {
                getParent().requestFocus();
                throw new BeanLookupException("Couldn't convert bean to string", e);
            }
        }
        
    }
    
    protected class FindSuggestionsTask extends Task<List<T>> {

        @Override
        protected List<T> call() throws Exception {
            return (List<T>) finderService.findSuggestionsByString(
                    getSupportedClass(), getText());
        }
        
        @Override
        protected void succeeded() {
            super.succeeded();
            getItems().setAll(getValue());
        }
        
        @Override
        protected void failed() {
            super.failed();
            getParent().requestFocus();
            throw new BeanLookupException("Couldn't find suggestions", getException());
        }
    }
}
