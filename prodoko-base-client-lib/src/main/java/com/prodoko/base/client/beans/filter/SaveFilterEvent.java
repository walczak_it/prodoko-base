package com.prodoko.base.client.beans.filter;

import javafx.event.Event;
import javafx.event.EventType;

import com.prodoko.base.model.beans.filter.FilterData;

public class SaveFilterEvent<T extends FilterData> extends Event {
    
    private static final long serialVersionUID = -5862932371885265424L;
    
    public static final EventType<SaveFilterEvent<?>> ADD_FILTER
            = new EventType<>("ADD_FILTER");
            
    public static final EventType<SaveFilterEvent<?>> MODIFY_FILTER
            = new EventType<>("MODIFY_FILTER");
    
    private T filter;

    public SaveFilterEvent(T filter, boolean isNew) {
        super(isNew ? ADD_FILTER : MODIFY_FILTER);
        this.filter = filter;
    }

    /**
     * TODO
     */
    public T getFilter() {
        return filter;
    }
}
