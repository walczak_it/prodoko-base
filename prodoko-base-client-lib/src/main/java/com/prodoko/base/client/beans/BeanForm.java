package com.prodoko.base.client.beans;

import javafx.beans.binding.Bindings;

import com.prodoko.base.model.beans.BusinessBeanDescription;
import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;
import com.prodoko.base.model.beans.BeanWithCode;
import com.prodoko.base.model.beans.BeanWithName;
import com.prodoko.fx.control.form.ItemForm;

public abstract class BeanForm<T> extends ItemForm<T> {
    
    private static final String DEFAULT_STYLE_CLASS = "bean-form";

    public BeanForm() {
        super();
        construct();
    }

    public BeanForm(T item) {
        super(item);
        construct();
    }
    
    private void construct() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        if(getText() == null && getItem() != null) {
            initTextPropertyFromItem();
        }
    }
    
    @Override
    protected void itemChanged(T oldValue) {
        super.itemChanged(oldValue);
        initTextPropertyFromItem();
    }
    
    protected void initTextPropertyFromItem() {
        String itemsClassDisplayName = getItemsBusinessDescription().getLocalised()
                .getDisplayName();
        boolean textSet = false;
        if(getItem() instanceof BeanWithCode) {
            BeanWithCode itemWithCode = (BeanWithCode)getItem();
            if(itemWithCode.getCode() != null) {
                setText(itemsClassDisplayName + " " + itemWithCode.getCode());
                textSet = true;
            }
        } else if(getItem() instanceof BeanWithName) {
            BeanWithName itemWithName = (BeanWithName)getItem();
            if(itemWithName.getName() != null) {
                setText(itemsClassDisplayName + " " + itemWithName.getName());
                textSet = true;
            }
        }
        if(!textSet){
            setText(itemsClassDisplayName);
        }
    }
    
    public BusinessBeanDescription getItemsBusinessDescription() {
        return BusinessDescriptionIntrospector.getInstance().beanDescription(getItem().getClass());
    }
}
