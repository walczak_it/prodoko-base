package com.prodoko.base.client.conf.security;

import static com.prodoko.base.util.BundleUtils.message;
import javafx.scene.Node;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.conf.ConfigurationMenuSection;
import com.prodoko.fx.control.workbench.MenuNodeDefinition;
import com.prodoko.fx.control.workbench.MenuPositionDefinition;

@Component
public class UserLoginsMenuPosition extends MenuPositionDefinition {
    
    private ConfigurationMenuSection configurationMenuSection;
    private ObjectFactory<UserLoginsManager> userLoginsBrowserFactory;

    @Override
    public MenuNodeDefinition getParent() {
        return configurationMenuSection;
    }

    @Autowired
    public void setConfigurationMenuSection(ConfigurationMenuSection configurationMenuSection) {
        this.configurationMenuSection = configurationMenuSection;
    }

    @Autowired
    public void setUserLoginsBrowserFactory(
            ObjectFactory<UserLoginsManager> userLoginsBrowserFactory) {
        this.userLoginsBrowserFactory = userLoginsBrowserFactory;
    }

    @Override
    public Node createContent() {
        Node node = userLoginsBrowserFactory.getObject();
        return node;
    }

    @Override
    public String getTitle() {
        return message(UserLoginsMenuPosition.class);
    }
}
