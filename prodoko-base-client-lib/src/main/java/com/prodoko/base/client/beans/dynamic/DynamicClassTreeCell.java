package com.prodoko.base.client.beans.dynamic;

import javafx.scene.control.TreeCell;

import com.prodoko.base.model.beans.dynamic.MainDynamicClass;

public class DynamicClassTreeCell<T extends MainDynamicClass> extends TreeCell<T> {

    @Override
    protected void updateItem(T value, boolean empty) {
        super.updateItem(value, empty);
        
        if(value != null && !empty) {
            setText(value.getName());
        } else {
            setText(null);
        }
    }
}
