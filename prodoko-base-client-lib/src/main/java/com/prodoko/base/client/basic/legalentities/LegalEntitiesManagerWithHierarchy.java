package com.prodoko.base.client.basic.legalentities;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.dynamic.BeansManagerWithHierarchy;
import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;
import com.prodoko.fx.control.BreadcrumbsPane;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LegalEntitiesManagerWithHierarchy
        extends BeansManagerWithHierarchy<LegalEntity, LegalEntityType> {

    
}
