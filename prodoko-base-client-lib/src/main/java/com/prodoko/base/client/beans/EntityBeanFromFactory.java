package com.prodoko.base.client.beans;

import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.base.model.entities.StandardEntity;

public abstract class EntityBeanFromFactory<T extends StandardEntity>
        implements BeanFormFactory<T> {

    private EntityEditorService<? super T> editorService;
    
    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }

    public EntityEditorService<? super T> getEditorService() {
        return editorService;
    }

    @Autowired
    public void setEditorService(EntityEditorService<? super T> editorService) {
        this.editorService = editorService;
    }
    
    @Override
    public BeanForm<T> createWithNewBean() {
        T stub = editorService.createStub(getSupportedClass());
        return createWith(stub);
    }
}
