package com.prodoko.base.client.beans.dynamic;

import javax.annotation.PostConstruct;

import javafx.scene.control.Label;
import javafx.scene.control.Skin;

import org.springframework.beans.factory.annotation.Autowired;

import com.prodoko.base.client.beans.browser.BeansManager;
import com.prodoko.base.model.beans.dynamic.DynamicBean;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.fx.MirrorBinder;
import com.prodoko.fx.control.BreadcrumbsPane;
import com.prodoko.fx.control.BreadcrumbsPaneAware;

public abstract class BeansManagerWithHierarchy<T extends DynamicBean, D extends MainDynamicClass>
        extends Label implements BreadcrumbsPaneAware {
    
    private final static String DEFAULT_STYLE_CLASS = "beans-manager-with-hierarchy";
    
    private DynamicHierarchyEditor<D> hierarchyEditor;
    private BeansManager<T> beansManager;

    public BeansManagerWithHierarchy() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }
    
    /**
     * TODO
     */
    public DynamicHierarchyEditor<D> getHierarchyEditor() {
        return hierarchyEditor;
    }

    /**
     * TODO
     */
    public BeansManager<T> getBeansManager() {
        return beansManager;
    }
    
    @Autowired
    public void initHierarchyEditor(DynamicHierarchyEditor<D> hierarchyEditor) {
        this.hierarchyEditor = hierarchyEditor;
    }
    
    @Autowired
    public void initBeansManager(BeansManager<T> beansManager) {
        this.beansManager = beansManager;
    }
    
    @PostConstruct
    public void postConstruct() {
        MirrorBinder.bindLabelProperties(this, beansManager);
    }
    
    @Override
    public void setBreadcrumbsContext(BreadcrumbsPane pane, int yourIndex) {
        beansManager.setBreadcrumbsContext(pane, yourIndex);
        hierarchyEditor.setBreadcrumbsContext(pane, yourIndex);
    }
    
    @Override
    public int getBreadcrumbsIndex() {
        return beansManager.getBreadcrumbsIndex();
    }
    
    @Override
    public BreadcrumbsPane getBreadcrumbsPane() {
        return beansManager.getBreadcrumbsPane();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new BeansManagerWithHierarchySkin<BeansManagerWithHierarchy<T,D>, T, D>(this);
    }
}
