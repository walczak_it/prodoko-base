package com.prodoko.base.client.basic.legalentities.types;

import static com.prodoko.base.util.BundleUtils.message;

import org.controlsfx.control.action.Action;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.prodoko.base.client.beans.dynamic.DynamicHierarchyEditor;
import com.prodoko.base.model.basic.legalentities.types.LegalEntityType;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LegalEntityHierarhyEditor extends DynamicHierarchyEditor<LegalEntityType> {

    public LegalEntityHierarhyEditor() {
    }

    @Override
    protected String getCreateDynamicSubclassActionText() {
        return message(LegalEntityHierarhyEditor.class, "createDynamicSubclassAction");
    }
}
