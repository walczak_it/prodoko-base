package com.prodoko.base.client.beans;

import com.prodoko.fx.control.form.FieldsetGrid;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;

public abstract class SectionedBeanFormSkin<C extends BeanForm<T>, T> extends BeanFormSkin<C,T> {
    
    private ScrollPane sectionsScrollPane;
    private VBox sectionsPane;
    private TitledPane headerSectionPane;
    private FieldsetGrid headerSectionFieldset;

    public SectionedBeanFormSkin(C form) {
        super(form);
        sectionsScrollPane = createAndAddSectionsScrollPane();
        sectionsPane = createAndAddSectionsPane();
        headerSectionPane = createAndAddHeaderSectionPane();
        headerSectionFieldset = createAndAddHeaderSectionFieldset();
    }
    
    protected ScrollPane createAndAddSectionsScrollPane() {
        ScrollPane sectionsScrollPane = new ScrollPane();
        sectionsScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
        sectionsScrollPane.setFitToWidth(true);
        getFieldsPane().setContent(sectionsScrollPane);
        return sectionsScrollPane;
    }
    
    protected VBox createAndAddSectionsPane() {
        VBox sectionsPane = new VBox();
        sectionsScrollPane.setContent(sectionsPane);
        return sectionsPane;
    }

    protected TitledPane createAndAddHeaderSectionPane() {
        TitledPane headerSectionPane = new TitledPane();
        if(getSkinnable().getItem() != null) {
            setupItemsDescription(headerSectionPane);
        }
        sectionsPane.getChildren().add(headerSectionPane);
        return headerSectionPane;
    }
    
    protected FieldsetGrid createAndAddHeaderSectionFieldset() {
        FieldsetGrid headerSectionFieldset = new FieldsetGrid();
        headerSectionPane.setContent(headerSectionFieldset);
        return headerSectionFieldset;
    }
    
    @Override
    protected void itemChanged(T oldValue) {
        super.itemChanged(oldValue);
        if(getSkinnable().getItem() != null) {
            setupItemsDescription(headerSectionPane);
        }
    }

    /**
     * TODO
     */
    protected ScrollPane getSectionsScrollPane() {
        return sectionsScrollPane;
    }

    /**
     * TODO
     */
    protected VBox getSectionsPane() {
        return sectionsPane;
    }

    /**
     * TODO
     */
    protected TitledPane getHeaderSectionPane() {
        return headerSectionPane;
    }

    /**
     * TODO
     */
    protected FieldsetGrid getHeaderSectionFieldset() {
        return headerSectionFieldset;
    }
}
