package com.prodoko.base.client.beans.dynamic;

import static com.prodoko.base.util.BundleUtils.message;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;

import org.controlsfx.control.action.AbstractAction;

import com.prodoko.base.client.beans.BeanFormFactory;
import com.prodoko.base.model.beans.dynamic.DynamicClass;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.base.util.ClassSupportersMap;
import com.prodoko.fx.control.BreadcrumbsPaneAware;
import com.prodoko.fx.control.form.ItemForm;

public class CreateDynamicSubclassAction<T extends DynamicClass, RT extends MainDynamicClass>
        extends AbstractAction {
    
    private BreadcrumbsPaneAware paneAware;
    private ClassSupportersMap<DynamicClassFormFactory<? extends T, RT>> formFactories;
    private ObjectProperty<RT> dynamicSupperclass;
    
    public CreateDynamicSubclassAction(BreadcrumbsPaneAware paneAware,
            ClassSupportersMap<DynamicClassFormFactory<? extends T, RT>> formFactories) {
        this(message(CreateDynamicSubclassAction.class), paneAware, formFactories);
    }

    public CreateDynamicSubclassAction(String text, BreadcrumbsPaneAware paneAware,
            ClassSupportersMap<DynamicClassFormFactory<? extends T, RT>> formFactories) {
        super(text);
        this.paneAware = paneAware;
        this.formFactories = formFactories;
        this.dynamicSupperclass = new SimpleObjectProperty<>();
    }
    
    public ObjectProperty<RT> dynamicSupperclassProperty() {
        return dynamicSupperclass;
    }

    /**
     * TODO
     */
    public BreadcrumbsPaneAware getPaneAware() {
        return paneAware;
    }

    /**
     * TODO
     */
    public ClassSupportersMap<DynamicClassFormFactory<? extends T, RT>> getFormFactores() {
        return formFactories;
    }

    @Override
    public void execute(ActionEvent ae) {
        if(dynamicSupperclass.get() == null) {
            throw new IllegalStateException("No dynamic super class is selected");
        }
        DynamicClassFormFactory<? extends T, RT> formFactory = formFactories.getOrThrow(
                dynamicSupperclass.get().getClass());
        ItemForm<? extends T> form = formFactory.createWithNewBean(
                dynamicSupperclass.get().getId());
        paneAware.getBreadcrumbsPane().openAfter(paneAware.getBreadcrumbsIndex(), form);
    }
}
