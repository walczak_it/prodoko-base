package com.prodoko.base.client.basic.legalentities.types;

import com.prodoko.base.client.beans.dynamic.DynamicClassFormSkin;
import com.prodoko.base.model.basic.legalentities.types.PersonType;

public class PersonTypeFormSkin<
            C extends PersonTypeForm<T>,
            T extends PersonType>
        extends LegalEntityTypeFormSkin<C, T>{

    public PersonTypeFormSkin(C form) {
        super(form);
    }

}
