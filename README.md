Prodoko BASE
============

Spring based framework for customizable client-server business and manufacturing systems.

ALPHA version - work in progress ...

General developer libraries:

* prodoko-remoting - automated registration of export/asses remoting beans in Spring
* prodoko-graphics - graphic files like icons or logos
* prodoko-fx - various JavaFX controls

Components utilizing the Prodoko BASE domain model:

* prodoko-base-server-api - BASE domain model interfaces and data beans, also some utility classes/interfaces
* prodoko-base-server-lib - implements the interfaces from prodoko-base-server-api and provides general server functionality and configuration
* prodoko-base-client-lib - provides a general desktop client implementation for the BASE domain model

Examples that show how you can construct your own software based on Prodoko BASE:

* prodoko-base-server-example - example war project that utilizes prodoko-server-lib
* prodoko-base-client-example - example desktop client luncher that utilizes prodoko-client-lib

License
=======

All files (except the ones in prodoko-graphics) are available on the terms of the Apache License. See the LICENSE file.

Files in prodoko-graphics have various open licenses. Some require attribution of the authors. See the prodoko-base-graphics/CONTRIB file for more details.

Authors
=======

WALCZAK.IT company

Custom IT systems for business and manufacturing

Website: [walczak.it](http://walczak.it/)

