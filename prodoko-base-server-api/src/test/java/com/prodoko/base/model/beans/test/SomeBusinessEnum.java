package com.prodoko.base.model.beans.test;

import com.prodoko.base.model.beans.BusinessEnum;

@BusinessEnum
public enum SomeBusinessEnum {
    VALUE_A, VALUE_B, VALUE_C
}
