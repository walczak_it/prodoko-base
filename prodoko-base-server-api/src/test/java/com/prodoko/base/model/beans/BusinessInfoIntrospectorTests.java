package com.prodoko.base.model.beans;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.prodoko.base.model.beans.test.SomeBusinessEntity;
import com.prodoko.base.model.beans.test.SomeBusinessEnum;

@RunWith(JUnit4.class)
public class BusinessInfoIntrospectorTests {

    @Test
    public void localizedBeanDescriptionSimpleTest() throws Exception {
        LocalisedDescription desc = BusinessDescriptionIntrospector.getInstance()
                .beanDescription(SomeBusinessEntity.class).getLocalised();
        assertTrue(desc.isDisplayNameResolved());
        assertEquals("Some entity", desc.getDisplayName());
        assertTrue(desc.isShortDescriptionResolved());
        assertEquals("Some random entity for tests", desc.getShortDescription());
        assertNull(desc.getMoreInfo());
        assertFalse(desc.isAllResolved());
    }
    
    @Test
    public void localizedPropertyDescriptionSimpleTest() throws Exception {
        LocalisedDescription desc = BusinessDescriptionIntrospector.getInstance()
                .propertyDescription(SomeBusinessEntity.class, "propA").getLocalised();
        assertTrue(desc.isDisplayNameResolved());
        assertEquals("Prop A", desc.getDisplayName());
        assertFalse(desc.isShortDescriptionResolved());
        assertEquals("http://localhost/propA", desc.getMoreInfo().toString());
        assertFalse(desc.isAllResolved());
    }
    
    @Test
    public void beansPropertiesDescriptionsTest() throws Exception {
        List<BusinessPropertyDescription> props = BusinessDescriptionIntrospector.getInstance()
                .beansPropertiesDescriptions(SomeBusinessEntity.class);
        BusinessPropertyDescription propCDesc = props.get(0);
        BusinessPropertyDescription propADesc = props.get(1);
        assertNotNull(propCDesc);
        assertEquals("propC", propCDesc.getPropertyName());
        assertNotNull(propADesc);
        assertEquals("propA", propADesc.getPropertyName());
        assertEquals(2, props.size());
    }
    
    @Test
    public void localizedEnumDescriptionTest() throws Exception {
        BusinessEnumDescription desc = BusinessDescriptionIntrospector.getInstance()
                .enumDescription(SomeBusinessEnum.class);
        assertEquals("Some enum", desc.getLocalised().getDisplayName());
        LocalisedDescription valueADesc = desc.getValuesLocalised().get(SomeBusinessEnum.VALUE_A);
        assertEquals("Value A", valueADesc.getDisplayName());
        LocalisedDescription valueCDesc = desc.getValuesLocalised().get(SomeBusinessEnum.VALUE_C);
        assertEquals("Value C", valueCDesc.getDisplayName());
        assertEquals("Some random enum value", valueCDesc.getShortDescription());
    }
}
