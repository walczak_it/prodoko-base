package com.prodoko.base.model.beans.test;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessProperty;

@BusinessBean
public class SomeBusinessEntity {

    private String propA;
    private String propB;
    private boolean propC;

    @BusinessProperty
    public String getPropA() {
        return propA;
    }

    public void setPropA(String propA) {
        this.propA = propA;
    }

    public String getPropB() {
        return propB;
    }

    public void setPropB(String propB) {
        this.propB = propB;
    }

    @BusinessProperty(primary=true)
    public boolean getPropC() {
        return propC;
    }

    public void setPropC(boolean propC) {
        this.propC = propC;
    }
}
