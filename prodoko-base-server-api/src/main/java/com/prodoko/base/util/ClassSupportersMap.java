package com.prodoko.base.util;

import java.util.Collection;
import java.util.HashMap;

public class ClassSupportersMap<T extends ClassSupporter> extends HashMap<Class<?>, T> {

    private static final long serialVersionUID = -6500320153713069190L;
    
    public ClassSupportersMap() {
        super();
    }
    
    public ClassSupportersMap(Collection<T> supporters) {
        super(supporters.size());
        putAllAbsentOrThrow(supporters);
    }
    
    public ClassSupportersMap(ClassSupportersMap<T> map, Collection<T> moreSupporters) {
        super(map);
        putAllAbsentOrThrow(moreSupporters);
    }

    public void putAllAbsentOrThrow(Collection<T> supporters) {
        for(T supporter : supporters) {
            assureAbsent(supporter);
        }
        for(T supporter : supporters) {
            put(supporter);
        }
    }
    
    public void putAbsentOrThrow(T supporter) {
        assureAbsent(supporter);
        put(supporter);
    }
    
    public void put(T supporter) {
        put(supporter.getSupportedClass(), supporter);
    }
    
    public T getOrThrow(Class<?> clazz) {
        T supporter = get(clazz);
        if(supporter == null) {
            throw new IllegalArgumentException("Supporter for class " + clazz.getName()
                    + " not found");
        }
        return supporter;
    }
    
    protected void assureAbsent(T supporter) {
        if(containsKey(supporter.getSupportedClass())) {
            throw new IllegalArgumentException("Duplicate supporters for class "
                    + supporter.getSupportedClass().getName());
        }
    }
}
