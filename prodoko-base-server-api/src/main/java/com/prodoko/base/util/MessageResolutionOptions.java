package com.prodoko.base.util;

public class MessageResolutionOptions {

    private boolean returnNull = false;
    private boolean forceMainBundle = false;
    
    private static final MessageResolutionOptions inst = new MessageResolutionOptions();
    
    public static MessageResolutionOptions getDefaultInstance() {
        return inst;
    }
    
    /**
     * TODO
     */
    public boolean isReturnNull() {
        return returnNull;
    }
    
    /**
     * @see #getReturnNull
     */
    public void setReturnNull(boolean returnNull) {
        this.returnNull = returnNull;
    }

    /**
     * TODO
     */
    public boolean isForceMainBundle() {
        return forceMainBundle;
    }

    /**
     * @see #getForceMainBundle
     */
    public void setForceMainBundle(boolean forceMainBundle) {
        this.forceMainBundle = forceMainBundle;
    }
}
