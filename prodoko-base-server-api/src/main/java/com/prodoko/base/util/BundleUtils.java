package com.prodoko.base.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;

public class BundleUtils {
    
    private static final Log LOG = LogFactory.getLog(BundleUtils.class);
    
    private BundleUtils() {}
    
    public static String message(Class<?> preffixClass) {
        return message(preffixClass, null, LocaleContextHolder.getLocale());
    }
    
    public static String message(Class<?> preffixClass, String keySuffix) {
        return message(preffixClass, keySuffix, LocaleContextHolder.getLocale());
    }
    
    public static String message(Class<?> preffixClass, MessageResolutionOptions options) {
        return message(preffixClass, null, LocaleContextHolder.getLocale(), options);
    }
    
    public static String message(Class<?> preffixClass, String keySuffix,
            MessageResolutionOptions options) {
        return message(preffixClass, keySuffix, LocaleContextHolder.getLocale(), options);
    }
    
    public static String message(Class<?> preffixClass, String keySuffix, Locale locale) {
        return message(preffixClass, keySuffix, locale,
                MessageResolutionOptions.getDefaultInstance());
    }
    
    public static String message(Class<?> preffixClass, String keySuffix, Locale locale,
            MessageResolutionOptions options) {
        return message(messageBundleBaseName(preffixClass), messageKey(preffixClass, keySuffix),
                locale, options);
    }
    
    public static String message(String bundleBaseName, String key) {
        return message(bundleBaseName, key, LocaleContextHolder.getLocale(),
                MessageResolutionOptions.getDefaultInstance());
    }
    
    public static String message(String bundleBaseName, String key, Locale locale,
            MessageResolutionOptions options) {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(bundleBaseName,
                    locale, BundleUtils.class.getClassLoader(),
                    new UTF8Control());
            return bundle.getString(key);
        } catch (MissingResourceException ex) {
            String unresovledMessage = "?" + key + "?";
            if(options.isReturnNull()) {
                LOG.debug("Expected Unresolved key bundleBaseName=" + bundleBaseName
                        + ", key=" + key);
                return null;
            } else {
                LOG.error("Unresolved key bundleBaseName=" + bundleBaseName
                        + ", key=" + key);
                return unresovledMessage;
            }
        }
    }
    
    public static String messageBundleBaseName(Class<?> clazz) {
        String bundleBaseName = clazz.getPackage().getName() + ".messages";
        return bundleBaseName;
    }

    public static String messageKey(Class<?> preffixClass) {
        return messageKey(preffixClass, null);
    }

    public static String messageKey(Class<?> preffixClass, String keySuffix) {
        String clazzName = preffixClass.getSimpleName();
        Class<?> enclosingClass = preffixClass.getEnclosingClass();
        while(enclosingClass != null) {
            clazzName = enclosingClass.getSimpleName() + "." + clazzName;
            enclosingClass = enclosingClass.getEnclosingClass();
        }
        if(keySuffix != null) {
            return clazzName + "." + keySuffix;
        } else {
            return clazzName;
        }
    }

    private static class UTF8Control extends Control {
        public ResourceBundle newBundle(String baseName, Locale locale,
                String format, ClassLoader loader, boolean reload)
                throws IllegalAccessException, InstantiationException,
                IOException {
            // The below is a copy of the default implementation.
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");
            ResourceBundle bundle = null;
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                try {
                    // Only this line is changed to make it to read properties
                    // files as UTF-8.
                    bundle = new PropertyResourceBundle(new InputStreamReader(
                            stream, "UTF-8"));
                } finally {
                    stream.close();
                }
            }
            return bundle;
        }
    }
}
