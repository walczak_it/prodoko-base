package com.prodoko.base.util;

import java.util.Map;

public class ClassUtils extends org.springframework.util.ClassUtils {

    public static <T> T findByClosestClass(Class<?> clazz, Map<Class<?>, T> map) {
        Class<?> clazzItr = clazz;
        do {
            T obj = map.get(clazzItr);
            if(obj != null) {
                return obj;
            }
            clazzItr = clazzItr.getSuperclass();
        } while(clazzItr != null);
        return null;
    }
}
