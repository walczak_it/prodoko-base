package com.prodoko.base.util;

public interface ClassSupporter {

    public Class<?> getSupportedClass();
    
    public boolean isSupportsSubclasses();
}
