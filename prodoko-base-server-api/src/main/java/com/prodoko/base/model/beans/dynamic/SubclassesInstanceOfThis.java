package com.prodoko.base.model.beans.dynamic;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SubclassesInstanceOfThisValidator.class)
@Documented
@Inherited
public @interface SubclassesInstanceOfThis {
    String message() default "blabla";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
