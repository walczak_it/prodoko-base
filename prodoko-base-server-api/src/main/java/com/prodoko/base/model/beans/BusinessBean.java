package com.prodoko.base.model.beans;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.prodoko.base.model.beans.filter.FilterData;

@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface BusinessBean {
    public Class<? extends FilterData>[] possibleFilters() default {};
    public boolean useFirstFilterAsDefault() default true;
}
