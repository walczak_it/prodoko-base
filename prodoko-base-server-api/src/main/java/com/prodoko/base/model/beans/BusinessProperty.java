package com.prodoko.base.model.beans;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.prodoko.base.model.beans.filter.FilterData;
import com.prodoko.base.model.beans.presentation.SimpleStringPresentation;
import com.prodoko.base.model.beans.presentation.TypePresentation;


@Target( { ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface BusinessProperty {

    public boolean primary() default false;
    public RelationDistance relation() default RelationDistance.CLOSE;
    public Class<? extends TypePresentation>[] possiblePresentations()
        default {SimpleStringPresentation.class};
    public Class<? extends FilterData>[] possibleFilters() default {};
    public boolean useFirstFilterAsDefault() default true;
    
    public static enum RelationDistance {
        CLOSE, MEDIUM, FAR;
    }
}
