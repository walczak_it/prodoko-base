package com.prodoko.base.model.beans.dynamic;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.prodoko.base.model.beans.BusinessBean;

@BusinessBean
@Embeddable
public class GridPosition implements Serializable {

    private static final long serialVersionUID = -2317959705829896455L;
    
    private Integer rowIndex;
    private Integer columnIndex;
    private Integer rowSpan;
    private Integer columnSpan;

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    public Integer getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(Integer columnIndex) {
        this.columnIndex = columnIndex;
    }

    /**
     * TODO
     */
    public Integer getRowSpan() {
        return rowSpan;
    }
    
    /**
     * @see #getRowSpan
     */
    public void setRowSpan(Integer rowSpan) {
        this.rowSpan = rowSpan;
    }
    
    /**
     * TODO
     */
    public Integer getColumnSpan() {
        return columnSpan;
    }
    
    /**
     * @see #getColumnSpan
     */
    public void setColumnSpan(Integer columnSpan) {
        this.columnSpan = columnSpan;
    }
}
