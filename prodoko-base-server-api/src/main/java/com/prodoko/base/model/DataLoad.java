package com.prodoko.base.model;

import java.io.Serializable;
import java.util.List;

import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotablePropertiesHolder;

@RemotablePropertiesHolder
public class DataLoad<T> implements Serializable {

    private static final long serialVersionUID = 5124215758594613411L;
    
    private List<T> data;
    private int totalResults;
    
    /**
     * TODO
     */
    @CollectionPropertyRemoting
    public List<T> getData() {
        return data;
    }
    
    /**
     * @see #getData
     */
    public void setData(List<T> data) {
        this.data = data;
    }

    /**
     * TODO
     */
    public int getTotalResults() {
        return totalResults;
    }

    /**
     * @see #getTotalResults
     */
    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
}
