package com.prodoko.base.model.conf.security;

import com.prodoko.base.model.beans.BeanFinderService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface UserLoginFinderService extends BeanFinderService<UserLogin> {

}
