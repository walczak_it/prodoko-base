package com.prodoko.base.model.beans.presentation;

import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class NamedBeanPresentation extends BeanPresentation {

    private static final long serialVersionUID = 5621765324332353702L;
    
    private String presentationName;

    /**
     * TODO
     */
    @NotBlank
    public String getPresentationName() {
        return presentationName;
    }
    
    /**
     * @see #getPresentationName
     */
    public void setPresentationName(String presentationName) {
        this.presentationName = presentationName;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NamedBeanPresentation [beanClass=" + getBaseBeanClass()
                + ", presentationName=" + presentationName + ", getId()="
                + getId() + "]";
    } 
}
