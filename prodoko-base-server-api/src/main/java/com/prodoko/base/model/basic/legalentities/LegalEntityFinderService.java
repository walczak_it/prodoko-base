package com.prodoko.base.model.basic.legalentities;

import com.prodoko.base.model.beans.BeanFinderService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface LegalEntityFinderService extends BeanFinderService<LegalEntity> {

}
