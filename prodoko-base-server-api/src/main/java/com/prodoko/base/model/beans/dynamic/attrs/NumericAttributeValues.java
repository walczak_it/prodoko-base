package com.prodoko.base.model.beans.dynamic.attrs;

import javax.persistence.Embeddable;

import com.prodoko.base.model.beans.dynamic.DynamicAttributeValues;

@Embeddable
public class NumericAttributeValues extends DynamicAttributeValues<Double> {

    private static final long serialVersionUID = -4582469728744774519L;
    
}
