package com.prodoko.base.model;

public class BaseSystemInfo {

    public String getSystemName() {
        return "Prodoko BASE";
    }
    
    public String getSystemsLogoPath() {
        return "/com/prodoko/graphics/logos/prodokoBase.png";
    }
    
    final public String getBaseLayerVersion() {
        return "0.1";
    }
    
    public String getCustomLayerVersion() {
        return null;
    }
}
