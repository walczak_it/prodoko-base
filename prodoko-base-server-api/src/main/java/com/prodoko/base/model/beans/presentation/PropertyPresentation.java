package com.prodoko.base.model.beans.presentation;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;
import com.prodoko.base.model.beans.BusinessPropertyDescription;

@Entity
public class PropertyPresentation implements Serializable {

    private static final long serialVersionUID = 8855772513802989117L;
    
    private Long id;
    private BeanPresentation beanPresentation;
    private Class<?> declaringBean;
    private String propertyName;
    private TypePresentation propertiesTypePresentation;

    public PropertyPresentation() { }

    public PropertyPresentation(PropertyPresentation original, BeanPresentation newOwner) {
        super();
        this.id = original.getId();
        this.beanPresentation = newOwner;
        this.declaringBean = original.getDeclaringBean();
        this.propertyName = original.getPropertyName();
        this.propertiesTypePresentation = original.getPropertiesTypePresentation().clone();
    }

    public PropertyPresentation(BeanPresentation beanPresentation, Class<?> declaringBean,
            String propertyName, TypePresentation propertiesTypePresentation) {
        super();
        this.beanPresentation = beanPresentation;
        this.declaringBean = declaringBean;
        this.propertyName = propertyName;
        this.propertiesTypePresentation = propertiesTypePresentation;
    }

    @Id @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * TODO
     */
    @ManyToOne
    @NotNull
    public BeanPresentation getBeanPresentation() {
        return beanPresentation;
    }

    /**
     * @see #getBeanPresentation
     */
    public void setBeanPresentation(BeanPresentation beanPresentation) {
        this.beanPresentation = beanPresentation;
    }
    
    /**
     * TODO
     */
    @NotNull
    public Class<?> getDeclaringBean() {
        return declaringBean;
    }

    /**
     * @see #getDeclaringBean
     */
    public void setDeclaringBean(Class<?> declaringBean) {
        this.declaringBean = declaringBean;
    }


    /**
     * TODO
     */
    @NotEmpty
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @see #setPropertyName
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
    
    /**
     * TODO
     */
    @OneToOne
    public TypePresentation getPropertiesTypePresentation() {
        return propertiesTypePresentation;
    }

    /**
     * @see #getPropertiesTypePresentation
     */
    public void setPropertiesTypePresentation(
            TypePresentation propertiesTypePresentation) {
        this.propertiesTypePresentation = propertiesTypePresentation;
    }
    
    @Transient
    public BusinessPropertyDescription getPropertyDescription() {
        return BusinessDescriptionIntrospector.getInstance().propertyDescription(
                getBeanPresentation().getBaseBeanClass(), getPropertyName());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PropertyPresentation other = (PropertyPresentation) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TypePresentation [id=" + id + "]";
    }
    
    public PropertyPresentation clone(BeanPresentation newOwner) {
        return new PropertyPresentation(this, newOwner);
    }
}
