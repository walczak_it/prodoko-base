package com.prodoko.base.model.beans;

import java.util.Map;

public class BusinessEnumDescription extends BusinessDescription<BusinessEnum> {

    private Class<?> enumClass;
    private Map<Object, LocalisedDescription> valuesLocalised;
    
    /**
     * TODO
     */
    public Class<?> getEnumClass() {
        return enumClass;
    }
    
    /**
     * @see #getEnumClass
     */
    public void setEnumClass(Class<?> enumClass) {
        this.enumClass = enumClass;
    }
    
    /**
     * TODO
     */
    public Map<Object, LocalisedDescription> getValuesLocalised() {
        return valuesLocalised;
    }
    
    /**
     * @see #getValuesLocalized
     */
    public void setValuesLocalized(Map<Object, LocalisedDescription> valuesLocalized) {
        this.valuesLocalised = valuesLocalized;
    }
}
