package com.prodoko.base.model.basic;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.RemotablePropertiesHolder;
import com.prodoko.remoting.support.cloning.UpdatingMode;

@Entity
@RemotableAsClone
@RemotablePropertiesHolder
public class Site extends StandardEntity {

    private static final long serialVersionUID = -8187475583630587266L;
    
    private String type;
    private Class<?> typeEnumClass;
    private String description; 
    private Address address;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Class<?> getTypeEnumClass() {
        return typeEnumClass;
    }

    public void setTypeEnumClass(Class<?> typeEnumClass) {
        this.typeEnumClass = typeEnumClass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NotNull
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_DEEP, updating=UpdatingMode.UPDATE)
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
    @Override
    public void initStub() {
        super.initStub();
        this.address = new Address();
    }
}
