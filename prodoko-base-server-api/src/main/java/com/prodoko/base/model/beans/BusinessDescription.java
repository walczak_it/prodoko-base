package com.prodoko.base.model.beans;

import java.lang.annotation.Annotation;

public abstract class BusinessDescription<T extends Annotation> {

    private T metadata;
    private LocalisedDescription localised;
    
    /**
     * TODO
     */
    
    public T getMetadata() {
        return metadata;
    }
    
    /**
     * @see #getMetadata
     */
    public void setMetadata(T metadata) {
        this.metadata = metadata;
    }
    
    /**
     * TODO
     */
    public LocalisedDescription getLocalised() {
        return localised;
    }
    
    /**
     * @see #getLocalised
     */
    public void setLocalised(LocalisedDescription localised) {
        this.localised = localised;
    }
}
