package com.prodoko.base.model.conf.security;

import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface UserRoleFinderService extends MainDynamicClassFinderService<UserRole> {

}
