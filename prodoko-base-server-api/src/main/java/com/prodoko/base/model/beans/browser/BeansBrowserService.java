package com.prodoko.base.model.beans.browser;

import com.prodoko.base.model.DataLoad;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.remoting.support.cloning.CloneRemotedProperties;

public interface BeansBrowserService<T> extends ClassSupporter {
    
    @Override
    public Class<T> getSupportedClass();

    public @CloneRemotedProperties DataLoad<T> browse(BeansLoadRequest request);
}
