package com.prodoko.base.model.beans.dynamic;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.UpdatingMode;

@Entity
public class HelperDynamicClass extends DynamicClass {

    private static final long serialVersionUID = 3477982168476054842L;
    
    private MainDynamicClass limitingDynamicClass;

    @ManyToOne
    public MainDynamicClass getLimitingDynamicClass() {
        return limitingDynamicClass;
    }

    public void setLimitingDynamicClass(MainDynamicClass limitingDynamicClass) {
        this.limitingDynamicClass = limitingDynamicClass;
    }

    @Override
    @Transient
    @BusinessProperty
    @BeanPropertyRemoting(cloning = CloningMode.CHOPPED,
            updating = UpdatingMode.FIND_CORRESPONDING_AND_UPDATE)
    public MainDynamicClass getDynamicSuperclass() {
        return getLimitingDynamicClass();
    }

    @Override
    public void setDynamicSuperclass(MainDynamicClass dynClass) {
        setLimitingDynamicClass(dynClass);
    }
}
