package com.prodoko.base.model.basic.legalentities.types;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.prodoko.base.model.basic.legalentities.Organization;
import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.dynamic.DynamicBean;

@BusinessBean
@Entity
public class OrganizationType extends LegalEntityType {

    private static final long serialVersionUID = 1046896334499420802L;

    @Override
    @Transient
    public Class<? extends DynamicBean> getSupportedClass() {
        return Organization.class;
    }
}
