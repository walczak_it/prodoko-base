package com.prodoko.base.model.constraints;

public class StringConsts {
    
    public static final int VERY_SHORT_SIZE = 24;
    public static final int SHORT_SIZE = 128;
    public static final int MEDIUM_SIZE = 1024;
}
