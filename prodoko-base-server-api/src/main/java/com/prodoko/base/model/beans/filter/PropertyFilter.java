package com.prodoko.base.model.beans.filter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;

@Entity
public abstract class PropertyFilter extends FilterData {

    private static final long serialVersionUID = -4841149071893537519L;
    
    private Class<?> appliableBeanClass;
    private String propertyPath;
    
    public PropertyFilter() {
        
    }
    
    public PropertyFilter(PropertyFilter original, BeansSubsetPresentation newOwner) {
        super(original, newOwner);
        this.propertyPath = original.getPropertyPath();
    }

    /**
     * TODO
     */
    @NotNull
    public Class<?> getAppliableBeanClass() {
        return appliableBeanClass;
    }

    /**
     * @see #getAppliableBeanClass
     */
    public void setAppliableBeanClass(Class<?> appliableBeanClass) {
        this.appliableBeanClass = appliableBeanClass;
    }

    /**
     * TODO
     */
    @NotEmpty
    public String getPropertyPath() {
        return propertyPath;
    }

    /**
     * @see #getProperty
     */
    public void setPropertyPath(String propertyPath) {
        this.propertyPath = propertyPath;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PropertyFilter [id=" + getId() + "]";
    }
    
    @Override
    public abstract PropertyFilter clone(BeansSubsetPresentation newOwner);
}
