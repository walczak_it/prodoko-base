package com.prodoko.base.model.beans.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.prodoko.base.model.beans.BusinessBeanDescription;
import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;

@Entity
public class BeanPresentation extends TypePresentation {

    private static final long serialVersionUID = -7577028151322670058L;
    
    private Class<?> baseBeanClass;
    private List<PropertyPresentation> presentedProperties = new ArrayList<>();
    
    public BeanPresentation() {
        
    }

    public BeanPresentation(BeanPresentation original) {
        super(original);
        this.baseBeanClass = original.getBaseBeanClass();
        for(PropertyPresentation p : original.getPresentedProperties()) {
            presentedProperties.add(p.clone(this));
        }
    }

    /**
     * TODO
     */
    @NotNull
    public Class<?> getBaseBeanClass() {
        return baseBeanClass;
    }

    /**
     * @see #getBaseBeanClass
     */
    public void setBaseBeanClass(Class<?> beanClass) {
        this.baseBeanClass = beanClass;
    }

    /**
     * TODO
     */
    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="beanPresentation")
    @OrderColumn
    public List<PropertyPresentation> getPresentedProperties() {
        return presentedProperties;
    }
    
    /**
     * @see #getPresentedProperties
     */
    public void setPresentedProperties(
            List<PropertyPresentation> presentedProperties) {
        this.presentedProperties = presentedProperties;
    }
    
    @Transient
    public BusinessBeanDescription getBaseBeanDescription() {
        return BusinessDescriptionIntrospector.getInstance().beanDescription(getBaseBeanClass());
    }
    
    @Override
    public BeanPresentation clone() {
        return new BeanPresentation(this);
    }
}
