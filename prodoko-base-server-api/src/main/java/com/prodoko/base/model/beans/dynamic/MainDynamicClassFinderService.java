package com.prodoko.base.model.beans.dynamic;

import java.util.List;
import java.util.Set;

import com.prodoko.base.model.beans.BeanFinderService;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;
import com.prodoko.remoting.support.cloning.CloneRemotedCollection;
import com.prodoko.remoting.support.cloning.CloneRemotedProperties;

public interface MainDynamicClassFinderService<T extends MainDynamicClass>
    extends BeanFinderService<T> {
    
    public boolean hasMostSuperclass(Class<? extends T> dynamicClassClass);
    
    public boolean isMostSuperclass(Class<? extends T> dynamicClassClass, Long id);
    
    public @CloneRemotedBean <TT extends T> TT findMostSuperclass(
            Class<TT> dynamicClassClass);
    
    public @CloneRemotedBean <TT extends T> TT findMostSuperclassForBean(
            Class<? extends DynamicBean> dynamicBeanClass);
    
    public @CloneRemotedCollection <TT extends T> Set<TT> findMostSuperclassSubclasses(
            Class<TT> dynamicClassClass);

    public @CloneRemotedCollection <TT extends T> Set<TT> findSubclasses(
            Class<TT> dynamicClassClass, Long superId);
    
    public @CloneRemotedCollection <TT extends T> List<TT> findSubclassesDeep(
            Class<TT> dynamicClassClass, Long superId);
    
    public @CloneRemotedProperties HierarchyDto<T> createHierarchyDto();
    
    public @CloneRemotedProperties <TT extends T> HierarchyDto<TT> createHierarchyDto(
            Class<TT> rootDynamicClassClass);
}
