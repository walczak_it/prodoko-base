package com.prodoko.base.model.beans.filter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessDescriptionIntrospector;
import com.prodoko.base.model.beans.BusinessEnum;
import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;

@Entity
@BusinessBean
public class StringPropertyFilter extends PropertyFilter {
    
    private static final long serialVersionUID = -1077763758852738097L;
    
    private Operator operator;
    private String comparingString;
    
    public StringPropertyFilter() {
        
    }
    
    public StringPropertyFilter(StringPropertyFilter original, BeansSubsetPresentation newOwner) {
        super(original, newOwner);
        this.operator = original.getOperator();
        this.comparingString = original.getComparingString();
    }
    
    @BusinessEnum
    public static enum Operator {
        CONTAINS, STARTS_WITH, ENDS_WITH, EXACTLY_THE_SAME;
    }

    /**
     * TODO
     */
    @NotNull
    public Operator getOperator() {
        return operator;
    }

    /**
     * @see #getOperator
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * TODO
     */
    @NotEmpty
    public String getComparingString() {
        return comparingString;
    }

    /**
     * @see #getComparingString
     */
    public void setComparingString(String comparingString) {
        this.comparingString = comparingString;
    }
    
    @Override
    public String toDisplayString() {
        StringBuilder sb = new StringBuilder();
        Class<?> subjectBean = getPresentation().getBaseBeanClass();
        String propertyDisplayName = BusinessDescriptionIntrospector.getInstance()
                .deepPropertyDescription(subjectBean, getPropertyPath()).getLocalised()
                .getDisplayName();
        sb.append(propertyDisplayName);
        String operatorString = BusinessDescriptionIntrospector.getInstance().enumValueDescription(
                getOperator()).getDisplayName();
        sb.append(' ').append(operatorString).append(' ').append(getComparingString());
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StringPropertyFilter [operator=" + operator
                + ", comparingString=" + comparingString
                + ", getId()=" + getId() + "]";
    }
    
    @Override
    public PropertyFilter clone(BeansSubsetPresentation newOwner) {
        return new StringPropertyFilter(this, newOwner);
    }
}
