package com.prodoko.base.model.conf.security;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.dynamic.DynamicBean;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@BusinessBean
@Entity
@RemotableAsClone
public class UserRole extends MainDynamicClass {

    private static final long serialVersionUID = -874091809925224681L;

    @Override
    @Transient
    public Class<? extends DynamicBean> getSupportedClass() {
        return UserLogin.class;
    }
}
