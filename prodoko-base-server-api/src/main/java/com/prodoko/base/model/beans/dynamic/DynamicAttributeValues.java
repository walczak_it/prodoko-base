package com.prodoko.base.model.beans.dynamic;

import java.io.Serializable;

public abstract class DynamicAttributeValues<T> implements Serializable {
    
    private static final long serialVersionUID = -7650129748374632631L;

    public static final int VALUES_LIMIT = 20;

    private T value0;
    private T value1;
    private T value2;
    private T value3;
    private T value4;
    private T value5;
    private T value6;
    private T value7;
    private T value8;
    private T value9;
    private T value10;
    private T value11;
    private T value12;
    private T value13;
    private T value14;
    private T value15;
    private T value16;
    private T value17;
    private T value18;
    private T value19;
    
    public T getValue0() {
        return value0;
    }
    
    public void setValue0(T value0) {
        this.value0 = value0;
    }

    public T getValue1() {
        return value1;
    }

    public void setValue1(T value1) {
        this.value1 = value1;
    }

    public T getValue2() {
        return value2;
    }

    public void setValue2(T value2) {
        this.value2 = value2;
    }

    public T getValue3() {
        return value3;
    }

    public void setValue3(T value3) {
        this.value3 = value3;
    }

    public T getValue4() {
        return value4;
    }

    public void setValue4(T value4) {
        this.value4 = value4;
    }

    public T getValue5() {
        return value5;
    }

    public void setValue5(T value5) {
        this.value5 = value5;
    }

    public T getValue6() {
        return value6;
    }

    public void setValue6(T value6) {
        this.value6 = value6;
    }

    public T getValue7() {
        return value7;
    }

    public void setValue7(T value7) {
        this.value7 = value7;
    }

    public T getValue8() {
        return value8;
    }

    public void setValue8(T value8) {
        this.value8 = value8;
    }

    public T getValue9() {
        return value9;
    }

    public void setValue9(T value9) {
        this.value9 = value9;
    }

    public T getValue10() {
        return value10;
    }

    public void setValue10(T value10) {
        this.value10 = value10;
    }

    public T getValue11() {
        return value11;
    }

    public void setValue11(T value11) {
        this.value11 = value11;
    }

    public T getValue12() {
        return value12;
    }

    public void setValue12(T value12) {
        this.value12 = value12;
    }

    public T getValue13() {
        return value13;
    }

    public void setValue13(T value13) {
        this.value13 = value13;
    }

    public T getValue14() {
        return value14;
    }

    public void setValue14(T value14) {
        this.value14 = value14;
    }

    public T getValue15() {
        return value15;
    }

    public void setValue15(T value15) {
        this.value15 = value15;
    }

    public T getValue16() {
        return value16;
    }

    public void setValue16(T value16) {
        this.value16 = value16;
    }

    public T getValue17() {
        return value17;
    }

    public void setValue17(T value17) {
        this.value17 = value17;
    }

    public T getValue18() {
        return value18;
    }

    public void setValue18(T value18) {
        this.value18 = value18;
    }

    public T getValue19() {
        return value19;
    }

    public void setValue19(T value19) {
        this.value19 = value19;
    }
}
