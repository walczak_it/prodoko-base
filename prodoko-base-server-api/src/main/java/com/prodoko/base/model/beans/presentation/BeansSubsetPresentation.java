package com.prodoko.base.model.beans.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

import com.prodoko.base.model.beans.filter.BeanOrder;
import com.prodoko.base.model.beans.filter.FilterData;

@Entity
public class BeansSubsetPresentation extends BeanPresentation {

    private static final long serialVersionUID = -1292593778644336380L;
    
    private List<FilterData> filters = new ArrayList<>();
    private List<BeanOrder> orders = new ArrayList<>();
    
    public BeansSubsetPresentation() {
        
    }

    public BeansSubsetPresentation(BeansSubsetPresentation original) {
        super(original);
        for(FilterData f : original.getFilters()) {
            filters.add(f.clone(this));
        }
        for(BeanOrder o : original.getOrders()) {
            orders.add(o.clone(this));
        }
    }

    /**
     * TODO
     */
    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="presentation")
    @OrderColumn
    public List<FilterData> getFilters() {
        return filters;
    }

    /**
     * @see #getFilters
     */
    public void setFilters(List<FilterData> filters) {
        this.filters = filters;
    }

    /**
     * TODO
     */
    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="presentation")
    @OrderColumn
    public List<BeanOrder> getOrders() {
        return orders;
    }

    /**
     * @see #getOrders
     */
    public void setOrders(List<BeanOrder> orders) {
        this.orders = orders;
    }
    
    @Override
    public BeansSubsetPresentation clone() {
        return new BeansSubsetPresentation(this);
    }
}
