package com.prodoko.base.model.entities;

public interface MinimalDataLoaderService {

    public void loadMinimalDataIfNeeded();
}
