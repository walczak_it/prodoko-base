package com.prodoko.base.model;

import java.io.Serializable;

public class DataLoadRequest implements Serializable {

    private static final long serialVersionUID = -3895580761464584514L;
    
    private int firstResult;
    private int maxResults;
    
    /**
     * TODO
     */
    public int getFirstResult() {
        return firstResult;
    }
    
    /**
     * @see #setFirstResult
     */
    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }
    
    /**
     * TODO
     */
    public int getMaxResults() {
        return maxResults;
    }
    
    /**
     * @see #setMaxResults
     */
    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }
}
