package com.prodoko.base.model.beans.dynamic;

import com.prodoko.base.model.entities.EntityEditorService;

public interface MainDynamicClassEditorService<T extends MainDynamicClass>
        extends EntityEditorService<T> {

}
