package com.prodoko.base.model.basic.legalentities;

import javax.persistence.Entity;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@BusinessBean
@Entity
@RemotableAsClone
public abstract class Organization extends LegalEntity {

    private static final long serialVersionUID = -5773175911153513155L;

}
