package com.prodoko.base.model.beans.filter;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;

@Entity
public abstract class FilterData implements Serializable {

    private static final long serialVersionUID = 1391349558157198336L;
    private Long id;
    private BeansSubsetPresentation presentation;
    
    public FilterData() {
        
    }
    
    public FilterData(FilterData original, BeansSubsetPresentation newOwner) {
        this.id = original.getId();
        this.presentation = newOwner;
    }

    @Id @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * TODO
     */
    @ManyToOne
    @NotNull
    public BeansSubsetPresentation getPresentation() {
        return presentation;
    }

    /**
     * @see #getPresentation
     */
    public void setPresentation(BeansSubsetPresentation presentation) {
        this.presentation = presentation;
    }

    public abstract String toDisplayString();
    
    @Transient
    public abstract Class<?> getAppliableBeanClass();

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Filter [id=" + id + "]";
    }

    public abstract FilterData clone(BeansSubsetPresentation newOwner);
}
