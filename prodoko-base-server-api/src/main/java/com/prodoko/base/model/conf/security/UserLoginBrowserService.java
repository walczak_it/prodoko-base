package com.prodoko.base.model.conf.security;

import com.prodoko.base.model.beans.browser.BeansBrowserService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface UserLoginBrowserService
        extends BeansBrowserService<UserLogin> {

}
