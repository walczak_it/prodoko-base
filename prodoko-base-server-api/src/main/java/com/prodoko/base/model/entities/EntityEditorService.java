package com.prodoko.base.model.entities;

import com.prodoko.base.util.ClassSupporter;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;
import com.prodoko.remoting.support.cloning.CloneRemotedProperties;
import com.prodoko.remoting.support.cloning.UncloneRemotedBean;

public interface EntityEditorService<T extends StandardEntity> extends ClassSupporter {
    
    @Override
    public Class<T> getSupportedClass();
    
    public @CloneRemotedProperties <E extends T> E createStub(Class<E> stubClass);

    public @CloneRemotedBean <E extends T> E save(@UncloneRemotedBean E entity);
    
    public void remove(Long id);
}
