package com.prodoko.base.model.beans;

import java.util.List;

public class BusinessBeanDescription extends BusinessDescription<BusinessBean> {

    private Class<?> beanClass;
    
    public BusinessBeanDescription() {
        super();
    }

    /**
     * TODO
     */
    public Class<?> getBeanClass() {
        return beanClass;
    }

    /**
     * @see #getBeanClass
     */
    public void setBeanClass(Class<?> beanClass) {
        this.beanClass = beanClass;
    }
    
    public List<BusinessPropertyDescription> getBusinessProperties() {
        return BusinessDescriptionIntrospector.getInstance().beansPropertiesDescriptions(
                getBeanClass());
    }
}
