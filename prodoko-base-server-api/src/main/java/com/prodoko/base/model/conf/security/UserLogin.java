package com.prodoko.base.model.conf.security;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.userdetails.UserDetails;

import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.beans.StringRepresentableBean;
import com.prodoko.base.model.beans.dynamic.DynamicBean;
import com.prodoko.base.model.beans.filter.StringPropertyFilter;
import com.prodoko.base.model.constraints.StringConsts;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@BusinessBean
@Entity
@RemotableAsClone
public class UserLogin extends DynamicBean implements UserDetails, StringRepresentableBean {
    
    private static final long serialVersionUID = -7638456898546135970L;

    private String username;
    private String password;
    private Set<UserAuthority> individualAuthorities = new HashSet<>();
    private LegalEntity details;
    private String description;
    private Boolean accountNonExpired = true;
    private Boolean accountNonLocked = true;
    private Boolean credentialsNonExpired = true;
    private Boolean enabled = true;
    
    @BusinessProperty(primary=true, possibleFilters=StringPropertyFilter.class)
    @NotBlank
    @Override
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * Actually a hash of the password
     */
    @BusinessProperty(possiblePresentations={})
    @Override
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * TODO
     */
    @ManyToMany
    @CollectionPropertyRemoting(cloning=CloningMode.DEEP)
    public Set<UserAuthority> getIndividualAuthorities() {
        return individualAuthorities;
    }

    /**
     * @see #getIndividualAuthorities
     */
    @CollectionPropertyRemoting(cloning=CloningMode.DEEP)
    public void setIndividualAuthorities(Set<UserAuthority> individualAuthorities) {
        this.individualAuthorities = individualAuthorities;
    }

    @Transient
    @Override
    public Set<UserAuthority> getAuthorities() {
        return getIndividualAuthorities();
    }
    
    /**
     * TODO
     */
    @OneToOne
    @BeanPropertyRemoting(cloning=CloningMode.DEEP)
    public LegalEntity getDetails() {
        return details;
    }

    /**
     * @see #getDetails
     */
    public void setDetails(LegalEntity details) {
        this.details = details;
    }

    /**
     * TODO
     */
    @BusinessProperty
    @Size(max=StringConsts.MEDIUM_SIZE)
    public String getDescription() {
        return description;
    }

    /**
     * @see #getDescription
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }
    
    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }
    
    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }
    
    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }
    
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
    
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toRepresentiveString(Hint hint) {
        return getUsername();
    }
}
