package com.prodoko.base.model.entities;

import com.prodoko.base.util.ClassSupporter;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;

public interface EntityFinderService<T extends StandardEntity> extends ClassSupporter {
    
    @Override
    public Class<T> getSupportedClass();

    public @CloneRemotedBean T find(Long id);
    
    public @CloneRemotedBean <E extends T> E find(Class<E> subclass, Long id);
}
