package com.prodoko.base.model.beans;

public interface StringRepresentableBean {

    public String toRepresentiveString(Hint hint);
    
    public static enum Hint {
        SHORT, LONG
    }
}
