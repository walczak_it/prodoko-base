package com.prodoko.base.model.basic.legalentities.types;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.prodoko.base.model.basic.legalentities.Institution;
import com.prodoko.base.model.beans.BusinessBean;

@BusinessBean
@Entity
public class InstitutionType extends OrganizationType {

    private static final long serialVersionUID = -7282168629733898194L;

    @Override
    @Transient
    public Class<? extends Institution> getSupportedClass() {
        return Institution.class;
    }
}
