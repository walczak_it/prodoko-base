package com.prodoko.base.model.basic.legalentities;

import com.prodoko.base.model.beans.browser.BeansBrowserService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface LegalEntitiesBrowserService<T extends LegalEntity>
        extends BeansBrowserService<T> {

}
