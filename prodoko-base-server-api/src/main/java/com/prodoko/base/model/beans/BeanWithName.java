package com.prodoko.base.model.beans;

import com.prodoko.base.model.beans.filter.StringPropertyFilter;

public interface BeanWithName {
    
    /**
     * TODO
     */
    @BusinessProperty(possibleFilters=StringPropertyFilter.class)
    public String getName();
}
