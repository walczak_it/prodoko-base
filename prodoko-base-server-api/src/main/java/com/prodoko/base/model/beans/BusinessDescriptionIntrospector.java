package com.prodoko.base.model.beans;

import static com.prodoko.base.util.BundleUtils.message;
import static com.prodoko.base.util.BundleUtils.messageKey;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.prodoko.base.util.MessageResolutionOptions;

public class BusinessDescriptionIntrospector {
    
    private static final Log LOG = LogFactory.getLog(BusinessDescriptionIntrospector.class);
    
    public static final String PLURAL_DISPLAY_NAME_KEY_SUFFIX = "shortDescription";
    public static final String SHORT_DESCRIPTION_KEY_SUFFIX = "shortDescription";
    public static final String MORE_INFO_URL_KEY_SUFFIX = "moreInfoUrl";
    
    private static BusinessDescriptionIntrospector inst = null;
    
    private MessageResolutionOptions options;
    
    protected BusinessDescriptionIntrospector() {
        options = new MessageResolutionOptions();
        options.setReturnNull(true);
        options.setForceMainBundle(false);
    }
    
    public static BusinessDescriptionIntrospector getInstance() {
        if(inst == null) {
            inst = new BusinessDescriptionIntrospector(); 
        }
        return inst;
    }
    
    public BusinessBeanDescription beanDescription(Class<?> clazz) {
        BusinessBean meta = assertBusinessBean(clazz);
        BusinessBeanDescription desc = new BusinessBeanDescription();
        desc.setBeanClass(clazz);
        desc.setMetadata(meta);
        desc.setLocalised(localisedDescription(clazz));
        return desc;
    }
    
    public List<BusinessBeanDescription> beanDescriptions(Class<?>... clazzes) {
        List<BusinessBeanDescription> desces = new ArrayList<>(clazzes.length);
        for(Class<?> clazz : clazzes) {
            desces.add(beanDescription(clazz));
        }
        return desces;
    }
    
    public BusinessPropertyDescription propertyDescription(Class<?> clazz, String propertyName) {
        assertBusinessBean(clazz);
        Method getter = findGetter(clazz, propertyName);
        BusinessProperty meta = assertBusinessProperty(clazz, propertyName, getter);
        BusinessPropertyDescription desc = new BusinessPropertyDescription();
        desc.setOwnerBeanClass(clazz);
        desc.setPropertyClass(getter.getReturnType());
        desc.setPropertyName(propertyName);
        desc.setMetadata(meta);
        desc.setLocalised(localisedDescription(clazz, propertyName));
        return desc;
    }
    
    public BusinessPropertyDescription deepPropertyDescription(Class<?> clazz,
            String propertyPath) {
        assertBusinessBean(clazz);
        String[] propertyNames = propertyPath.split("\\.");
        int lastNameIndex = propertyNames.length-1;
        Class<?> clazzItr = clazz;
        for(int i = 0; i < lastNameIndex; ++i) {
            Method getter = findGetter(clazzItr, propertyNames[i]);
            clazzItr = getter.getReturnType();
            assertBusinessBean(clazzItr);
        }
        return propertyDescription(clazzItr, propertyNames[lastNameIndex]);
    }
    
    public List<BusinessPropertyDescription> beansPropertiesDescriptions(Class<?> clazz) {
        assertBusinessBean(clazz);
        LinkedList<BusinessPropertyDescription> descs = new LinkedList<>();
        for(Method method : clazz.getMethods()) {
            String propertyName = null;
            if(method.getName().startsWith("get")) {
                propertyName = Character.toLowerCase(method.getName().charAt(3))
                        + method.getName().substring(4);
            } else if(method.getName().startsWith("is")) {
                propertyName = Character.toLowerCase(method.getName().charAt(2))
                        + method.getName().substring(3);
            }
            if(propertyName != null) {
                BusinessProperty meta = method.getAnnotation(BusinessProperty.class);
                if(meta != null) {
                    BusinessPropertyDescription desc = new BusinessPropertyDescription();
                    desc.setOwnerBeanClass(clazz);
                    desc.setPropertyClass(method.getReturnType());
                    desc.setPropertyName(propertyName);
                    desc.setMetadata(meta);
                    desc.setLocalised(localisedDescription(clazz, propertyName));
                    if(meta.primary()) {
                        descs.push(desc);
                    } else {
                        descs.add(desc);
                    }
                }
            }
        }
        return descs;
    }
    
    public BusinessEnumDescription enumDescription(Class<?> clazz) {
        if(!clazz.isEnum()) {
            throw new IllegalArgumentException("Class " + clazz.getName() + " is not an enum");
        }
        BusinessEnum meta = assertBusinessEnum(clazz);
        BusinessEnumDescription desc = new BusinessEnumDescription();
        desc.setEnumClass(clazz);
        desc.setMetadata(meta);
        desc.setLocalised(localisedDescription(clazz));
        try {
            Method valuesMethod = clazz.getMethod("values");
            Object[] values = (Object[])valuesMethod.invoke(null);
            Map<Object, LocalisedDescription> vlocs = new HashMap<>(values.length);
            for(Object value : values) {
                LocalisedDescription vloc = localisedDescription(clazz, value.toString());
                vlocs.put(value, vloc);
            }
            desc.setValuesLocalized(vlocs);
            return desc;
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new IllegalStateException(
                    "Enums don't have a public static values() method on this JVM ?", e);
        } catch (SecurityException e) {
            throw new IllegalStateException(
                    "Security wont let us invoke values method on enums", e);
        }
    }
    
    public LocalisedDescription enumValueDescription(Enum<?> value) {
        return enumDescription(value.getClass()).getValuesLocalised().get(value);
    }
    
    public LocalisedDescription enumValueDescription(String stringValue, Class<?> enumClass) {
        if(!enumClass.isEnum()) {
            throw new IllegalArgumentException("Defining enum class "
                    + enumClass.getName() + " is not an enum");
        }
        try {
            Method fromString = enumClass.getMethod("fromString", String.class);
            Enum<?> enumValue = (Enum<?>) fromString.invoke(null, stringValue); 
            return enumValueDescription(enumValue);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException("No public static fromString(String) "
                    + "method in enum", e);
        } catch (SecurityException e) {
            throw new IllegalStateException("JVM did not lets us invoke the static "
                    + "Enum.fromString(String) method");
        }
    }
    
    public BusinessMethodDescription methodDescription(Class<?> clazz, String methodName) {
        BusinessMethod meta = assertBusinessMethod(clazz, methodName);
        BusinessMethodDescription desc = new BusinessMethodDescription(meta,
                localisedDescription(clazz, methodName));
        return desc;
    }
    
    /*

    public BeanPresentation beanPresentation(Class<?> clazz) {
        assertBusinessBean(clazz);
        BeanPresentation doc = info(clazz, "");
        return doc;
    }
    
    public BusinessInfo propertyInfo(Class<?> clazz, String propertyName) {
        assertBusinessBean(clazz);
        BusinessInfo doc = info(clazz, propertyName + ".");
        return doc;
    }*/
    /*
    public Map<String, BusinessPropertyInfo> businessProperties(Class<?> clazz) {
        assertBusinessBean(clazz);
        Method[] methods = clazz.getMethods();
        Set<BusinessPropertyInfo> props = new HashSet<>();
        for(Method method : methods) {
            String methodName = method.getName();
            BusinessBean anno = method.getAnnotation(BusinessBean.class);
            boolean getGetter = methodName.startsWith("get")
                    && Character.isUpperCase(methodName.charAt(3));
            boolean isGetter = methodName.startsWith("is")
                    && Character.isUpperCase(methodName.charAt(2));
            if(anno != null && (getGetter || isGetter)) {
                String propertyName;
                if(getGetter) {
                    propertyName = Character.toLowerCase(methodName.charAt(3))
                            + methodName.substring(4);
                } else { // isGetter
                    propertyName = Character.toLowerCase(methodName.charAt(2))
                            + methodName.substring(3);
                }
                BusinessInfo info = propertyInfo(clazz, propertyName);
                BusinessPropertyInfo prop = new BusinessPropertyInfo();
                prop.setInfo(info);
                prop.setPropertyName(propertyName);
                prop.setPropertyType(method.getReturnType());
                props.add(prop);
            }
        }
        return props;
    }*/
    

    
    /**
     * TODO
     */
    protected MessageResolutionOptions getOptions() {
        return options;
    }

    protected BusinessBean assertBusinessBean(Class<?> clazz) {
        BusinessBean meta = clazz.getAnnotation(BusinessBean.class);
        if(meta == null) {
            throw new IllegalArgumentException("Given beans class " + clazz.getName() + " "
                    + "is not annotated as a BusinessBean");
        }
        return meta;
    }
    
    protected Method findGetter(Class<?> clazz, String propertyName) {
        String getterSuffix = propertyName.substring(0, 1).toUpperCase()
                + propertyName.substring(1);
        String getterName = "get" + getterSuffix;
        String boolGetterName = "is" + getterSuffix;
        Method getter = null;
        for(Method m : clazz.getMethods()) {
            if((m.getName().equals(getterName) || m.getName().equals(boolGetterName))
                    && m.getParameterCount() == 0) {
                getter = m;
                break;
            }
        }

        if(getter == null) {
            throw new IllegalArgumentException("No getter for property " + propertyName
                    + " in class " + clazz.getName() + " found");
        }
        return getter;
    }
    
    protected BusinessProperty assertBusinessProperty(Class<?> clazz,
            String propertyName, Method getter) {
        BusinessProperty meta = getter.getAnnotation(BusinessProperty.class);
        if(meta == null) {
            throw new IllegalArgumentException("Given property " + propertyName + " of class "
                    + clazz.getName() + " is not annotated as a BusinessProperty");
        }
        return meta;
    }
    
    protected BusinessEnum assertBusinessEnum(Class<?> clazz) {
        BusinessEnum meta = clazz.getAnnotation(BusinessEnum.class);
        if(meta == null) {
            throw new IllegalArgumentException("Given beans class " + clazz.getName() + " "
                    + "is not annotated as a BusinessEnum");
        }
        return meta;
    }
    
    protected BusinessMethod assertBusinessMethod(Class<?> clazz, String methodName) {
        Class<?> declaringClazz = null;
        BusinessMethod meta = null;
        for(Method method : clazz.getMethods()) {
            if(method.getName().equals(methodName)) {
                continue;
            }
            BusinessMethod currentMeta = method.getAnnotation(BusinessMethod.class);
            if(currentMeta == null) {
                continue;
            }
            if(declaringClazz == null
                    || declaringClazz.isAssignableFrom(method.getDeclaringClass())) {
                declaringClazz = method.getDeclaringClass();
                meta = currentMeta;
            }
        }
        if(meta == null) {
            throw new IllegalArgumentException("Couldn't find any method with name "
                    + methodName + "and annotated as a BusinessMethod");
        }
        return meta;
    }
    
    protected LocalisedDescription localisedDescription(Class<?> clazz) {
        return localisedDescription(clazz, null);
    }
    
    protected LocalisedDescription localisedDescription(Class<?> clazz,
            String propertyOrEnumValueName) {

        LocalisedDescription loc = new LocalisedDescription();
        String actualPluralDisplayNameKeySuffix = PLURAL_DISPLAY_NAME_KEY_SUFFIX;
        String actualShortDescriptionKeySuffix = SHORT_DESCRIPTION_KEY_SUFFIX;
        String actualMoreInfoUrlKeySuffix = MORE_INFO_URL_KEY_SUFFIX;
        if(propertyOrEnumValueName != null) {
            actualPluralDisplayNameKeySuffix = propertyOrEnumValueName + "."
                    + actualPluralDisplayNameKeySuffix;
            actualShortDescriptionKeySuffix = propertyOrEnumValueName + "."
                    + actualShortDescriptionKeySuffix;
            actualMoreInfoUrlKeySuffix = propertyOrEnumValueName + "."
                    + actualMoreInfoUrlKeySuffix;
            loc.setDisplayName("?<" + messageKey(clazz, propertyOrEnumValueName) + ">?");
        } else {
            loc.setDisplayName("?<" + messageKey(clazz) + ">?");
        }
        loc.setShortDescription("?<" + messageKey(clazz, actualShortDescriptionKeySuffix) + ">?");
        
        loc.setDisplayNameResolved(false);
        loc.setPluralDisplayNameResolved(false);
        loc.setShortDescriptionResolved(false);
        
        Class<?> clazzItr = clazz;
        do {
            if(!loc.isDisplayNameResolved()) {
                String displayName = null;
                if(propertyOrEnumValueName == null) {
                    displayName = message(clazzItr, getOptions());
                } else {
                    displayName = message(clazzItr, propertyOrEnumValueName, getOptions());
                }
                if(displayName != null) {
                    loc.setDisplayName(displayName);
                    loc.setDisplayNameResolved(true);
                }
            }
            if(!loc.isPluralDisplayNameResolved()) {
                String pluralDisplayName = message(clazzItr, actualPluralDisplayNameKeySuffix,
                        getOptions());
                if(pluralDisplayName != null) {
                    loc.setPluralDisplayName(pluralDisplayName);
                    loc.setPluralDisplayNameResolved(true);
                }
            }
            if(!loc.isShortDescriptionResolved()) {
                String shortDescription = message(clazzItr, actualShortDescriptionKeySuffix,
                        getOptions());
                if(shortDescription != null) {
                    loc.setShortDescription(shortDescription);
                    loc.setShortDescriptionResolved(true);
                }
            }
            if(loc.getMoreInfo() == null) {
                String moreInfoStr = message(clazzItr, actualMoreInfoUrlKeySuffix,
                        getOptions());
                if(moreInfoStr != null) {
                    try {
                        URL moreInfo = new URL(moreInfoStr);
                        loc.setMoreInfo(moreInfo);
                    } catch(MalformedURLException ex) {
                        String exc = "Found a malformed URL when trying to "
                                + "extract localized description for " + clazz.getName();
                        if(propertyOrEnumValueName != null) {
                            exc += " property " + propertyOrEnumValueName;
                        }
                        throw new IllegalStateException(exc, ex);
                    }
                }
            }
            clazzItr = clazzItr.getSuperclass();
        } while(clazzItr != null || loc.isAllResolved());
        
        if(!loc.isDisplayNameResolved()) {
            String err = "No display name found for " + clazz.getName();
            if(propertyOrEnumValueName != null) {
                err += " property " + propertyOrEnumValueName;
            }
            LOG.error(err);
        }
        
        return loc;
    }
}
