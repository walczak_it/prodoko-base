package com.prodoko.base.model.basic.legalentities;

import javax.persistence.Entity;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.RemotablePropertiesHolder;

@BusinessBean
@Entity
@RemotableAsClone
@RemotablePropertiesHolder
public class Person extends LegalEntity {

    private static final long serialVersionUID = 170592719406462989L;

}
