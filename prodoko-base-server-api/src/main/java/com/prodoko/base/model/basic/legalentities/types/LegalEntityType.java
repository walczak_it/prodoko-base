package com.prodoko.base.model.basic.legalentities.types;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.prodoko.base.model.basic.legalentities.LegalEntity;
import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.dynamic.DynamicBean;
import com.prodoko.base.model.beans.dynamic.MainDynamicClass;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@BusinessBean
@Entity
@RemotableAsClone
public class LegalEntityType extends MainDynamicClass {

    private static final long serialVersionUID = -1693057490142844254L;
    
    @Override
    @Transient
    public Class<? extends DynamicBean> getSupportedClass() {
        return LegalEntity.class;
    }
}
