package com.prodoko.base.model.basic.legalentities.types;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.prodoko.base.model.basic.legalentities.Person;
import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.RemotablePropertiesHolder;

@BusinessBean
@Entity
@RemotableAsClone
@RemotablePropertiesHolder
public class PersonType extends LegalEntityType {

    private static final long serialVersionUID = 5061324691718429725L;

    @Override
    @Transient
    public Class<? extends Person> getSupportedClass() {
        return Person.class;
    }
}
