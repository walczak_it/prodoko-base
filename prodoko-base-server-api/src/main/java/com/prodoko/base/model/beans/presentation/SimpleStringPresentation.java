package com.prodoko.base.model.beans.presentation;

import javax.persistence.Entity;


@Entity
public class SimpleStringPresentation extends TypePresentation {

    private static final long serialVersionUID = -2797167499505320670L;
    
    public SimpleStringPresentation() {
        
    }
    
    public SimpleStringPresentation(SimpleStringPresentation original) {
        super(original);
    }

    @Override
    public SimpleStringPresentation clone() {
        return new SimpleStringPresentation(this);
    }
}
