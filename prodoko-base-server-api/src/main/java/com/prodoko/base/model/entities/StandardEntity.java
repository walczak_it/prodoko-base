package com.prodoko.base.model.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.prodoko.base.model.beans.BusinessProperty;

@MappedSuperclass
public abstract class StandardEntity implements Serializable {

    private static final long serialVersionUID = -5412003390392495211L;
    
    private Long id;
    private Date lastUpdate;
    
    @Id @GeneratedValue
    public Long getId() {
        return id;
    }
    
    /**
     * @see #getId()
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * TODO
     */
    @BusinessProperty
    @Version
    @Temporal(TemporalType.TIMESTAMP)
    public Date getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @see #getLastUpdate
     */
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public void initStub() {
        
    }
    

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StandardEntity other = (StandardEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
    
    @Override
    public String toString() {
        return getClass().getName() + ":" + getId();
    }
}
