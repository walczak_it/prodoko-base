package com.prodoko.base.model.basic.legalentities.types;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.prodoko.base.model.basic.legalentities.Company;
import com.prodoko.base.model.beans.BusinessBean;

@BusinessBean
@Entity
public class CompanyType extends OrganizationType {

    private static final long serialVersionUID = 3206059836915509898L;

    @Override
    @Transient
    public Class<? extends Company> getSupportedClass() {
        return Company.class;
    }
}
