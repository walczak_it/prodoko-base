package com.prodoko.base.model.conf.security;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.base.model.entities.MinimalDataLoaderService;
import com.prodoko.remoting.support.ServiceInterface;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;

@ServiceInterface
@Transactional
public interface SecurityService extends MinimalDataLoaderService {
    
    public static final String SECURITY_SERVICE_NAME = "securityService";
    public static final String DEV_ADMIN_USERNAME = "devAdmin";

    /**
     * Login data of the currently authenticated user.
     */
    @PreAuthorize("isAuthenticated()")
    public @CloneRemotedBean UserLogin getYourUserLogin();
    
    public boolean isNotDevAdmin(UserLogin login);
}
