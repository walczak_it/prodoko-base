package com.prodoko.base.model.basic.legalentities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.prodoko.base.model.basic.Site;
import com.prodoko.base.model.beans.BeanWithCode;
import com.prodoko.base.model.beans.BeanWithName;
import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.beans.StringRepresentableBean;
import com.prodoko.base.model.beans.dynamic.DynamicBean;
import com.prodoko.base.model.beans.filter.StringPropertyFilter;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@BusinessBean
@Entity
@RemotableAsClone
public abstract class LegalEntity extends DynamicBean
    implements BeanWithCode, BeanWithName, StringRepresentableBean {

    private static final long serialVersionUID = 4779751376299893425L;
    
    private String code;
    private String name;
    private String additionalDescription;
    private Site mainAddress;
    private List<Site> otherSites;

    @Override
    @BusinessProperty(possibleFilters = StringPropertyFilter.class)
    @NotBlank
    @Column(unique = true)
    public String getCode() {
        return code;
    }

    /**
     * @see #setCode
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    @Override
    @BusinessProperty(possibleFilters = StringPropertyFilter.class)
    public String getName() {
        return name;
    }
    
    /**
     * @see #getName
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * TODO
     */
    @BusinessProperty(possibleFilters=StringPropertyFilter.class)
    public String getAdditionalDescription() {
        return additionalDescription;
    }
    
    /**
     * @see #getDescription
     */
    public void setAdditionalDescription(String additionalDescription) {
        this.additionalDescription = additionalDescription;
    }

    /**
     * TODO
     */
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @NotNull
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_DEEP)
    public Site getMainAddress() {
        return mainAddress;
    }

    /**
     * @see #getMainAddress
     */
    public void setMainAddress(Site mainAddress) {
        this.mainAddress = mainAddress;
    }

    /**
     * TODO
     */
    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_DEEP)
    public List<Site> getOtherSites() {
        return otherSites;
    }

    /**
     * @see #getOtherSites
     */
    public void setOtherSites(List<Site> otherSites) {
        this.otherSites = otherSites;
    }
    
    @Transient
    public List<Site> getAllSites() {
        LinkedList<Site> sites = new LinkedList<>(getOtherSites());
        sites.addFirst(getMainAddress());
        return sites;
    }
    
    @Override
    public String toRepresentiveString(Hint hint) {
        if(Hint.SHORT == hint) {
            return getCode();
        } else {
            return getCode() + "-" + getName();
        }
    }
    
    @Override
    public void initStub() {
        super.initStub();
        this.mainAddress = new Site();
        this.mainAddress.initStub();
    }
}
