package com.prodoko.base.model.beans;

import java.util.List;

import com.prodoko.base.model.entities.EntityFinderService;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.remoting.support.cloning.CloneRemotedBean;
import com.prodoko.remoting.support.cloning.CloneRemotedCollection;

public interface BeanFinderService<T extends StandardEntity & StringRepresentableBean>
    extends EntityFinderService<T> {

    public @CloneRemotedBean T findByString(String string);
    
    public @CloneRemotedBean <E extends T> E findByString(Class<E> subclass, String shortString);
    
    public @CloneRemotedCollection List<T> findSuggestionsByString(String partOfLongString);
    
    public @CloneRemotedCollection <E extends T> List<E> findSuggestionsByString(
            Class<E> subclass, String partOfLongString);
}
