package com.prodoko.base.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationInfoService {

    private BaseSystemInfo systemInfo;

    /**
     * TODO
     */
    public BaseSystemInfo getSystemInfo() {
        return systemInfo;
    }

    /**
     * @see #getSystemInfo
     */
    @Autowired
    public void setSystemInfo(BaseSystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }
}
