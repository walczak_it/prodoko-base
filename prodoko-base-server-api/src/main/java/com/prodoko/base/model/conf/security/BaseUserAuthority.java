package com.prodoko.base.model.conf.security;

public enum BaseUserAuthority {

    DEV_ADMIN, ADMIN;
}
