package com.prodoko.base.model.basic.legalentities;

import javax.persistence.Entity;

import com.prodoko.base.model.beans.BusinessBean;

@BusinessBean
@Entity
public class Institution extends Organization {

    private static final long serialVersionUID = 8078131965753249218L;

}
