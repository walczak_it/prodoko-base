package com.prodoko.base.model.beans.dynamic;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.beans.dynamic.attrs.NumericAttributeValues;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.UpdatingMode;

@BusinessBean
@Entity
@RemotableAsClone
public abstract class DynamicBean extends StandardEntity {

    private static final long serialVersionUID = -2587662922457556573L;
    
    private MainDynamicClass mainDynamicClass;
    private List<HelperDynamicClass> helperDynamicClasses;
    private NumericAttributeValues numericAttributeValues;
    
    /**
     * TODO
     */
    @BusinessProperty
    @NotNull
    @ManyToOne(fetch=FetchType.LAZY, optional=false)
    @BeanPropertyRemoting(cloning=CloningMode.CHOPPED,
            updating=UpdatingMode.FIND_CORRESPONDING_AND_UPDATE)
    public MainDynamicClass getMainDynamicClass() {
        return mainDynamicClass;
    }
    
    /**
     * @see #getMainDynamicClass
     */
    public void setMainDynamicClass(MainDynamicClass mainDynamicClass) {
        this.mainDynamicClass = mainDynamicClass;
    }
    
    /**
     * TODO
     */
    @ManyToMany
    @CollectionPropertyRemoting(cloning=CloningMode.CHOPPED)
    public List<HelperDynamicClass> getHelperDynamicClasses() {
        return helperDynamicClasses;
    }
    
    /**
     * @see #getHelperDynamicClasses
     */
    public void setHelperDynamicClasses(List<HelperDynamicClass> helperDynamicClasses) {
        this.helperDynamicClasses = helperDynamicClasses;
    }

    @NotNull
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_DEEP, updating=UpdatingMode.SIMPLE_REPLACE)
    public NumericAttributeValues getNumericAttributeValues() {
        return numericAttributeValues;
    }

    public void setNumericAttributeValues(
            NumericAttributeValues numericAttributeValues) {
        this.numericAttributeValues = numericAttributeValues;
    }
    
    @Override
    public void initStub() {
        super.initStub();
        this.numericAttributeValues = new NumericAttributeValues();
    }
}
