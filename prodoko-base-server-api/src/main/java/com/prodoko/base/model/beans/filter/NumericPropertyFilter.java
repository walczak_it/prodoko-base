package com.prodoko.base.model.beans.filter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;

@Entity
public class NumericPropertyFilter extends PropertyFilter {
    
    private static final long serialVersionUID = 5962975302281028555L;
    
    private Operator operator;
    private Number comparingNumber;
    
    public NumericPropertyFilter() {
        
    }
    
    public NumericPropertyFilter(NumericPropertyFilter original,
            BeansSubsetPresentation newOwner) {
        super(original, newOwner);
        this.operator = original.getOperator();
        this.comparingNumber = original.getComparingNumber();
    }

    public static enum Operator {
        GREATER, GREATER_OR_EQUAL, EQUAL, LESS, LESS_OR_EQUAL;
    }

    /**
     * TODO
     */
    @NotNull
    public Operator getOperator() {
        return operator;
    }

    /**
     * @see #getOperator
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * TODO
     */
    @NotNull
    public Number getComparingNumber() {
        return comparingNumber;
    }

    /**
     * @see #getComparingNumber
     */
    public void setComparingNumber(Number comparingNumber) {
        this.comparingNumber = comparingNumber;
    }

    @Override
    public String toDisplayString() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public NumericPropertyFilter clone(BeansSubsetPresentation newOwner) {
        return new NumericPropertyFilter(this, newOwner);
    }
}
