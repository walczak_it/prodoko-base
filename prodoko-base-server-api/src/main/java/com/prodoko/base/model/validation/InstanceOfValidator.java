package com.prodoko.base.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InstanceOfValidator implements ConstraintValidator<InstanceOf, Object> {
    
    private InstanceOf constraintAnnotation;

    @Override
    public void initialize(InstanceOf constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if(value == null) {
            return true;
        }
        return constraintAnnotation.value().isAssignableFrom(value.getClass());
    }

}
