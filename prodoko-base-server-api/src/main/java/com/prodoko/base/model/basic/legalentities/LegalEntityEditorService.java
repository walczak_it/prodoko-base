package com.prodoko.base.model.basic.legalentities;

import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface LegalEntityEditorService
        extends EntityEditorService<LegalEntity> {

    
}
