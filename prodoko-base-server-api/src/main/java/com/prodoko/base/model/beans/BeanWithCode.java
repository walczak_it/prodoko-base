package com.prodoko.base.model.beans;

import javax.persistence.Column;

import org.hibernate.validator.constraints.NotBlank;

import com.prodoko.base.model.beans.filter.StringPropertyFilter;

public interface BeanWithCode {
    
    /**
     * TODO
     */
    @BusinessProperty(possibleFilters=StringPropertyFilter.class)
    @NotBlank
    @Column(unique=true)
    public String getCode();
}
