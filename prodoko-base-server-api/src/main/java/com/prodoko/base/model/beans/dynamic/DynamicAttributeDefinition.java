package com.prodoko.base.model.beans.dynamic;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.UpdatingMode;

@BusinessBean
@Entity
@RemotableAsClone
public abstract class DynamicAttributeDefinition extends StandardEntity {

    private static final long serialVersionUID = 4252886764528329825L;
    
    private String code;
    private String name;
    private DynamicClass definingDynamicClass;
    private GridPosition gridPosition;
    private Integer tableColumnOrder;
    private Integer valueIndex;
    
    @BusinessProperty
    @NotBlank
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * TODO
     */
    @BusinessProperty
    public String getName() {
        return name;
    }
    
    /**
     * @see #setName
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * TODO
     */
    @BusinessProperty
    @ManyToOne(optional=false)
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    @NotNull
    public DynamicClass getDefiningDynamicClass() {
        return definingDynamicClass;
    }

    /**
     * @see #setDefiningDynamicClass
     */
    public void setDefiningDynamicClass(DynamicClass definingDynamicClass) {
        this.definingDynamicClass = definingDynamicClass;
    }

    /**
     * TODO
     */
    @BusinessProperty
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_DEEP, updating=UpdatingMode.UPDATE)
    public GridPosition getGridPosition() {
        return gridPosition;
    }

    /**
     * @see #getGridPosition
     */
    public void setGridPosition(GridPosition gridPosition) {
        this.gridPosition = gridPosition;
    }

    /**
     * TODO
     */
    @BusinessProperty
    public Integer getTableColumnOrder() {
        return tableColumnOrder;
    }

    /**
     * @see #getTableColumnOrder
     */
    public void setTableColumnOrder(Integer tableColumnOrder) {
        this.tableColumnOrder = tableColumnOrder;
    }

    @NotNull
    public Integer getValueIndex() {
        return valueIndex;
    }

    public void setValueIndex(Integer valueIndex) {
        this.valueIndex = valueIndex;
    }
}
