package com.prodoko.base.model.beans.filter;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.prodoko.base.model.beans.presentation.BeansSubsetPresentation;

@Entity
public class BeanOrder implements Serializable {
    
    private static final long serialVersionUID = -3014205702916090180L;
    
    private Long id;
    private BeansSubsetPresentation presentation;
    private String property;
    private Boolean ascending;
    
    public BeanOrder() {
        
    }
    
    public BeanOrder(BeanOrder original, BeansSubsetPresentation newOwner) {
        this.id = original.getId();
        this.presentation = newOwner;
        this.property = original.getProperty();
        this.ascending = original.getAscending();
    }

    @Id @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * TODO
     */
    @ManyToOne
    @NotNull
    public BeansSubsetPresentation getPresentation() {
        return presentation;
    }

    /**
     * @see #getPresentation
     */
    public void setPresentation(BeansSubsetPresentation presentation) {
        this.presentation = presentation;
    }

    /**
     * TODO
     */
    @NotEmpty
    public String getProperty() {
        return property;
    }
    
    /**
     * @see #getProperty
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * TODO
     */
    @NotNull
    public Boolean getAscending() {
        return ascending;
    }

    /**
     * @see #getAscending
     */
    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BeanOrder other = (BeanOrder) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public BeanOrder clone(BeansSubsetPresentation newOwner) {
        return new BeanOrder(this, newOwner);
    }
}
