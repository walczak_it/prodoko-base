package com.prodoko.base.model.basic.equipment;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Equipment implements Serializable {

    private static final long serialVersionUID = -4913070821922242230L;
    
    private Long id;
    private String code;
    
    /**
     * TODO
     */
    @Id @GeneratedValue
    public Long getId() {
        return id;
    }
    
    /**
     * @see #getId
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * TODO
     */
    public String getCode() {
        return code;
    }
    
    /**
     * @see #getCode
     */
    public void setCode(String code) {
        this.code = code;
    }
}
