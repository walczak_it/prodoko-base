package com.prodoko.base.model.basic.legalentities.types;

import com.prodoko.base.model.beans.dynamic.MainDynamicClassEditorService;
import com.prodoko.base.model.entities.MinimalDataLoaderService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface LegalEntityTypeEditorService
        extends MainDynamicClassEditorService<LegalEntityType>, MinimalDataLoaderService {

}
