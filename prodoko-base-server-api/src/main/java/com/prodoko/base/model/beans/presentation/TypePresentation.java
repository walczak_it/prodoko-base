package com.prodoko.base.model.beans.presentation;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public abstract class TypePresentation implements Serializable, Cloneable {

    private static final long serialVersionUID = 3254954961474076793L;
    
    private Long id;
    
    public TypePresentation() {
        
    }

    public TypePresentation(TypePresentation original) {
        super();
        this.id = original.getId();
    }

    /**
     * TODO
     */
    @Id @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * @see #getId
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public abstract TypePresentation clone();
}
