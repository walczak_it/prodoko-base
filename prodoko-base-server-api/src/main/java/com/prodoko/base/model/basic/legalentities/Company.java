package com.prodoko.base.model.basic.legalentities;

import javax.persistence.Entity;

import com.prodoko.base.model.beans.BusinessBean;

@BusinessBean
@Entity
public class Company extends Organization {

    private static final long serialVersionUID = -6124516744161892410L;

}
