package com.prodoko.base.model.beans.dynamic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.PropertieyMembersPropertiesRemoting;
import com.prodoko.remoting.support.cloning.RemotablePropertiesHolder;

@RemotablePropertiesHolder
public class HierarchyDto<T extends MainDynamicClass> implements Serializable {

    private static final long serialVersionUID = -5094707639151605438L;
    
    private T mainDynamicClass;
    private List<HierarchyDto<T>> subclasses = new ArrayList<>();
    private Set<HelperDynamicClass> helperDynamicSubclasses = new HashSet<>();
    private boolean mostSuperclass;
    
    public HierarchyDto() {
        super();
    }

    public HierarchyDto(T mainDynamicClass) {
        super();
        this.mainDynamicClass = mainDynamicClass;
    }

    /**
     * TODO
     */
    @BeanPropertyRemoting(cloning=CloningMode.CHOPPED)
    public T getMainDynamicClass() {
        return mainDynamicClass;
    }
    
    /**
     * @see #getMainDynamicClass
     */
    public void setMainDynamicClass(T mainDynamicClass) {
        this.mainDynamicClass = mainDynamicClass;
    }
    
    /**
     * TODO
     */
    @PropertieyMembersPropertiesRemoting
    public List<HierarchyDto<T>> getSubclasses() {
        return subclasses;
    }

    /**
     * @see #getSubclasses
     */
    public void setSubclasses(List<HierarchyDto<T>> subclasses) {
        this.subclasses = subclasses;
    }

    /**
     * TODO
     */
    @CollectionPropertyRemoting
    public Set<HelperDynamicClass> getHelperDynamicSubclasses() {
        return helperDynamicSubclasses;
    }
    
    /**
     * @see #getHelperDynamicClasses
     */
    public void setHelperDynamicSubclasses(
            Set<HelperDynamicClass> helperDynamicSubclasses) {
        this.helperDynamicSubclasses = helperDynamicSubclasses;
    }

    /**
     * TODO
     */
    public boolean isMostSuperclass() {
        return mostSuperclass;
    }

    /**
     * @see #isMostSuperclass
     */
    public void setMostSuperclass(boolean mostSuperclass) {
        this.mostSuperclass = mostSuperclass;
    }
}
