package com.prodoko.base.model.beans.dynamic;

import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface DynamicAttributeDefinitionEditor
    extends EntityEditorService<DynamicAttributeDefinition> {

}
