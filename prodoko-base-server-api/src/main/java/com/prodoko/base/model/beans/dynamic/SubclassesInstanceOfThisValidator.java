package com.prodoko.base.model.beans.dynamic;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SubclassesInstanceOfThisValidator
        implements ConstraintValidator<SubclassesInstanceOfThis, MainDynamicClass> {
    
    private SubclassesInstanceOfThis constraintAnnotation;

    @Override
    public void initialize(SubclassesInstanceOfThis constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(MainDynamicClass value, ConstraintValidatorContext context) {
        Collection<MainDynamicClass> dynamicSubclasses = value.getDynamicSubclasses();
        if(dynamicSubclasses != null) {
            Class<?> valueClass = value.getClass();
            for(MainDynamicClass dynamicSubclass : dynamicSubclasses) {
                if(!valueClass.isAssignableFrom(dynamicSubclass.getClass())){
                    return false;
                }
            }
        }
        return true;
    }

}
