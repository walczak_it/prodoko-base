package com.prodoko.base.model.beans;

public class BusinessMethodDescription {

    private BusinessMethod metadata;
    private LocalisedDescription localised;
    
    public BusinessMethodDescription(BusinessMethod metadata,
            LocalisedDescription localised) {
        super();
        this.metadata = metadata;
        this.localised = localised;
    }

    /**
     * TODO
     */
    public BusinessMethod getMetadata() {
        return metadata;
    }

    /**
     * @see #getMetadata
     */
    public void setMetadata(BusinessMethod metadata) {
        this.metadata = metadata;
    }

    /**
     * TODO
     */
    public LocalisedDescription getLocalised() {
        return localised;
    }

    /**
     * @see #getLocalised
     */
    public void setLocalised(LocalisedDescription localised) {
        this.localised = localised;
    }
}
