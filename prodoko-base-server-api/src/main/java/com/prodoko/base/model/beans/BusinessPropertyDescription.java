package com.prodoko.base.model.beans;

public class BusinessPropertyDescription extends BusinessDescription<BusinessProperty> {

    private Class<?> ownerBeanClass;
    private Class<?> propertyClass;
    private String propertyName;
    
    public BusinessPropertyDescription() {
        super();
    }

    /**
     * TODO
     */
    public Class<?> getOwnerBeanClass() {
        return ownerBeanClass;
    }

    /**
     * @see #getOwnerBeanClass
     */
    public void setOwnerBeanClass(Class<?> ownerBeanClass) {
        this.ownerBeanClass = ownerBeanClass;
    }

    /**
     * TODO
     */
    public Class<?> getPropertyClass() {
        return propertyClass;
    }

    /**
     * @see #getPropertyClass
     */
    public void setPropertyClass(Class<?> propertyClass) {
        this.propertyClass = propertyClass;
    }
    
    public boolean isBusinessBean() {
        return getPropertyClass().isAnnotationPresent(BusinessBean.class);
    }
    
    public BusinessBeanDescription getBusinessBeanDescription() {
        return BusinessDescriptionIntrospector.getInstance().beanDescription(getPropertyClass());
    }

    /**
     * TODO
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @see #getPropertyName
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
