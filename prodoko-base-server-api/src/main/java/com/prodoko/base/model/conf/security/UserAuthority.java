package com.prodoko.base.model.conf.security;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@Entity
@RemotableAsClone
public class UserAuthority implements GrantedAuthority {

    private static final long serialVersionUID = 1367337630253646359L;
    
    private String authority; 
    private Class<?> authorityEnumClass;
    private Set<UserLogin> userLogins;
    
    public UserAuthority() {
        
    }
    
    public UserAuthority(Enum<?> enumValue) {
        authority = enumValue.toString();
        authorityEnumClass = enumValue.getClass();
    }

    @Id
    @NotBlank
    @Override
    public String getAuthority() {
        return authority;
    }
    
    public void setAuthority(String authority) {
        this.authority = authority;
    }
    
    /**
     * TODO
     */
    @NotNull
    public Class<?> getAuthorityEnumClass() {
        return authorityEnumClass;
    }

    /**
     * @see #getAuthorityEnumClass
     */
    public void setAuthorityEnumClass(Class<?> authorityEnumClass) {
        this.authorityEnumClass = authorityEnumClass;
    }

    /**
     * TODO
     */
    @ManyToMany(mappedBy="individualAuthorities")
    @CollectionPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    public Set<UserLogin> getUserLogins() {
        return userLogins;
    }

    /**
     * @see #getUserLogins
     */
    public void setUserLogins(Set<UserLogin> userLogins) {
        this.userLogins = userLogins;
    }
}
