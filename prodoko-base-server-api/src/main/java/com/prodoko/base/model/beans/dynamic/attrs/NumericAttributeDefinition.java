package com.prodoko.base.model.beans.dynamic.attrs;

import javax.persistence.Entity;

import com.prodoko.base.model.beans.BusinessBean;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.beans.dynamic.DynamicAttributeDefinition;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@BusinessBean
@Entity
@RemotableAsClone
public class NumericAttributeDefinition extends DynamicAttributeDefinition {

    private static final long serialVersionUID = 9216685436208841730L;
    
    private Double min;
    private Double max;
    
    /**
     * TODO
     */
    @BusinessProperty
    public Double getMin() {
        return min;
    }
    
    /**
     * @see #getMin
     */
    public void setMin(Double min) {
        this.min = min;
    }
    
    /**
     * TODO
     */
    @BusinessProperty
    public Double getMax() {
        return max;
    }
    
    /**
     * @see #getMax
     */
    public void setMax(Double max) {
        this.max = max;
    }
    
}
