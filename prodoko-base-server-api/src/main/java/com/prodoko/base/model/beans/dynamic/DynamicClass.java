package com.prodoko.base.model.beans.dynamic;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.prodoko.base.model.beans.BeanWithCode;
import com.prodoko.base.model.beans.BeanWithName;
import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.model.beans.StringRepresentableBean;
import com.prodoko.base.model.beans.filter.StringPropertyFilter;
import com.prodoko.base.model.entities.StandardEntity;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.UpdatingMode;

@Entity
@RemotableAsClone
public abstract class DynamicClass extends StandardEntity
        implements BeanWithCode, BeanWithName, StringRepresentableBean {

    private static final long serialVersionUID = 7752399912055739769L;
    
    private String code;
    private String name;
    private String hierarchyPath;
    private List<DynamicAttributeDefinition> dynamicAttributes;
    
    @Transient
    @BusinessProperty
    @BeanPropertyRemoting(cloning=CloningMode.CHOPPED,
            updating=UpdatingMode.FIND_CORRESPONDING_AND_UPDATE)
    public abstract MainDynamicClass getDynamicSuperclass();
    
    public abstract void setDynamicSuperclass(MainDynamicClass dynClass);
    
    @Override
    @BusinessProperty(possibleFilters=StringPropertyFilter.class)
    @NotBlank
    @Column(unique = true)
    public String getCode() {
        return code;
    }
    
    /**
     * @see #getCode
     */
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    @BusinessProperty(possibleFilters=StringPropertyFilter.class)
    public String getName() {
        return name;
    }

    /**
     * @see #getName
     */
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toRepresentiveString(Hint hint) {
        if(hint == Hint.SHORT) {
            return getCode();
        } else {
            return getCode() + "-" + getName();
        }
    }

    /**
     * TODO
     */
    @NotNull
    public String getHierarchyPath() {
        return hierarchyPath;
    }

    /**
     * @see #getHierarchyPath
     */
    public void setHierarchyPath(String hierarchyPath) {
        this.hierarchyPath = hierarchyPath;
    }
    
    /**
     * TODO
     */
    @OneToMany(mappedBy="definingDynamicClass")
    @CollectionPropertyRemoting(cloning=CloningMode.DEEP,
            updating=UpdatingMode.FIND_CORRESPONDING_AND_UPDATE)
    public List<DynamicAttributeDefinition> getDynamicAttributes() {
        return dynamicAttributes;
    }

    /**
     * @see #getDynamicAttributes
     */
    public void setDynamicAttributes(
            List<DynamicAttributeDefinition> dynamicAttributes) {
        this.dynamicAttributes = dynamicAttributes;
    }

    @PrePersist
    @PreUpdate
    public void buildHierarchyPath() {
        String newPath = "/";
        MainDynamicClass superclassItr = getDynamicSuperclass();
        while(superclassItr != null) {
            newPath = "/" + superclassItr.getId() + newPath;
            superclassItr = superclassItr.getDynamicSuperclass();
        }
        setHierarchyPath(newPath);
    }
}
