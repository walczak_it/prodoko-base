package com.prodoko.base.model.beans.browser;

import java.util.List;

import com.prodoko.base.model.DataLoadRequest;
import com.prodoko.base.model.beans.filter.BeanOrder;
import com.prodoko.base.model.beans.filter.FilterData;

public class BeansLoadRequest extends DataLoadRequest {

    private static final long serialVersionUID = 6971404998256294086L;
    
    private List<BeanOrder> orders;
    private List<FilterData> filters;
    
    /**
     * TODO
     */
    public List<BeanOrder> getOrders() {
        return orders;
    }
    
    /**
     * @see #getOrders
     */
    public void setOrders(List<BeanOrder> orders) {
        this.orders = orders;
    }
    
    /**
     * TODO
     */
    public List<FilterData> getFilters() {
        return filters;
    }
    
    /**
     * @see #getFilters
     */
    public void setFilters(List<FilterData> filters) {
        this.filters = filters;
    }
}
