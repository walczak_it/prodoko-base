package com.prodoko.base.model.beans.presentation;

import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class NamedBeansSubsetPresentation extends BeansSubsetPresentation {

    private static final long serialVersionUID = 1621509432878698166L;
    
    private String presentationName;
    
    public NamedBeansSubsetPresentation() { }

    public NamedBeansSubsetPresentation(NamedBeansSubsetPresentation original) {
        super(original);
        this.presentationName = original.getPresentationName();
    }

    public NamedBeansSubsetPresentation(Class<?> beanClass,
            String presentationName) {
        super();
        setBaseBeanClass(beanClass);
        this.presentationName = presentationName;
    }

    /**
     * TODO
     */
    @NotBlank
    public String getPresentationName() {
        return presentationName;
    }
    
    /**
     * @see #getPresentationName
     */
    public void setPresentationName(String presentationName) {
        this.presentationName = presentationName;
    }
    
    @Override
    public NamedBeansSubsetPresentation clone() {
        return new NamedBeansSubsetPresentation(this);
    }
}
