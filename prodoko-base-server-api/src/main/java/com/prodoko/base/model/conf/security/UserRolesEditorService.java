package com.prodoko.base.model.conf.security;

import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.base.model.entities.MinimalDataLoaderService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface UserRolesEditorService
        extends EntityEditorService<UserRole>, MinimalDataLoaderService {

}
