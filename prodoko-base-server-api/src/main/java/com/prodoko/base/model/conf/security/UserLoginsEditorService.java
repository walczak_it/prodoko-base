package com.prodoko.base.model.conf.security;

import org.springframework.security.access.prepost.PreAuthorize;

import com.prodoko.base.model.entities.EntityEditorService;
import com.prodoko.base.model.entities.MinimalDataLoaderService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface UserLoginsEditorService
        extends EntityEditorService<UserLogin>, MinimalDataLoaderService {
    
    @PreAuthorize("hasAuthority('BLABLA') and @securityService.isNotDevAdmin(#entity)")
    @Override
    public <E extends UserLogin> E save(E entity);
    
    public <E extends UserLogin> E saveWithNewPassword(E entity, String newPassword);
    
    @PreAuthorize("@securityService.isNotDevAdminId(#id)")
    @Override
    public void remove(Long id);
}
