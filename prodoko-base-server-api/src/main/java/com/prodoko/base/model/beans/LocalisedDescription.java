package com.prodoko.base.model.beans;

import java.net.URL;

public class LocalisedDescription {
    
    private String displayName;
    private boolean displayNameResolved;
    private String pluralDisplayName;
    private boolean pluralDisplayNameResolved;
    private String shortDescription;
    private boolean shortDescriptionResolved;
    private URL moreInfo;

    public LocalisedDescription() {
        super();
    }

    /**
     * TODO
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * TODO
     */
    public boolean isDisplayNameResolved() {
        return displayNameResolved;
    }

    /**
     * @see #getDisplayNameResolved
     */
    public void setDisplayNameResolved(boolean displayNameResolved) {
        this.displayNameResolved = displayNameResolved;
    }

    /**
     * @see #getDisplayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * TODO
     */
    public String getPluralDisplayName() {
        return pluralDisplayName;
    }

    /**
     * @see #getPluralDisplayName
     */
    public void setPluralDisplayName(String pluralDisplayName) {
        this.pluralDisplayName = pluralDisplayName;
    }

    /**
     * TODO
     */
    public boolean isPluralDisplayNameResolved() {
        return pluralDisplayNameResolved;
    }

    /**
     * @see #isPluralDisplayNameResolved
     */
    public void setPluralDisplayNameResolved(boolean pluralDisplayNameResolved) {
        this.pluralDisplayNameResolved = pluralDisplayNameResolved;
    }

    /**
     * TODO
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * @see #getShortDescription
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * TODO
     */
    public boolean isShortDescriptionResolved() {
        return shortDescriptionResolved;
    }

    /**
     * @see #getShortDescriptionResolved
     */
    public void setShortDescriptionResolved(boolean shortDescriptionResolved) {
        this.shortDescriptionResolved = shortDescriptionResolved;
    }

    /**
     * TODO
     */
    public URL getMoreInfo() {
        return moreInfo;
    }

    /**
     * @see #setMoreInfoUrl
     */
    public void setMoreInfo(URL moreInfo) {
        this.moreInfo = moreInfo;
    }

    public boolean isAllResolved() {
        return isDisplayNameResolved() && isShortDescriptionResolved() && getMoreInfo() != null;
    }
}