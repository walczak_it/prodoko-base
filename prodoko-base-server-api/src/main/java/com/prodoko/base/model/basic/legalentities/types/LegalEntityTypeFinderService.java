package com.prodoko.base.model.basic.legalentities.types;

import com.prodoko.base.model.beans.dynamic.MainDynamicClassFinderService;
import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface LegalEntityTypeFinderService
        extends MainDynamicClassFinderService<LegalEntityType> {

}
