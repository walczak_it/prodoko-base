package com.prodoko.base.model.beans.dynamic;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.prodoko.base.model.beans.BusinessProperty;
import com.prodoko.base.util.ClassSupporter;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.UpdatingMode;

@Entity
@RemotableAsClone
@SubclassesInstanceOfThis
public abstract class MainDynamicClass extends DynamicClass implements ClassSupporter {

    private static final long serialVersionUID = -9064737028740527373L;

    private MainDynamicClass dynamicSuperclass;
    private Set<MainDynamicClass> dynamicSubclasses;
    private Set<HelperDynamicClass> helperDynamicSubclasses;
    
    @Transient
    @Override
    public boolean isSupportsSubclasses() {
        return false;
    }
    
    @Override
    @Column(updatable=false)
    @BeanPropertyRemoting(updating=UpdatingMode.NEVER_UPDATE)
    public abstract Class<? extends DynamicBean> getSupportedClass();
    
    public void setSupportedClass(Class<? extends DynamicBean> clazz) {
        
    }

    /**
     * TODO
     */
    @BusinessProperty
    @ManyToOne(fetch=FetchType.LAZY)
    @BeanPropertyRemoting(cloning=CloningMode.CHOPPED,
            updating=UpdatingMode.FIND_CORRESPONDING_AND_UPDATE)
    public MainDynamicClass getDynamicSuperclass() {
        return dynamicSuperclass;
    }

    /**
     * @see #getDynamicSuperclass
     */
    public void setDynamicSuperclass(MainDynamicClass dynamicSuperclass) {
        this.dynamicSuperclass = dynamicSuperclass;
    }

    /**
     * TODO
     */
    @BusinessProperty
    @OneToMany(mappedBy="dynamicSuperclass")
    @CollectionPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT,
            updating=UpdatingMode.NEVER_UPDATE)
    public Set<MainDynamicClass> getDynamicSubclasses() {
        return dynamicSubclasses;
    }

    /**
     * @see #getDynamicSubclasses
     */
    public void setDynamicSubclasses(Set<MainDynamicClass> dynamicSubclasses) {
        this.dynamicSubclasses = dynamicSubclasses;
    }

    /**
     * TODO
     */
    @OneToMany(mappedBy="limitingDynamicClass")
    @CollectionPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT,
            updating=UpdatingMode.FIND_CORRESPONDING_AND_UPDATE)
    public Set<HelperDynamicClass> getHelperDynamicSubclasses() {
        return helperDynamicSubclasses;
    }

    /**
     * @see #getHelperDynamicSubclasses
     */
    public void setHelperDynamicSubclasses(
            Set<HelperDynamicClass> helperDynamicSubclasses) {
        this.helperDynamicSubclasses = helperDynamicSubclasses;
    }
}
