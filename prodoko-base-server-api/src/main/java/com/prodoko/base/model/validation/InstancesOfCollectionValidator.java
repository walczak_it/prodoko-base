package com.prodoko.base.model.validation;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InstancesOfCollectionValidator
    implements ConstraintValidator<InstancesOf, Collection<?>> {
    
    private InstancesOf constraintAnnotation;

    @Override
    public void initialize(InstancesOf constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(Collection<?> values, ConstraintValidatorContext context) {
        if(values == null) {
            return true;
        }
        for(Object value : values) {
            if(!constraintAnnotation.value().isAssignableFrom(value.getClass())){
                return false;
            }
        }
        return true;
    }

}
