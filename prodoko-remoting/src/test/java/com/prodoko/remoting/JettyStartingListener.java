package com.prodoko.remoting;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.eclipse.jetty.util.component.AbstractLifeCycle.AbstractLifeCycleListener;
import org.eclipse.jetty.util.component.LifeCycle;
import org.springframework.web.SpringServletContainerInitializer;

public class JettyStartingListener extends AbstractLifeCycleListener {

    private final ServletContext sc;
    private final Set<Class<?>> wai;

    public JettyStartingListener(ServletContext sc,
            Set<Class<?>> webApplicationInitializers){
        super();
        this.sc = sc;
        this.wai = webApplicationInitializers;
    }

    @Override
    public void lifeCycleStarting(LifeCycle event) {
        try {
            new SpringServletContainerInitializer().onStartup(wai, sc);
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }
}