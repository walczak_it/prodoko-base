package com.prodoko.remoting.support;

import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;

public class SomeListedBean {

    private SomeBean listHolder;
    private String name;

    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    public SomeBean getListHolder() {
        return listHolder;
    }

    public void setListHolder(SomeBean listHolder) {
        this.listHolder = listHolder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
