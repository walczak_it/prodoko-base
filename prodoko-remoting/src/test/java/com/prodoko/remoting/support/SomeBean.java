package com.prodoko.remoting.support;

import java.util.List;

import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotableAsClone;

@RemotableAsClone
public class SomeBean {

    private Long id;
    private SomeBean parent;
    private List<SomeBean> children;
    private String name;
    private Address address;
    private List<SomeListedBean> listedBeans;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @BeanPropertyRemoting(cloning=CloningMode.CHOPPED)
    public SomeBean getParent() {
        return parent;
    }

    public void setParent(SomeBean parent) {
        this.parent = parent;
    }

    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    public List<SomeBean> getChildren() {
        return children;
    }

    public void setChildren(List<SomeBean> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_DEEP)
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @CollectionPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    public List<SomeListedBean> getListedBeans() {
        return listedBeans;
    }

    public void setListedBeans(List<SomeListedBean> listedBeans) {
        this.listedBeans = listedBeans;
    }
}
