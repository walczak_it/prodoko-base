package com.prodoko.remoting.support;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.prodoko.remoting.support.cloning.RemoteCloner;

@RunWith(JUnit4.class)
public class RemoteClonerTests {

    @Test
    public void basicCloning() {
        
        RemoteCloner cloner = new RemoteCloner();
        
        // setup source bean graph
        
        SomeBean source = new SomeBean();
        source.setId(1l);
        source.setName("L");
        Address sourceAddress = new Address();
        sourceAddress.setCity("Lity");
        sourceAddress.setStreet("Ltreet");
        source.setAddress(sourceAddress);
        
        SomeListedBean sourceListedBean = new SomeListedBean();
        sourceListedBean.setName("L-LB1");
        source.setListedBeans(Arrays.asList(sourceListedBean));
        sourceListedBean.setListHolder(source);
        
        SomeBean sourceParent = new SomeBean();
        sourceParent.setId(2l);
        sourceParent.setName("P");
        Address sourceParentAddress = new Address();
        sourceParentAddress.setCity("Pity");
        sourceParentAddress.setStreet("Ptreet");
        sourceParent.setAddress(sourceParentAddress);
        
        SomeBean sourceRoot = new SomeBean();
        sourceRoot.setId(3l);
        sourceRoot.setName("R");

        source.setParent(sourceParent);
        sourceParent.setParent(sourceRoot);
        
        // clone
        
        SomeBean clone = (SomeBean) cloner.cloneBean(source);
        
        // check if cloning mode modifiers worked
        
        assertEquals(new Long(1), clone.getId());
        assertEquals("L", clone.getName());
        assertEquals("Lity", clone.getAddress().getCity());
        assertEquals("Ltreet", clone.getAddress().getStreet());
        
        assertNull(clone.getListedBeans());
        
        assertNotNull(clone.getParent());
        assertEquals(new Long(2), clone.getParent().getId());
        assertEquals("P", clone.getParent().getName());
        assertEquals("Pity", clone.getParent().getAddress().getCity());
        assertEquals("Ptreet", clone.getParent().getAddress().getStreet());
        
        assertNull(clone.getParent().getParent());
        
        // modify the clone
        
        clone.setName("L-mod");
        clone.getAddress().setStreet("Ltreet-mod");
        clone.getParent().setName("P-mod");
        clone.getParent().getAddress().setCity("Pity-mod");
        
        // check if source was not modified
        
        assertEquals(new Long(1), source.getId());
        assertEquals("L", source.getName());
        assertEquals("Lity", source.getAddress().getCity());
        assertEquals("Ltreet", source.getAddress().getStreet());
        
        assertNotNull(source.getListedBeans());
        
        assertNotNull(source.getParent());
        assertEquals(new Long(2), source.getParent().getId());
        assertEquals("P", source.getParent().getName());
        assertEquals("Pity", source.getParent().getAddress().getCity());
        assertEquals("Ptreet", source.getParent().getAddress().getStreet());
        
        assertNotNull(source.getParent().getParent());
        assertEquals(new Long(3), source.getParent().getParent().getId());
        assertEquals("R", source.getParent().getParent().getName());
        
        // copy clone values back to source
        
        cloner.updateSourceBean(source, clone);
        
        assertEquals(new Long(1), source.getId());
        assertEquals("L-mod", source.getName());
        assertEquals("Lity", source.getAddress().getCity());
        assertEquals("Ltreet-mod", source.getAddress().getStreet());
        
        assertNotNull(source.getListedBeans());
        
        assertNotNull(source.getParent());
        assertEquals(new Long(2), source.getParent().getId());
        assertEquals("P-mod", source.getParent().getName());
        assertEquals("Pity-mod", source.getParent().getAddress().getCity());
        assertEquals("Ptreet", source.getParent().getAddress().getStreet());
        
        assertNotNull(source.getParent().getParent());
        assertEquals(new Long(3), source.getParent().getParent().getId());
        assertEquals("R", source.getParent().getParent().getName());
    }
}
