package com.prodoko.remoting.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.CollectionPropertyRemoting;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.RemotableAsClone;
import com.prodoko.remoting.support.cloning.UpdatingMode;

/**
 * Both an entity and a general purpose DTO
 *  
 * @author walec51
 */
@Entity
@RemotableAsClone
public class SomeBean {

    private Long id;
    private SomeBean parent;
    private List<SomeBean> children;
    private String name;
    private Address address;
    private List<SomeListedBean> listedBeans;

    @Id @GeneratedValue
    @BeanPropertyRemoting(updating=UpdatingMode.NEVER_UPDATE)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Chopped cloning will only set primitive and simple properties of the parent SomeBean.
     * This way we wont get all the parents - just the direct one.
     */
    @ManyToOne
    @BeanPropertyRemoting(cloning=CloningMode.CHOPPED, updating=UpdatingMode.FIND_CORRESPONDING_AND_UPDATE)
    public SomeBean getParent() {
        return parent;
    }

    public void setParent(SomeBean parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy="parent")
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    public List<SomeBean> getChildren() {
        return children;
    }

    public void setChildren(List<SomeBean> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * However this is ignored in CHOPPED property cloning mode. So on the client you
     * CAN: receivedBean.getParent().getAddress().getCity()
     * but you CANNOT: receivedBean.getParent().getParent().get... this will throw NPE
     */
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_DEEP)
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @OneToMany(mappedBy="listHolder")
    @CollectionPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    public List<SomeListedBean> getListedBeans() {
        return listedBeans;
    }

    public void setListedBeans(List<SomeListedBean> listedBeans) {
        this.listedBeans = listedBeans;
    }
}
