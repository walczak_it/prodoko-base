package com.prodoko.remoting.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitTransactionType;

import org.hibernate.cfg.Configuration;
import org.hibernate.ejb.EntityManagerFactoryImpl;
import org.hibernate.service.BootstrapServiceRegistry;
import org.hibernate.service.BootstrapServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.prodoko.remoting.support.cloning.RemoteCloner;

@RunWith(JUnit4.class)
public class ByIdReplacerTests {

    private EntityManager em;
    
    @Before
    public void buildEntityManager() {
        Configuration cfg = new Configuration();
        
        Properties props = new Properties();
        props.setProperty("hibernate.show_sql", "true");
        props.setProperty("hibernate.format_sql", "true");
        props.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        props.setProperty("hibernate.connection.url", "jdbc:h2:mem:test");
        props.setProperty("hibernate.connection.username", "sa");
        props.setProperty("hibernate.connection.password", "");
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        props.setProperty("hibernate.hbm2ddl.auto", "update");
        cfg.setProperties(props);
        
        cfg.addAnnotatedClass(Address.class)
            .addAnnotatedClass(SomeBean.class)
            .addAnnotatedClass(SomeListedBean.class);
        
        BootstrapServiceRegistry bootstrapServiceRegistry = new BootstrapServiceRegistryBuilder()
                .build();
        ServiceRegistryBuilder srb = new ServiceRegistryBuilder(bootstrapServiceRegistry);
        ServiceRegistry serviceRegistry = srb.applySettings(props).buildServiceRegistry();
        
        EntityManagerFactory emf = new EntityManagerFactoryImpl(
                PersistenceUnitTransactionType.RESOURCE_LOCAL, true, null,
                cfg, serviceRegistry, "testPU");
        em = emf.createEntityManager();
    }
    
    @After
    public void closeEntityManager() {
        em.close();
        em = null;
    }
    
    @Test
    public void basicCloning() {
        
        RemoteCloner cloner = new RemoteCloner();
        ByIdEntityFinder byIdEntityFinder = new ByIdEntityFinder();
        byIdEntityFinder.setEntityManager(em);
        cloner.setDefaultFinder(byIdEntityFinder);
        
        // setup source bean graph
        
        em.getTransaction().begin();
        
        SomeBean source = new SomeBean();
        source.setName("L");
        Address sourceAddress = new Address();
        sourceAddress.setCity("Lity");
        sourceAddress.setStreet("Ltreet");
        source.setAddress(sourceAddress);
        source.setListedBeans(new ArrayList<SomeListedBean>());
        em.persist(source);
        Long sourceId = source.getId();
        
        SomeListedBean sourceListedBean = new SomeListedBean();
        sourceListedBean.setName("L-LB1");
        em.persist(sourceListedBean);
        Long sourceListedBeanId = sourceListedBean.getId();
        sourceListedBean.setListHolder(source);
        source.getListedBeans().add(sourceListedBean);
        
        SomeBean sourceParent = new SomeBean();
        sourceParent.setName("P");
        Address sourceParentAddress = new Address();
        sourceParentAddress.setCity("Pity");
        sourceParentAddress.setStreet("Ptreet");
        sourceParent.setAddress(sourceParentAddress);
        sourceParent.setChildren(new ArrayList<SomeBean>());
        em.persist(sourceParent);
        Long sourceParentId = sourceParent.getId();
        
        SomeBean sourceRoot = new SomeBean();
        sourceRoot.setName("R");
        sourceRoot.setChildren(new ArrayList<SomeBean>());
        em.persist(sourceRoot);
        Long sourceRootId = sourceRoot.getId();

        source.setParent(sourceParent);
        sourceParent.getChildren().add(source);
        sourceParent.setParent(sourceRoot);
        sourceRoot.getChildren().add(sourceParent);
        
        // clone
        
        SomeBean clone = (SomeBean) cloner.cloneBean(source);
        em.getTransaction().commit();
        
        // check if cloning mode modifiers worked
        
        assertEquals(sourceId, clone.getId());
        assertEquals("L", clone.getName());
        assertEquals("Lity", clone.getAddress().getCity());
        assertEquals("Ltreet", clone.getAddress().getStreet());
        
        assertNull(clone.getListedBeans());
        
        assertNotNull(clone.getParent());
        assertEquals(sourceParentId, clone.getParent().getId());
        assertEquals("P", clone.getParent().getName());
        assertEquals("Pity", clone.getParent().getAddress().getCity());
        assertEquals("Ptreet", clone.getParent().getAddress().getStreet());
        
        assertNull(clone.getParent().getParent());
        
        // modify the clone
        
        clone.setName("L-mod");
        clone.getAddress().setStreet("Ltreet-mod");
        clone.getParent().setName("P-mod");
        clone.getParent().getAddress().setCity("Pity-mod");
        
        // copy clone values back to source
        
        em.getTransaction().begin();
        source = em.find(SomeBean.class, sourceId);
        sourceParent = em.find(SomeBean.class, sourceParentId);
        sourceRoot = em.find(SomeBean.class, sourceRootId);
        assertNotNull(source.getParent().getParent());
        
        cloner.updateSourceBean(source, clone);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        source = em.find(SomeBean.class, sourceId);
        sourceParent = em.find(SomeBean.class, sourceParentId);
        sourceRoot = em.find(SomeBean.class, sourceRootId);
        
        assertEquals("L-mod", source.getName());
        assertEquals("Lity", source.getAddress().getCity());
        assertEquals("Ltreet-mod", source.getAddress().getStreet());
        
        assertNotNull(source.getListedBeans());
        assertEquals(1l, source.getListedBeans().size());
        assertEquals(sourceListedBeanId, source.getListedBeans().get(0).getId());
        
        assertNotNull(source.getParent());
        assertEquals("P-mod", source.getParent().getName());
        assertEquals("Pity-mod", source.getParent().getAddress().getCity());
        assertEquals("Ptreet", source.getParent().getAddress().getStreet());
        
        assertNotNull(source.getParent().getParent());
        assertEquals("R", source.getParent().getParent().getName());
        em.getTransaction().commit();
    }
}
