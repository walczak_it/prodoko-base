package com.prodoko.remoting.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.prodoko.remoting.support.cloning.CloningMode;
import com.prodoko.remoting.support.cloning.BeanPropertyRemoting;
import com.prodoko.remoting.support.cloning.UpdatingMode;

@Entity
public class SomeListedBean {

    private Long id;
    private SomeBean listHolder;
    private String name;

    @Id @GeneratedValue
    @BeanPropertyRemoting(updating=UpdatingMode.NEVER_UPDATE)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @BeanPropertyRemoting(cloning=CloningMode.ALWAYS_TRANSISTENT)
    public SomeBean getListHolder() {
        return listHolder;
    }

    public void setListHolder(SomeBean listHolder) {
        this.listHolder = listHolder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
