package com.prodoko.remoting.jpa;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.prodoko.remoting.support.cloning.RemoteCloneAspect;
import com.prodoko.remoting.support.cloning.RemoteCloner;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class SomeConfig {
    
    @Autowired
    public DataSource dataSource;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setUsername("sa");
        return dataSource;
    }

    @Bean
    public JpaTransactionManager transactionManager(
            LocalContainerEntityManagerFactoryBean entityManagerFactoryBean)
            throws ClassNotFoundException {

        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(entityManagerFactoryBean
                .getObject());

        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory()
            throws ClassNotFoundException {

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean
            = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPackagesToScan(new String[] {"com.prodoko.remoting.jpa"});
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        Properties props = new Properties();
        props.setProperty("hibernate.show_sql", "true");
        props.setProperty("hibernate.format_sql", "true");
        props.setProperty("hibernate.connection.username", "sa");
        props.setProperty("hibernate.connection.password", "");
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        props.setProperty("hibernate.hbm2ddl.auto", "create");
        entityManagerFactoryBean.setJpaProperties(props);

        return entityManagerFactoryBean;
    }
    
    @Bean
    public SomeService someService() {
        return new SomeService();
    }
    
    @Bean
    public ByIdEntityFinder defaultFinder() {
        return new ByIdEntityFinder();
    }
    
    @Bean @Autowired
    public RemoteCloner remoteCloner(ByIdEntityFinder defaultFinder) {
        RemoteCloner cloner = new RemoteCloner();
        cloner.setDefaultFinder(defaultFinder);
        return cloner;
    }
    
    @Bean
    public RemoteCloneAspect remoteCloneAspect() {
        return new RemoteCloneAspect();
    }
}
