package com.prodoko.remoting.jpa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class,
    classes={SomeConfig.class})
@Transactional
public class RemoteCloneEntityAspectTests {

    @Autowired
    public SomeService service;
    
    @Test
    public void createAndCloneTest() {
        
        SomeBean a = service.createABean();
        assertFalse(service.contains(a));
        assertEquals("A", a.getName());
        assertNull(a.getListedBeans());
        
        a.setName("A-mod");
        service.updateSomeBean(a);
        SomeBean amod = service.find(a.getId());
        assertEquals("A-mod", amod.getName());
        service.assertHasOneListedItem(a.getId());
    }
    
    @Test
    public void collectionCloneTest() {
        
        SomeBean a = service.createABean();
        List<SomeListedBean> list = service.findListedBeans(a.getId());
        assertNotNull(list);
        assertEquals(ArrayList.class, list.getClass());
        assertEquals(1, list.size());
        assertEquals("al", list.get(0).getName());
    }
}
