package com.prodoko.remoting.jpa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.remoting.support.cloning.CloneRemotedBean;
import com.prodoko.remoting.support.cloning.CloneRemotedCollection;
import com.prodoko.remoting.support.cloning.UncloneRemotedBean;

@Service
@Transactional
public class SomeService {

    @PersistenceContext
    public EntityManager em;
    
    public @CloneRemotedBean SomeBean createABean() {
        SomeBean a = new SomeBean();
        a.setName("A");
        a.setListedBeans(new ArrayList<SomeListedBean>());
        em.persist(a);
        
        SomeListedBean al = new SomeListedBean();
        al.setName("al");
        em.persist(a);
        al.setListHolder(a);
        a.getListedBeans().add(al);
        return a;
    }
    
    public void updateSomeBean(@UncloneRemotedBean SomeBean bean) {
        
    }
    
    public SomeBean find(Long id) {
        return em.find(SomeBean.class, id);
    }
    
    public void assertHasOneListedItem(Long id) {
        SomeBean bean = em.find(SomeBean.class, id);
        assertNotNull(bean.getListedBeans());
        assertEquals(1, bean.getListedBeans().size());
    }
    
    public boolean contains(Object entity) {
        return em.contains(entity);
    }
    
    public @CloneRemotedCollection List<SomeListedBean> findListedBeans(Long listHolderId) {
        SomeBean listHolder = em.find(SomeBean.class, listHolderId);
        return listHolder.getListedBeans();
    }
}
