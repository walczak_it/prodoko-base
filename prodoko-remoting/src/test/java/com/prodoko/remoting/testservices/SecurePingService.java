package com.prodoko.remoting.testservices;

import org.springframework.security.access.prepost.PreAuthorize;

import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface SecurePingService {

    @PreAuthorize("hasRole('ROLE_USER')")
    public String ping(String returnValue);
}
