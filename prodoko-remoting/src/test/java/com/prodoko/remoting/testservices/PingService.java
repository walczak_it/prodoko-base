package com.prodoko.remoting.testservices;

import com.prodoko.remoting.support.ServiceInterface;

@ServiceInterface
public interface PingService {

    public String ping(String returnValue);
}
