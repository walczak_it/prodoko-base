package com.prodoko.remoting.testservices;

import org.springframework.stereotype.Service;

@Service
public class PingServiceImpl implements PingService {

    @Override
    public String ping(String returnValue) {
        return returnValue;
    }

}
