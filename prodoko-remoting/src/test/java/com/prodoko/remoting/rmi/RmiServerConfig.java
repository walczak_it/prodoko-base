package com.prodoko.remoting.rmi;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.prodoko.remoting.rmi.EnableRmiExport;

@Configuration
@ComponentScan("com.prodoko.remoting.testservices")
@EnableRmiExport
public class RmiServerConfig {
    
    public RmiServerConfig() {}
}