package com.prodoko.remoting.rmi;

import org.springframework.context.annotation.Configuration;

import com.prodoko.remoting.rmi.EnableRmiAccess;

@Configuration
@EnableRmiAccess(basePackages="com.prodoko.remoting.testservices",
        host="localhost")
public class RmiClientConfig {
    
    public RmiClientConfig() { } 
}