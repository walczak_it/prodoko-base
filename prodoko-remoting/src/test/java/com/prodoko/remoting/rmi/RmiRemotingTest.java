package com.prodoko.remoting.rmi;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.prodoko.remoting.testservices.PingService;

@RunWith(JUnit4.class)
public class RmiRemotingTest {

    @Test
    public void rmiRemotingTest() throws Exception {
        
        AnnotationConfigApplicationContext serverContext
            = new AnnotationConfigApplicationContext();
        serverContext.register(RmiServerConfig.class);
        serverContext.refresh();
        serverContext.start();
        
        AnnotationConfigApplicationContext clientContext
            = new AnnotationConfigApplicationContext();
        clientContext.register(RmiClientConfig.class);
        clientContext.refresh();
        clientContext.start();
        
        PingService pingService = (PingService) clientContext.getBean(PingService.class);
        String ret = pingService.ping("pong");
        assertEquals(ret, "pong");
        
        clientContext.close();
        serverContext.close();
    }
}
