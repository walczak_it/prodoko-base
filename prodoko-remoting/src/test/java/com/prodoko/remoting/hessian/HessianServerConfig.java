package com.prodoko.remoting.hessian;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.prodoko.remoting.hessian.EnableHessianExport;

@Configuration
@ComponentScan("com.prodoko.remoting.testservices")
@EnableHessianExport
public class HessianServerConfig {

}
