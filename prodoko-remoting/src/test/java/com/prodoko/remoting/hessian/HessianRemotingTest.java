package com.prodoko.remoting.hessian;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.prodoko.remoting.JettyStartingListener;
import com.prodoko.remoting.SecurityWebApplicationInitializer;
import com.prodoko.remoting.testservices.PingService;
import com.prodoko.remoting.testservices.SecurePingService;

//@RunWith(JUnit4.class)
public class HessianRemotingTest {

    Server server;
    
    AnnotationConfigApplicationContext clientContext;
    
    //@Before
    public void nullify() {
        server = null;
        clientContext = null;
    }
    
    //@Test
    public void hessianRemotingTest() throws Exception {
        
        try {
            //server = bootServer(HessianServerConfig.class);
            server = bootServer(UnsecuredWebApplicationInitializer.class);
            
            clientContext = bootClient(
                    HessianClientConfig.class);
            
            PingService pingService = (PingService) clientContext.getBean(
                    PingService.class);
            String ret = pingService.ping("pong");
            assertEquals(ret, "pong");
        } finally {
            if(clientContext != null)
                clientContext.close();
            if(server != null)
                server.stop();
        }
    }
    
    //@Test
    public void secureHessianRemotingTest() throws Exception {

        try {
            //server = bootServer(SecureHessianServerConfig.class);
            server = bootServer(SecurityWebApplicationInitializer.class,
                    SecuredWebApplicationInitializer.class);
            
            clientContext = bootClient(SecureHessianClientConfig.class);
            
            SecurePingService pingService
                    = (SecurePingService) clientContext.getBean(
                            SecurePingService.class);
            String ret = pingService.ping("pong");
            assertEquals(ret, "pong");
        } finally {
            if(clientContext != null)
                clientContext.close();
            if(server != null)
                server.stop();
        }
    }
    
    //@Test
    public void secureRemotingWithClientConDataInFileTest() throws Exception {

        try {
            //server = bootServer(SecureHessianServerConfig.class);
            server = bootServer(SecurityWebApplicationInitializer.class,
                    SecuredWebApplicationInitializer.class);
            
            clientContext = bootClient(
                    SecureHessianClientWithoutConnDataConfig.class);
            
            SecurePingService pingService
                    = (SecurePingService) clientContext.getBean(
                            SecurePingService.class);
            String ret = pingService.ping("pong");
            assertEquals(ret, "pong");
        } finally {
            if(clientContext != null)
                clientContext.close();
            if(server != null)
                server.stop();
        }
    }
    
    //@Test
    public void secureRemotingWithWrongPasswordTest() throws Exception {

        try {
            //server = bootServer(SecureHessianServerConfig.class);
            server = bootServer(SecurityWebApplicationInitializer.class,
                    SecuredWebApplicationInitializer.class);
            
            clientContext = bootClient(
                    SecureHessianClientWrongPasswordConfig.class);
            
            SecurePingService pingService
                    = (SecurePingService) clientContext.getBean(
                            SecurePingService.class);
            String ret = pingService.ping("pong");
            assertEquals(ret, "pong");
        } finally {
            if(clientContext != null)
                clientContext.close();
            if(server != null)
                server.stop();
        }
    }
    
    private Server bootServer(Class<?>... wais) 
            throws Exception {
        Server server = new Server(8080);
        ServletContextHandler resource = new ServletContextHandler(
                ServletContextHandler.SESSIONS);
        resource.setContextPath("/");
        ServletContext sc = resource.getServletContext();
        HashSet<Class<?>> waiSet = new HashSet<>();
        for(Class<?> wai : wais) {
            waiSet.add(wai);
        }
        server.setHandler(resource);
        resource.setLogger(Log.getLogger("workaround logger"));
        server.addLifeCycleListener(new JettyStartingListener(sc, waiSet));
        
        /*
        ServletHolder dispatcher = resource.addServlet(DispatcherServlet.class, "/api/*");
        dispatcher.setInitParameter("contextClass",
                AnnotationConfigWebApplicationContext.class.getName());
        dispatcher.setInitParameter("contextConfigLocation",
                springConfig.getName());
        dispatcher.setInitOrder(0);
        */
        server.setStopAtShutdown(true);
        server.start();
        return server;
    }
    
    private AnnotationConfigApplicationContext bootClient(Class<?> springConfig)
            throws Exception {
        AnnotationConfigApplicationContext clientContext
            = new AnnotationConfigApplicationContext();
        clientContext.register(springConfig);
        clientContext.refresh();
        return clientContext;
    }
}
