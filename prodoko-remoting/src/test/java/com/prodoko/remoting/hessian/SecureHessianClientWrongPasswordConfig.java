package com.prodoko.remoting.hessian;

import org.springframework.context.annotation.Configuration;

import com.prodoko.remoting.hessian.EnableHessianAccess;

@Configuration
@EnableHessianAccess(basePackages="com.prodoko.remoting.testservices", pathPreffix="api/",
        host="localhost", port=8080, username="someusr", password="wrong")
public class SecureHessianClientWrongPasswordConfig {
}
