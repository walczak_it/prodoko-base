package com.prodoko.remoting.hessian;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.prodoko.remoting.hessian.EnableHessianAccess;

@Configuration
@PropertySource(value="classpath:com/prodoko/remoting/hessian/conn.properties")
@EnableHessianAccess(basePackages="com.prodoko.remoting.testservices", pathPreffix="api/")
public class SecureHessianClientWithoutConnDataConfig {
    
}
