package com.prodoko.remoting.hessian;

public enum HessianAccessPropertyNames {

    HOST("prodoko.remoting.hessian.host"),
    PORT("prodoko.remoting.hessian.port"),
    USE_SSL("prodoko.remoting.hessian.useSsl"),
    USERNAME("prodoko.remoting.hessian.username"),
    PASSWORD("prodoko.remoting.hessian.password");
    
    private String propertyName;
    
    private HessianAccessPropertyNames(String propertyName) {
        this.propertyName = propertyName;
    }
    
    public String getPropertyName() {
        return propertyName;
    }
}
