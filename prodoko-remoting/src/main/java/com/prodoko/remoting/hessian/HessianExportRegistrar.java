package com.prodoko.remoting.hessian;

import java.lang.annotation.Annotation;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

import com.caucho.hessian.io.SerializerFactory;
import com.prodoko.remoting.support.RemotingExportRegistrar;
import com.prodoko.remoting.support.ServiceMetadata;

public class HessianExportRegistrar extends RemotingExportRegistrar
        implements ImportBeanDefinitionRegistrar {
    
    private Class<? extends SerializerFactory> serializerFactoryClass;

    public Class<? extends SerializerFactory> getSerializerFactoryClass() {
        return serializerFactoryClass;
    }

    public void setSerializerFactoryClass(Class<? extends SerializerFactory> serializerFactoryClass) {
        this.serializerFactoryClass = serializerFactoryClass;
    }

    @Override
    public Class<? extends RemoteExporter> getExporterClass() {
        return HessianServiceExporter.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void registerBeanDefinitions(
            AnnotationMetadata importingClassMetadata,
            BeanDefinitionRegistry registry) {
        
        AnnotationAttributes annoAttrs = AnnotationAttributes.fromMap(
                importingClassMetadata .getAnnotationAttributes(
                        EnableHessianExport.class.getName()));
        setServiceImplAnnotationType((Class<? extends Annotation>) annoAttrs
                .getClass("serviceImplAnnotationType"));
        setSerializerFactoryClass((Class<? extends SerializerFactory>) annoAttrs
                .getClass("serializerFactoryClass"));
        setInterceptorBeanNames(annoAttrs.getStringArray("interceptorBeanNames"));
        registerBeanDefinitions(registry);
    }
    
    @Override
    protected String exportBeanName(ServiceMetadata meta) {
        return "/" + super.exportBeanName(meta);
    }
    
    @Override
    protected void initializeOtherProperties(
            BeanDefinitionBuilder exportBeanBuilder, ServiceMetadata meta) {
        if(getSerializerFactoryClass() != null) {
            Class<? extends SerializerFactory> clazz = getSerializerFactoryClass();
            try {
                SerializerFactory sf = clazz.newInstance();
                exportBeanBuilder.addPropertyValue("serializerFactory", sf);
                
            } catch (InstantiationException | IllegalAccessException ex) {
                throw new IllegalStateException("Couldn't instatiate the given "
                        + "serialization factory of class" + clazz.getName(),
                        ex);
            }
        }
    }
}
