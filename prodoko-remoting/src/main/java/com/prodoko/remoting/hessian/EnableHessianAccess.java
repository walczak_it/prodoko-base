package com.prodoko.remoting.hessian;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.caucho.hessian.io.SerializerFactory;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(HessianAccessRegistrar.class)
public @interface EnableHessianAccess {

    String[] basePackages();
    /**
     * This attribute is required ! It must be set in this annotation or in
     * the enviorment property
     * @see HessianAccessPropertyNames
     */
    String host() default "";
    int port() default 80; 
    String pathPreffix() default "";
    boolean useSsl() default false;
    String username() default "";
    String password() default "";
    Class<? extends SerializerFactory> serializerFactoryClass() default SerializerFactory.class;
    boolean overloadEnabled() default false;
}
