package com.prodoko.remoting.hessian;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import org.springframework.remoting.support.RemoteAccessor;
import org.springframework.util.StringUtils;

import com.caucho.hessian.io.SerializerFactory;
import com.prodoko.remoting.support.RemotingAccessRegistrar;
import com.prodoko.remoting.support.ServiceInterfaceMetadata;

public class HessianAccessRegistrar extends RemotingAccessRegistrar
        implements ImportBeanDefinitionRegistrar, EnvironmentAware {
    
    private Environment env;
    
    private String host;
    private int port = 0;
    private String pathPreffix = null;
    private boolean useSsl = false;
    private String username = null;
    private String password = null;
    private Class<? extends SerializerFactory> serializerFactoryClass;
    private boolean overloadEnabled;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPathPreffix() {
        return pathPreffix;
    }

    public void setPathPreffix(String pathPreffix) {
        this.pathPreffix = pathPreffix;
    }

    public boolean isUseSsl() {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl) {
        this.useSsl = useSsl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * TODO
     */
    public Class<? extends SerializerFactory> getSerializerFactoryClass() {
        return serializerFactoryClass;
    }

    /**
     * @see #getSerializerFactoryClass
     */
    public void setSerializerFactoryClass(
            Class<? extends SerializerFactory> serializerFactoryClass) {
        this.serializerFactoryClass = serializerFactoryClass;
    }

    /**
     * TODO
     */
    public boolean isOverloadEnabled() {
        return overloadEnabled;
    }

    /**
     * @see #setOverloadEnabled
     */
    public void setOverloadEnabled(boolean overloadEnabled) {
        this.overloadEnabled = overloadEnabled;
    }

    @Override
    protected Class<? extends RemoteAccessor> getAccessorClass() {
        return HessianProxyFactoryBean.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void registerBeanDefinitions(
            AnnotationMetadata importingClassMetadata,
            BeanDefinitionRegistry registry) {
        
        AnnotationAttributes annoAttrs = AnnotationAttributes.fromMap(
                importingClassMetadata .getAnnotationAttributes(
                        EnableHessianAccess.class.getName()));
        setBasePackages(annoAttrs.getStringArray("basePackages"));
        setPathPreffix(annoAttrs.getString("pathPreffix"));
        
        setHost(annoAttrs.getString("host"));
        String envHost = env.getProperty(HessianAccessPropertyNames.HOST
            .getPropertyName());
        if(StringUtils.hasText(envHost)) {
            setHost(envHost);
        }
        if(!StringUtils.hasText(getHost())) {
            throw new RemoteAccessException("host property is required");
        }
        
        setPort(annoAttrs.getNumber("port").intValue());
        String envPortStr = env.getProperty(HessianAccessPropertyNames.PORT
            .getPropertyName());
        if(StringUtils.hasText(envPortStr)) {
            setPort(Integer.parseInt(envPortStr));
        }
        
        setUseSsl(annoAttrs.getBoolean("useSsl"));
        String envUseSslStr = env.getProperty(HessianAccessPropertyNames.USE_SSL
            .getPropertyName());
        if(StringUtils.hasText(envPortStr)) {
            setUseSsl(Boolean.parseBoolean(envUseSslStr));
        }
        
        setUsername(annoAttrs.getString("username"));
        String envUsername = env.getProperty(HessianAccessPropertyNames.USERNAME
            .getPropertyName());
        if(StringUtils.hasText(envUsername)) {
            setUsername(envUsername);
        }
        
        setPassword(annoAttrs.getString("password"));
        String envPassword = env.getProperty(HessianAccessPropertyNames.PASSWORD
            .getPropertyName());
        if(StringUtils.hasText(envPassword)) {
            setPassword(envPassword);
        }
        
        setSerializerFactoryClass((Class<? extends SerializerFactory>) annoAttrs
                .getClass("serializerFactoryClass"));
        
        setOverloadEnabled(annoAttrs.getBoolean("overloadEnabled"));
            
        registerBeanDefinitions(registry);
    }
    
    public static String serviceUrl(String host, int port,
            String pathPreffix, boolean useSsl, String serviceRemoteName) {
        StringBuilder url = new StringBuilder();
        if(useSsl) {
            url.append("https");
        } else {
            url.append("http");
        }
        url.append("://");
        url.append(host);
        if(port != 0) {
            url.append(':').append(port);
        }
        url.append('/');
        if(StringUtils.hasText(pathPreffix)) {
            url.append(pathPreffix);
        }
        url.append(serviceRemoteName);
        return url.toString();
    }
    
    @Override
    protected void initializeOtherProperties(
            BeanDefinitionBuilder accessBeanBuilder,
            ServiceInterfaceMetadata meta) throws RemoteAccessException {
        
        String serviceUrl = serviceUrl(getHost(), getPort(), getPathPreffix(),
                isUseSsl(), meta.getRemoteName());
        accessBeanBuilder.addPropertyValue("serviceUrl", serviceUrl);
        
        if(StringUtils.hasText(getUsername())) {
            accessBeanBuilder.addPropertyValue("username", getUsername());
        }
        if(StringUtils.hasText(getPassword())) {
            accessBeanBuilder.addPropertyValue("password", getPassword());
        }
        
        if(getSerializerFactoryClass() != null) {
            Class<? extends SerializerFactory> clazz = getSerializerFactoryClass();
            try {
                SerializerFactory sf = clazz.newInstance();
                accessBeanBuilder.addPropertyValue("serializerFactory", sf);
                
            } catch (InstantiationException | IllegalAccessException ex) {
                throw new IllegalStateException("Couldn't instatiate the given "
                        + "serialization factory of class" + clazz.getName(),
                        ex);
            }
        }
        
        if(isOverloadEnabled()) {
            accessBeanBuilder.addPropertyValue("overloadEnabled", true);
        }
    }

    @Override
    public void setEnvironment(Environment env) {
        this.env = env;
    }
}
