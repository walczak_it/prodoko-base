package com.prodoko.remoting.hessian;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;

import com.caucho.hessian.io.SerializerFactory;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(HessianExportRegistrar.class)
public @interface EnableHessianExport {

    Class<? extends Annotation> serviceImplAnnotationType()
        default Service.class; 
    String[] interceptorBeanNames() default {};
    Class<? extends SerializerFactory> serializerFactoryClass() default SerializerFactory.class;
}
