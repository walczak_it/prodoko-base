package com.prodoko.remoting.hessian.fx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import com.caucho.hessian.io.AbstractHessianOutput;
import com.caucho.hessian.io.CollectionSerializer;
import com.caucho.hessian.io.HessianProtocolException;
import com.caucho.hessian.io.MapSerializer;
import com.caucho.hessian.io.Serializer;
import com.caucho.hessian.io.SerializerFactory;

public class FxSerializerFactory extends SerializerFactory {

    private FxListSerializer listSerializer = new FxListSerializer();
    private FxMapSerializer mapSerializer = new FxMapSerializer();

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Serializer getSerializer(Class cl) throws HessianProtocolException {
        if (cl.isAssignableFrom(ObservableMap.class)) {
            return mapSerializer;
        } else if (cl.isAssignableFrom(ObservableList.class)) {
            return listSerializer;
        }
        return super.getSerializer(cl);
    }

    private static class FxListSerializer implements Serializer {

        private CollectionSerializer delegate = new CollectionSerializer();

        @SuppressWarnings({ "unchecked", "rawtypes" })
        public void writeObject(Object obj, AbstractHessianOutput out)
                throws IOException {
            delegate.writeObject(new ArrayList((Collection) obj), out);
        }

    }

    private static class FxMapSerializer implements Serializer {

        private MapSerializer delegate = new MapSerializer();

        @SuppressWarnings({ "unchecked", "rawtypes" })
        public void writeObject(Object obj, AbstractHessianOutput out)
                throws IOException {
            delegate.writeObject(new HashMap((Map) obj), out);
        }

    }
}
