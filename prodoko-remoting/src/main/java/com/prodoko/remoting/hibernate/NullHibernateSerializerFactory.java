package com.prodoko.remoting.hibernate;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.collection.internal.AbstractPersistentCollection;

import com.caucho.hessian.io.AbstractHessianOutput;
import com.caucho.hessian.io.HessianProtocolException;
import com.caucho.hessian.io.Serializer;
import com.caucho.hessian.io.SerializerFactory;

public class NullHibernateSerializerFactory extends SerializerFactory {
    
    private NullSerializer nullSerializer = new NullSerializer();

    @SuppressWarnings("rawtypes")
    public Serializer getSerializer(Class cl) throws HessianProtocolException {
        if(AbstractPersistentCollection.class.isAssignableFrom(cl)) {
            return nullSerializer;
        } else {
            return super.getSerializer(cl);
        }
    }

    private static class NullSerializer implements Serializer {
        
        private static final Log LOG = LogFactory.getLog(NullSerializer.class);
        
        public void writeObject(Object obj, AbstractHessianOutput out)
                throws IOException {
            LOG.warn("Nullifying a Hibernate object during serialization "
                    + "class=" + obj.getClass().getName());
            out.writeNull();
        }
    }

}