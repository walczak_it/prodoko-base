package com.prodoko.remoting.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.PersistenceUtil;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.transaction.annotation.Transactional;

import com.prodoko.remoting.support.cloning.SimpleInstanceProducer;

@Transactional
public class HibernateAwareInstanceProducer extends SimpleInstanceProducer {

    private EntityManager entityManager;
    private PersistenceUnitUtil util;

    /**
     * TODO
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @see #getEntityManager
     */
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.util = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
    }
    
    /**
     * TODO
     */
    public PersistenceUtil getUtil() {
        return util;
    }

    @Override
    protected Object produceBean(Object source, Class<?> sourceAsPropOfType,
            Class<?> forceInstanceClass) {
        if(forceInstanceClass != null) {
            return super.produceBean(source, sourceAsPropOfType, forceInstanceClass);
        }
        if(source instanceof HibernateProxy) {
            Class<?> trueClass = Hibernate.getClass(source);
            return super.produceBean(source, sourceAsPropOfType, trueClass);
        }
        return super.produceBean(source, sourceAsPropOfType, null);
    }
}
