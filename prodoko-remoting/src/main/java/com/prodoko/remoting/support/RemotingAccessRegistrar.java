package com.prodoko.remoting.support;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.remoting.support.RemoteAccessor;

/**
 * Abstract base class for providing automatic access to services.
 * 
 * <p>
 * Scans for service interfaces annotated with {@link ServiceInterface} in the
 * given base packages.
 * 
 * @author Adam Walczak - WALCZAK.IT
 */ 
public abstract class RemotingAccessRegistrar {

    private static final Log LOG = LogFactory
            .getLog(RemotingAccessRegistrar.class);

    private String[] basePackages;

    public String[] getBasePackages() {
        return basePackages;
    }

    public void setBasePackages(String[] basePackages) {
        this.basePackages = basePackages;
    }

    public void registerBeanDefinitions(
            AnnotationMetadata importingClassMetadata,
            BeanDefinitionRegistry registry) throws BeansException {

    }

    public void registerBeanDefinitions(BeanDefinitionRegistry registry,
            String... basePackages) throws BeansException {

        setBasePackages(basePackages);
        registerBeanDefinitions(registry);
    }

    public void registerBeanDefinitions(BeanDefinitionRegistry registry)
            throws BeansException {

        List<ServiceInterfaceMetadata> metas = findServiceInterfaces();
        LOG.debug("importing " + metas.size() + " services that were found.");
        for (ServiceInterfaceMetadata meta : metas) {
            LOG.debug("importing remote service '" + meta.getRemoteName() + "'");
            BeanDefinition accessBeanDef = accessBeanDefinition(meta);
            String accessBeanName = accessBeanName(meta);
            registry.registerBeanDefinition(accessBeanName, accessBeanDef);
        }
    }

    protected BeanDefinition accessBeanDefinition(ServiceInterfaceMetadata meta)
            throws RemoteAccessException {
        BeanDefinitionBuilder exportBeanBuilder = BeanDefinitionBuilder
                .rootBeanDefinition(getAccessorClass());
        initializeServiceInterfaceProperty(exportBeanBuilder, meta);
        initializeOtherProperties(exportBeanBuilder, meta);
        return exportBeanBuilder.getBeanDefinition();
    }

    protected String accessBeanName(ServiceInterfaceMetadata meta) {
        return meta.getRemoteName();
    }

    protected abstract Class<? extends RemoteAccessor> getAccessorClass();

    protected void initializeServiceInterfaceProperty(
            BeanDefinitionBuilder exportBeanBuilder,
            ServiceInterfaceMetadata meta) {
        exportBeanBuilder.addPropertyValue("serviceInterface", meta
                .getInterfaceClass());
    }

    protected void initializeOtherProperties(
            BeanDefinitionBuilder exportBeanBuilder,
            ServiceInterfaceMetadata meta) throws RemoteAccessException {
        // nothing
    }

    protected List<ServiceInterfaceMetadata> findServiceInterfaces() {
        ClassPathScanningServiceInterfaceProvider scanner
                = new ClassPathScanningServiceInterfaceProvider();;

        List<ServiceInterfaceMetadata> interfs = new ArrayList<>();
        scanner.addIncludeFilter(new AnnotationTypeFilter(
                ServiceInterface.class));

        for (String basePackage : basePackages) {
            for (BeanDefinition beanDef : scanner
                    .findCandidateComponents(basePackage)) {
                Class<?> interfaceClass = null;
                try {
                    interfaceClass = Class.forName(beanDef.getBeanClassName());
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException("class "
                            + beanDef.getBeanClassName() + " not found", e);
                }
                ServiceInterface interfaceAnnotation = AnnotationUtils
                        .findAnnotation(interfaceClass, ServiceInterface.class);
                interfs.add(new ServiceInterfaceMetadata(interfaceClass,
                        interfaceAnnotation));
            }
        }
        return interfs;
    }
    
    private class ClassPathScanningServiceInterfaceProvider
            extends ClassPathScanningCandidateComponentProvider {

        public ClassPathScanningServiceInterfaceProvider() {
            super(false);
        }
        
        @Override
        protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
            return beanDefinition.getMetadata().isIndependent();
        }
    }
}
