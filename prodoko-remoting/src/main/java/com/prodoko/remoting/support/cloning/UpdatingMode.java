package com.prodoko.remoting.support.cloning;

public enum UpdatingMode {
    UPDATE, SIMPLE_REPLACE, FIND_CORRESPONDING_AND_UPDATE, NEVER_UPDATE
}
