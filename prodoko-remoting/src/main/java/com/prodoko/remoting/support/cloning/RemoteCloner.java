package com.prodoko.remoting.support.cloning;

import java.beans.PropertyDescriptor;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class RemoteCloner implements ApplicationContextAware {
    
    private ApplicationContext applicationContext;
    private InstanceProducer defaultProducer;
    private InstanceFinder defaultFinder;

    public RemoteCloner() {
        this(new SimpleInstanceProducer());
    }
    
    public RemoteCloner(InstanceProducer defaultProducer) {
        super();
        this.defaultProducer = defaultProducer;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        this.applicationContext = applicationContext;
    }
    
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * TODO
     */
    public InstanceProducer getDefaultProducer() {
        return defaultProducer;
    }

    /**
     * @see #getDefaultProducer
     */
    public void setDefaultProducer(InstanceProducer defaultProducer) {
        this.defaultProducer = defaultProducer;
    }

    /**
     * TODO
     */
    public InstanceFinder getDefaultFinder() {
        return defaultFinder;
    }

    /**
     * @see #getDefaultFinder
     */
    public void setDefaultFinder(InstanceFinder defaultFinder) {
        this.defaultFinder = defaultFinder;
    }

    public Object cloneBean(Object source) {
        if(source == null) {
            return null;
        }
        RemotableAsClone remotedAsClone = source.getClass().getAnnotation(RemotableAsClone.class);
        if(remotedAsClone == null) {
            throw new IllegalArgumentException("Class " + source.getClass()
                    + " is not remoted as clone");
        }
        Object clone = cloneBean(source, remotedAsClone.instanceProducerBeanName(),
                remotedAsClone.cloning());
        return clone;
    }

    public Object cloneBean(Object source, String cloneRootProducerBeanName, CloningMode mode) {
        BeanWrapper sourceWrap = new BeanWrapperImpl(source);
        Object clone = produceInstance(cloneRootProducerBeanName, source, null);
        BeanWrapper cloneWrap = new BeanWrapperImpl(clone);
        for(PropertyDescriptor propDesc : cloneWrap.getPropertyDescriptors()) {
            if(propDesc.getWriteMethod() == null) {
                continue;
            }
            String propName = propDesc.getName();
            Class<?> propType = propDesc.getPropertyType();
            if(propType.isPrimitive()) {
                cloneCopyPrimitiveProperty(sourceWrap, cloneWrap, propName);
            } else if(BeanUtils.isSimpleValueType(propType)) {
                cloneCopySimpleProperty(sourceWrap, cloneWrap, propName);
            } else {
                CollectionPropertyRemoting colPropAnno = propDesc.getReadMethod().getAnnotation(
                        CollectionPropertyRemoting.class);
                BeanPropertyRemoting beanPropAnno = propDesc.getReadMethod().getAnnotation(
                        BeanPropertyRemoting.class);
                if(colPropAnno != null && shouldGoDeeper(colPropAnno.cloning(), mode)) {
                    if(propType.isArray()) {
                        cloneArrayProperty(sourceWrap, cloneWrap, mode,
                                propName, colPropAnno, propType);
                    } else if(Collection.class.isAssignableFrom(propType)) {
                        cloneActualCollectionProperty(sourceWrap, cloneWrap, mode,
                                propName, colPropAnno, propType);
                    } else if(Map.class.isAssignableFrom(propType)) {
                        cloneMapProperty(sourceWrap, cloneWrap, mode,
                                propName, colPropAnno, propType);
                    } 
                } else if(beanPropAnno != null && shouldGoDeeper(beanPropAnno.cloning(), mode)) {
                    cloneBeanProperty(sourceWrap, cloneWrap, propName, beanPropAnno);
                }
            }
        }
        return clone;
    }

    public boolean shouldGoDeeper(CloningMode propMode, CloningMode sourceMode) {
        return propMode != CloningMode.ALWAYS_TRANSISTENT && (sourceMode != CloningMode.CHOPPED
                || propMode == CloningMode.ALWAYS_DEEP);
    }
    
    public void cloneMembersProperties(Object source) {
        Collection<?> col = null;
        if(source instanceof Collection) {
            col = (Collection<?>)source;
        } else if(source instanceof Map<?, ?>) {
            Map<?, ?> map = (Map<?, ?>)source;
            col = map.values();
        } else {
            throw new IllegalArgumentException("Class " + source.getClass()
                    + " is not a collection nor a map");
        }
        for(Object member : col) {
            cloneBeansProperties(member);
        }
    }
    
    public void cloneBeansProperties(Object source) {
        RemotablePropertiesHolder hasRemotableProperties = source.getClass().getAnnotation(
                RemotablePropertiesHolder.class);
        if(hasRemotableProperties == null) {
            throw new IllegalArgumentException("Class " + source.getClass()
                    + " is not marked to have remotable properties");
        }
        BeanWrapper sourceWarp = new BeanWrapperImpl(source);
        cloneBeansProperties(sourceWarp, CloningMode.DEEP);
    }
    
    public void cloneBeansProperties(BeanWrapper sourceWrap, CloningMode sourceMode) {
        for(PropertyDescriptor propDesc : sourceWrap.getPropertyDescriptors()) {
            if(propDesc.getWriteMethod() == null) {
                continue;
            }
            String propName = propDesc.getName();
            Class<?> propType = propDesc.getPropertyType();
            CollectionPropertyRemoting colPropAnno = propDesc.getReadMethod().getAnnotation(
                    CollectionPropertyRemoting.class);
            BeanPropertyRemoting beanPropAnno = propDesc.getReadMethod().getAnnotation(
                    BeanPropertyRemoting.class);
            PropertieyMembersPropertiesRemoting colPropPropsAnno = propDesc.getReadMethod()
                    .getAnnotation(PropertieyMembersPropertiesRemoting.class);
            BeanPropertieyPropertiesRemoting beanPropPropsAnno = propDesc.getReadMethod()
                    .getAnnotation(BeanPropertieyPropertiesRemoting.class);
            if(colPropAnno != null && shouldGoDeeper(colPropAnno.cloning(), sourceMode)) {
                if(propType.isArray()) {
                    cloneArrayProperty(sourceWrap, sourceWrap, sourceMode,
                            propName, colPropAnno, propType);
                } else if(Collection.class.isAssignableFrom(propType)) {
                    cloneActualCollectionProperty(sourceWrap, sourceWrap, sourceMode,
                            propName, colPropAnno, propType);
                } else if(Map.class.isAssignableFrom(propType)) {
                    cloneMapProperty(sourceWrap, sourceWrap, sourceMode,
                            propName, colPropAnno, propType);
                }
            } else if(beanPropAnno != null && shouldGoDeeper(beanPropAnno.cloning(), sourceMode)) {
                    cloneBeanProperty(sourceWrap, sourceWrap, propName, beanPropAnno);
            } else if(colPropPropsAnno != null) {
                Object nestedSource = sourceWrap.getPropertyValue(propName);
                if(nestedSource != null) {
                    cloneMembersProperties(nestedSource);
                }
            } else if(beanPropPropsAnno != null) {
                Object nestedSource = sourceWrap.getPropertyValue(propName);
                if(nestedSource != null) {
                    cloneBeansProperties(nestedSource);
                }
            }
        }
    }
    
    public Object cloneCollection(Object sourceCol) {
        return cloneCollection(sourceCol, CloningMode.DEEP_OR_SIMPLE_TYPE, sourceCol.getClass(),
                RemotableAsClone.USE_DEFAULT, RemotableAsClone.USE_DEFAULT);
    }

    @SuppressWarnings({ "rawtypes" })
    public Object cloneCollection(Object sourceCol, CloningMode memberCloningMode,
            Class<?> collectionClass, String collectionProducerBeanName,
            String memberProducerBeanName) {
        Object clone;
        if(Map.class.isAssignableFrom(collectionClass)) {
            clone = cloneMap((Map) sourceCol, memberCloningMode, collectionClass,
                    collectionProducerBeanName, memberProducerBeanName);
        } else if(Collection.class.isAssignableFrom(collectionClass)) {
            clone = cloneActualCollection((Collection) sourceCol, memberCloningMode,
                    collectionClass, collectionProducerBeanName, memberProducerBeanName);
        } else {
            throw new IllegalArgumentException("Source is not a collection (nor a map)");
        }
        return clone;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected Collection cloneActualCollection(Collection sourceCol, CloningMode memberCloningMode,
            Class<?> collectionClass, String collectionProducerBeanName,
            String memberProducerBeanName) {
        if(sourceCol == null) {
            return null;
        }
        Collection cloneCol = (Collection) produceInstance(collectionProducerBeanName,
                sourceCol, collectionClass);
        for(Object sourceMember : sourceCol) {
            Object cloneMember = cloneBean(sourceMember, memberProducerBeanName,
                    memberCloningMode);
            cloneCol.add(cloneMember);
        }
        return cloneCol;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected Map cloneMap(Map sourceMap, CloningMode memberCloningMode,
            Class<?> collectionClass, String collectionProducerBeanName,
            String memberProducerBeanName) {
        if(sourceMap == null) {
            return null;
        }
        Map cloneMap = (Map) produceInstance(collectionProducerBeanName,
                sourceMap, collectionClass);
        for(Object sourceMemberKey : sourceMap.keySet()) {
            Object sourceMember = sourceMap.get(sourceMemberKey);
            Object cloneMember = cloneBean(sourceMember,
                    memberProducerBeanName, memberCloningMode);
            cloneMap.put(sourceMemberKey, cloneMember);
        }
        return cloneMap;
    }

    protected void cloneCopyPrimitiveProperty(BeanWrapper from,
            BeanWrapper to, String propName) {
        to.setPropertyValue(propName,
                from.getPropertyValue(propName));
    }

    protected void cloneCopySimpleProperty(BeanWrapper from,
            BeanWrapper to, String name) {
        Object fromPropValue = from.getPropertyValue(name);
        Object toPropValue = null;
        if(fromPropValue instanceof String) {
            toPropValue = new String((String)fromPropValue);
        } else if(fromPropValue instanceof Long) {
            toPropValue = new Long(((Long) fromPropValue).longValue());
        } else if(fromPropValue instanceof Class<?>) {
            toPropValue = fromPropValue;
        }
        // TODO
        to.setPropertyValue(name, toPropValue);
    }

    protected void cloneArrayProperty(BeanWrapper sourceWrap, BeanWrapper cloneWrap,
            CloningMode mode, String propName, CollectionPropertyRemoting colPropAnno,
            Class<?> propType) {
        // TODO Auto-generated method stub
        
    }
    
    @SuppressWarnings({ "rawtypes" })
    protected void cloneActualCollectionProperty(
            BeanWrapper sourceWrap, BeanWrapper cloneWrap, CloningMode memberCloningMode,
            String propName, CollectionPropertyRemoting propAnno, Class<?> propType) {
        Collection sourceCol = (Collection) sourceWrap.getPropertyValue(propName);
        Collection cloneCol = cloneActualCollection(sourceCol, memberCloningMode, propType,
                propAnno.collectionInstanceProducerBeanName(),
                propAnno.memberInstanceFinderBeanName());
        cloneWrap.setPropertyValue(propName, cloneCol);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void cloneMapProperty(BeanWrapper sourceWrap, BeanWrapper cloneWrap,
            CloningMode memberCloningMode, String propName, 
            CollectionPropertyRemoting propAnno, Class<?> propType) {
        Map sourceMap = (Map) sourceWrap.getPropertyValue(propName);
        Map cloneMap = cloneMap(sourceMap, memberCloningMode, propType,
                propAnno.collectionInstanceProducerBeanName(),
                propAnno.memberInstanceProducerBeanName());
        cloneWrap.setPropertyValue(propName, cloneMap);
    }

    protected void cloneBeanProperty(BeanWrapper sourceWrap,
            BeanWrapper cloneWrap, String propName,
            BeanPropertyRemoting propAnno) {
        Object sourceValue = sourceWrap.getPropertyValue(propName);
        if(sourceValue != null) {
            Object cloneValue = cloneBean(sourceValue,
                    propAnno.instanceProducerBeanName(),
                    propAnno.cloning());
            cloneWrap.setPropertyValue(propName, cloneValue);
        }
    }
    
    public Object updateSourceBean(Object clone) {
        RemotableAsClone remotedAsClone = clone.getClass().getAnnotation(RemotableAsClone.class);
        if(remotedAsClone == null) {
            throw new IllegalArgumentException("Class " + clone.getClass()
                    + " is not remoted as clone");
        }
        Object source = findInstance(remotedAsClone.instanceFinderBeanName(), clone, null);
        if(source == null) {
            source = produceInstance(remotedAsClone.instanceProducerBeanName(),
                    clone, null);
        }
        updateSourceBean(source, clone, remotedAsClone.cloning());
        return source;
    }

    public void updateSourceBean(Object source, Object clone) {
        RemotableAsClone remotedAsClone = source.getClass().getAnnotation(RemotableAsClone.class);
        if(remotedAsClone == null) {
            throw new IllegalArgumentException("Class " + source.getClass()
                    + " is not remoted as clone");
        }
        updateSourceBean(source, clone, remotedAsClone.cloning());
    }
    
    public void updateSourceBean(Object source, Object clone, CloningMode sourceMode) {
        BeanWrapper sourceWrap = new BeanWrapperImpl(source);
        BeanWrapper cloneWrap = new BeanWrapperImpl(clone);
        for(PropertyDescriptor propDesc : cloneWrap.getPropertyDescriptors()) {
            if(propDesc.getWriteMethod() == null) {
                continue;
            }
            String propName = propDesc.getName();
            Class<?> propType = propDesc.getPropertyType();
            if(propType.isPrimitive()) {
                cloneCopyPrimitiveProperty(cloneWrap, sourceWrap, propName);
            } else if(BeanUtils.isSimpleValueType(propType)) {
                cloneCopySimpleProperty(cloneWrap, sourceWrap, propName);
            } else {
                CollectionPropertyRemoting colPropAnno = propDesc.getReadMethod().getAnnotation(
                        CollectionPropertyRemoting.class);
                BeanPropertyRemoting beanPropAnno = propDesc.getReadMethod().getAnnotation(
                        BeanPropertyRemoting.class);
                if(colPropAnno != null && shouldGoDeeper(colPropAnno.cloning(), sourceMode)) {
                    if(propType.isArray()) {
                        copyArrayProperty(cloneWrap, sourceWrap, propName,
                                sourceMode, colPropAnno);
                    } else if(Collection.class.isAssignableFrom(propType)) {
                        copyCollectionProperty(cloneWrap, sourceWrap, propName,
                                propType, colPropAnno, sourceMode);
                    } else if(Map.class.isAssignableFrom(propType)) {
                        copyMapProperty(cloneWrap, sourceWrap, propName,
                                propType, colPropAnno, sourceMode);
                    }
                } else if(beanPropAnno != null
                        && shouldGoDeeper(beanPropAnno.cloning(), sourceMode)) {
                    copyBeanProperty(cloneWrap, sourceWrap, propName, propType, beanPropAnno);
                }
            }
        }
    }

    protected void copyArrayProperty(BeanWrapper cloneWrap,
            BeanWrapper sourceWrap, String propName, CloningMode beanCloningMode,
            CollectionPropertyRemoting propAnno) {
        // TODO Auto-generated method stub
        
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void copyCollectionProperty(BeanWrapper sourceWrap, BeanWrapper cloneWrap,
            String propName, Class<?> propType, CollectionPropertyRemoting propAnno,
            CloningMode beanCloningMode) {
        Collection cloneCol = (Collection) cloneWrap.getPropertyValue(propName);
        if(cloneCol == null) {
            sourceWrap.setPropertyValue(propName, null);
        } else {
            Collection sourceCol = (Collection) sourceWrap.getPropertyValue(propName);
            if(sourceCol != null) {
                sourceCol.clear();
            } else {
                sourceCol = (Collection) produceInstance(
                        propAnno.collectionInstanceProducerBeanName(),
                        cloneCol, propType);
                sourceWrap.setPropertyValue(propName, sourceCol);
            }
            for(Object cloneMember : cloneCol) {
                Object newSourceMember = findInstance(propAnno.memberInstanceFinderBeanName(),
                        cloneMember, null);
                if(newSourceMember == null) {
                    newSourceMember = produceInstance(propAnno.memberInstanceProducerBeanName(),
                            cloneMember, null);
                }
                updateSourceBean(newSourceMember, cloneMember, propAnno.cloning());
                sourceCol.add(newSourceMember);
            }
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void copyMapProperty(BeanWrapper sourceWrap, BeanWrapper cloneWrap,
            String propName, Class<?> propType, CollectionPropertyRemoting propAnno,
            CloningMode beanCloningMode) {
        Map cloneMap = (Map) cloneWrap.getPropertyValue(propName);
        if(cloneMap == null) {
            sourceWrap.setPropertyValue(propName, null);
        } else {
            Map sourceMap = (Map) sourceWrap.getPropertyValue(propName);
            if(sourceMap != null) {
                sourceMap.clear();
            } else {
                sourceMap = (Map) produceInstance(propAnno.collectionInstanceProducerBeanName(),
                        cloneMap, propType);
                sourceWrap.setPropertyValue(propName, sourceMap);
            }
            for(Object cloneMemberKey : cloneMap.keySet()) {
                Object cloneMember = cloneMap.get(cloneMemberKey);
                Object newSourceMember = findInstance(propAnno.memberInstanceFinderBeanName(),
                        cloneMember, null);
                if(newSourceMember == null) {
                    newSourceMember = produceInstance(propAnno.memberInstanceProducerBeanName(),
                            cloneMember, null);
                }
                updateSourceBean(newSourceMember, cloneMember, propAnno.cloning());
                sourceMap.put(cloneMemberKey, newSourceMember);
            }
        }
    }

    protected void copyBeanProperty(BeanWrapper cloneWrap,
            BeanWrapper sourceWrap, String propName, Class<?> propType,
            BeanPropertyRemoting propAnno) {
        Object clonePropValue = cloneWrap.getPropertyValue(propName);
        if(clonePropValue == null) {
            sourceWrap.setPropertyValue(propName, null);
        } else {
            switch (propAnno.updating()) {
                case SIMPLE_REPLACE : {
                    sourceWrap.setPropertyValue(propName, clonePropValue);
                } break;
                case UPDATE : {
                    Object sourceValue = sourceWrap.getPropertyValue(propName);
                    if(sourceValue == null) {
                        sourceValue = produceInstance(propAnno.instanceProducerBeanName(),
                                sourceValue, propType);
                        sourceWrap.setPropertyValue(propName, sourceValue);
                    }
                    updateSourceBean(sourceValue, clonePropValue, propAnno.cloning());
                } break;
                case FIND_CORRESPONDING_AND_UPDATE : {
                    Object sourcePropValue = findInstance(
                            propAnno.instanceFinderBeanName(),
                            clonePropValue,
                            sourceWrap.getPropertyValue(propName));
                    if(sourcePropValue == null) {
                        sourcePropValue = produceInstance(propAnno.instanceProducerBeanName(),
                                clonePropValue, propType);
                    }
                    sourceWrap.setPropertyValue(propName, sourcePropValue);
                    updateSourceBean(sourcePropValue, clonePropValue, propAnno.cloning());
                } break;
                case NEVER_UPDATE : {
                    
                }
            }
        }
    }

    protected <T> Object produceInstance(String producerBeanName,
            Object correspondingCloneOrSource, Class<?> propType) {
        InstanceProducer producer;
        if(producerBeanName.equals(RemotableAsClone.USE_DEFAULT)) {
            producer = defaultProducer;
        } else {
            producer = (InstanceProducer) applicationContext
                    .getBean(producerBeanName);
        }
        return producer.produce(correspondingCloneOrSource, propType);
    }
    
    protected Object findInstance(String finderBeanName, Object correspondingClone,
            Object currentSource) {
        InstanceFinder finder;
        if(finderBeanName.equals(RemotableAsClone.USE_DEFAULT)) {
            if(defaultFinder == null) {
                throw new IllegalStateException("No default finder set");
            }
            finder = defaultFinder;
        } else {
            finder  = (InstanceFinder) applicationContext.getBean(finderBeanName);
        }
        return finder.find(correspondingClone, currentSource);
    }
}
