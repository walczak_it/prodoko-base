package com.prodoko.remoting.support.cloning;


public interface InstanceProducer {
    
    public Object produce(Object source, Class<?> sourceAsPropOfType);
}