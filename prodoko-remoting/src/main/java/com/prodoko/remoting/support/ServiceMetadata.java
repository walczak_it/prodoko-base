package com.prodoko.remoting.support;

public class ServiceMetadata extends ServiceInterfaceMetadata {
    
    private String implementationBeanName;
    private Class<?> implementationClass;

    public ServiceMetadata() {
    }
    
    public ServiceMetadata(Class<?> interfaceClass,
            ServiceInterface interfaceAnnotation,
            String implementationBeanName, Class<?> implementationClass) {
        super(interfaceClass, interfaceAnnotation);
        this.implementationBeanName = implementationBeanName;
        this.implementationClass = implementationClass;
    }
    
    public ServiceMetadata(ServiceInterfaceMetadata interfaceMetadata,
            String implementationBeanName, Class<?> implementationClass) {
        super(interfaceMetadata.getInterfaceClass(), interfaceMetadata
            .getInterfaceAnnotation());
        this.implementationBeanName = implementationBeanName;
        this.implementationClass = implementationClass;
    }

    public String getImplementationBeanName() {
        return implementationBeanName;
    }

    public void setImplementationBeanName(String implementationBeanName) {
        this.implementationBeanName = implementationBeanName;
    }

    public Class<?> getImplementationClass() {
        return implementationClass;
    }

    public void setImplementationClass(Class<?> implementationClass) {
        this.implementationClass = implementationClass;
    }
}