package com.prodoko.remoting.support.cloning;

import java.lang.annotation.Annotation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;


@Aspect
public class RemoteCloneAspect {
    
    private RemoteCloner cloner;
    private EntityManager entityManager;

    /**
     * TODO
     */
    public RemoteCloner getCloner() {
        return cloner;
    }

    /**
     * @see #getCloner
     */
    @Autowired
    public void setCloner(RemoteCloner cloner) {
        this.cloner = cloner;
    }
    
    /**
     * TODO
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @see #getEntityManager
     */
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Around("@annotation(com.prodoko.remoting.support.cloning.CloneRemotedCollection)")
    public Object methodReturningCollectionToClone(final ProceedingJoinPoint pjp) 
            throws Throwable {
        Object rval = pjp.proceed();
        return cloner.cloneCollection(rval);
    }
    
    @Around("@annotation(com.prodoko.remoting.support.cloning.CloneRemotedBean)")
    public Object methodReturningEntityToClone(final ProceedingJoinPoint pjp) 
            throws Throwable {
        Object rval = pjp.proceed();
        return cloner.cloneBean(rval);
    }

    @Around("execution(public * *(.., @com.prodoko.remoting.support.cloning.UncloneRemotedBean (*), ..))")
    public void methodWithClonedEntityParams(final ProceedingJoinPoint pjp)
            throws Throwable {
        Object[] args = pjp.getArgs();
        if(pjp.getSignature() instanceof MethodSignature) {
            MethodSignature signature = (MethodSignature) pjp.getSignature();
            Annotation[][] paramsAnnos = signature.getMethod().getParameterAnnotations();
            for(int i = 0; i < paramsAnnos.length; ++i) {
                Annotation[] paramAnnos = paramsAnnos[i];
                UncloneRemotedBean paramAnno = null;
                for(int j = 0; j < paramAnnos.length; ++j) {
                    if(paramAnnos[j] instanceof UncloneRemotedBean) {
                        paramAnno = (UncloneRemotedBean) paramAnnos[j];
                        break;
                    }
                }
                if(paramAnno != null) {
                    Object potentialClone = args[i];
                    if(!entityManager.contains(potentialClone)) {
                        Object source = cloner.updateSourceBean(potentialClone);
                        args[i] = source;
                    }
                }
            }
        }
        pjp.proceed(args);
    }
}
