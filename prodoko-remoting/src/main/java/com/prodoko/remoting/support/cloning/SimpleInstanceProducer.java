package com.prodoko.remoting.support.cloning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class SimpleInstanceProducer implements InstanceProducer {
    
    private boolean simplifyCollections = true;
    
    /**
     * TODO
     */
    public boolean isSimplifyCollections() {
        return simplifyCollections;
    }

    /**
     * @see #getSimplifyCollections
     */
    public void setSimplifyCollections(boolean simplifyCollections) {
        this.simplifyCollections = simplifyCollections;
    }

    @Override
    public Object produce(Object source, Class<?> sourceAsPropOfType) {
        if(simplifyCollections) {
            if(sourceAsPropOfType != null) {
                if(Set.class.isAssignableFrom(sourceAsPropOfType)) {
                    return new HashSet<>();
                } else if(Collection.class.isAssignableFrom(sourceAsPropOfType)) {
                    return new ArrayList<>();
                } else if(Map.class.isAssignableFrom(sourceAsPropOfType)) {
                    return new HashMap<>();
                }
            } else if(source != null) {
                if(Set.class.isAssignableFrom(source.getClass())) {
                    return new HashSet<>();
                } else if(Collection.class.isAssignableFrom(source.getClass())) {
                    return new ArrayList<>();
                } else if(Map.class.isAssignableFrom(source.getClass())) {
                    return new HashMap<>();
                }
            } else {
                return null;
            }
        }
        return produceBean(source, sourceAsPropOfType, null);
    }
    
    protected Object produceBean(Object source, Class<?> sourceAsPropOfType,
            Class<?> forceInstanceClass) {
        if(forceInstanceClass != null) {
            return BeanUtils.instantiateClass(forceInstanceClass);
        } else if(source != null) {
            return BeanUtils.instantiateClass(source.getClass());
        } else if(sourceAsPropOfType != null) {
            return BeanUtils.instantiateClass(sourceAsPropOfType);
        } else {
            return null;
        }
    }
}
