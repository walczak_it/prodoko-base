package com.prodoko.remoting.support;

import org.springframework.core.annotation.AnnotationUtils;

public class ServiceInterfaceMetadata {

    private Class<?> interfaceClass;
    private ServiceInterface interfaceAnnotation;
    
    public ServiceInterfaceMetadata() {
        
    }
    
    public ServiceInterfaceMetadata(Class<?> interfaceClass,
            ServiceInterface interfaceAnnotation) {
        super();
        this.interfaceClass = interfaceClass;
        this.interfaceAnnotation = interfaceAnnotation;
    }

    public Class<?> getInterfaceClass() {
        return interfaceClass;
    }

    public void setInterfaceClass(Class<?> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    public ServiceInterface getInterfaceAnnotation() {
        return interfaceAnnotation;
    }

    public void setInterfaceAnnotation(ServiceInterface interfaceAnnotation) {
        this.interfaceAnnotation = interfaceAnnotation;
    }

    public String getRemoteName() {
        return remoteName(getInterfaceAnnotation(), getInterfaceClass());
    }
    
    public static String remoteName(Class<?> interfaceClass) {
        ServiceInterface interfaceAnnotation = AnnotationUtils
            .findAnnotation(interfaceClass, ServiceInterface.class);
        return remoteName(interfaceAnnotation, interfaceClass);
    }
    
    public static String remoteName(ServiceInterface interfaceAnnotation,
            Class<?> interfaceClass) {
        String specifiedName = interfaceAnnotation.value();
        if(specifiedName != null && !specifiedName.isEmpty()) {
            return specifiedName;
        } else {
            return interfaceClass.getSimpleName();
        }
    }
}
