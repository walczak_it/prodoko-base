package com.prodoko.remoting.support;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.ManagedArray;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.remoting.support.RemoteExporter;
import org.springframework.stereotype.Service;

/**
 * Abstract base class for providing automatic remote export of services found
 * inside a Spring context.
 * 
 * <p>
 * When a bean is annotated with {@link Service} <em>and</em> implements an
 * interface annotated with {@link ServiceInterface}, the bean will be exported
 * as a service with this interface.
 * <p>
 * Based on RemotingExporter from <a
 * href=http://jira.springframework.org/browse/SPR-3926>Jira issue SPR-3926</a>
 * 
 * @author James Douglas
 * @author Henno Vermeulen
 * @author Adam Walczak - WALCZAK.IT
 */ 
public abstract class RemotingExportRegistrar {

    private static final Log LOG = LogFactory.getLog(RemotingExportRegistrar.class);

    private Class<? extends Annotation> serviceImplAnnotationType;
    
    private String[] interceptorBeanNames;
    
    private List<ServiceMetadata> serviceMetas = new ArrayList<ServiceMetadata>();

    public void setServiceImplAnnotationType(
            Class<? extends Annotation> serviceImplAnnotationType) {
        this.serviceImplAnnotationType = serviceImplAnnotationType;
    }

    public Class<? extends Annotation> getServiceImplAnnotationType() {
        return serviceImplAnnotationType;
    }
    
    public String[] getInterceptorBeanNames() {
        return interceptorBeanNames;
    }

    public void setInterceptorBeanNames(String[] interceptorBeanNames) {
        this.interceptorBeanNames = interceptorBeanNames;
    }

    public void registerBeanDefinitions(BeanDefinitionRegistry registry,
            Class<? extends Annotation> serviceImplAnnotationType)
                    throws BeansException {

        setServiceImplAnnotationType(serviceImplAnnotationType);
        registerBeanDefinitions(registry);
    }

    public void registerBeanDefinitions(BeanDefinitionRegistry registry)
            throws BeansException {
        scanForServicesToRemote(registry);
        
        LOG.debug("exposing " + serviceMetas.size()
                + " services that were found.");
        for (ServiceMetadata meta : serviceMetas) {
            LOG.info("Exposing service bean named "
                + meta.getImplementationBeanName() + " of class "
                + meta.getImplementationClass() + " using service remote name "
                + meta.getRemoteName()
                + " and interface " + meta.getInterfaceClass());
            try {
                BeanDefinition exportBeanDef = exportBeanDefinition(meta);
                String exportBeanName = exportBeanName(meta);
                registry.registerBeanDefinition(exportBeanName, exportBeanDef);
                
            } catch (Exception e) {
                throw new RuntimeException(
                        "exception while creating remote exporter", e);
            }
        }
    }
    
    protected BeanDefinition exportBeanDefinition(ServiceMetadata meta) {
        BeanDefinitionBuilder exportBeanBuilder = BeanDefinitionBuilder
                .rootBeanDefinition(getExporterClass());
        initializeRemoteExporterProperties(exportBeanBuilder, meta);
        initializeOtherProperties(exportBeanBuilder, meta);
        return exportBeanBuilder.getBeanDefinition();
    }
    
    protected String exportBeanName(ServiceMetadata meta) {
        return meta.getRemoteName();
    }

    protected abstract Class<? extends RemoteExporter> getExporterClass();
    
    protected void initializeRemoteExporterProperties(
            BeanDefinitionBuilder exportBeanBuilder, ServiceMetadata meta) {
        exportBeanBuilder.addPropertyReference("service", meta.getImplementationBeanName());
        exportBeanBuilder.addPropertyValue("serviceInterface", meta.getInterfaceClass().getName());
        if(getInterceptorBeanNames().length > 0) {
            int count = getInterceptorBeanNames().length;
            ManagedArray interceptorReferences = new ManagedArray(
                    Object.class.getName(), count);
            for(int i = 0; i < count; ++i) {
                interceptorReferences.add(new RuntimeBeanReference(
                        getInterceptorBeanNames()[i]));
            }
            exportBeanBuilder.addPropertyValue("interceptors",
                    interceptorReferences);
        }
    }
    
    protected void initializeOtherProperties(
            BeanDefinitionBuilder exportBeanBuilder, ServiceMetadata meta) {
        // nothing
    }
    
    protected void scanForServicesToRemote(BeanDefinitionRegistry registery) {
        LOG.info("processing beans for remote export");
        for (String beanName : registery.getBeanDefinitionNames()) {
            BeanDefinition beanDef = registery.getBeanDefinition(beanName);
            if (beanDef.isAbstract()) {
                LOG.info("skipping abstract bean '" + beanName + "'");
                continue;
            }
            String beanClassName = beanDef.getBeanClassName();
            if(beanClassName == null) {
                LOG.info("skipping bean '" + beanName
                    + "' without class name");
                continue;
            }
            Class<?> beanClass = null;
            try {
                beanClass = Class.forName(beanClassName);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("class of bean " + beanName
                        + " not found", e);
            }
            ServiceInterfaceMetadata serviceInterfaceMetadata
                = createMetadataIfHasServiceInterface(beanClass);
            if (serviceInterfaceMetadata != null) {
                LOG.debug("found service bean with name '" + beanName
                        + "' and interface " + serviceInterfaceMetadata
                            .getInterfaceClass().getName());
                serviceMetas.add(new ServiceMetadata(serviceInterfaceMetadata,
                        beanName, beanClass));
            } else {
                LOG
                        .debug("skipping bean because no remotable interface was found '"
                                + beanName + "'");
            }
        }
    }
    
    protected ServiceInterfaceMetadata createMetadataIfHasServiceInterface(Class<?> beanClass) {
        if (AnnotationUtils.isAnnotationDeclaredLocally(
                getServiceImplAnnotationType(), beanClass)) {
            for (Class<?> interfaceClass : beanClass.getInterfaces()) {
                ServiceInterface interfaceAnnotation = AnnotationUtils
                        .findAnnotation(interfaceClass, ServiceInterface.class);
                if (interfaceAnnotation != null) {
                    ServiceInterfaceMetadata meta = new ServiceInterfaceMetadata();
                    meta.setInterfaceAnnotation(interfaceAnnotation);
                    meta.setInterfaceClass(interfaceClass);
                    return meta;
                }
            }
        }
        return null;
    }
}
