package com.prodoko.remoting.support.cloning;

import java.lang.reflect.Parameter;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class RemoteCloningInterceptor implements MethodInterceptor {
    
    private static final Log LOG = LogFactory.getLog(RemoteCloningInterceptor.class);
    
    private RemoteCloner cloner;

    /**
     * TODO
     */
    public RemoteCloner getCloner() {
        return cloner;
    }

    /**
     * @see #getCloner
     */
    @Autowired
    public void setCloner(RemoteCloner cloner) {
        this.cloner = cloner;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        
        try {
            uncloneParametersIfNeeded(invocation);
        } catch(Exception e) {
            LOG.error("Remote params unclonning error", e);
            throw new RemoteClonerException("Failed to unclone remoted objects", e);
        }
        
        Object rval = invocation.proceed();
        
        try {
            rval = cloneReturnValueIfNeeded(invocation, rval);
        } catch(Exception e) {
            LOG.error("Remote return value clonning error", e);
            throw new RemoteClonerException("Failed to clone remoted objects", e);
        }
        
        return rval;
    }

    protected void uncloneParametersIfNeeded(MethodInvocation invocation) {
        Parameter[] params = invocation.getMethod().getParameters();
        Object[] args = invocation.getArguments();
        for(int i = 0; i < params.length; ++i) {
            Object arg = args[i];
            Parameter param = params[i];
            if(arg != null && param.getAnnotation(UncloneRemotedBean.class) != null) {
                args[i] = cloner.updateSourceBean(arg);
            }
        }
    }
    
    protected Object cloneReturnValueIfNeeded(MethodInvocation invocation, Object rval) {
        CloneRemotedBean cloneRemotedBeanAnno = invocation.getMethod().getAnnotation(
                CloneRemotedBean.class);
        if(cloneRemotedBeanAnno != null) {
            return cloner.cloneBean(rval);
        }
        CloneRemotedCollection cloneRemotedCollectionAnno = invocation.getMethod()
                .getAnnotation(CloneRemotedCollection.class);
        if(cloneRemotedCollectionAnno != null) {
            return cloner.cloneCollection(rval);
        }
        CloneRemotedProperties cloneRemotedPropertiesAnno = invocation.getMethod()
                .getAnnotation(CloneRemotedProperties.class);
        if(cloneRemotedPropertiesAnno != null) {
            cloner.cloneBeansProperties(rval);
        }
        return rval;
    }
}
