package com.prodoko.remoting.support.cloning;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;


public class TransactionalCloningInterceptor extends RemoteCloningInterceptor {
    
    private static final Log LOG = LogFactory.getLog(TransactionalCloningInterceptor.class);
    
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * TODO
     */
    public PlatformTransactionManager getTransactionManager() {
        return transactionManager;
    }

    /**
     * @see #getTransactionManager
     */
    public void setTransactionManager(PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        
        TransactionDefinition transDef = new DefaultTransactionDefinition();
        TransactionStatus transStatus = transactionManager.getTransaction(transDef);
        
        try {
            uncloneParametersIfNeeded(invocation);
        } catch(Exception e) {
            if(!transStatus.isCompleted()) {
                transactionManager.rollback(transStatus);
            }
            LOG.error("Remote params unclonning error", e);
            throw new RemoteClonerException("Failed to unclone remoted objects", e);
        }
        
        Object rval;
        try {
            rval = invocation.proceed();
        } catch(Exception e) {
            if(!transStatus.isCompleted()) {
                transactionManager.rollback(transStatus);
            }
            LOG.error("Remoted jointpoint execution thrown an exception", e);
            throw e;
        }
        
        try {
            rval = cloneReturnValueIfNeeded(invocation, rval);
        } catch(Exception e) {
            if(!transStatus.isCompleted()) {
                transactionManager.rollback(transStatus);
            }
            LOG.error("Remote return value clonning error", e);
            throw new RemoteClonerException("Failed to clone remoted objects", e);
        }

        try {
            if(!transStatus.isCompleted()) {
                transactionManager.commit(transStatus);
            }
        } catch(Exception e) {
            LOG.error("Remoted jointpoint commit thrown an exception", e);
            throw e;
        }
        
        return rval;
    }
}
