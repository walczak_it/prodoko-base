package com.prodoko.remoting.support.cloning;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BeanPropertyRemoting {
    CloningMode cloning() default CloningMode.DEEP_OR_SIMPLE_TYPE;
    String instanceProducerBeanName() default RemotableAsClone.USE_DEFAULT;
    UpdatingMode updating() default UpdatingMode.UPDATE;
    String instanceFinderBeanName() default RemotableAsClone.USE_DEFAULT;
}
