package com.prodoko.remoting.support.cloning;

public interface InstanceFinder {

    public Object find(Object correspondingClone, Object currentSource);
}
