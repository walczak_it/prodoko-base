package com.prodoko.remoting.support.cloning;

public class RemoteClonerException extends RuntimeException {

    public RemoteClonerException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public RemoteClonerException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public RemoteClonerException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public RemoteClonerException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public RemoteClonerException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
