package com.prodoko.remoting.support.cloning;

public enum CloningMode {
    DEEP_OR_SIMPLE_TYPE, DEEP, CHOPPED, ALWAYS_TRANSISTENT, ALWAYS_DEEP
}