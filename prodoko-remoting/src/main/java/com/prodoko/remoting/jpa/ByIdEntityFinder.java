package com.prodoko.remoting.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.prodoko.remoting.support.cloning.InstanceFinder;

public class ByIdEntityFinder implements InstanceFinder {
    
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Object find(Object correspondingClone, Object currentSource) {
        Object clonePropId = entityManager.getEntityManagerFactory().getPersistenceUnitUtil()
                .getIdentifier(correspondingClone);
        if(clonePropId == null) {
            return null;
        }
        if(currentSource != null) {
            Object sourcePropId = entityManager.getEntityManagerFactory().getPersistenceUnitUtil()
                    .getIdentifier(currentSource);
            if(sourcePropId.equals(clonePropId)) {
                return currentSource;
            }
        }
        return entityManager.find(correspondingClone.getClass(), clonePropId);
    }

}
