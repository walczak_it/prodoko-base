package com.prodoko.remoting.rmi;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(RmiExportRegistrar.class)
public @interface EnableRmiExport {

    Class<? extends Annotation> serviceImplAnnotationType()
        default Service.class;
    String[] interceptorBeanNames() default {};
    int registeryPort() default 1099;
}
