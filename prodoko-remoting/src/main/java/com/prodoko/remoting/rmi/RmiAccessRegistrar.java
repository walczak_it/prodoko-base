package com.prodoko.remoting.rmi;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.remoting.support.RemoteAccessor;

import com.prodoko.remoting.support.RemotingAccessRegistrar;
import com.prodoko.remoting.support.ServiceInterfaceMetadata;

public class RmiAccessRegistrar extends RemotingAccessRegistrar
    implements ImportBeanDefinitionRegistrar {
    
    private Integer registryPort = null;
    private String host = null;

    public Integer  getRegistryPort() {
        return registryPort;
    }

    public void setRegistryPort(Integer  registryPort) {
        this.registryPort = registryPort;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public void registerBeanDefinitions(
            AnnotationMetadata importingClassMetadata,
            BeanDefinitionRegistry registry) {
        
        AnnotationAttributes annoAttrs = AnnotationAttributes.fromMap(
                importingClassMetadata .getAnnotationAttributes(
                        EnableRmiAccess.class.getName()));
        setBasePackages(annoAttrs.getStringArray("basePackages"));
        setRegistryPort((Integer)annoAttrs.getNumber("registeryPort"));
        setHost(annoAttrs.getString("host"));
        registerBeanDefinitions(registry);
    }

    @Override
    protected Class<? extends RemoteAccessor> getAccessorClass() {
        return RmiProxyFactoryBean.class;
    }

    @Override
    protected void initializeOtherProperties(
            BeanDefinitionBuilder accessBeanBuilder,
            ServiceInterfaceMetadata meta) throws RemoteAccessException {
        StringBuilder url = new StringBuilder("rmi://");
        url.append(getHost());
        if(getRegistryPort() != 0) {
            url.append(':').append(getRegistryPort());
        }
        url.append("/").append(meta.getRemoteName());
        accessBeanBuilder.addPropertyValue("serviceUrl", url.toString());
    }
}
