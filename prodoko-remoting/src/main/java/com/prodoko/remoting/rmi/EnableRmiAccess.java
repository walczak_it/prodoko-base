package com.prodoko.remoting.rmi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(RmiAccessRegistrar.class)
public @interface EnableRmiAccess {

    String[] basePackages();
    String host();
    int registeryPort() default 1099;
}
