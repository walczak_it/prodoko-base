package com.prodoko.remoting.rmi;

import java.lang.annotation.Annotation;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

import com.prodoko.remoting.support.RemotingExportRegistrar;
import com.prodoko.remoting.support.ServiceMetadata;


public class RmiExportRegistrar extends RemotingExportRegistrar
        implements ImportBeanDefinitionRegistrar {
    
    private Integer registryPort;

    public Integer getRegistryPort() {
        return registryPort;
    }

    public void setRegistryPort(Integer registryPort) {
        this.registryPort = registryPort;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void registerBeanDefinitions(
            AnnotationMetadata importingClassMetadata,
            BeanDefinitionRegistry registry) {
        
        AnnotationAttributes annoAttrs = AnnotationAttributes.fromMap(
                importingClassMetadata .getAnnotationAttributes(
                        EnableRmiExport.class.getName()));
        setServiceImplAnnotationType((Class<? extends Annotation>) annoAttrs
                .getClass("serviceImplAnnotationType"));
        setInterceptorBeanNames(annoAttrs.getStringArray("interceptorBeanNames"));
        registerBeanDefinitions(registry);
    }

    @Override
    public Class<? extends RemoteExporter> getExporterClass() {
        return RmiServiceExporter.class;
    }
    
    @Override
    protected void initializeOtherProperties(
            BeanDefinitionBuilder exportBeanBuilder, ServiceMetadata meta) {
        exportBeanBuilder.addPropertyValue("serviceName", meta.getRemoteName());
    }
}
